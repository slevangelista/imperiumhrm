<?php

/**
 * This is the model class for table "{{education}}".
 *
 * The followings are the available columns in table '{{education}}':
 * @property integer $id
 * @property integer $emp_id
 * @property string $school_name
 * @property integer $education_level_id
 * @property string $date_started
 * @property string $date_ended
 * @property double $GPA
 * @property string $comment
 * @property string $created_at
 * @property string $updated_at
 *
 * The followings are the available model relations:
 * @property Employee $emp
 * @property EducationLevel $educationLevel
 */
class Education extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{education}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('emp_id, school_name, education_level_id, date_started, date_ended', 'required'),
			array('emp_id, education_level_id', 'numerical', 'integerOnly'=>true),
			array('GPA', 'numerical'),
			array('school_name', 'length', 'max'=>150),
			array('comment', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, emp_id, school_name, education_level_id, date_started, date_ended, GPA, comment, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'emp' => array(self::BELONGS_TO, 'Employee', 'emp_id'),
			'educationLevel' => array(self::BELONGS_TO, 'EducationLevel', 'education_level_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'emp_id' => 'Emp',
			'school_name' => 'School Name',
			'education_level_id' => 'Education Level',
			'date_started' => 'Date Started',
			'date_ended' => 'Date Ended',
			'GPA' => 'Gpa',
			'comment' => 'Comment',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('emp_id',$this->emp_id);
		$criteria->compare('school_name',$this->school_name,true);
		$criteria->compare('education_level_id',$this->education_level_id);
		$criteria->compare('date_started',$this->date_started,true);
		$criteria->compare('date_ended',$this->date_ended,true);
		$criteria->compare('GPA',$this->GPA);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Education the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
