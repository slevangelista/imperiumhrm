<?php

/**
 * This is the model class for table "{{license}}".
 *
 * The followings are the available columns in table '{{license}}':
 * @property integer $id
 * @property integer $emp_id
 * @property string $license_name
 * @property string $license_code
 * @property string $created_at
 * @property string $updated_at
 *
 * The followings are the available model relations:
 * @property Employee $emp
 */
class License extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{license}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('emp_id, license_name, license_code', 'required'),
			array('emp_id', 'numerical', 'integerOnly'=>true),
			array('license_name', 'length', 'max'=>100),
			array('license_code', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, emp_id, license_name, license_code, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'emp' => array(self::BELONGS_TO, 'Employee', 'emp_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'emp_id' => 'Emp',
			'license_name' => 'License Name',
			'license_code' => 'License Code',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('emp_id',$this->emp_id);
		$criteria->compare('license_name',$this->license_name,true);
		$criteria->compare('license_code',$this->license_code,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return License the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getEmployeeLicense($emp_id){
		$sql = "SELECT id,license_name, license_code"
		     . " FROM tbl_license"
		     . " WHERE emp_id=:emp_id";
		$data = Yii::app()->db->CreateCommand($sql);
		$data->bindParam(":emp_id",$emp_id,PDO::PARAM_INT);
		$licenses = $data->queryAll();

		return isset($licenses) ? $licenses : false;
	}
}
