<?php

/**
 * This is the model class for table "{{ot_request}}".
 *
 * The followings are the available columns in table '{{ot_request}}':
 * @property integer $id
 * @property integer $emp_id
 * @property string $start_time
 * @property string $end_time
 * @property string $date
 * @property string $date_submitted
 * @property integer $request_type_id
 * @property string $request_dissaproval
 * @property integer $position_id
 * @property string $remarks
 * @property string $total_hours
 *
 * The followings are the available model relations:
 * @property Position $position
 * @property Employee $emp
 * @property RequestType $requestType
 */
class OtRequest extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{ot_request}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('emp_id, request_type_id, position_id', 'numerical', 'integerOnly'=>true),
			array('request_dissaproval, remarks', 'length', 'max'=>255),
			array('total_hours', 'length', 'max'=>5),
			array('start_time, end_time, date, date_submitted', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, emp_id, start_time, end_time, date, date_submitted, request_type_id, request_dissaproval, position_id, remarks, total_hours', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'position' => array(self::BELONGS_TO, 'Position', 'position_id'),
			'emp' => array(self::BELONGS_TO, 'Employee', 'emp_id'),
			'requestType' => array(self::BELONGS_TO, 'RequestType', 'request_type_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'emp_id' => 'Emp',
			'start_time' => 'Start Time',
			'end_time' => 'End Time',
			'date' => 'Date',
			'date_submitted' => 'Date Submitted',
			'request_type_id' => 'Request Status',
			'request_dissaproval' => 'Request Dissaproval',
			'position_id' => 'Position',
			'remarks' => 'Remarks',
			'total_hours' => 'Total Hours',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('emp_id',$this->emp_id);
		$criteria->compare('start_time',$this->start_time,true);
		$criteria->compare('end_time',$this->end_time,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('date_submitted',$this->date_submitted,true);
		$criteria->compare('request_type_id',$this->request_type_id);
		$criteria->compare('request_dissaproval',$this->request_dissaproval,true);
		$criteria->compare('position_id',$this->position_id);
		$criteria->compare('remarks',$this->remarks,true);
		$criteria->compare('total_hours',$this->total_hours,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OtRequest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
