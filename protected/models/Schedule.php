<?php

/**
 * This is the model class for table "{{schedule}}".
 *
 * The followings are the available columns in table '{{schedule}}':
 * @property integer $id
 * @property integer $emp_id
 * @property string $date
 * @property integer $is_active
 * @property integer $shift_id
 * @property integer $is_restday
 * @property integer $is_leave
 * @property integer $is_holiday
 *
 * The followings are the available model relations:
 * @property LeaveType $isLeave
 * @property Holidays $isHoliday
 * @property Shift $shift
 * @property Employee $emp
 */
class Schedule extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{schedule}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('emp_id, date', 'required'),
			array('emp_id, is_active, shift_id, is_restday, is_leave, is_holiday', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, emp_id, date, is_active, shift_id, is_restday, is_leave, is_holiday', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'isLeave' => array(self::BELONGS_TO, 'LeaveType', 'is_leave'),
			'isHoliday' => array(self::BELONGS_TO, 'Holidays', 'is_holiday'),
			'shift' => array(self::BELONGS_TO, 'Shift', 'shift_id'),
			'emp' => array(self::BELONGS_TO, 'Employee', 'emp_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'emp_id' => 'Emp',
			'date' => 'Date',
			'is_active' => 'Is Active',
			'shift_id' => 'Shift',
			'is_restday' => 'Is Restday',
			'is_leave' => 'Is Leave',
			'is_holiday' => 'Is Holiday',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('emp_id',$this->emp_id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('is_active',$this->is_active);
		$criteria->compare('shift_id',$this->shift_id);
		$criteria->compare('is_restday',$this->is_restday);
		$criteria->compare('is_leave',$this->is_leave);
		$criteria->compare('is_holiday',$this->is_holiday);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Schedule the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
