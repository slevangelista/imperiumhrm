<?php

/**
 * This is the model class for table "{{logs}}".
 *
 * The followings are the available columns in table '{{logs}}':
 * @property integer $id
 * @property integer $emp_id
 * @property string $name
 * @property string $date
 * @property string $checkin
 * @property string $checkout
 * @property integer $is_active
 * @property integer $status
 * @property string $schedule
 * @property integer $hr_status
 * @property string $hr_logs
 *
 * The followings are the available model relations:
 * @property LogStatus $hrStatus
 * @property LogStatus $status0
 * @property Employee $emp
 */
class Logs extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{logs}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('emp_id, name, date', 'required'),
			array('emp_id, is_active, status, hr_status', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>100),
			array('schedule, hr_logs', 'length', 'max'=>255),
			array('checkin, checkout', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, emp_id, name, date, checkin, checkout, is_active, status, schedule, hr_status, hr_logs', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'hrStatus' => array(self::BELONGS_TO, 'LogStatus', 'hr_status'),
			'status0' => array(self::BELONGS_TO, 'LogStatus', 'status'),
			'emp' => array(self::BELONGS_TO, 'Employee', 'emp_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'emp_id' => 'Emp',
			'name' => 'Name',
			'date' => 'Date',
			'checkin' => 'Checkin',
			'checkout' => 'Checkout',
			'is_active' => 'Is Active',
			'status' => 'Status',
			'schedule' => 'Schedule',
			'hr_status' => 'Hr Status',
			'hr_logs' => 'Hr Logs',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('emp_id',$this->emp_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('checkin',$this->checkin,true);
		$criteria->compare('checkout',$this->checkout,true);
		$criteria->compare('is_active',$this->is_active);
		$criteria->compare('status',$this->status);
		$criteria->compare('schedule',$this->schedule,true);
		$criteria->compare('hr_status',$this->hr_status);
		$criteria->compare('hr_logs',$this->hr_logs,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Logs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
