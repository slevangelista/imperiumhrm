<?php

/**
 * This is the model class for table "{{leave_request}}".
 *
 * The followings are the available columns in table '{{leave_request}}':
 * @property integer $id
 * @property integer $emp_id
 * @property string $start_date
 * @property string $end_date
 * @property string $reason
 * @property string $date_submitted
 * @property integer $leave_type_id
 * @property string $if_leave_type_others
 * @property integer $days_with_pay
 * @property integer $tl_emp_id
 * @property integer $tl_request_id
 * @property string $tl_dissaproval
 * @property integer $sv_emp_id
 * @property integer $sv_request_id
 * @property string $sv_dissaproval
 * @property integer $hr_emp_id
 * @property integer $hr_request_id
 * @property string $hr_dissaproval
 *
 * The followings are the available model relations:
 * @property LeaveType $leaveType
 * @property Employee $tlEmp
 * @property RequestType $tlRequest
 * @property Employee $svEmp
 * @property RequestType $svRequest
 * @property Employee $hrEmp
 * @property RequestType $hrRequest
 * @property Employee $emp
 */
class LeaveRequest extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{leave_request}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('start_date, end_date, leave_type_id', 'required'),
			array('emp_id, leave_type_id, days_with_pay, request_type_id, position_id', 'numerical', 'integerOnly'=>true),
			// array('reason, if_leave_type_others', 'length', 'max'=>100),
			array('request_disapproval', 'length', 'max'=>255),
			array('date_filed, start_date, end_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('date_filed, start_date, end_date, reason, date_submitted, leave_type_id, if_leave_type_others, days_with_pay, request_type_id, request_disapproval, position_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'position' => array(self::BELONGS_TO, 'Employee', 'position_id'),
			'requestType' => array(self::BELONGS_TO, 'RequestType', 'request_type_id'),
			'leaveType' => array(self::BELONGS_TO, 'LeaveType', 'leave_type_id'),
			'emp' => array(self::BELONGS_TO, 'Employee', 'emp_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'emp_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'emp_id' => 'Employee ID',
			'date_filed' => 'Date of Filing',
			'start_date' => 'Start Date',
			'end_date' => 'End Date',
			'date_submitted' => 'Date Submitted',
			'leave_type_id' => 'Type Of Leave',
			'if_leave_type_others' => 'If Leave Type Others',
			'days_with_pay' => 'Days With Pay',
			'request_type_id' => 'Leave Status',
			'request_disapproval' => 'Reason for Disapproval',
			'position_id' => 'Position',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('emp_id',$this->emp_id,true);
		$criteria->compare('date_filed',$this->date_filed,true);
		$criteria->compare('start_date',$this->start_date,true);
		$criteria->compare('end_date',$this->end_date,true);
		$criteria->compare('reason',$this->reason,true);
		$criteria->compare('date_submitted',$this->date_submitted,true);
		$criteria->compare('leave_type_id',$this->leave_type_id);
		$criteria->compare('if_leave_type_others',$this->if_leave_type_others,true);
		$criteria->compare('days_with_pay',$this->days_with_pay);
		$criteria->compare('request_type_id',$this->request_type_id);
		$criteria->compare('request_disapproval',$this->request_disapproval,true);
		$criteria->compare('position_id',$this->position_id);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LeaveRequest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getHierarchyId($posId)
	{
		$pos = Position::model()->findByAttributes(array('id'=>$posId));
			return ($pos->hierarchy_id+1);
	}

	public function getHierarchyName($hierarchyId)
	{
		$pos = Position::model()->findByAttributes(array('hierarchy_id'=>$hierarchyId));
			return $pos->name;
	}

	public function getUserPositionName($pid)
	{
		$pos = Position::model()->findByAttributes(array('id'=>$pid));
			return $pos->name;
	}

	public function getUserPositionId($empid)
	{
		$emp = Employee::model()->findByAttributes(array('emp_id'=>$empid));
			return $emp->position_id;
		// $pos = Position::model()->findByAttributes(array('id'=>$pid));
		// 	return $pos->id;
	}
}
