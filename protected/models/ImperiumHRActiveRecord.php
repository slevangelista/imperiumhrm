<?php
abstract class ImperiumHRActiveRecord extends CActiveRecord

{
    protected function beforeSave()
    {
        if(null !== Yii::app()->user)
            $id=Yii::app()->user->id;
        else
            $id=1;

        return parent::beforeSave();
    }

    /**
    * Attaches the timestamp behavior to update our create and update times
    */

    public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
            'class' => 'zii.behaviors.CTimestampBehavior',
            'createAttribute' => 'create_at',
            'updateAttribute' => 'lastvisit',
            'setUpdateOnCreate' => true,
            ),
        );
    }

    /**
     *apply a hash on the password before we store it in the database
     */

    protected function afterValidate()
    {
        parent::afterValidate();
        if(!$this->hasErrors())
            $this->password = $this->hashPassword($this->password);
    }

    /**
    * Generates the password hash.
    * @param string password
    * @return string hash
    */

    public function hashPassword($password)
    {
        return md5($password);
    }
}
?>

