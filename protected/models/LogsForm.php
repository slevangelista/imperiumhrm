<?php

class LogsForm extends CFormModel
{

    // Set Fields in form here:
    public $select_date;
    public $select_filter = array(0 => 'Department', 1 => 'Employee');
    public $logs;
    public $generate_report = array(0 => 'Cutoff Period', 1 => 'Date Picker');

    public function rules()
    {
        return array(

            array('csv_file', 'file', 
                'types'=>'csv',
                'maxSize'=>1024 * 1024 * 10, // 10MB
                'tooLarge'=>'The file was larger than 10MB. Please upload a smaller file.',
                'allowEmpty' => false
            ),

        );
    }

    public function attributeLabels()
    {
        return array(
            'file' => 'Select file',
        );
    }

}
