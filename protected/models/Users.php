<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $create_at
 * @property string $lastvisit
 * @property integer $superuser
 * @property integer $status
 */
class Users extends ImperiumHRActiveRecord
{
    const YES=1;
    const NO=0;

    const ACTIVATED=1;
    const DEACTIVATED=0;

    private static $_empids = array();

    public $confirm_password;
    //public $profile_pic;  
            
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'users';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('username, emp_id, password, email, confirm_password', 'required'),
            array('username', 'unique', 'message'=>'Username {value} is already in use!'),
            array('email', 'unique', 'message'=>'Email {value} is already in use!'),
            array('superuser, status, emp_id', 'numerical', 'integerOnly'=>true),
            array('username', 'length', 'max'=>20),
            array('password, email', 'length', 'max'=>128),
            array('password', 'compare','compareAttribute'=>'confirm_password'),
            array('confirm_password', 'safe'),
	        //array('profile_pic', 'file', 'types'=>'jpg, gif, png'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('username, email, create_at, superuser, status', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'User Id',
            'emp_id' => 'Employee ID',
            'username' => 'Username',
            'password' => 'Password',
            'email' => 'Email',
            'superuser' => 'Superuser',
            'status' => 'Status',
            'create_at' => 'Create Time',
            'lastvisit' => 'Last Login Time',
	        //'profile_pic' => 'Profile Picture',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
                $criteria->compare('emp_id',$this->emp_id);
        $criteria->compare('username',$this->username,true);
        $criteria->compare('password',$this->password,true);
        $criteria->compare('email',$this->email,true);
        $criteria->compare('create_at',$this->create_at,true);
        $criteria->compare('lastvisit',$this->lastvisit,true);
        $criteria->compare('superuser',$this->superuser);
        $criteria->compare('status',$this->status);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
                        'sort'=>array('defaultOrder'=>'email ASC'),
                        'pagination'=>array('pageSize'=>10),
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Users the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

        public function behaviors()
    {
        return array(
            'CTimestampBehavior' => array(
            'class' => 'zii.behaviors.CTimestampBehavior',
            'createAttribute' => 'create_at',
            'updateAttribute' => 'lastvisit',
            'setUpdateOnCreate' => true,
            ),
        );
    }

    /**
     * Checks if the given password is correct.
     * @param string the password to be validated
     * @return boolean whether the password is valid
     */
    public function validatePassword($password)
    {
        return $this->hashPassword($password)===$this->password;
    }

    public function isSuperuser()
    {
        return array(
                    self::YES=>'Yes',
                        self::NO=>'No',
                );
    }

    public function isActivated()
    {
        return array(
                self::ACTIVATED=>'Activated',
                self::DEACTIVATED=>'Deactivated',
                );
    }

    private static function loadEmpIds($user_id){

        self::$_empids[$user_id]=array();
        $models=self::model()->findAll(array(
                            'condition'=>'id=:id',
                            'params'=>array(':id'=>$user_id),
                ));
                
        foreach($models as $model)
                self::$_empids[$user_id][$model->username]=$model->emp_id;
    }

    public static function userEmpId($user_id,$username)
    {
        if(!isset(self::$_empids[$user_id]))
        self::loadEmpIds($user_id);
        return isset(self::$_empids[$user_id][$username]) ? self::$_empids[$user_id][$username] : false;
    }
}
