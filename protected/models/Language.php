<?php

/**
 * This is the model class for table "{{language}}".
 *
 * The followings are the available columns in table '{{language}}':
 * @property integer $id
 * @property integer $emp_id
 * @property string $language_name
 * @property integer $proficiency_id
 * @property string $comment
 * @property string $created_at
 * @property string $updated_at
 *
 * The followings are the available model relations:
 * @property Employee $emp
 * @property Proficiency $proficiency
 */
class Language extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{language}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('emp_id, language_name, proficiency_id', 'required'),
			array('emp_id, proficiency_id', 'numerical', 'integerOnly'=>true),
			array('language_name', 'length', 'max'=>150),
			array('comment', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, emp_id, language_name, proficiency_id, comment, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'emp' => array(self::BELONGS_TO, 'Employee', 'emp_id'),
			'proficiency' => array(self::BELONGS_TO, 'Proficiency', 'proficiency_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'emp_id' => 'Emp',
			'language_name' => 'Language Name',
			'proficiency_id' => 'Proficiency',
			'comment' => 'Comment',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('emp_id',$this->emp_id);
		$criteria->compare('language_name',$this->language_name,true);
		$criteria->compare('proficiency_id',$this->proficiency_id);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Language the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getEmployeeLanguages($emp_id){
		$sql = "SELECT lang.id,lang.emp_id, lang.language_name,lang.proficiency_id, prof.name AS proficiency"
		     . " FROM tbl_language lang, tbl_proficiency prof"
		     . " WHERE prof.id=lang.proficiency_id"
		     . " AND lang.emp_id = :emp_id";
		$data = Yii::app()->db->CreateCommand($sql);
		$data->bindParam(":emp_id",$emp_id,PDO::PARAM_INT);
		$languages = $data->queryAll();

		return isset($languages) ? $languages : false;
	}
}
