<?php

/**
 * This is the model class for table "{{request_type}}".
 *
 * The followings are the available columns in table '{{request_type}}':
 * @property integer $id
 * @property string $name
 *
 * The followings are the available model relations:
 * @property LeaveRequest[] $leaveRequests
 * @property LeaveRequest[] $leaveRequests1
 * @property LeaveRequest[] $leaveRequests2
 * @property OtRequest[] $otRequests
 * @property OtRequest[] $otRequests1
 * @property OtRequest[] $otRequests2
 */
class RequestType extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{request_type}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'leaveRequests' => array(self::HAS_MANY, 'LeaveRequest', 'tl_request_id'),
			'leaveRequests1' => array(self::HAS_MANY, 'LeaveRequest', 'sv_request_id'),
			'leaveRequests2' => array(self::HAS_MANY, 'LeaveRequest', 'hr_request_id'),
			'otRequests' => array(self::HAS_MANY, 'OtRequest', 'tl_request_id'),
			'otRequests1' => array(self::HAS_MANY, 'OtRequest', 'sv_request_id'),
			'otRequests2' => array(self::HAS_MANY, 'OtRequest', 'hr_request_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RequestType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
