<?php

/**
 * This is the model class for table "{{employee}}".
 *
 * The followings are the available columns in table '{{employee}}':
 * @property integer $id
 * @property integer $emp_id
 * @property string $lastname
 * @property string $firstname
 * @property string $middle_initial
 * @property string $birthdate
 * @property integer $gender_id
 * @property integer $nationality_id
 * @property integer $marital_status_id
 * @property string $employment_date
 * @property integer $position_id
 * @property integer $sub_department_id
 * @property integer $status_id
 * @property string $created_at
 * @property string $updated_at
 *
 * The followings are the available model relations:
 * @property Address[] $addresses
 * @property ContactNo[] $contactNos
 * @property Dependents[] $dependents
 * @property Education[] $educations
 * @property Email[] $emails
 * @property Gender $gender
 * @property Nationality $nationality
 * @property Position $position
 * @property SubDepartment $subDepartment
 * @property Status $status
 * @property MaritalStatus $maritalStatus
 * @property Experience[] $experiences
 * @property Government[] $governments
 * @property Language[] $languages
 * @property LeaveRequest[] $leaveRequests
 * @property License[] $licenses
 * @property Logs[] $logs
 * @property OtRequest[] $otRequests
 * @property Schedule[] $schedules
 * @property Skills[] $skills
 * @property SlCount[] $slCounts
 * @property VlCount[] $vlCounts
 */
class Employee extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{employee}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('emp_id', 'required'),
			array('emp_id, gender_id, nationality_id, marital_status_id, position_id, sub_department_id, status_id', 'numerical', 'integerOnly'=>true),
			array('lastname, firstname', 'length', 'max'=>50),
			array('middle_initial', 'length', 'max'=>255),
			array('birthdate, employment_date, created_at, updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, emp_id, lastname, firstname, middle_initial, birthdate, gender_id, nationality_id, marital_status_id, employment_date, position_id, sub_department_id, status_id, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'addresses' => array(self::HAS_MANY, 'Address', 'emp_id'),
			'contactNos' => array(self::HAS_MANY, 'ContactNo', 'emp_id'),
			'dependents' => array(self::HAS_MANY, 'Dependents', 'emp_id'),
			'educations' => array(self::HAS_MANY, 'Education', 'emp_id'),
			'emails' => array(self::HAS_MANY, 'Email', 'emp_id'),
			'gender' => array(self::BELONGS_TO, 'Gender', 'gender_id'),
			'nationality' => array(self::BELONGS_TO, 'Nationality', 'nationality_id'),
			'position' => array(self::BELONGS_TO, 'Position', 'position_id'),
			'subDepartment' => array(self::BELONGS_TO, 'SubDepartment', 'sub_department_id'),
			'status' => array(self::BELONGS_TO, 'Status', 'status_id'),
			'maritalStatus' => array(self::BELONGS_TO, 'MaritalStatus', 'marital_status_id'),
			'experiences' => array(self::HAS_MANY, 'Experience', 'emp_id'),
			'governments' => array(self::HAS_MANY, 'Government', 'emp_id'),
			'languages' => array(self::HAS_MANY, 'Language', 'emp_id'),
			'leaveRequests' => array(self::HAS_MANY, 'LeaveRequest', 'emp_id'),
			'licenses' => array(self::HAS_MANY, 'License', 'emp_id'),
			'logs' => array(self::HAS_MANY, 'Logs', 'emp_id'),
			'otRequests' => array(self::HAS_MANY, 'OtRequest', 'emp_id'),
			'schedules' => array(self::HAS_MANY, 'Schedule', 'emp_id'),
			'skills' => array(self::HAS_MANY, 'Skills', 'emp_id'),
			'slCounts' => array(self::HAS_MANY, 'SlCount', 'emp_id'),
			'vlCounts' => array(self::HAS_MANY, 'VlCount', 'emp_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'emp_id' => 'Emp',
			'lastname' => 'Lastname',
			'firstname' => 'Firstname',
			'middle_initial' => 'Middlename',
			'birthdate' => 'Birthdate',
			'gender_id' => 'Gender',
			'nationality_id' => 'Nationality',
			'marital_status_id' => 'Marital Status',
			'employment_date' => 'Employment Date',
			'position_id' => 'Position',
			'sub_department_id' => 'Sub Department',
			'status_id' => 'Status',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('emp_id',$this->emp_id);
		$criteria->compare('lastname',$this->lastname,true);
		$criteria->compare('firstname',$this->firstname,true);
		$criteria->compare('middle_initial',$this->middle_initial,true);
		$criteria->compare('birthdate',$this->birthdate,true);
		$criteria->compare('gender_id',$this->gender_id);
		$criteria->compare('nationality_id',$this->nationality_id);
		$criteria->compare('marital_status_id',$this->marital_status_id);
		$criteria->compare('employment_date',$this->employment_date,true);
		$criteria->compare('position_id',$this->position_id);
		$criteria->compare('sub_department_id',$this->sub_department_id);
		$criteria->compare('status_id',$this->status_id);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Employee the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
 * Custom Methods
 * 
 * Methods not auto-generated by the model generator. Used for model extendibility
 *
 */

	/**
	 * Public Function getFullName
	 *
	 * Returns concatenated result of lastname . ', ' . firstname . ' ' . middle_initial
	 *
	 * @return string
	 */
	public function getFullName()
	{
		return $this->lastname . ", " . $this->firstname . " " . $this->middle_initial;
	}

	public function getFullNameUnformal()
	{
		return $this->firstname . ' ' . $this->lastname;
	}

	// public function getFullName($emp_id){

	// 	$sql = "SELECT lastname, firstname FROM tbl_employee"
	// 		 . " WHERE emp_id = :emp_id";
	//     $dataReader = Yii::app()->db->CreateCommand($sql);
	//     $dataReader->bindParam(":emp_id",$emp_id,PDO::PARAM_INT);
	//     $name = $dataReader->queryAll();
	//     $firstname = $name[0]["firstname"];
	//     $lastname = $name[0]["lastname"];
	//     $fullname = $lastname.", ".$firstname;

	//     return $fullname;
	// }

	public function getIdFromEmployee($emp_id)
	{

		$result = array();
                $criteria = new CDbCriteria();
                $criteria->select = 'id';
                $criteria->condition = 'emp_id=:emp_id';
                $criteria->params = array(':emp_id'=>$emp_id);
                $model = Employee::model()->find($criteria);
                return isset($model) ? $model->id : false;
	}

	public function getEmployeeId($id){

		$result = array();
                $criteria = new CDbCriteria();
                $criteria->select = 'emp_id';
                $criteria->condition = 'id=:id';
                $criteria->params = array(':id'=>$id);
                $model = Employee::model()->find($criteria);
                return isset($model) ? $model->emp_id : false;
	}
}
