<?php

/**
 * This is the model class for table "{{skills}}".
 *
 * The followings are the available columns in table '{{skills}}':
 * @property integer $id
 * @property integer $emp_id
 * @property string $skill_name
 * @property integer $proficiency_id
 * @property integer $years_of_experience
 * @property string $comment
 * @property string $created_at
 * @property string $updated_at
 *
 * The followings are the available model relations:
 * @property Employee $emp
 * @property Proficiency $proficiency
 */
class Skills extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{skills}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('emp_id, skill_name, proficiency_id, years_of_experience', 'required'),
			array('emp_id, proficiency_id, years_of_experience', 'numerical', 'integerOnly'=>true),
			array('skill_name', 'length', 'max'=>100),
			array('comment', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, emp_id, skill_name, proficiency_id, years_of_experience, comment, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'emp' => array(self::BELONGS_TO, 'Employee', 'emp_id'),
			'proficiency' => array(self::BELONGS_TO, 'Proficiency', 'proficiency_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'emp_id' => 'Emp',
			'skill_name' => 'Skill Name',
			'proficiency_id' => 'Proficiency',
			'years_of_experience' => 'Years Of Experience',
			'comment' => 'Comment',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('emp_id',$this->emp_id);
		$criteria->compare('skill_name',$this->skill_name,true);
		$criteria->compare('proficiency_id',$this->proficiency_id);
		$criteria->compare('years_of_experience',$this->years_of_experience);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Skills the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function getEmployeeSkills($emp_id){
		$sql = "SELECT skills.id,skills.emp_id,skills.skill_name,skills.proficiency_id,prof.name as proficiency,skills.years_of_experience AS years" 
		     . " FROM tbl_skills skills, tbl_proficiency prof"
		     . " WHERE prof.id = skills.proficiency_id"
		     . " AND skills.emp_id = :emp_id";
		$data = Yii::app()->db->CreateCommand($sql);
		$data->bindParam(":emp_id",$emp_id,PDO::PARAM_INT);
		$skills = $data->queryAll();

		return $skills;

	}
}
