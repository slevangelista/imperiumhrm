<?php
/* @var $this ProficiencyController */
/* @var $model Proficiency */

$this->breadcrumbs=array(
	'Proficiencies'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Proficiency', 'url'=>array('index')),
	array('label'=>'Create Proficiency', 'url'=>array('create')),
	array('label'=>'View Proficiency', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Proficiency', 'url'=>array('admin')),
);
?>

<h1>Update Proficiency <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>