<?php
/* @var $this ProficiencyController */
/* @var $model Proficiency */

$this->breadcrumbs=array(
	'Proficiencies'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Proficiency', 'url'=>array('index')),
	array('label'=>'Create Proficiency', 'url'=>array('create')),
	array('label'=>'Update Proficiency', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Proficiency', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Proficiency', 'url'=>array('admin')),
);
?>

<h1>View Proficiency #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
	),
)); ?>
