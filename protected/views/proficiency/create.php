<?php
/* @var $this ProficiencyController */
/* @var $model Proficiency */

$this->breadcrumbs=array(
	'Proficiencies'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Proficiency', 'url'=>array('index')),
	array('label'=>'Manage Proficiency', 'url'=>array('admin')),
);
?>

<h1>Create Proficiency</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>