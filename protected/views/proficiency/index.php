<?php
/* @var $this ProficiencyController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Proficiencies',
);

$this->menu=array(
	array('label'=>'Create Proficiency', 'url'=>array('create')),
	array('label'=>'Manage Proficiency', 'url'=>array('admin')),
);
?>

<h1>Proficiencies</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
