<?php
/* @var $this GovRequirementsController */
/* @var $model GovRequirements */

$this->breadcrumbs=array(
	'Gov Requirements'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List GovRequirements', 'url'=>array('index')),
	array('label'=>'Create GovRequirements', 'url'=>array('create')),
	array('label'=>'Update GovRequirements', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete GovRequirements', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage GovRequirements', 'url'=>array('admin')),
);
?>

<h1>View GovRequirements #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
	),
)); ?>
