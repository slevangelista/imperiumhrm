<?php
/* @var $this GovRequirementsController */
/* @var $model GovRequirements */

$this->breadcrumbs=array(
	'Gov Requirements'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List GovRequirements', 'url'=>array('index')),
	array('label'=>'Manage GovRequirements', 'url'=>array('admin')),
);
?>

<h1>Create GovRequirements</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>