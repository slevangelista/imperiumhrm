<?php
/* @var $this GovRequirementsController */
/* @var $model GovRequirements */

$this->breadcrumbs=array(
	'Gov Requirements'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List GovRequirements', 'url'=>array('index')),
	array('label'=>'Create GovRequirements', 'url'=>array('create')),
	array('label'=>'View GovRequirements', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage GovRequirements', 'url'=>array('admin')),
);
?>

<h1>Update GovRequirements <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>