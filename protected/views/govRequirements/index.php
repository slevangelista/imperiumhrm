<?php
/* @var $this GovRequirementsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Gov Requirements',
);

$this->menu=array(
	array('label'=>'Create GovRequirements', 'url'=>array('create')),
	array('label'=>'Manage GovRequirements', 'url'=>array('admin')),
);
?>

<h1>Gov Requirements</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
