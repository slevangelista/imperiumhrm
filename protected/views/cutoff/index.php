<!-- Load Route-Specific JS -->
<script>
    var app_url = "<?php echo Yii::app()->request->baseUrl; ?>"; // Get Base URL using Yii
</script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/angularApp/attendanceCutoff.js"></script> <!-- AttendanceCutoff -->

<div class="payroll-container" ng-app="attendanceCutoffApp" ng-controller="attendanceCutoffController" ng-init="getCutoffAll();">

    <h3>Manage Cutoff</h3>
    <p>Add, edit or delete cutoff periods here</p>
    <br />

    <table class="table table-bordered">
        <thead>
            <tr>
                <th style='width: 60px;'>#</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th style='width: 70px;'>Actions</th>
            </tr>
        </thead>
        <tbody>
            <tr ng-repeat='value in cutoff'>
                <td>{{value.id}}</td>
                <td>{{value.date_start}}</td>
                <td>{{value.date_end}}</td>
                <td style='text-align: center;'>
                    <a ng-click='editCutoff(value);' href='' style='padding: 0 2px 0 2px;' data-toggle="tooltip" title="Edit"><span class='glyphicon glyphicon-pencil'></span></a>
                    <a ng-click='deleteCutoff(value);' href='' style='padding: 0 2px 0 2px;' data-toggle="tooltip" title="Delete"><span class='glyphicon glyphicon-trash'></span></a>
                </td>
            </tr>
        </tbody>
    </table>

    <button class="btn btn-default" ng-click="addCutoff()">Add New Cutoff</button>

</div>
