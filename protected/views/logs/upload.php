<!-- Upload DOM -->
<div class="payroll-container">

    <!-- Set CWidget ActiveForm -->
    <?php  
        $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
            'action' => Yii::app()->getBaseUrl(true) . "/index.php/logs/generateupload",
            'htmlOptions' => array('enctype' => 'multipart/form-data'), 
        ));
    ?>

    <h3>Attendance Upload</h3>
    <p style="color: gray;">Upload attendance log here</p>
    <br />

    <p>CSV Format</p>
    <table class="table table-bordered">
    <thead>
        <tr>
            <th>Department</th>
            <th>Name</th>
            <th>No.</th>
            <th>Date/Time</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Systems</td>
            <td>Imperium Employee</td>
            <td>21</td>
            <td>4/15/2014 8:00:00 AM</td>
            <td>C/In</td>
        </tr>
        <tr>
            <td>Systems</td>
            <td>Imperium Employee</td>
            <td>21</td>
            <td>4/15/2014 6:00:00 PM</td>
            <td>C/Out</td>
        </tr>
    </tbody>
    </table>
    
    <br>
    
    <p style="font-style: ITALIC;">Note: Column arrangement shown above IS important. Please follow the column arrangement given above. </p>
    <p style="font-style: ITALIC; margin-left: 38px;">Additional column information after `Status` are NOT relevant and can be omitted in the CSV file. </p>

    <div class="payroll-panel">
        <?php echo CHtml::activeFileField($model['logs_form'], 'logs'); ?>
    </div>

    <!-- Submit Form -->
    <?php echo CHtml::submitButton('Upload Attendance', array('class' => 'btn btn-default')); ?>

    <!-- End Widget -->
    <?php $this->endWidget(); ?>

</div>