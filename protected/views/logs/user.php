<!-- Load Route-Specific JS -->
<script>
    var app_url = "<?php echo Yii::app()->request->baseUrl; ?>"; // Get Base URL using Yii
</script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/angularApp/logsUser.js"></script> <!-- Logs User -->

<div class="payroll-container" ng-app="logsUserApp" ng-controller="logsUserController" ng-init="getCutoffList();">

    <!-- Set CWidget ActiveForm -->
    <?php  
        $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
            'action' => Yii::app()->getBaseUrl(true) . "/index.php/logs",
            'htmlOptions' => array('enctype' => 'multipart/form-data'), 
        ));
    ?>

    <h3>User Logs: <?php echo $user; ?></h3>
    <p>View your logs here</p>
    <br />

    <a href="" ng-click="is_filters = !is_filters"><div class="payroll-header">
        <input type="text" ng-model="is_filters" ng-hide="true" name="is_filters"/>
        <span class="glyphicon glyphicon-calendar"></span> Generate Report
        <span class="caret" ng-show="is_filters"></span>
        <span class="dropup" ng-hide="is_filters">
            <span class="caret"></span>
        </span>
    </div></a>

    <div class="payroll-panel" ng-show="is_filters">
    
        <?php 
            echo $form->select2Group(
                $model['logs_form'],
                'generate_report',
                array(
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5',
                    ),
                    'widgetOptions' => array(
                        'asDropDownList' => true,
                        'data' => array(0 => 'By Cutoff Period', 1 => 'By Date Picker'),
                        'htmlOptions' => array(
                            'ng-model' => 'date_filter_type',
                            'ng-init' => 'getCurrentCutoff();',
                        ),
                        'options' => array(
                            'width' => '100%',
                        )
                    )
                )
            ); 
        ?>

        <div ng-show="date_filter_type == 0">
            <b>Select Cutoff Period</b> <br />
            <ui-select ng-model="$parent.dateRange" theme="select2" style="width: 100%; margin: 5px 0 15px 0;">
                <ui-select-match>{{$select.selected.value}}</ui-select-match>
                <ui-select-choices repeat="cutoff in cutoffList | selectFilter: {value: $select.search}">
                    <div ng-bind-html="cutoff.value | highlight: $select.search"></div>
                </ui-select-choices>
            </ui-select>
        </div>

        <div ng-show="date_filter_type == 1">
            <?php 
                echo $form->dateRangeGroup(
                $model['logs_form'],
                'select_date',
                array(
                    'widgetOptions' => array(
                        'callback' => 'js:function(start, end){console.log(start.toString("MMMM d, yyyy") + " - " + end.toString("MMMM d, yyyy"));}',
                        'htmlOptions' => array('ng-model' => 'dateRange'),
                    ),
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5',
                    ),
                    'format' => 'yyyy-mm-dd',
                    'viewformat' => 'mm/dd/yyyy',
                    'prepend' => '<i class="glyphicon glyphicon-calendar"></i>'
                )); 
            ?>
        </div>
    </div>

    <button onclick="return false;" ng-show="is_filters" ng-click="generateLogs();" class="btn btn-default">Generate Logs</button>

    <div ng-show="is_logs">
        <div class="payroll-header">
            <span class="glyphicon glyphicon-list-alt"></span> View Logs
        </div>

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Logs</th>
                    <th>Schedule</th>
                    <th>Status</th>
                </tr>
            </thead>

            <tbody>
                <tr ng-repeat="value in employees[2]">
                    <td style="width: 25%">{{value.date}}</td>
                    <td style="width: 25%; background-color: {{value.color}};">{{value.logs}}</td>
                    <td style="width: 25%;">{{value.schedule}}</td>
                    <td style="width: 25%;">{{value.status}}</td>
                </tr>
            </tbody>
        </table>
    </div>

    <!-- End Widget -->
    <?php $this->endWidget(); ?>

</div>