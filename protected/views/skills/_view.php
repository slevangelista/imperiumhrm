<?php
/* @var $this SkillsController */
/* @var $data Skills */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_id')); ?>:</b>
	<?php echo CHtml::encode($data->emp_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('skill_name')); ?>:</b>
	<?php echo CHtml::encode($data->skill_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('proficiency_id')); ?>:</b>
	<?php echo CHtml::encode($data->proficiency_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('years_of_experience')); ?>:</b>
	<?php echo CHtml::encode($data->years_of_experience); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('comment')); ?>:</b>
	<?php echo CHtml::encode($data->comment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_at')); ?>:</b>
	<?php echo CHtml::encode($data->created_at); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_at')); ?>:</b>
	<?php echo CHtml::encode($data->updated_at); ?>
	<br />

	*/ ?>

</div>