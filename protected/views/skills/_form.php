<?php
	$emp_id = $_GET['emp_id'];
	$form = $this->beginWidget('booster.widgets.TbActiveForm',
            							array('id' => 'horizontalForm',
                                	  		  'type' => 'horizontal',
                                              'htmlOptions' => array('class' => 'well'), // for inset effect
                                      		 )
                                      );
  
?>
	<fieldset>
		<Legend>Skills</Legend>
	
	<?php echo $form->textFieldGroup($model,'skill_name'); ?>
	<?php echo $form->error($model,'skill_name'); ?>
	<?php

		

		$list = Proficiency::model()->getAllProficiency();
		echo $form->select2Group($model,'proficiency_id',
										array('wrapperHtmlOptions' => array('class' => 'col-sm-5',),
											  'widgetOptions' => array(
											  						'name' => 'proficiency_id',
											  						'data' => $list,
											  						'htmlOptions' => array(
											  									'id' => 'issue-574-checker-select'
																				)
																)
											)
									);
	?>

		<?php 
			$numbers = array();
			$initialize = 1;
			while($initialize <= 20){
				$numbers[$initialize] = $initialize;
				$initialize++;
			}
			echo $form->dropDownListGroup($model,'years_of_experience',
												array('wrapperHtmlOptions' => array('class' => 'col-sm-5',),
													  'widgetOptions' => array(
													  						'name' => 'years_of_experience',
																			'data' => $numbers,
																			'htmlOptions' => array(),
																			)
													)
											    ); 
		?>
	
	    <?php 
			$this->widget('booster.widgets.TbButton',
                           array('buttonType' => 'submit', 'label' => $model->isNewRecord ? 'Create' : 'Save' , 'context' => 'success'));
			echo "&nbsp;&nbsp;";
			
			$this->widget('booster.widgets.TbButton',
        					array(
                			'buttonType' => 'link',
							'label' => 'Qualifications',
							'context' => 'info',
                			'url' => array('education/employeeEducation?emp_id='.$emp_id),
        				)
			);

		?>
</fieldset>

<?php $this->endWidget(); ?>

