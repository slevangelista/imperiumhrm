<?php
/* @var $this OtRequestController */
/* @var $model OtRequest */

$this->breadcrumbs=array(
	'Ot Requests'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List OtRequest', 'url'=>array('index')),
	array('label'=>'Manage OtRequest', 'url'=>array('admin')),
);
?>

<h1>O.T.Application Form</h1>-

<?php $this->renderPartial('_form', array('model'=>$model)); ?>