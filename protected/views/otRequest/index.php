<?php
/* @var $this OtRequestController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ot Requests',
);

$this->menu=array(
	array('label'=>'Create OtRequest', 'url'=>array('create')),
	array('label'=>'Manage OtRequest', 'url'=>array('admin')),
);
?>

<!--<h1>Ot Requests</h1>-->

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
