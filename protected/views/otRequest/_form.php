<?php
/* @var $this OtRequestController */
/* @var $model OtRequest */
/* @var $form CActiveForm */
?>



<div class="ot_application_form">
<!--
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ot-request-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'emp_id'); ?>
		<?php echo $form->textField($model,'emp_id'); ?>
		<?php echo $form->error($model,'emp_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'start_time'); ?>
		<?php echo $form->textField($model,'start_time'); ?>
		<?php echo $form->error($model,'start_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'end_time'); ?>
		<?php echo $form->textField($model,'end_time'); ?>
		<?php echo $form->error($model,'end_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
		<?php echo $form->error($model,'date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'reason'); ?>
		<?php echo $form->textField($model,'reason',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'reason'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_submitted'); ?>
		<?php echo $form->textField($model,'date_submitted'); ?>
		<?php echo $form->error($model,'date_submitted'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tl_emp_id'); ?>
		<?php echo $form->textField($model,'tl_emp_id'); ?>
		<?php echo $form->error($model,'tl_emp_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tl_request_id'); ?>
		<?php echo $form->textField($model,'tl_request_id'); ?>
		<?php echo $form->error($model,'tl_request_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tl_dissaproval'); ?>
		<?php echo $form->textField($model,'tl_dissaproval',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'tl_dissaproval'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sv_emp_id'); ?>
		<?php echo $form->textField($model,'sv_emp_id'); ?>
		<?php echo $form->error($model,'sv_emp_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sv_request_id'); ?>
		<?php echo $form->textField($model,'sv_request_id'); ?>
		<?php echo $form->error($model,'sv_request_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sv_dissaproval'); ?>
		<?php echo $form->textField($model,'sv_dissaproval',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'sv_dissaproval'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hr_emp_id'); ?>
		<?php echo $form->textField($model,'hr_emp_id'); ?>
		<?php echo $form->error($model,'hr_emp_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hr_request_id'); ?>
		<?php echo $form->textField($model,'hr_request_id'); ?>
		<?php echo $form->error($model,'hr_request_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hr_dissaproval'); ?>
		<?php echo $form->textField($model,'hr_dissaproval',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'hr_dissaproval'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>
-->

<!--OT Application form begins here -->
<div class="ot_request_background">
<h1>O.T.Application Form</h1>
<div class= "ot_request_employee">

<b> <?php echo "Name:" ; ?> <b>
<br>
<b> <?php echo "Position:"; ?> <b>


<div>
<div class = "ot_request">

<table>
 	 <th>

<?php /*OT Request Fields */?>

<?php echo "Date:" ?>
<?php $date = date("d/m/y");
$this->widget(
    'booster.widgets.TbDatePicker',
     array(
        'name' => 'current_date',
        'value' =>$date,
        'htmlOptions' => array('class'=>'col-md-1'),
    )
); ?>
	</th>
	<th>
<?php echo "StartTime:" ?>

<?php
$this->widget(
    'booster.widgets.TbTimePicker',
     array(
        'name' => 'some_time',
        'value' => 'null',
        'options' => array(
            'disableFocus' => true,
            'showMeridian' => false 
        ),
        'wrapperHtmlOptions' => array('class' => 'col-md-1'),
    )
);
 ?>
	</th>
	<th>
<?php echo "Remarks:" ?>
<?php echo CHtml::textArea('Text','',
 array('id'=>'idTextField', 
       'col'=>200, 
       'maxlength'=> 150) ); ?>
	</th>

</table>
	<center>
	<br>
<?php /*OT request Form Buttons */?>
<?php
$this->widget(
    'booster.widgets.TbButton',
    array(
        'label' => 'Submit',
        'context' => 'primary',
    )
); echo '';

?> 
	<th>
	<?php
$this->widget(
    'booster.widgets.TbButton',
    array(
        'label' => 'Back',
        'context' => 'primary',
    )
); echo '';
?> 
	</center>


	</th>
</div>
</div>
<?php $this->endWidget(); ?>
</table>
</div> <!-- form -->
</div>
</div>