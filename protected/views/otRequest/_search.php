<?php
/* @var $this OtRequestController */
/* @var $model OtRequest */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'emp_id'); ?>
		<?php echo $form->textField($model,'emp_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'start_time'); ?>
		<?php echo $form->textField($model,'start_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'end_time'); ?>
		<?php echo $form->textField($model,'end_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'reason'); ?>
		<?php echo $form->textField($model,'reason',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date_submitted'); ?>
		<?php echo $form->textField($model,'date_submitted'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tl_emp_id'); ?>
		<?php echo $form->textField($model,'tl_emp_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tl_request_id'); ?>
		<?php echo $form->textField($model,'tl_request_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tl_dissaproval'); ?>
		<?php echo $form->textField($model,'tl_dissaproval',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sv_emp_id'); ?>
		<?php echo $form->textField($model,'sv_emp_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sv_request_id'); ?>
		<?php echo $form->textField($model,'sv_request_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sv_dissaproval'); ?>
		<?php echo $form->textField($model,'sv_dissaproval',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hr_emp_id'); ?>
		<?php echo $form->textField($model,'hr_emp_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hr_request_id'); ?>
		<?php echo $form->textField($model,'hr_request_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hr_dissaproval'); ?>
		<?php echo $form->textField($model,'hr_dissaproval',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->