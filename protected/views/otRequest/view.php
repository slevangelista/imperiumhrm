<?php
/* @var $this OtRequestController */
/* @var $model OtRequest */

$this->breadcrumbs=array(
	'Ot Requests'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List OtRequest', 'url'=>array('index')),
	array('label'=>'Create OtRequest', 'url'=>array('create')),
	array('label'=>'Update OtRequest', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete OtRequest', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage OtRequest', 'url'=>array('admin')),
);
?>

<h1>View OtRequest #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'emp_id',
		'start_time',
		'end_time',
		'date',
		'reason',
		'date_submitted',
		'tl_emp_id',
		'tl_request_id',
		'tl_dissaproval',
		'sv_emp_id',
		'sv_request_id',
		'sv_dissaproval',
		'hr_emp_id',
		'hr_request_id',
		'hr_dissaproval',
	),
)); ?>
