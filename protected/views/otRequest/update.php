<?php
/* @var $this OtRequestController */
/* @var $model OtRequest */

$this->breadcrumbs=array(
	'Ot Requests'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List OtRequest', 'url'=>array('index')),
	array('label'=>'Create OtRequest', 'url'=>array('create')),
	array('label'=>'View OtRequest', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage OtRequest', 'url'=>array('admin')),
);
?>

<h1>Update OtRequest <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>