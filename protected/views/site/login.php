<!DOCTYPE HTML>
<html>
<head>

    <title>Imperium HRM - Login</title>

    <!-- Dev. Team CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/imperium/jerome.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/imperium/noi.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/imperium/karen.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/imperium/syd.css">

</head>
<body class="container">

<div class="image-header">
            <div style="width:90%;float:left;"><a href="<?= Yii::app()->getBaseUrl(true) . "/index.php/home"; ?>" ><img src="<?= Yii::app()->getBaseUrl(true) . "/images/logo.jpeg"; ?>" /></a></div>
</div>

<div class="content">
    <div class="payroll-container">

    <!-- <img src="<?=Yii::app()->baseUrl."/images/line.gif"; ?>" width="100%"> -->

    <h3>Imperium HRM</h3>
    <p>Human Resource Management System of Imperium IT Support and Outsourcing Inc.</p>
    <br />
    <?php
    	$form = $this->beginWidget(
    			'booster.widgets.TbActiveForm',
    			array(
    				'id' => 'verticalForm',
    				'htmlOptions' => array(), // for inset effect
    									)
    			);
    ?>

    <!-- <img src="<?=Yii::app()->baseUrl."/images/imperium.png"; ?>" width="70%" /> -->
    <!-- <div class="payroll-header">
        <span class="glyphicon glyphicon-th"></span> Login Page
    </div> -->
    <div class="payroll-panel">
    <?php echo $form->textFieldGroup($model, 'username'); ?>
    <?php echo $form->passwordFieldGroup($model, 'password'); ?>
    <?php echo $form->checkboxGroup($model, 'rememberMe'); ?>
    </div>
    <?php 
        $this->widget(
        'booster.widgets.TbButton',
                array('buttonType' => 'submit', 'label' => 'Login', 'context' => 'default')
        );
    ?>
    <?php $this->endWidget(); ?>

    </div>
</div>

<div class="clear"></div>

<div class="footer">
    Copyright &copy; <?php echo date('Y'); ?> by Imperium IT Outsourcing Inc.<br/>
    All Rights Reserved.<br/>
    <?php //echo Yii::powered(); ?>
</div><!-- footer -->

</body>
</html>

