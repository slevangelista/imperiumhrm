<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<div class='payroll-container'>
<h3>Welcome to <i>Imperium</i> Employee Dashboard</h3><br>

<!--<p>Congratulations! You have successfully created your Yii application.</p>

<p>You may change the content of this page by modifying the following two files:</p>
<ul>
	<li>View file: <code><?php //echo __FILE__; ?></code></li>
	<li>Layout file: <code><?php //echo $this->getLayoutFile('main'); ?></code></li>
</ul>

<p>For more details on how to further develop this application, please read
the <a href="http://www.yiiframework.com/doc/">documentation</a>.
Feel free to ask in the <a href="http://www.yiiframework.com/forum/">forum</a>,
should you have any questions.</p>
-->
<?php 
// if(!Yii::app()->user->isGuest):
?>
<?php

// print_r($leaveFiled);die();
$tr = '';
foreach ($leaveFiled as $lf)
{
	$start_date = $lf['start_date'];
	$end_date = $lf['end_date'];
	$leave_type = $lf['leave_type_id'];
	$request_type = $lf['request_type_id'];

	$sd = date('F d, Y',strtotime($start_date));
	$ed = date('F d, Y',strtotime($end_date));
	if($sd == $ed){
		$date = $sd;
	}else{
		$date = $sd.' - '.$ed;
	}
	$tr .= "<tr><td>$date</td><td>$leaveTypeArr[$leave_type]</td><td>$requestTypeArr[$request_type]</td></tr>";
}
?>
<?php //leave credits panel widget
$currMonth = date('F Y');
$title1 = "Your Available Leave Credits as of $currMonth";
$title2 = "Leave Filed for the Month of $currMonth";
$scheduleTitle = "Your Schedule for the Month of $currMonth";
?>

<?php 
$box = $this->beginWidget(
    'booster.widgets.TbPanel',
    array(
        'title' => $scheduleTitle,
        'headerIcon' => 'list-alt',
        'context' => 'info',
    	'padContent' => false,
        'htmlOptions' => array('class' => 'panel1-dashboard')
    )
);
?>
	<div id='leave-credits-home'>
		<table id='home-leavecredits-table'>
			<tr>
				<th>Date</th>
				<th>Schedule</th>
			</tr>
			<tr>
				<?php echo $td; ?>
			</tr>
		</table>
	</div>

<?php $this->endWidget();?>
<?php 
$box = $this->beginWidget(
    'booster.widgets.TbPanel',
    array(
        'title' => $title1,
        'headerIcon' => 'list-alt',
        'context' => 'info',
    	'padContent' => false,
        'htmlOptions' => array('class' => 'panel1-dashboard')
    )
);
?>
	<div id='leave-credits-home'>
		<table id='home-leavecredits-table'>
			<tr>
				<th>Vacation Leave</th>
				<th><?php echo $empVlCount; ?></th>
			</tr>
			<tr>
				<th>Sick Leave</th>
				<th><?php echo $empSlCount; ?></th>
			</tr>
		</table>
	</div>

<?php $this->endWidget();?>

<?php  //leave filed panel widget
$box = $this->beginWidget(
    'booster.widgets.TbPanel',
    array(
        'title' => $title2,
        'headerIcon' => 'pencil',
        'context' => 'info',
    	'padContent' => false,
        'htmlOptions' => array('class' => 'panel2-dashboard')
    )
);
?>
	<div id='leave-filed-home'>
		<table id='home-leavefiled-table'>
			<tr>
				<th>Date</th>
				<th>Type</th>
				<th>Status</th>
			</tr>
			<?php echo $tr;?>
		</table>
	</div>
</div>
<?php $this->endWidget();?>

<?php 
// endif;
?>
