<?php
/* @var $this ContactNoController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Contact Nos',
);

$this->menu=array(
	array('label'=>'Create ContactNo', 'url'=>array('create')),
	array('label'=>'Manage ContactNo', 'url'=>array('admin')),
);
?>

<h1>Contact Nos</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
