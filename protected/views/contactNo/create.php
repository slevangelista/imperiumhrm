<?php
/* @var $this ContactNoController */
/* @var $model ContactNo */

/*$this->breadcrumbs=array(
	'Contact Nos'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ContactNo', 'url'=>array('index')),
	array('label'=>'Manage ContactNo', 'url'=>array('admin')),
);*/
?>

<div class="payroll-container">
<h3>Contacts</h3>
<p>User contact list</p>
<br />
<?php $this->renderPartial('_form', array('model'=>$model)); ?>
</div>