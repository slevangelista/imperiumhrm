<script>
    var app_url = "<?php echo Yii::app()->request->baseUrl; ?>"; // Get Base URL using Yii
</script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/angularApp/employeeContactApp.js"></script>
<?php
/* @var $this ContactNoController */
/* @var $model ContactNo */
/* @var $form CActiveForm */
?>

<?php
	//$emp_id = Yii::app()->request->getQuery('id');
	$emp_id = $_GET['emp_id'];
	$path = Yii::app()->request->baseUrl;
?>

<div ng-app="employeeContactApp" ng-controller="employeeContactController" ng-init="getContact(<?php echo $emp_id; ?>)">
	
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Contact Type</th>
				<th>Number</th>
				<th>Edit</th>
			</tr>
		</thead>
		<tbody>
			<tr ng-repeat="value in contact">
				<td>{{value.name}}</td>
				<td>{{value.number}}</td>
				<td>
				<a ng-href="<?php echo $path;?>/index.php/contactNo/update?emp_id=<?php echo $emp_id; ?>&id={{value.id}}">
				<span class="glyphicon glyphicon-pencil"></span></td>
			</tr>
		</tbody>
	</table>

	<form action="" method="POST">
	    <div>
	      <div ng-repeat="item in items">
	  		<input type="text" placeholder="{{item.typePlaceholder}}" ng-model="item.contact">
	      	<input type="text" placeholder="{{item.numberPlaceholder}}" ng-model="item.number">
	      </div>
	      </br>
	      <button class="btn btn-default" onClick="return false" ng-click="add()">Add Contact Field</button>
	      <button class="btn btn-default" onClick="return false" ng-click="remove()">Remove Contact Field</button>
	      <?php //$emp_id = Yii::app()->request->getQuery('id');?>
	      <button ng-click="save(<?php echo $emp_id; ?>);" class="btn btn-default">Save</button>
	    </div>
	</form>


<script type="text/javascript">

  
</script>

</div>