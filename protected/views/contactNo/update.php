<?php
/* @var $this ContactNoController */
/* @var $model ContactNo */

/*$this->breadcrumbs=array(
	'Contact Nos'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ContactNo', 'url'=>array('index')),
	array('label'=>'Create ContactNo', 'url'=>array('create')),
	array('label'=>'View ContactNo', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ContactNo', 'url'=>array('admin')),
);*/

	$form = $this->beginWidget('booster.widgets.TbActiveForm',
            							array('id' => 'horizontalForm',
                                	  		  'type' => 'horizontal',
                                              'htmlOptions' => array('class' => 'payroll-container'), // for inset effect
                                      		 )
                                      );

     echo $form->errorSummary($model); 
?>


<fieldset>
		<h3>Contacts</h3>

	
	    <p class="note">Fields with <span class="required">*</span> are required.</p>
	   
		<?php echo $form->textFieldGroup($model,'name'); ?>
		<?php echo $form->error($model,'name'); ?>

		<?php echo $form->textFieldGroup($model,'number'); ?>
		<?php echo $form->error($model,'number');?>

		<?php 
			$this->widget('booster.widgets.TbButton',
                           array('buttonType' => 'submit', 'label' => 'Save', 'context' => 'default'));

		?>
</fieldset>
<?php $this->endWidget(); ?>

