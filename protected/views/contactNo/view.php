<?php

	$gridDataProvider = new CArrayDataProvider($model);

	$gridColumns = array(
					array('name' => 'type', 'header' => 'Contact Type'),
					array('name' => 'number', 'header' => 'Number'),	
	);

	$this->widget(
			'booster.widgets.TbGridView',
			array(
				'dataProvider' => $gridDataProvider,
				'template' => "{items}",
				'columns' => $gridColumns,
			)
	);
?>
