<script>
    var app_url = "<?php echo Yii::app()->request->baseUrl; ?>"; // Get Base URL using Yii
</script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/angularApp/leaveCreditsApp.js"></script>

<div class='payroll-container' ng-app="leaveCreditsApp" ng-controller="leaveCreditsController" ng-init="getAllCredits()">

	<h3>Employee Leave Credits</h3>
	<p>Add, edit or delete employee leave credits here</p>

	<!-- <button class="btn btn-info" ng-click='bulkCredits()'>Add Bulk Leave Credits</button> -->
	<br>
	<div class="payroll-header">
	    <span class="glyphicon glyphicon-calendar"></span> Employee Leave Credits
	</div>
	<table class='table table-bordered table-striped table-hover'>
		<thead>
		<tr>
			<th>Employee ID</th>
			<th>Employee Name</th>
			<th>Vacation Leave Credits</th>
			<th>Sick Leave Credits</th>
			<th>Action</th>
		</tr>
		</thead>
		<tr ng-repeat="(key, value) in credits">
			<td>{{key}}</td>
			<td>{{value.fullname}}</td>
			<td>{{value.vl}}</td>
			<td>{{value.sl}}</td>
			<td><a href='' ng-click='editCredits(key, value.fullname, value.vl, value.sl)'><span class = 'glyphicon glyphicon-pencil'></span></a></td>
		</tr>
	</table>
</div>