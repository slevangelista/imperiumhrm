<!-- Load Route-Specific JS -->
<script>
    var app_url = "<?php echo Yii::app()->request->baseUrl; ?>"; // Get Base URL using Yii
</script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/angularApp/holidayIndex.js"></script> <!-- HolidayIndex -->

<div ng-app="holidayIndexApp" ng-controller="holidayIndexController" class="payroll-container" ng-init="getHolidayAll()">

    <h3>Manage Holidays</h3>
    <p>Add, edit or delete holiday periods here</p>

    <table class="table table-bordered">
        <thead>
            <tr>
                <th style='font-weight: bold; width: 100px;'>#</th>
                <th style='font-weight: bold;'>Holiday Name</th>
                <th style='font-weight: bold; width: 70px;'>Actions</th>
            </tr>
        </thead>
        <tbody>
            <tr ng-repeat='(key, value) in holiday'>
                <td>{{value.id}}</td>
                <td>{{value.name}}</td>
                <td style='text-align: center;'>
                    <a ng-click='editHoliday(value.id, value.name);' href='' style='padding: 0 2px 0 2px;' data-toggle="tooltip" title="Edit"><span class='glyphicon glyphicon-pencil'></span></a>
                    <a ng-click='deleteHoliday(value.id);' href='' style='padding: 0 2px 0 2px;' data-toggle="tooltip" title="Delete"><span class='glyphicon glyphicon-trash'></span></a>
                </td>
            </tr>
        </tbody>
    </table>

    <button class="btn btn-default" ng-click="addHoliday();">Add New Holiday</button>

</div>