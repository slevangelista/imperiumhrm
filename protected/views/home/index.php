<!-- Load Route-Specific JS -->
<script>
    var app_url = "<?php echo Yii::app()->request->baseUrl; ?>"; // Get Base URL using Yii
</script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/angularApp/homeIndex.js"></script> <!-- homeIndex -->

<div ng-app="homeIndexApp" ng-controller="homeIndexController" ng-init='setHome(<?php echo json_encode($day); ?>)' class="payroll-container">

	<h3>Imperium HRM Dashboard</h3>
	<p>Welcome, <?php echo $user; ?></p>
    <br />

    <!-- Current Schedule -->
    <div>
		<h4>Your Current Schedule:</h4>

	    <div class="payroll-header">
	        <span class="glyphicon glyphicon-calendar"></span> This Week Schedule
	    </div>

	    <!-- Button div -->
	    <table class="table table-bordered">
	        <thead>
	            <tr>
	                <th style="width: 10%; font-weight: bold;">Name</th>
	                <th style="width: 10%;" ng-repeat="val in currentWeek">{{val}}</th>
	            </tr>
	        </thead>
	        <tbody>
	            <tr>
	                <td>{{employees.name}}</td>
	                <td ng-repeat="(date, val) in employees.schedule" style="background-color: {{val.color}}" >
	                    {{val.name + ' ' + val.schedule}}
	                </td>
	            </tr>
	        </tbody>
	    </table>
    <div>

   	<br />
    <div>
    	<h4>Your Current Leave:</h4>

    	<div class="payroll-header">
	        <span class="glyphicon glyphicon-calendar"></span> Available Leave Credits
	    </div>

	    <table class="table table-bordered">
	    	<tr>
	    		<th>Vacation Leave</th>
	    		<td>{{leaveCredits.vl}}</td>
	    	</tr>
	    	<tr>
	    		<th>Sick Leave</th>
	    		<td>{{leaveCredits.sl}}</td>
	    	</tr>
	    </table>
    </div>

    <br />
    <div>
    	<h4>Your Leave Request:</h4>

    	<div class="payroll-header">
	        <span class="glyphicon glyphicon-calendar"></span> Leave Request For This Month
	    </div>

	    <table class="table table-bordered">
	    	<thead>
	    	<tr>
	    		<th>Leave Start Date</th>
	    		<th>Leave End Date</th>
	    		<th>Leave Type</th>
	    		<th>Leave Status</th>
	    	</tr>
	    	</thead>
	    	<tr  ng-repeat="(key, val) in leaveRequest">
	    		<td>{{val.start_date}}</td>
	    		<td>{{val.end_date}}</td>
	    		<td>{{val.leave_type}}</td>
	    		<td>{{val.request_type}}</td>
	    	</tr>
	    </table>
    </div>

</div>