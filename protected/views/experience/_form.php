<?php
	$emp_id = $_GET['emp_id'];
	$form = $this->beginWidget('booster.widgets.TbActiveForm',
            							array('id' => 'horizontalForm',
                                	  		  'type' => 'horizontal',
                                              'htmlOptions' => array('class' => 'well'), // for inset effect
                                      		 )
                                      );
  
?>
	<fieldset>
		<Legend>Work Experience</Legend>
	

	<?php echo $form->textFieldGroup($model,'company'); ?>
	<?php echo $form->error($model,'company'); ?>

	<?php echo $form->textFieldGroup($model,'job_title'); ?>
	<?php echo $form->error($model,'job_title'); ?>

	<?php echo $form->datePickerGroup($model,'date_started',
							array(
								'widgetOptions' => array(
										'options' => array(
											'language' => 'en',
											 'format' => 'yyyy-mm-dd',
											),
										),
								'wrapperHtmlOptions' => array(
											'class' => 'col-sm-5',
											),

								'prepend' => '<i class="glyphicon glyphicon-calendar"></i>'
								)
						); 
		?>
					
		<?php echo $form->error($model,'date_started'); ?>
		
		<?php echo $form->datePickerGroup($model,'date_ended',
							array(
								'widgetOptions' => array(
										'options' => array(
											'language' => 'en',
											 'format' => 'yyyy-mm-dd',
											),
										),
								'wrapperHtmlOptions' => array(
											'class' => 'col-sm-5',
											),

								'prepend' => '<i class="glyphicon glyphicon-calendar"></i>'
								)
						); 
		?>
		
	    <?php 
			$this->widget('booster.widgets.TbButton',
                           array('buttonType' => 'submit', 'label' => $model->isNewRecord ? 'Create' : 'Save' , 'context' => 'success'));

			echo "&nbsp;&nbsp;";
			
			$this->widget('booster.widgets.TbButton',
        					array(
                			'buttonType' => 'link',
							'label' => 'Qualifications',
							'context' => 'info',
                			'url' => array('education/employeeEducation?emp_id='.$emp_id),
        				)
			);

		?>
</fieldset>
<?php $this->endWidget(); ?>
