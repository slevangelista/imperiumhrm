<?php
	$emp_id = $_GET['emp_id'];
	$form = $this->beginWidget('booster.widgets.TbActiveForm',
            							array('id' => 'horizontalForm',
                                	  		  'type' => 'horizontal',
                                              'htmlOptions' => array('class' => 'well'), // for inset effect
                                      		 )
                                      );
  
?>
	<fieldset>
		<Legend>Skills</Legend>
		<?php echo $form->textFieldGroup($model,'license_name'); ?>
		<?php echo $form->error($model,'license_name'); ?>

		<?php echo $form->textFieldGroup($model,'license_code'); ?>
		<?php echo $form->error($model,'license_code'); ?>

	    <?php 
			$this->widget('booster.widgets.TbButton',
                           array('buttonType' => 'submit', 'label' => $model->isNewRecord ? 'Create' : 'Save' , 'context' => 'success'));
			echo "&nbsp;&nbsp;";
			
			$this->widget('booster.widgets.TbButton',
        					array(
                			'buttonType' => 'link',
							'label' => 'Qualifications',
							'context' => 'info',
                			'url' => array('education/employeeEducation?emp_id='.$emp_id),
        				)
			);

		?>
</fieldset>

<?php $this->endWidget(); ?>

