<?php
	$form = $this->beginWidget('booster.widgets.TbActiveForm',
            							array('id' => 'horizontalForm',
                                	  		  'type' => 'horizontal',
                                              'htmlOptions' => array('class' => 'well'), // for inset effect
                                      		 )
                                      ); 
?>

<fieldset>
		<Legend>Government</Legend>
	    <p class="note">Fields with <span class="required">*</span> are required.</p>
<?php  

	 $emp_id = $_GET['emp_id'];
	 $model->emp_id = $emp_id;
	 
	  $list = GovernmentController::getGovernmentIdTypes();
	  echo $form->dropDownListGroup($model,'gov_requirement_type_id',
								  array('wrapperHtmlOptions' => array('class' => 'col-sm-5',),
										'widgetOptions' => array('data' => $list,
										'htmlOptions' => array(),
										)
								)
							); 
	  echo $form->textFieldGroup($model,'value');
	  $this->widget('booster.widgets.TbButton',
                           array('buttonType' => 'submit', 'label' => 'Save' , 'context' => 'success'));
?>
</fieldset>
<?php $this->endWidget(); ?>