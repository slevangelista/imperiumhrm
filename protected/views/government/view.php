<?php
  $gridDataProvider = new CArrayDataProvider($model);

	$gridColumns = array(
					array('name' => 'id_type', 'header' => 'Government ID Type'),
					array('name' => 'value', 'header' => 'ID Number'),	
	);

	$this->widget(
			'booster.widgets.TbGridView',
			array(
				'dataProvider' => $gridDataProvider,
				'template' => "{items}",
				'columns' => $gridColumns,
			)
	);
?>
