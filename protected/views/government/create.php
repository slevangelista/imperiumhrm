<?php
/* @var $this GovernmentController */
/* @var $model Government */

/*$this->breadcrumbs=array(
	'Governments'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Government', 'url'=>array('index')),
	array('label'=>'Manage Government', 'url'=>array('admin')),
);*/
?>
<div class="payroll-container">
<h3>Government</h3>
<p>User government requirement list</p>
<br/>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>
</div>