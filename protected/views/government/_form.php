<?php
/* @var $this GovernmentController */
/* @var $model Government */
/* @var $form CActiveForm */
?>

	
<?php
		//$emp_id = Yii::app()->request->getQuery('id');
		$emp_id = $_GET['emp_id'];
		$path = Yii::app()->request->baseUrl;
		// $edit_icon = CHtml::image(Yii::app()->request->baseUrl.'/images/pencil.png','admin',array("width" => '30px',"height" => '30px'));
    $edit_icon = "<span class='glyphicon glyphicon-pencil'></span>";
        $governmentIds = GovernmentController::getEmployeeGovernmentIds($emp_id);
       
        $view_ids = "<table class='table table-bordered'>"
        		  . "<tr>"
                  . "<th>Government Requirement Type</th>"
                  . "<th>ID Number</font></th>"
                  . "<th>Edit</th>"
                  . "</tr>";
        foreach($governmentIds as $key=>$value){
        	$view_ids = $view_ids."<tr>"
        			  . "<td>".$value['id_type']."</td>"
        			  . "<td>".$value['value']."</td>"
        			  . "<td><a href=".$path."/index.php/government/update?emp_id=".$emp_id."&id=".$value['id'].">"
				        .$edit_icon."</a></td>"
				        . "</tr>";

        }

        $view_ids = $view_ids."</table>";
        echo $view_ids;
		/*$gridDataProvider = new CArrayDataProvider($governmentIds);

		$gridColumns = array(
						array('name' => 'id_type', 'header' => 'Government Requirement Type'),
						array('name' => 'value', 'header' => 'Number'),
						
					);

		$this->widget(
    		'booster.widgets.TbGridView',
    		array(
    			'dataProvider' => $gridDataProvider,
    			'template' => "{items}",
    			'columns' => $gridColumns,
    		)
    	);*/
?>

<br />
<?php
	$model->emp_id = $emp_id;
	$form = $this->beginWidget('booster.widgets.TbActiveForm',
            							array('id' => 'horizontalForm',
                                	  		  'type' => 'horizontal',
                                              'htmlOptions' => array(), // for inset effect
                                      		 )
                                      );
	
	$list = GovernmentController::getGovernmentIdTypes();
	echo $form->dropDownListGroup($model,'gov_requirement_type_id',
								  array('wrapperHtmlOptions' => array(),
										'widgetOptions' => array('data' => $list,
										'htmlOptions' => array(),
										)
								)
							); 
	echo $form->textFieldGroup($model,'value');
	$this->widget('booster.widgets.TbButton',
                           array('buttonType' => 'submit', 'label' => 'Create' , 'context' => 'default'));
?>
<?php $this->endWidget(); ?>