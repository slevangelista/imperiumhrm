<?php
/* @var $this GovernmentController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Governments',
);

$this->menu=array(
	array('label'=>'Create Government', 'url'=>array('create')),
	array('label'=>'Manage Government', 'url'=>array('admin')),
);
?>

<h1>Governments</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
