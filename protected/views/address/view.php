<?php

	$addresses = array();
	foreach($model as $key){

		$info = array();
		$info['type'] = $key->name;
		$info['location'] = $key->location;
		$addresses[] = $info;	
	}

		
	$gridDataProvider = new CArrayDataProvider($addresses);

	$gridColumns = array(
					array('name' => 'type', 'header' => 'Address Type'),
					array('name' => 'location', 'header' => 'Location'),	
	);

	$this->widget(
			'booster.widgets.TbGridView',
			array(
				'dataProvider' => $gridDataProvider,
				'template' => "{items}",
				'columns' => $gridColumns,
			)
	);

?>
