<?php
/* @var $this AddressController */
/* @var $model Address */

/*$this->breadcrumbs=array(
	'Addresses'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);*/

/*$this->menu=array(
	array('label'=>'List Address', 'url'=>array('index')),
	array('label'=>'Create Address', 'url'=>array('create')),
	array('label'=>'View Address', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Address', 'url'=>array('admin')),
);*/

	$form = $this->beginWidget('booster.widgets.TbActiveForm',
            							array('id' => 'horizontalForm',
                                	  		  'type' => 'horizontal',
                                              'htmlOptions' => array('class' => 'payroll-container'), // for inset effect
                                      		 )
                                      );

       echo $form->errorSummary($model); ?>


<fieldset>
		<h3>Update Address</h3>
	
	    <p class="note">Fields with <span class="required">*</span> are required.</p>
	   
		<?php echo $form->textFieldGroup($model,'name'); ?>
		<?php echo $form->error($model,'name'); ?>

		<?php echo $form->textFieldGroup($model,'location'); ?>
		<?php echo $form->error($model,'location');?>

		<?php 
			$this->widget('booster.widgets.TbButton',
                           array('buttonType' => 'submit', 'label' => 'Save', 'context' => 'default'));

		?>
</fieldset>
<?php $this->endWidget(); ?>


<?php //$this->renderPartial('_form', array('model'=>$model)); ?>