<script>
    var app_url = "<?php echo Yii::app()->request->baseUrl; ?>"; // Get Base URL using Yii
</script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/angularApp/employeeAddressApp.js"></script>

<?php
	$emp_id = $_GET['emp_id'];

    $path = Yii::app()->request->baseUrl;
?>

<div ng-app="employeeAddressApp" ng-controller="employeeAddressController" ng-init="getAddress(<?php echo $emp_id; ?>)">
	
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Address Type</th>
				<th>Location</th>
				<th>Edit</th>
			</tr>
		</thead>
		<tbody>
			<tr ng-repeat="value in address">
				<td>{{value.name}}</td>
				<td>{{value.location}}</td>
				<td>
				<a ng-href="<?php echo $path;?>/index.php/address/update?emp_id=<?php echo $emp_id; ?>&id={{value.id}}">
				<span class="glyphicon glyphicon-pencil"></span></td>
		</tbody>
	</table>

	<form action="" method="POST">
	    <div>
	      <div ng-repeat="item in items">
	  		<input type="text" placeholder="{{item.typePlaceholder}}" ng-model="item.address">
	      	<input type="text" placeholder="{{item.locationPlaceholder}}" ng-model="item.location">
	      </div>
	      </br>
	      <button class="btn btn-default" onClick="return false" ng-click="add()">Add Address Field</button>
	      <button class="btn btn-default" onClick="return false" ng-click="remove()">Remove Address Field</button>
	      <button ng-click="save(<?php echo $emp_id; ?>);" class="btn btn-default">Save</button>
	    </div>
	</form>

</div>

<script type="text/javascript">

  
</script>