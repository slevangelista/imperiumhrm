<!-- Load Route-Specific JS -->
<script>
    var app_url = "<?php echo Yii::app()->request->baseUrl; ?>"; // Get Base URL using Yii
</script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/angularApp/scheduleUser.js"></script> <!-- scheduleUser -->

<div ng-app="scheduleUserApp" ng-controller="scheduleUserController"
    ng-init='setSchedule(<?php echo json_encode($day); ?>)' class="payroll-container">

    <h3>User Schedule: <?php echo $user; ?></h3>
    <p>View your schedules here</p>
    <br />

    <div class="payroll-header">
        <span class="glyphicon glyphicon-calendar"></span> Schedule Calendar
    </div>

    <div class="payroll-panel">
        <!-- Button div -->
        <div style="padding-bottom: 10px; text-align: center;">
            <ul class="schedulenav">
            <li><button ng-click="getPreviousWeek()" id="calendar-left" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-chevron-left"></span></button></li>
            <li><label id='weekLabel'>{{weekTitle}}</label></li>
            <li><button ng-click="getNextWeek()" id="calendar-right" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-chevron-right"></span></button></li>
            </ul>
        </div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <td style="width: 10%; font-weight: bold;">Name</td>
                    <td style="width: 10%;" ng-repeat="val in currentWeek">{{val}}</td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{employees.name}}</td>
                    <td ng-repeat="(date, val) in employees.schedule" style="background-color: {{val.color}}" >
                        {{val.name + ' ' + val.schedule}}
                    </td>
                </tr>
            </tbody>
        </table>

    </div>

</div>