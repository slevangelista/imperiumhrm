<!-- Load Route-Specific JS -->
<script>
    var app_url = "<?php echo Yii::app()->request->baseUrl; ?>"; // Get Base URL using Yii
</script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/angularApp/scheduleIndex.js"></script> <!-- ScheduleIndex -->

<div class="payroll-container" ng-app="scheduleIndexApp" ng-controller="scheduleIndexController" 
    ng-init='setSchedule(<?php echo json_encode($day); ?>); getLeaveList(); getHolidayList();'>
<!-- 
    {{test_time}}
    <input class="form-control" size="1" ng-model="test_time" name="time" bs-timepicker data-time-format="HH:mm" data-length="1" data-minute-step="1" data-autoclose="1" type="text">
    {{formatDate(test_date)}}
    <input class="form-control" ng-model="test_date" data-date-format="yyyy-MM-dd" data-autoclose="1" name="datepicker" bs-datepicker type="text">
-->
    <h3>Manage Schedule</h3>
    <p>Lets you create & assign shifts, modify or edit schedule, or just view schedule report here</p>
    <br />

    <div class="payroll-header">
        <span class="glyphicon glyphicon-filter"></span> Select Filter
    </div>

    <div class="payroll-panel">
        <label>Select Department</label>
        <?php $department_list = CHtml::listData($model['department'], 'id', 'name'); ?>
        <?php 
            $this->widget(
            'booster.widgets.TbSelect2', array(
                'name' => 'department',
                'data' => $department_list,
                'options' => array(
                    'placeholder' => 'Select department to display',
                    'width' => '100%',
                ),
                'htmlOptions' =>array(
                    'style' => 'padding-top: 5px; padding-bottom: 10px;',
                    'ng-change' => 'startSchedule()',
                    'ng-init' => 'setShift()',
                    'ng-model' => 'department',
                )
            )); 
        ?>

        <div ng-hide="is_department">
            <p>Note: Please select department in order to proceed</p>
        </div>

        <div ng-show="is_department">

        </div>

    </div>

    <!-- Weekly Calendar -->
    <div ng-show="is_department">
    <div class="payroll-header">
        <span class="glyphicon glyphicon-calendar"></span> Schedule Calendar
    </div>

    <div class="payroll-panel">
        <!-- Button div -->
        <div style="padding-bottom: 10px; text-align: center;">
            <ul class="schedulenav">
            <li><button ng-click="getPreviousWeek()" id="calendar-left" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-chevron-left"></span></button></li>
            <li><label id='weekLabel'>{{weekTitle}}</label></li>
            <li><button ng-click="getNextWeek()" id="calendar-right" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-chevron-right"></span></button></li>
            </ul>
        </div>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <td style="width: 10%; font-weight: bold;">Name</td>
                    <td style="width: 10%;" ng-repeat="val in currentWeek">{{val}}</td>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="(key,value) in employees">
                    <td>{{value.name}}</td>
                    <td ng-repeat="(date, val) in employees[key]['schedule']" style="background-color: {{val.color}}" >
                        <a ng-click="modalUpdateSchedule(key, value.emp_id, date)" href="">{{val.name + ' ' + val.schedule}}</a>
                    </td>
                </tr>
            </tbody>
        </table>

    </div>

    <button ng-click="modalCreateShift()" class="btn btn-default">Add Shift</button>
    <button ng-click="modalAddSchedule()" class="btn btn-default">Assign Shift to Employee</button>
    <button ng-click="modalSetHoliday()" class="btn btn-default" ng-hide="false">Set Holiday</button>
    <button ng-click="modalSetLeave()" class="btn btn-default" ng-hide="false">Set Leave</button>
    <button style="float: right;" ng-click="check()" class="btn btn-default" ng-hide="false"><span class="glyphicon glyphicon-wrench"></span> Settings</button>
    </div>

</div>