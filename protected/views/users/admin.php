<?php
/* @var $this UsersController */
/* @var $model Users */

$this->widget(
	'booster.widgets.TbBreadcrumbs',
	array(
		'links' => array('Manage'=>'',
        			 'Assignments' => Rights::getBaseUrl(),
        			 'Permissions' => Rights::getBaseUrl().'/authItem/permissions',
        			 'Roles' => Rights::getBaseUrl().'/authItem/roles',
        			 'Tasks' => Rights::getBaseUrl().'/authItem/tasks',
        			 'Operations' => Rights::getBaseUrl().'/authItem/operations',
                                 
                                 ),
        )

);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#users-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Users</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php /*echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); */?>
</div><!-- search-form -->


<?php 

	$gridDataProvider = new CArrayDataProvider($model->search()->getData());

	$gridColumns = array(
				array('name'=>'username', 'header'=>'Username'),
				array('name'=>'email', 'header'=>'Email'),
				array('name'=>'create_at', 'header'=>'Create Time'),
				array('name'=>'lastvisit', 'header'=>'Last Login Time'),
                                array(
                    			'name'=>'superuser',
                    			'header'=>'Superuser',
                    			'filter'=> array('1'=>'Yes', '0'=>'No'),
                    			'value'=>'($data->superuser=="1")?("Yes"):("No")'                 
                		),

				array( 
				       'name'=>'status', 
				       'header'=>'Status', 
				       'filter'=>array('1'=>'Activated','0'=>'Deactivated'), 
				       'value'=>'($data->status=="1")?("Activated"):("Deactivated")' 
				     ),

				/*array(
                                        'header' => 'Actions',
					'htmlOptions' => array('nowrap'=>'nowrap'),
					'class'=>'booster.widgets.TbButtonColumn',
					'viewButtonUrl'=>'Yii::app()->createUrl("users/view",$params=array("id"=>$data["id"]))',
					'updateButtonUrl'=>'Yii::app()->createUrl("users/update",$params=array("id"=>$data["id"]))',
					'deleteButtonUrl'=>'Yii::app()->createUrl("users/delete",$params=array("id"=>$data["id"]))',
				),*/
			);

    $this->widget(
    		'booster.widgets.TbGridView',
    		array(
    			'dataProvider' => $gridDataProvider,
    			'template' => "{items}",
    			'columns' => $gridColumns,
                        'filter' => $model,
    		)
    );


	$this->widget(
		'booster.widgets.TbButton',
        	array(
                	'buttonType' => 'link',
					'label' => 'Create New User',
					'context' => 'success',
                	'url' => array('users/create'),
        	)
	);
?>
