<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>


<div style="width: 100%; overflow: hidden;">
	<div style="width: 12%; float: left;">
	<?php 
	/*$form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	));*/

	  if(!Yii::app()->user->isGuest){
                    $user_id = Yii::app()->user->id;
                    $username = Yii::app()->user->name;
                    $emp_id = Users::model()->userEmpId($user_id,$username);
                    $id = Employee::model()->getIdFromEmployee($emp_id);
                    $profile_url = array('/employee/view/'.$emp_id);
                }

	echo CHtml::image(Yii::app()->request->baseUrl.'/images/profile/admin.png','admin',
						array("width" => '150px',"height" => '150px')); 

	 $this->widget(
     	'booster.widgets.TbMenu',
     		array(
         		'type' => 'list',
    		 	'items' => array(
    								array('label' => 'Personal',
    									  'url' => $profile_url,
    									  'itemOptions' => array('class' => 'active')
    									),
    								array('label' => 'Address', 'url' => '#'),
    								array('label' => 'Government', 'url' => '#'),
    								array('label' => 'Contact', 'url' => '#'),
    								array('label' => 'Job', 'url' => '#'),
    								array('label' => 'Qualifications', 'url' => '#'),
    								array('label' => 'Personnel', 'url' => '#'),
    							)
    			)
    );
	?>
	</div>

	<div class="form">
	<fieldset>

	<?php
	/** @var TbActiveForm $form */
			$form = $this->beginWidget('booster.widgets.TbActiveForm',
										array('id' => 'verticalForm',
											  'type' => 'horizontal',
											  'htmlOptions' => array('class' => 'well'), // for inset effect
			                                 )
	                                  );
	?>
	<Legend>Account Settings</Legend>
	            
	<p class="note">Fields with <span class="required">*</span> are required.</p></br>
	<?php echo $form->errorSummary($model); ?>
	<?php
			
		$list =UsersController::getEmployeeNames();
		
		$action = Yii::app()->controller->action->id;
		
		if(Yii::app()->user->checkAccess('Admin') && $action == 'create'){
		
			echo $form->select2Group($model,'emp_id',
					array('wrapperHtmlOptions' => array('class' => 'col-sm-5',),
					      'widgetOptions' => array(
									'name' => 'emp_id',
									'data' => $list,
									'htmlOptions' => array(
									'id' => 'issue-574-checker-select',
									)
								      )
					     )
					);
		}

		elseif(Yii::app()->user->checkAccess('Admin') && $action == 'update'){
			echo $form->textFieldGroup($model,'emp_id');
		}
       ?>
	<?php //echo $form->error($model,'emp_id'); ?>
	<?php echo $form->textFieldGroup($model,'username'); ?>
	<?php //echo $form->error($model,'username'); ?>
	<?php echo $model->password = '';?>
	<?php echo $form->passwordFieldGroup($model,'password',array('size'=>60,'maxlength'=>50)); ?>
	<?php //echo $form->error($model,'password'); ?>
	<?php echo $form->passwordFieldGroup($model,'confirm_password',array('size'=>60,'maxlength'=>50)); ?>
	<?php //echo $form->error($model,'confirm_password'); ?>
	<?php echo $form->textFieldGroup($model,'email',array('size'=>60,'maxlength'=>128)); ?>
	<?php //echo $form->error($model,'email'); ?>
	<?php 
	      if(Yii::app()->user->checkAccess('Admin')){
		echo $form->dropDownListGroup($model,'superuser',
						array(
							'wrapperHtmlOptions' => array('class' => 'col-sm-5',),
							'widgetOptions' => array(
										'data' => Users::isSuperuser(),
										'htmlOptions' => array(),
										)
							)
					); 

		echo $form->dropDownListGroup($model,'status',
						array(
							'wrapperHtmlOptions' => array('class' => 'col-sm-5',),
							'widgetOptions' => array(
										'data' => Users::isActivated(),
										'htmlOptions' => array(),
										)
							)
					);
		} 
	?>

	<?php /*echo $form->fileFieldGroup($model, 'profile_pic',
				array(
						'wrapperHtmlOptions' => array(
						'class' => 'col-sm-5',
					),
				)
		   );*/
	?>

	<?php //echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); 
		$this->widget(
			'booster.widgets.TbButton',
			array('buttonType' => 'submit', 'label' => $model->isNewRecord ? 'Create' : 'Save', 'context' => 'success')
			);
	?>

	<?php $this->endWidget(); ?>
	</fieldset>
	</div><!-- form -->
</div>
