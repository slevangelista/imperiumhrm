<!-- Load Route-Specific JS -->
<script>
    var app_url = "<?php echo Yii::app()->request->baseUrl; ?>"; // Get Base URL using Yii
</script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/angularApp/manhoursIndex.js"></script> <!-- Manhours Index -->

<div class="payroll-container" ng-app="manhoursIndexApp" ng-controller="manhoursIndexController" ng-init="getCutoffList();">

    <!-- Set CWidget ActiveForm -->
    <?php  
        $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
            'action' => Yii::app()->getBaseUrl(true) . "/index.php/manhours",
            'htmlOptions' => array('enctype' => 'multipart/form-data'), 
        ));
    ?>

    <h3>Manhours</h3>
    <p>View total manhours report here</p>
    <br />

    <a href="" ng-click="is_filters = !is_filters"><div class="payroll-header">
        <input type="text" ng-model="is_filters" ng-hide="true" name="is_filters"/>
        <span class="glyphicon glyphicon-calendar"></span> Generate Report
        <span class="caret" ng-show="is_filters"></span>
        <span class="dropup" ng-hide="is_filters">
            <span class="caret"></span>
        </span>
    </div></a>

    <div class="payroll-panel" ng-show="is_filters">
    
        <?php 
            echo $form->select2Group(
                $model['logs_form'],
                'generate_report',
                array(
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5',
                    ),
                    'widgetOptions' => array(
                        'asDropDownList' => true,
                        'data' => array(0 => 'By Cutoff Period', 1 => 'By Date Picker'),
                        'htmlOptions' => array(
                            'ng-model' => 'date_filter_type',
                            'ng-init' => 'getCurrentCutoff();',
                        ),
                        'options' => array(
                            'width' => '100%',
                        )
                    )
                )
            ); 
        ?>

        <div ng-show="date_filter_type == 0">
            <b>Select Cutoff Period</b> <br />
            <ui-select ng-model="$parent.dateRange" theme="select2" style="width: 100%; margin: 5px 0 15px 0;">
                <ui-select-match>{{$select.selected.value}}</ui-select-match>
                <ui-select-choices repeat="cutoff in cutoffList | selectFilter: {value: $select.search}">
                    <div ng-bind-html="cutoff.value | highlight: $select.search"></div>
                </ui-select-choices>
            </ui-select>
        </div>

        <div ng-show="date_filter_type == 1">
            <?php 
                echo $form->dateRangeGroup(
                $model['logs_form'],
                'select_date',
                array(
                    'widgetOptions' => array(
                        'callback' => 'js:function(start, end){console.log(start.toString("MMMM d, yyyy") + " - " + end.toString("MMMM d, yyyy"));}',
                        'htmlOptions' => array('ng-model' => 'dateRange'),
                    ),
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5',
                    ),
                    'format' => 'yyyy-mm-dd',
                    'viewformat' => 'mm/dd/yyyy',
                    'prepend' => '<i class="glyphicon glyphicon-calendar"></i>'
                )); 
            ?>
        </div>

        <b>Show Department List</b> <br />
        <?php $department_list = CHtml::listData($model['department'], 'id', 'name'); ?>
        <?php 
            $this->widget(
            'booster.widgets.TbSelect2', array(
                'name' => 'department',
                'data' => $department_list,
                'options' => array(
                    'placeholder' => 'Select department to display',
                    'width' => '100%',
                ),
                'htmlOptions' =>array(
                    'style' => 'padding-top: 5px; padding-bottom: 10px;',
                    // 'ng-change' => 'setEmployees();',
                    'ng-model' => 'department',
                )
            )); 
        ?>

    </div>

    <button ng-disabled="department == null" onclick="return false;" ng-show="is_filters" ng-click="generateReport();" class="btn btn-default">Generate Manhours</button>

    <h3>{{department_name}}</h3>

    <div ng-show="is_man_hours">
        <div class="payroll-header">
            <span class="glyphicon glyphicon-list-alt"></span> Manhours
        </div>

        <table class="table table-bordered">
            <thead>
                <tr>
                    <td style="font-weight: bold;">Name</td>
                    <td>No Schedule</td>
                    <td>Absences</td>
                    <td>Vacation Leave</td>
                    <td>Sick Leave</td>
                    <td>Tardiness (Mins)</td>
                    <td>Tardiness (Hours)</td>
                    <td># of Days</td>
                    <td>Overtime</td>
                    <td>Restday Work</td>
                    <td>Total Hours Worked</td>
                </tr>
            </thead>
            <tbody>
                <tr ng-repeat="(key, value) in employees | orderBy:'name'">
                    <td>{{value.name}}</td>
                    <td>{{value.count.no_schedule}}</td>
                    <td><a ng-click="modalAbsences(key, value.count.absences, value.emp_id)" href="">{{value.count.absences}}</a></td>
                    <td><a ng-click="modalVacationLeave(key, value.count.vacation_leave, value.emp_id)" href="">{{value.count.vacation_leave}}</a></td>
                    <td><a ng-click="modalSickLeave(key, value.count.sick_leave, value.emp_id);" href="">{{value.count.sick_leave}}</a></td>
                    <td><a ng-click="modalTardinessMins(key, value.count.tardiness_mins, value.emp_id)" href="">{{value.count.tardiness_mins}}</a></td>
                    <td><a ng-click="modalTardinessHours(key, value.count.tardiness_hours, value.emp_id)" href="">{{value.count.tardiness_hours}}</a></td>
                    <td><a ng-click="modalNoOfDays(key, value.count.no_of_days, value.emp_id)" href="">{{value.count.no_of_days}}</a></td>
                    <td><a ng-click="modalOvertime(key, value.count.overtime_hours, value.emp_id)" href="">{{value.count.overtime_hours}}</a></td>
                    <td><a ng-click="modalRestdayWork(key, value.count.restday_work_hours, value.emp_id)" href="">{{value.count.restday_work_hours}}</a></td>
                    <td>{{ getTotalHours(value.count); }}</td>
                </tr>
            </tbody>
        </table>

        <button onclick="return false;" ng-click="check();" class="btn btn-default">Check</button>

    <div>

    <!-- End Widget -->
    <?php $this->endWidget(); ?>

</div>