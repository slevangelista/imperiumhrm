<html>

<div class =  'payroll-container'>  
			<h3>Submitted OT Request</h3>
            <p>View submitted OT request here</p>
			
<?php echo $result;

$this->widget('booster.widgets.TbGridView', array(
        'type' => 'striped bordered condensed',
        'dataProvider' => $model->search(),
        'template' => "{items}\n{pager}",
        'filter' => $model,
        'afterAjaxUpdate' => "function() {
         $('#datePickerId').datepicker({'format':'yyyy-mm-dd','autoclose':true});
         $('#typeID').select2({'allowClear':true});
        }",
        'columns' => array(
        		'emp_id',
                'start_time',
                'end_time',
                'total_hours',
                 array('name'=>'date',
				       'filter'=>$this->widget(
    				   'booster.widgets.TbDatePicker',
    				         array('model'=>$model,
    					           'attribute'=>'date',
        				           'options' => array('language' => 'en',
            				                          'format' => 'yyyy-mm-dd',
       					                             ),
                                                    'htmlOptions'=>array('id'=>'datePickerId',
                                                                         ),
    				               ),true
			             )

			           ),
        	    'remarks',
        		 array(
                'name'=>'request_type_id',
                'value'=>'RequestType::model()->FindByPk($data->request_type_id)->name',
                'filter'=>$this->widget(
                     'booster.widgets.TbSelect2',
                     array(
                         'model'=>$model,
                         'attribute' => 'request_type_id',
                          'htmlOptions'=>array(
                          'id' => 'typeID'
                         ),
                         'data' => $request_type_array,
                         'options' => array(
                             'placeholder' => 'Please select',
                             'allowClear' => true,
                             // 'width' => '40%',
                         )
                     ),true
                 )
             )
        	,'request_dissaproval'
        	),

    ));

?>



</html>