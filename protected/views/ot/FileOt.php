<!--OT Application form begins here -->

<!-- Set CWidget ActiveForm -->
<?php  
    $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
        'action' => Yii::app()->getBaseUrl(true) . "/index.php/ot/fileot",
        'htmlOptions' => array('enctype' => 'multipart/form-data'), 
    ));
?>

<div class="payroll-container">
<h3>OT Application Form</h3>
<p>File your OT application here</p>
<br />

<table class = 'table table-bordered'>
    <tr>
        <th>Name :</th>
        <td><?php echo $model['employee']['fullnameunformal']; ?></td>
    </tr>
    <tr>
        <th>Date of Filing : </th>
        <td>
            <?php 
                $date = date("Y-m-d"); 
                $this->beginWidget(
                    'booster.widgets.TbDatePicker',
                     array(
                        'name' => 'current_date',
                        'value' =>$date,
                        'options'=>array(
                            'format'=>'yyyy-mm-dd',
                        ),
                        'htmlOptions'=>array(
                            'style'=>'width: 41.8%;'
                        ),    
                    )
                ); 
                $this->endWidget();
            ?>
        </td>
    </tr>
    <tr>
        <th>Hours :</th>
        <td>
            <?php 
            $this->beginWidget(
                'booster.widgets.TbTimePicker',
                 array(
                    'name' => 'hours',
                    'value' => 'NULL',
                    'options' => array(
                        'disableFocus' => true,
                        'showMeridian' => false 
                    ),
                    'wrapperHtmlOptions' => array('class' => 'col-md-5'),
                    'htmlOptions' => array(
                        'style' => 'width: 100%',
                    ),
                )
            );
            $this->endWidget();
            ?>
        </td>
    </tr>
    <tr>
        <th>Remarks :</th>
        <td>
            <?php 
                echo CHtml::textArea(
                    'remarks',
                    '', 
                    array(
                        'col'=>200, 
                        'maxlength'=> 150,
                        'class' => 'form-control',
                    )
                );
            ?>
        </td>
    <tr>
        <td colspan="2" style="text-align: center;">
            <button type="submit"  name="submit" class="btn btn-primary">Submit OT</button> 
        </td>
    </tr>
</table>

<!-- End Widget -->
<?php $this->endWidget(); ?>

</div>