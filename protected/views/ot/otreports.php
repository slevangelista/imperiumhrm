<?php // echo $reports; ?>
<?php  echo $result; ?>
<html>
<form  method="POST">


<div class="payroll-container" >
<h3>OT Request Report</h3>
<p>View list of OT request here</p>
<br />

<center>
<table class="table table-bordered">
        
        <td>
     From: 
        </td>
         <td>           
<?php $this->widget(
                'booster.widgets.TbDatePicker',
                array(
                    'name' => 'start_date',
                    // 'id' => 'date1',
                    'htmlOptions' => array(
                        'class'=>'col-md-4', 
                        'style'=>'width: 25px;'
                    ),
                    'options'=>array(
                        'format'=>'yyyy-mm-dd',
                        'width' => '40%',
                    ),
                )
            );
            ?>
         </td>
         <td>
     To: 
         </td> 
          <td>   
<?php $this->widget(
                'booster.widgets.TbDatePicker',
                array(
                    'name' => 'end_date',
                    // 'id' => 'date1',
                    'htmlOptions' => array(
                        'class'=>'col-md-4', 
                        'style'=>'width: 25px;'
                    ),
                    'options'=>array(
                        'format'=>'yyyy-mm-dd',
                        'width' => '40%',
                    ),
                )
            ); ?>
         </td>
     <tr>
    <td>
    Name:     
    </td> 
    <td> 
<?php $this->widget(
                    'booster.widgets.TbSelect2',
                    array(
                        'name' => 'empID',
                        'data' => $emp_name_array,
                        'options' => array(
                            'placeholder' => 'Select Employee',
                            // 'width' => '70%',
                        )
                    )
                );
            ?>
        
     </td>
      <td>
            Department : 
        </td>
        <td>
            <?php 
                $this->widget(
                    'booster.widgets.TbSelect2',
                    array(
                        'name' => 'depID',
                        'data' => $department_array,
                        'options' => array(
                            'placeholder' => 'Select Department',
                            // 'width' => '70%',
                        )
                    )
                );
            ?>
        </td>
 </tr>
<td>
        Request Type : 
        </td>
        <td>
            <?php 
                $this->widget(
                    'booster.widgets.TbSelect2',
                    array(
                        'name' => 'requestTypeId',
                        'data' => $request_type_array,
                        'options' => array(
                            'placeholder' => 'Please select',
                            // 'width' => '40%',
                        )
                    )
                );
            ?>
        </td>

</th>
<th>
</th>
<th>
</th>
</center>
</table>
<p></p>

<button type="submit"  name="submit" class="btn btn-primary">Submit</button> 

<?php echo $html; ?>
</div>
</html>
