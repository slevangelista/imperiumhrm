<!-- Load Route-Specific JS -->
<script>
    var app_url = "<?php echo Yii::app()->request->baseUrl; ?>"; // Get Base URL using Yii
</script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/angularApp/otRequestApp.js"></script> <!-- AttendanceIndex -->

<div ng-app="otRequestApp" ng-controller="otRequestController" class="payroll-container" ng-init="getEmployeePendingRequest();">

	<h3>Pending OT Request</h3>
	<p>Approve/Dissaprove pending OT request here</p>
	<br />

	<div class="payroll-header">
		<span class="glyphicon glyphicon-list"></span> Request List
	</div>

	<table class="table table-bordered">
		<thead>
			<tr>
		 	    <th>Name</th>
		 	    <th>Date</th>
			 	<th>Start Time</th>
		   	    <th>End Time</th>
			   	<th>Total Hours</th>
			   	<th>Remarks</th>	
			 	<th>Action</th>
			 </tr>
		 <thead>
		 	<tr ng-repeat="(key, value) in pendingOT">
		 		<td>{{value.emp_name}}</td>
		 	    <td>{{value.date}}</td>
			 	<td>{{value.start_time}}</td>
		   	    <td>{{value.end_time}}</td>
			   	<td>{{value.total_hours}}</td>
			   	<td>{{value.remarks}}</td>
			   	<td>
	  				<button class='btn btn-success btn-xs'  ng-click='approveOT(key)'><span class='glyphicon glyphicon-ok'></span></button>
	  				<button class='btn btn-danger btn-xs' ng-click='dissaproveOT(key)'><span class='glyphicon glyphicon-remove'></span></button>
	  			</td>
	 		<tr>
		 <tbody>
		 </tbody>
	</table>

</div>