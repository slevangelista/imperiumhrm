
<?php

    $user_id = Yii::app()->user->id;
    $username = Yii::app()->user->name;
    $emp_id = Users::model()->userEmpId($user_id,$username);

    $education = EducationController::getEmployeeEducation($emp_id);    
?>
 <?php
        $educ_html = "<table class='table'>";
        foreach($education as $level_id=>$list){
            $educ_html = $educ_html."<tr><th colspan='4'>".EducationLevel::model()->getEducationLevelName($level_id)."</th></tr>"
                       . "<tr>"
                       . "<th>School</th>"
                       . "<th>Date Started</th>"
                       . "<th>Date Ended</th>"
                       . "<th>GPA</th>"
                       . "</tr>";
            foreach($list as $key=>$value){
                $educ_html = $educ_html."<tr>"
                           . "<td>".$value['school_name']."</td>"
                           . "<td>".date("F j,Y",strtotime($value['date_started']))."</td>"
                           . "<td>".date("F j,Y",strtotime($value['date_ended']))."</td>"
                           . "<td>".$value['GPA']."</td>"
                           . "</tr>";
            }
        }
        $educ_html = $educ_html."</table>";       
?>

<?php

    $experiences = Experience::model()->getEmployeeExperience($emp_id);
    $exp_html = "<table class='table'>"
              . "<tr>"
              . "<th>Company</th><th>Position</th><th>Date Started</th><th>Date Ended</th>"
              . "</tr>";

    foreach($experiences as $key=>$value){
        $exp_html = $exp_html."<tr>"
                  . "<td>".$value['company']."</td>"
                  . "<td>".$value['job_title']."</td>"
                  . "<td>".date("F j, Y",strtotime($value['date_started']))."</td>"
                  . "<td>".date("F j, Y",strtotime($value['date_ended']))."</td>"
                  . "</tr>";
    }
    $exp_html = $exp_html."</table>";

?>

<?php
  
    $skills = Skills::model()->getEmployeeSkills($emp_id);
    $skills_html = "<table class='table'>"
                 . "<tr>"
                 . "<th>Skills</th><th>Proficiency</th><th>Years of Experience</th>"
                 . "</tr>";
  
    foreach($skills as $key=>$value){
      $skills_html = $skills_html."<tr>"
                   . "<td>".$value['skill_name']."</td>"
                   . "<td>".$value['proficiency']."</td>" 
                   . "<td>".$value['years']."</td>"
                   . "</tr>";
    }

     $skills_html = $skills_html."</table>";

?>

<?php

    $languages = Language::model()->getEmployeeLanguages($emp_id);
    $lang_html = "<table class='table'>"
               . "<tr>"
               . "<th>Language</th><th>Proficiency</th>"
               . "<tr>";
    foreach($languages as $key=>$value){
      $lang_html = $lang_html."<tr>"
                 . "<td>".$value['language_name']."</td>"
                 . "<td>".$value['proficiency']."</td>"
                 . "</tr>";
    }
    $lang_html = $lang_html."</table>";

?>

<?php
    $licenses = License:: model()->getEmployeeLicense($emp_id);
    $license_html = "<table class='table'>"
                  . "<tr>"
                  . "<th>License</th><th>License Number</th>"
                  . "</tr>";
    foreach($licenses as $key=>$value){
        $license_html = $license_html."<tr>"
                      . "<td>".$value['license_name']."</td>"
                      . "<td>".$value['license_code']."</td>"
                      . "</tr>";
    }
    $license_html = $license_html."</table>";
?>
<?php

    $model = new Education;
        $this->widget(
                'booster.widgets.TbWizard',
                array(
                        'type' => 'tabs', // 'tabs' or 'pills'
                        'tabs' => array(
                            array(
                                'label' => 'Education',
                                'content' => $educ_html,
                                ),

                            array(
                                'label' => 'Work Experience',
                                'content' => $exp_html,
                                ),

                            array(
                                'label' => 'Skills',
                                'content' => $skills_html,
                                ),

                            array(
                                'label' => 'Languages',
                                'content' => $lang_html,
                                ),

                            array(
                                'label' => 'Licenses',
                                'content' => $license_html,
                                ),
                            
                        ),
                )
        );
?>


 