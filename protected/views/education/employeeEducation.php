<div class="payroll-container">

<h3>Qualifications</h3>  
<p>User qualification details (Education, Past Work Experience, Skills, Languages, Licenses)</p>
<br />

<?php

    //$emp_id = Yii::app()->request->getQuery('id');
    $emp_id = $_GET['emp_id'];
    $path = Yii::app()->request->baseUrl;

    $education = EducationController::getEmployeeEducation($emp_id);    
?>
 <?php
        $educ_html = "<table class='table table-bordered'>";
        foreach($education as $level_id=>$list){
            $educ_html = $educ_html."<tr><th colspan='5'>".EducationLevel::model()->getEducationLevelName($level_id)."</th></tr>"
                       . "<tr>"
                       . "<th>School</th>"
                       . "<th>Date Started</th>"
                       . "<th>Date Ended</th>"
                       . "<th>GPA</th>"
                       . "<th>Edit</th>"
                       . "</tr>";
            foreach($list as $key=>$value){
                $educ_html = $educ_html."<tr>"
                           . "<td>".$value['school_name']."</td>"
                           . "<td>".date("F j, Y",strtotime($value['date_started']))."</td>"
                           . "<td>".date("F j, Y",strtotime($value['date_ended']))."</td>"
                           . "<td>".$value['GPA']."</td>"
                           . "<td>"
                           . "<a href='".$path."/index.php/education/update?emp_id=".$emp_id."&id=".$value['id']."'>"
                           . "<span class='glyphicon glyphicon-pencil'></span>"."</a></td>"
                           . "</tr>";
            }
        }
        $educ_html = $educ_html."</table></br>";
        $link = "<a href='".$path."/index.php/education/create?emp_id=".$emp_id."'>";
        $educ_html = $educ_html."$link<button class='btn btn-default'>Add Education</button></a>";     
?>

<?php

    $experiences = Experience::model()->getEmployeeExperience($emp_id);
    $exp_html = "<table class='table table-bordered'>"
              . "<tr>"
              . "<th>Company</th>"
              . "<th>Position</th>"
              . "<th>Date Started</th>"
              . "<th>Date Ended</th>"
              . "<th>Edit</th>"
              . "</tr>";

    foreach($experiences as $key=>$value){
        $exp_html = $exp_html."<tr>"
                  . "<td>".$value['company']."</td>"
                  . "<td>".$value['job_title']."</td>"
                  . "<td>".date("F j, Y",strtotime($value['date_started']))."</td>"
                  . "<td>".date("F j, Y",strtotime($value['date_ended']))."</td>"
                  . "<td>"
                  . "<a href='".$path."/index.php/experience/update?emp_id=".$emp_id."&id=".$value['id']."'>"
                           . "<span class='glyphicon glyphicon-pencil'></span>"."</a></td>"
                  . "</tr>";
    }
    $exp_html = $exp_html."</table></br>";
    $link = "<a href='".$path."/index.php/experience/create?emp_id=".$emp_id."'>";
    $exp_html = $exp_html."$link<button class='btn btn-default'>Add Work Experience</button></a>";   

?>

<?php
  
    $skills = Skills::model()->getEmployeeSkills($emp_id);
    $skills_html = "<table class='table table-bordered'>"
                 . "<tr>"
                 . "<th>Skills</th>"
                 . "<th>Proficiency</th>"
                 . "<th>Years of Experience</th>"
                 . "<th>Edit</th>"
                 . "</tr>";
  
    foreach($skills as $key=>$value){
      $skills_html = $skills_html."<tr>"
                   . "<td>".$value['skill_name']."</td>"
                   . "<td>".$value['proficiency']."</td>" 
                   . "<td>".$value['years']."</td>"
                   . "<td>"
                   . "<a href='".$path."/index.php/skills/update?emp_id=".$emp_id."&id=".$value['id']."'>"
                   . "<span class='glyphicon glyphicon-pencil'></span>"."</a></td>"
                   . "</tr>";
                   
    }

     $skills_html = $skills_html."</table></br>";
     $link = "<a href='".$path."/index.php/skills/create?emp_id=".$emp_id."'>";
     $skills_html = $skills_html."$link<button class='btn btn-default'>Add Skills</button></a>";

?>

<?php

    $languages = Language::model()->getEmployeeLanguages($emp_id);
    $lang_html = "<table class='table table-bordered'>"
               . "<tr>"
               . "<th>Language</th>"
               . "<th>Proficiency</th>"
               . "<th>Edit</th>"
               . "<tr>";
    foreach($languages as $key=>$value){
      $lang_html = $lang_html."<tr>"
                 . "<td>".$value['language_name']."</td>"
                 . "<td>".$value['proficiency']."</td>"
                 . "<td>"
                 . "<a href='".$path."/index.php/language/update?emp_id=".$emp_id."&id=".$value['id']."'>"
                 . "<span class='glyphicon glyphicon-pencil'></span>"."</a></td>"
                 . "</tr>";
    }
    $lang_html = $lang_html."</table></br>";
    $link = "<a href='".$path."/index.php/language/create?emp_id=".$emp_id."'>";
    $lang_html = $lang_html."$link<button class='btn btn-default'>Add Language</button></a>";

?>

<?php
    $licenses = License:: model()->getEmployeeLicense($emp_id);
    $license_html = "<table class='table table-bordered'>"
                  . "<tr>"
                  . "<th>License</th>"
                  . "<th>License Number</th>"
                  . "<th>Edit</th>"
                  . "</tr>";
    foreach($licenses as $key=>$value){
        $license_html = $license_html."<tr>"
                      . "<td>".$value['license_name']."</td>"
                      . "<td>".$value['license_code']."</td>"
                      . "<td>"
                      . "<a href='".$path."/index.php/license/update?emp_id=".$emp_id."&id=".$value['id']."'>"
                      . "<span class='glyphicon glyphicon-pencil'></span>"."</a></td>"
                      . "</tr>";
    }
    $license_html = $license_html."</table></br>";
    $link = "<a href='".$path."/index.php/license/create?emp_id=".$emp_id."'>";
    $license_html = $license_html."$link<button class='btn btn-default'>Add License</button></a>";
?>
<?php

    $model = new Education;
        $this->widget(
                'booster.widgets.TbWizard',
                array(
                        'type' => 'tabs', // 'tabs' or 'pills'
                        'tabs' => array(
                            array(
                                'label' => 'Education',
                                'content' => $educ_html,
                                ),

                            array(
                                'label' => 'Work Experience',
                                'content' => $exp_html,
                                ),

                            array(
                                'label' => 'Skills',
                                'content' => $skills_html,
                                ),

                            array(
                                'label' => 'Languages',
                                'content' => $lang_html,
                                ),

                            array(
                                'label' => 'Licenses',
                                'content' => $license_html,
                                ),
                            
                        ),
                )
        );
?>

</div>