<?php
	$form = $this->beginWidget('booster.widgets.TbActiveForm',
            							array('id' => 'horizontalForm',
                                	  		  'type' => 'horizontal',
                                              'htmlOptions' => array('class' => 'well'), // for inset effect
                                      		 )
                                      );
  
?>
	<fieldset>
		<Legend>Education</Legend>
	
	
	<?php

		$list = EducationController::getAllEducationLevel();
		echo $form->select2Group($model,'education_level_id',
										array('wrapperHtmlOptions' => array('class' => 'col-sm-5',),
											  'widgetOptions' => array(
											  						'name' => 'education_level_id',
											  						'data' => $list,
											  						'htmlOptions' => array(
											  									'id' => 'issue-574-checker-select'
																				)
																)
											)
									);
	?>
	<?php echo $form->textFieldGroup($model,'school_name'); ?>
	<?php echo $form->error($model,'school_name'); ?>

	<?php echo $form->datePickerGroup($model,'date_started',
							array(
								'widgetOptions' => array(
										'options' => array(
											'language' => 'en',
											 'format' => 'yyyy-mm-dd',
											),
										),
								'wrapperHtmlOptions' => array(
											'class' => 'col-sm-5',
											),

								'prepend' => '<i class="glyphicon glyphicon-calendar"></i>'
								)
						); 
		?>
					
		<?php echo $form->error($model,'date_started'); ?>

		<?php echo $form->datePickerGroup($model,'date_ended',
							array(
								'widgetOptions' => array(
										'options' => array(
											'language' => 'en',
											 'format' => 'yyyy-mm-dd',
											),
										),
								'wrapperHtmlOptions' => array(
											'class' => 'col-sm-5',
											),

								'prepend' => '<i class="glyphicon glyphicon-calendar"></i>'
								)
						); 
		?>
					
		<?php echo $form->textFieldGroup($model,'GPA'); ?>
	    <?php echo $form->error($model,'GPA'); ?>
	    <?php 
			$this->widget('booster.widgets.TbButton',
                           array('buttonType' => 'submit', 'label' => $model->isNewRecord ? 'Create' : 'Save' , 'context' => 'success'));

		?>
</fieldset>

<?php $this->endWidget(); ?>

