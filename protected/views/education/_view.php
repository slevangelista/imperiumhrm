<?php
/* @var $this EducationController */
/* @var $data Education */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('emp_id')); ?>:</b>
	<?php echo CHtml::encode($data->emp_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('school_name')); ?>:</b>
	<?php echo CHtml::encode($data->school_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('education_level_id')); ?>:</b>
	<?php echo CHtml::encode($data->education_level_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_started')); ?>:</b>
	<?php echo CHtml::encode($data->date_started); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_ended')); ?>:</b>
	<?php echo CHtml::encode($data->date_ended); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('GPA')); ?>:</b>
	<?php echo CHtml::encode($data->GPA); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('comment')); ?>:</b>
	<?php echo CHtml::encode($data->comment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_at')); ?>:</b>
	<?php echo CHtml::encode($data->created_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_at')); ?>:</b>
	<?php echo CHtml::encode($data->updated_at); ?>
	<br />

	*/ ?>

</div>