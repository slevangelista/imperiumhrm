<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<div style="width: 100%; display: table;">
    <div style="display: table-row">
        <div style="width: 12%; display: table-cell;">
			<?php
				$emp_id = $_GET['emp_id'];
				if(Yii::app()->user->checkAccess('Admin')){
					echo "<center>";
				    /*echo CHtml::image(Yii::app()->request->baseUrl.'/images/profile/admin.png','admin',
	                                                array("width" => '150px',"height" => '150px'));*/
					$dir = Yii::app()->request->baseUrl.'/images/profile/';
					$profile_pic_url = file_exists(YiiBase::getPathOfAlias('webroot').'/images/profile/emp_'.$emp_id.'.png') ? $dir.'emp_'.$emp_id.'.png' : $dir.'admin.png';
					echo "<img src='".$profile_pic_url."'/>";
		     ?>      
			<?php
					//$emp_id = Yii::app()->request->getQuery('id');
					
					$employee = Employee::model()->findByAttributes(array('emp_id'=>$emp_id));
                    $fullname = $employee['fullname'];
					echo "<br><br><b><font color='#045FB4'>".$fullname;
					echo "</center></b></font></br>";
	          
	            	
	            	$profile_url = array('/employee/adminEmployee?emp_id='.$emp_id);
	            	$address_url = array('/address/create?emp_id='.$emp_id);
	            	$contact_url = array('/contactNo/create?emp_id='.$emp_id);
	            	$government_url = array('/government/create?emp_id='.$emp_id);
	            	//$job_url = array('/employee/jobdetails?emp_id='.$emp_id);
	            	$job_url = array('/employee/job?emp_id='.$emp_id);
	            	$qualifications = array('/education/employeeEducation?emp_id='.$emp_id);

	            	$this->widget(
	        				'booster.widgets.TbMenu',
	                			array(
	                        		'type' => 'list',
	                        		'items' => array(
	                                       array('label' => 'Personal',
	                                             'url' => $profile_url,
	                                             'itemOptions' => array('class' => 'active')),
	                                                              array('label' => 'Address', 'url' => $address_url),
	                                                              array('label' => 'Government', 'url' => $government_url),
	                                                              array('label' => 'Contact', 'url' => $contact_url),
	                                                              array('label' => 'Job', 'url' => $job_url),
	                                                              array('label' => 'Qualifications', 'url' => $qualifications),
	                                                              array('label' => 'Personnel', 'url' => '#'),
	                                            )
	                     )
	         		);
	        	}
			?>
		</div>

      	<div style="display: table-cell;">
      		<?php echo $content; ?>
      	</div>
    </div>
</div>
<?php $this->endContent(); ?>
