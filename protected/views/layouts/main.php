<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="en">

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <!-- Load Custom Headers -->
    <?php $this->beginContent('//layouts/header'); ?>
    <?php $this->endContent(); ?>

</head>

<body>

    <div class="container" id="page">

        <div class="image-header">
            <div style="width:90%;float:left;"><a href="<?= Yii::app()->getBaseUrl(true) . "/index.php/home"; ?>" ><img src="<?= Yii::app()->getBaseUrl(true) . "/images/logo.jpeg"; ?>" /></a></div>

            <div><br /><br /><br />
            <?php
                if(!Yii::app()->user->isGuest)
                {
                    $user_id = Yii::app()->user->id;
                    $username = Yii::app()->user->name;
                    $emp_id = Users::model()->userEmpId($user_id,$username);
                    $profile_url = array('/employee/view/'.$emp_id);
                    $update_url = array('/users/update/'.$user_id);

                    echo "Welcome " .$username."<br />";
                    echo CHtml::link('Account Settings', $update_url);
                    echo "<br />";
                    echo CHtml::link('Logout',array('/site/logout'));
                }
            ?>
            </div>
        </div>

        <div class="navbar navbar-default navbar-top" role="navigation">
            <div class="navbar-collapse collapse">

            <?php 
                $profile_url = Yii::app()->user->isGuest ? array('') : $profile_url;
                $this->widget('booster.widgets.TbMenu',array(
                    'htmlOptions' => array( 'class' => 'nav navbar-nav' ),
                    'activeCssClass'    => 'active',
                    'items'=>array(
                        array('label'=>'Home', 'icon'=>'glyphicon glyphicon-home', 'url'=>array('/home')),
                        array('label'=>'Profile', 'icon'=>'glyphicon glyphicon-briefcase','url'=> $profile_url , 'visible'=>!Yii::app()->user->checkAccess('Admin')),
                        array('label'=>'[HR] Employee List', 'icon'=>'glyphicon glyphicon-user','url'=>array('/employee/admin'),'visible'=>Yii::app()->user->checkAccess('Human Resource')),
                        array(
                            'label' => 'Logs',
                            'icon' => 'glyphicon glyphicon-list-alt',
                            'items' => array(
                                array('label' => 'View my Logs', 'url' => array('/logs/user'), 'visible'=>Yii::app()->user->checkAccess('Employee')),
                                array('label' => '[HR] Manage Logs', 'url' => array('/logs'), 'visible'=>Yii::app()->user->checkAccess('Human Resource')),
                                array('label' => '[HR] Upload Logs', 'url' => array('/logs/upload'), 'visible'=>Yii::app()->user->checkAccess('Human Resource')),
                                array('label' => '[HR] Set Cutoff', 'url' => array('/cutoff'), 'visible'=>Yii::app()->user->checkAccess('Human Resource')),
                            ),
                            //'visible'=>Yii::app()->user->checkAccess('Admin'),
                        ),
                        array(
                            'label' => 'Schedule',
                            'icon' => 'glyphicon glyphicon-book',
                            'items' => array(
                                array('label' => 'View my Schedule', 'url' => array('/schedule/user'), 'visible'=>Yii::app()->user->checkAccess('Employee')),
                                array('label' => '[HR] Manage Schedule', 'url' => array('/schedule'), 'visible'=>Yii::app()->user->checkAccess('Human Resource')),
                                array('label' => '[HR] Set Holidays', 'url' => array('/holiday'), 'visible'=>Yii::app()->user->checkAccess('Human Resource')),
                                //array('label' => 'Report: Others', 'url' => array('')),
                            ),
                            //'visible'=>Yii::app()->user->checkAccess('Admin'),
                        ),
                        array(
                            'label' => 'Manhours',
                            'icon' => 'glyphicon glyphicon-time',
                            'items' => array(
                                array('label' => '[HR] Generate Report', 'url' => array('/manhours'),'visible'=>Yii::app()->user->checkAccess('Human Resource')),
                            ),
                            //'visible'=>Yii::app()->user->checkAccess('Admin'),
                        ),
                        array(
                            'label' => 'Leave',
                            'icon' => 'glyphicon glyphicon-file',
                            'items' => array(
                                array('label' => 'File Leave', 'url' => array('/leaveRequest/create'), 'visible'=>Yii::app()->user->checkAccess('Employee')),
                                array('label' => 'View Leave Status', 'url' => array('/leaveRequest/leave'), 'visible'=>Yii::app()->user->checkAccess('Employee')),
                                array('label' => 'Manage Leave Pending', 'url' => array('/leaveRequest/'), 'visible'=>Yii::app()->user->checkAccess('Employee')),
                                array('label' => '[HR] View Leave Reports', 'url' => array('/leaveRequest/report'), 'visible'=>Yii::app()->user->checkAccess('Human Resource')),
                                array('label' => '[HR] Set Leave Type', 'url' => array('/leaveType/'), 'visible'=>Yii::app()->user->checkAccess('Human Resource')),
                                array('label' => '[HR] Set Leave Credits', 'url' => array('/leaveCredits/'), 'visible'=>Yii::app()->user->checkAccess('Human Resource')),
                            ),
                            //'visible'=>Yii::app()->user->checkAccess('Admin'),
                        ),
                        array(
                            'label' => 'OT',
                            'icon' => 'glyphicon glyphicon-file',
                             'items' => array(
                                 array('label' => 'File OT', 'url' => array('ot/fileot'), 'visible'=>Yii::app()->user->checkAccess('Employee')),
                                 array('label' => 'View OT Status', 'url' => array('ot/status'), 'visible'=>Yii::app()->user->checkAccess('Employee')),
                                 array('label' => 'Manage OT Pending', 'url' => array('ot/otPending'), 'visible'=>Yii::app()->user->checkAccess('Employee')),
                                 array('label' => '[HR] View OT Reports', 'url' => array('ot/otreports'), 'visible'=>Yii::app()->user->checkAccess('Human Resource')),   
                            ), 
                        ),
                        array('label'=>'Users', 'icon'=>'glyphicon glyphicon-user', 'url'=>array('/users/admin'),'visible'=>Yii::app()->user->checkAccess('Admin')),
                        array('label'=>'Login', 'icon'=>'glyphicon glyphicon-pencil', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
                    ),
                )); 
            ?>
            </div><!--/.nav-collapse -->
        </div>

        <?php if(isset($this->breadcrumbs)):?>
            <?php $this->widget('booster.widgets.TbBreadcrumbs', array(
                'links'=>$this->breadcrumbs,
            )); ?><!-- breadcrumbs -->
        <?php endif?>

        <div class = "content">
          <?php echo $content; ?>
        </div>

        <div class="clear"></div>

        <div class="footer">
            Copyright &copy; <?php echo date('Y'); ?> by Imperium IT Outsourcing Inc.<br/>
            All Rights Reserved.<br/>
            <?php //echo Yii::powered(); ?>
        </div><!-- footer -->

    </div><!-- page -->

</body>
</html>
