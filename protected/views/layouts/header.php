<!-- Initialize your headers here -->

<!-- CSS -->

    <!-- Angular CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/bower_components/angular-ui-bootstrap/angular-ui-bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/bower_components/bootstrap-additions/dist/bootstrap-additions.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/bower_components/angular-dialogs/angular-dialogs.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/bower_components/angular-ui-select/dist/select.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/bower_components/angular-bootstrap-colorpicker/css/colorpicker.css">

    <!-- Dev. Team CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/imperium/jerome.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/imperium/noi.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/imperium/karen.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/imperium/syd.css">

<!-- Javascript -->

    <!-- Underscore -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/bower_components/underscore/underscore-min.js"></script>

    <!-- Angular Scripts -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/bower_components/angular/angular.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/bower_components/angular-dialogs/angular-dialogs.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/bower_components/angular-sanitize/angular-sanitize.min.js"></script>
    
    <!-- Angular Strap -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/bower_components/angular-strap/dist/angular-strap.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/bower_components/angular-strap/dist/angular-strap.tpl.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/bower_components/angular-strap/src/helpers/date-parser.js"></script>

    <!-- Angular-Ui-Booster -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/bower_components/angular-ui-bootstrap/angular-ui-bootstrap.js"></script>
    
    <!-- Angular - Others -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/bower_components/angular-ui-select/dist/select.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/bower_components/angular-bootstrap-colorpicker/js/bootstrap-colorpicker-module.js"></script>

<!-- Set onLoad Scripts -->
<script>
window.onload = function () {

}
</script>