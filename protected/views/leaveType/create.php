<?php
/* @var $this LeaveTypeController */
/* @var $model LeaveType */

// $this->breadcrumbs=array(
// 	'Leave Types'=>array('index'),
// 	'Create',
// );

// $this->menu=array(
// 	array('label'=>'List LeaveType', 'url'=>array('index')),
// 	array('label'=>'Manage LeaveType', 'url'=>array('admin')),
// );
?>
<div class='payroll-container'>
<h3>Create LeaveType</h3>

<?php $this->renderPartial('_form', compact('model')); ?>