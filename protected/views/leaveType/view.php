<?php
/* @var $this LeaveTypeController */
/* @var $model LeaveType */

// $this->breadcrumbs=array(
// 	'Leave Types'=>array('index'),
// 	$model->name,
// );

// $this->menu=array(
// 	array('label'=>'List LeaveType', 'url'=>array('index')),
// 	array('label'=>'Create LeaveType', 'url'=>array('create')),
// 	array('label'=>'Update LeaveType', 'url'=>array('update', 'id'=>$model->id)),
// 	array('label'=>'Delete LeaveType', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
// 	array('label'=>'Manage LeaveType', 'url'=>array('admin')),
// );
?>

<h1>View Leave Type</h1>

<?php $this->widget('booster.widgets.TbDetailView', array(
	'id' => 'region-details',
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'for_leave_form'
	),
)); ?>
<br><br>
<?php
$this->widget('zii.widgets.jui.CJuiButton', array(
	'buttonType'=>'link',
	'name'=>'btnGo',
	'caption'=>'View Leave Type List',
   	'url'=>array('leaveType/index'),
   	'htmlOptions'=>array(
   		'class'=>'btn btn-primary',
   	),
));
?>