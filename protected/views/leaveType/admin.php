<?php
/* @var $this LeaveTypeController */
/* @var $model LeaveType */

// $this->breadcrumbs=array(
// 	'Leave Types'=>array('index'),
// 	'Manage',
// );

// $this->menu=array(
// 	array('label'=>'List LeaveType', 'url'=>array('index')),
// 	array('label'=>'Create LeaveType', 'url'=>array('create')),
// );

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#leave-type-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class='payroll-container'>
<h3>Manage Leave Types</h3>



<?php $this->widget('booster.widgets.TbGridView', array(
	// 'id'=>'leave-type-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'name',
		array(
			'class'=>'booster.widgets.TbButtonColumn',
			'htmlOptions'=> array(
				'width'=>'5%'
			),
		),
	),
)); ?>
</div>