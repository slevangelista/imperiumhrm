<?php
/* @var $this LeaveTypeController */
/* @var $dataProvider CActiveDataProvider */

// $this->breadcrumbs=array(
// 	'Leave Types',
// );

// $this->menu=array(
// 	array('label'=>'Create LeaveType', 'url'=>array('create')),
// 	array('label'=>'Manage LeaveType', 'url'=>array('admin')),
// );
?>
<div class='payroll-container'>
<h3>Set Leave Type</h3>
<p>Add, edit or delete leave types here</p>
<table class='table table-bordered'>
	<tr>
		<th>
			ID
		</th>
		<th>
			Leave Type Name
		</th>
		<th>
			For Leave Form
		</th>

	</tr>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

</table>
<br />
<?php
    $this->widget('zii.widgets.jui.CJuiButton', array(
        'buttonType'=>'link',
        'name'=>'btnGo',
        'caption'=>'Add Leave Type',
        'url'=>array('leaveType/create'),
        'htmlOptions'=>array(
            'class'=>'btn btn-default',
        ),
    ));
    ?>

<?php
    $this->widget('zii.widgets.jui.CJuiButton', array(
        'buttonType'=>'link',
        'name'=>'btnGo',
        'caption'=>'Manage Leave Type',
        'url'=>array('leaveType/admin'),
        'htmlOptions'=>array(
            'class'=>'btn btn-default',
        ),
    ));
    ?>

</div>