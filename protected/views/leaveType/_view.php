<?php
/* @var $this LeaveTypeController */
/* @var $data LeaveType */
?>



<tr>
	<td>
		<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	</td>
	<td>
		<?php echo CHtml::encode($data->name); ?>
	</td>
	<td>
		<?php
			$a = array('1'=>'Yes', '0'=>'No'); 
			echo $a[$data->for_leave_form]; 
		?>
	</td>
<tr>