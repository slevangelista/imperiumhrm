<?php
/* @var $this LeaveTypeController */
/* @var $model LeaveType */
/* @var $form CActiveForm */
?>

<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'=>'leaveType-request-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	// 'enableAjaxValidation'=>true,
)); ?>

<table class='table table-bordered'>
	<tr>
 		<th>
 			Leave Type Name :
 		</th>
  		<td>
  			<?php echo $form->textFieldGroup(
				$model,
				'name',
				array(
					'wrapperHtmlOptions' => array(
						'class' => 'col-sm-5',
					),
					'label' => '',
				)
			); ?>
 		</td>
	</tr>
	<tr>
		<th>
			For Leave Form
		</th>
		<td>
			<?php echo $form->dropDownListGroup(
				$model,
				'for_leave_form',
				array(
					'wrapperHtmlOptions' => array(
						'class' => 'col-sm-5',
					),
					'label'=>'',
		   			'widgetOptions' => array(
	   					'data' => array('1'=>'Yes', '0'=>'No'),
						#'htmlOptions' => array('multiple' => true),
					)
				)
			); ?>
		</td>
	</tr>
	<tr>
        <td colspan=2>
        	<center>
        	<?php $this->widget(
    			'booster.widgets.TbButton',
    				array(
        				'label' => 'Create Leave Type',
        				'context' => 'primary',
        				'buttonType' => 'submit',
        				#'url' => array('leaveRequest'),
        				'icon' => 'ok'
    				)
			);
            ?>
        </center>
        </td>
	</tr>
</table>
</div>
<?php $this->endWidget(); ?>
