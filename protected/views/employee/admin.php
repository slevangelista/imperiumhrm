<?php
/* @var $this EmployeeController */
/* @var $model Employee */

// $this->breadcrumbs=array(
// 	'Employees'=>array('index'),
// 	'Manage',
// );

/*$this->menu=array(
	array('label'=>'List Employee', 'url'=>array('index')),
	array('label'=>'Create Employee', 'url'=>array('create')),
);*/

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#employee-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="payroll-container">
<h3>Employee List</h3>
<p>View list of employees here</p>

<!--<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>-->

<?php /*echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); */
Yii::app()->getBaseUrl();
?>
<!--</div><!-- search-form -->


<?php $this->widget('booster.widgets.TbGridView', array(
	'id'=>'employee-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	/*'htmlOptions'=>array('style'=>'cursor: pointer;'),
    'selectionChanged'=>"function(id){window.location='" . Yii::app()->urlManager->createUrl('employee/adminEmployee', array('id'=>'')) . "' + $.fn.yiiGridView.getSelection(id);}",
*/
	'columns'=>array(
		//'id',
		//'emp_id',
		 array(
        'class'=>'CLinkColumn',
        'labelExpression'=>'$data->emp_id',
        'urlExpression'=>'Yii::app()->getBaseUrl()."/index.php/employee/adminEmployee?emp_id=".$data->emp_id',

        'header'=>'Employee ID'
      ),
		'lastname',
		'firstname',
		//'birthdate',
		//'gender_id',
		//'nationality_id',
		//'marital_status_id',
		//'employment_date',
		// 'position',
		// 'sub_department_id',
		//'status_id',
		//'created_at',
		//'updated_at',
		
		/*array(
			'class'=>'CButtonColumn',
		),*/
	),
));

	/*$gridDataProvider = new CArrayDataProvider($model->search()->getData());

	$gridColumns = array(
						array(
							'name' => 'emp_id', 
							'header' => 'Employee ID',
							'value' => array('/employee/update'),
							),
                            
						array('name' => 'lastname', 'header' => 'Lastname'),
						array('name' => 'firstname', 'header' => 'Firstname'),
						array('name' => 'middle_initial', 'header' => 'Middlename'),
						array(
                    			'header' => 'Actions',
								'htmlOptions' => array('nowrap'=>'nowrap'),
								'class'=>'booster.widgets.TbButtonColumn',
								'viewButtonUrl'=>'Yii::app()->createUrl("employee/view",$params=array("id"=>$data["id"]))',
								'updateButtonUrl'=>'Yii::app()->createUrl("employee/update",$params=array("id"=>$data["id"]))',
								'deleteButtonUrl'=>'Yii::app()->createUrl("employee/delete",$params=array("id"=>$data["id"]))',
				),
					);

	$this->widget(
    		'booster.widgets.TbGridView',
    		array(
    			'dataProvider' => $gridDataProvider,
    			'template' => "{items}",
    			'columns' => $gridColumns,
                        'filter' => $model,
    		)
    );*/

    
 //    $this->widget(
	// 	'booster.widgets.TbButton',
 //        	array(
 //                	'buttonType' => 'link',
	// 		'label' => 'Create New Employee',
	// 		'context' => 'success',
 //                	'url' => array('employee/create'),
 //        	)
	// );
?>
<a href="<?php echo Yii::app()->getBaseUrl(true) . "/index.php/employee/create"; ?>"><button class="btn btn-default">Create New Employee</button></a>
</div>