<?php 
    $emp_id = $_GET['emp_id'];
    $form = $this->beginWidget('booster.widgets.TbActiveForm',
            	array('id' => 'horizontalForm',
                      'type' => 'horizontal',
                               'htmlOptions' => array(
	                               'class' => 'payroll-container'
	                            ), // for inset effect
                      )
            );           	  		  
  
?>
	<?php echo $form->errorSummary($model); ?>

        <fieldset>
		<h3>Personal Details</h3>

	
	    <p class="note">Fields with <span class="required">*</span> are required.</p>
		<?php echo $form->textFieldGroup($model,'emp_id'); ?>
		<?php echo $form->error($model,'emp_id'); ?>

		<?php echo $form->textFieldGroup($model,'lastname'); ?>
		<?php echo $form->error($model,'lastname'); ?>

		<?php echo $form->textFieldGroup($model,'firstname'); ?>
		<?php echo $form->error($model,'firstname');?>

		<?php echo $form->textFieldGroup($model,'middle_initial'); ?>
		<?php echo $form->error($model,'middle_initial'); ?>

		<?php //echo $form->textFieldGroup($model,'birthdate'); ?>
		<?php echo $form->datePickerGroup($model,'birthdate',
							array(
								'widgetOptions' => array(
										'options' => array(
											'language' => 'en',
											 'format' => 'yyyy-mm-dd',
											),
										),
								'wrapperHtmlOptions' => array(
											'class' => 'col-sm-5',
											),

								'prepend' => '<i class="glyphicon glyphicon-calendar"></i>'
								)
						); 
		?>
					
		<?php echo $form->error($model,'birthdate'); ?>
		<?php 
			$list =EmployeeController::getGenderList();
			echo $form->radioButtonListGroup($model,'gender_id',
														array(
																'widgetOptions' => array(
																'data' => $list
															     )
												             )
											);			
		?>

		<?php echo $form->error($model,'gender_id'); ?>
	
		<?php

			$list =EmployeeController::getNationality();
			echo $form->select2Group($model,'nationality_id',
										array('wrapperHtmlOptions' => array('class' => 'col-sm-5',),
											  'widgetOptions' => array(
											  						'name' => 'nationality_id',
											  						'data' => $list,
											  						'htmlOptions' => array(
											  									'id' => 'issue-574-checker-select'
																				)
																)
											)
									);
		?>
		<?php echo $form->error($model,'nationality_id'); ?>
	
		<?php

			$list =EmployeeController::getMaritalStatus();

			echo $form->dropDownListGroup($model,'marital_status_id',
													array('wrapperHtmlOptions' => array('class' => 'col-sm-5',),
														  'widgetOptions' => array('data' => $list,
																				   'htmlOptions' => array(),
																				  )
														)
											); 
		?>
		<?php echo $form->error($model,'marital_status_id'); ?>
		<?php echo $form->datePickerGroup($model,'employment_date',
							array(
								'widgetOptions' => array(
										'options' => array(
											'language' => 'en',
											 'format' => 'yyyy-mm-dd',
											),
										),
								'wrapperHtmlOptions' => array(
											'class' => 'col-sm-5',
											),

								'prepend' => '<i class="glyphicon glyphicon-calendar"></i>'
								)
						); 
		?>
					
		<?php echo $form->error($model,'employment_date'); ?>
		<?php $this->endWidget(); ?>
		 <form enctype="multipart/form-data" action="uploadpic?emp_id=<?php echo $emp_id;?>" method="POST"> 
            <!-- MAX_FILE_SIZE must precede the file input field -->
              <input type="hidden" name="MAX_FILE_SIZE"/>
              <!-- Name of input element determines name in $_FILES array -->
			 <table width="100%">
			 	<tr>
			 	    <td width="15%"></td>
			 		<td width="20%"><input type="file" name="userfile" class="btn btn-default"/></td>
			 		<td width="65%" style="padding: 0px 0px 0px 14px;"><input type="submit" class="btn btn-success" value="Upload pic"/></td>
			 	</tr>
			 </table>
          </form>
		<?php 
			$this->widget('booster.widgets.TbButton',
                array('buttonType' => 'submit', 'label' => $model->isNewRecord ? 'Create' : 'Save', 'context' => 'default'));
		?>

	</fieldset>
	

