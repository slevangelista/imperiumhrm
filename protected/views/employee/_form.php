    <?php 
            $form = $this->beginWidget('booster.widgets.TbActiveForm',
                          array('id' => 'horizontalForm',
                                          'type' => 'horizontal',
                                              'htmlOptions' => array('class' => 'well'), // for inset effect
                                           )
                                      );
  
    ?>


  <?php echo $form->errorSummary($empModel); ?>
  <?php echo $form->errorSummary($userModel); ?>

  <fieldset>
    <Legend>Personal Details</Legend>

  
      <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->textFieldGroup($empModel,'emp_id'); ?>
    <?php echo $form->error($empModel,'emp_id'); ?>

    <?php echo $form->textFieldGroup($empModel,'lastname'); ?>
    <?php echo $form->error($empModel,'lastname'); ?>

    <?php echo $form->textFieldGroup($empModel,'firstname'); ?>
    <?php echo $form->error($empModel,'firstname');?>

    <?php echo $form->textFieldGroup($empModel,'middle_initial'); ?>
    <?php echo $form->error($empModel,'middle_initial'); ?>

    <?php //echo $form->textFieldGroup($model,'birthdate'); ?>
    <?php echo $form->datePickerGroup($empModel,'birthdate',
              array(
                'widgetOptions' => array(
                    'options' => array(
                      'language' => 'en',
                       'format' => 'yyyy-mm-dd',
                      ),
                    ),
                'wrapperHtmlOptions' => array(
                      'class' => 'col-sm-5',
                      ),

                'prepend' => '<i class="glyphicon glyphicon-calendar"></i>'
                )
            ); 
    ?>
          
    <?php echo $form->error($empModel,'birthdate'); ?>
    
    <?php 
      $list =EmployeeController::getGenderList();
      echo $form->radioButtonListGroup($empModel,'gender_id',
                            array(
                                'widgetOptions' => array(
                                'data' => $list
                                   )
                                     )
                      );      
    ?>

    <?php echo $form->error($empModel,'gender_id'); ?>
  
    <?php

      $list =EmployeeController::getNationality();
      echo $form->select2Group($empModel,'nationality_id',
                    array('wrapperHtmlOptions' => array('class' => 'col-sm-5',),
                        'widgetOptions' => array(
                                    'name' => 'nationality_id',
                                    'data' => $list,
                                    'val' => 62,
                                    'htmlOptions' => array(
                                          'id' => 'issue-574-checker-select'
                                        )
                                )
                      )
                  );
    ?>
    <?php echo $form->error($empModel,'nationality_id'); ?>
  
    <?php

      $list =EmployeeController::getMaritalStatus();

      echo $form->dropDownListGroup($empModel,'marital_status_id',
                          array('wrapperHtmlOptions' => array('class' => 'col-sm-5',),
                              'widgetOptions' => array('data' => $list,
                                           'htmlOptions' => array(),
                                          )
                            )
                      ); 
    ?>
    <?php echo $form->error($empModel,'marital_status_id'); ?>
    <?php echo $form->datePickerGroup($empModel,'employment_date',
              array(
                'widgetOptions' => array(
                    'options' => array(
                      'language' => 'en',
                       'format' => 'yyyy-mm-dd',
                      ),
                    ),
                'wrapperHtmlOptions' => array(
                      'class' => 'col-sm-5',
                      ),

                'prepend' => '<i class="glyphicon glyphicon-calendar"></i>'
                )
            ); 
    ?>
          
    <?php echo $form->error($empModel,'employment_date'); ?>
</fieldset>

<fieldset>
  <Legend>User Details</Legend>

    <?php echo $form->textFieldGroup($userModel,'username'); ?>
    <?php //echo $form->error($model,'username'); ?>
    <?php echo $form->passwordFieldGroup($userModel,'password',array('size'=>60,'maxlength'=>50)); ?>
    <?php //echo $form->error($model,'password'); ?>
    <?php echo $form->passwordFieldGroup($userModel,'confirm_password',array('size'=>60,'maxlength'=>50)); ?>
    <?php //echo $form->error($model,'confirm_password'); ?>
    <?php echo $form->textFieldGroup($userModel,'email',array('size'=>60,'maxlength'=>128)); ?>
    <?php //echo $form->error($model,'email'); ?>
    <?php 
         echo $form->dropDownListGroup($userModel,'superuser',
            array(
              'wrapperHtmlOptions' => array('class' => 'col-sm-5',),
              'widgetOptions' => array(
                    'data' => Users::isSuperuser(),
                    'htmlOptions' => array(),
                    )
              )
          ); 

         echo $form->dropDownListGroup($userModel,'status',
            array(
              'wrapperHtmlOptions' => array('class' => 'col-sm-5',),
              'widgetOptions' => array(
                    'data' => Users::isActivated(),
                    'htmlOptions' => array(),
                    )
              )
          );

?>
<?php

      $list =EmployeeController::getRoles();
      echo $form->select2Group($roleModel,'itemname',
                    array('wrapperHtmlOptions' => array('class' => 'col-sm-5',),
                        'widgetOptions' => array(
                                    'name' => 'nationality_id',
                                    'data' => $list,
                                    'htmlOptions' => array(
                                          'id' => 'issue-574-checker-select'
                                        )
                                )
                      )
                  );
    ?>
</fieldset>


    <?php 
      $this->widget('booster.widgets.TbButton',
                           array('buttonType' => 'submit', 'label' => $empModel->isNewRecord ? 'Create' : 'Save', 'context' => 'success'));

    ?>
     
    

  
  

<?php $this->endWidget(); ?>
