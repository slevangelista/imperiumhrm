<script>
    var app_url = "<?php echo Yii::app()->request->baseUrl; ?>"; // Get Base URL using Yii
</script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/angularApp/employeeJobApp.js"></script>
<?php
	$emp_id = $_GET['emp_id'];
?>

<div class="payroll-container" ng-app="employeeJobApp" ng-controller="employeeJobController" ng-init="getAllData();">	

	<h3>Job</h3>
	<p>User job details</p>
	<br />

    <b>Department</b></br>
	<select class="form-control" ng-model="currentDepartment">
		<option value="select">--Select department--</option>
		<option ng-repeat="dep in department" value="{{dep.id}}">{{dep.name}}</option>
	</select></br>

	<b>Subdepartment</b></br>
	<select class="form-control" ng-model="currentSubDepartment">
		<option disabled>--Select subdepartment--</option>
		<option ng-repeat="(key, subdep) in subDepartment[currentDepartment]" value="{{key}}">{{subdep.name}}</option>
	</select></br>

	<b>Postion</b></br>
	<select class="form-control" ng-model="currentPositions">
		<option disabled>--Select position--</option>
		<option ng-repeat="pos in positions" id="pos.position_id" value="{{pos.position_id}}">{{pos.name}}</option>
	</select></br>

	<button ng-click="save(currentSubDepartment,currentPositions,<?php echo $emp_id;?>)" class="btn btn-default">Save</button>


</div>