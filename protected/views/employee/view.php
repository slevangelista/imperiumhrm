<?php
    $form = $this->beginWidget('booster.widgets.TbActiveForm',
                array('id' => 'horizontalForm',
                      'type' => 'horizontal',
                               'htmlOptions' => array('class' => 'well'), // for inset effect
                      )
            ); 
?>
<?php echo $form->errorSummary($model); ?>
<fieldset>
        <Legend>Personal Details</Legend>
        <?php echo $form->textFieldGroup($model,'emp_id'); ?>
        <?php echo $form->error($model,'emp_id'); ?>
        <?php echo $form->textFieldGroup($model,'lastname'); ?>
        <?php echo $form->error($model,'lastname'); ?>

        <?php echo $form->textFieldGroup($model,'firstname'); ?>
        <?php echo $form->error($model,'firstname');?>

        <?php echo $form->textFieldGroup($model,'middle_initial'); ?>
        <?php echo $form->error($model,'middle_initial'); ?>

        
        <?php
            $model->birthdate = date("F j,Y",strtotime($model->birthdate)); 
            echo $form->textFieldGroup($model,'birthdate');
        ?>
                    
        <?php echo $form->error($model,'birthdate'); ?>
        <?php 
            $list =EmployeeController::getGenderList();
            $model->gender_id = $list[$model->gender_id];
            echo $form->textFieldGroup($model,'gender_id');      
        ?>

        <?php echo $form->error($model,'gender_id'); ?>
    
        <?php

            $list =EmployeeController::getNationality();
            $model->nationality_id = $list[$model->nationality_id];
            echo $form->textFieldGroup($model,'nationality_id');
           
        ?>
        <?php echo $form->error($model,'nationality_id'); ?>
    
        <?php

            $list =EmployeeController::getMaritalStatus();
            $model->marital_status_id = $list[$model->marital_status_id];
            echo $form->textFieldGroup($model,'marital_status_id');

        ?>
        <?php echo $form->error($model,'marital_status_id'); ?>
        <?php
            $model->employment_date = date("F j,Y",strtotime($model->employment_date)); 
            echo $form->textFieldGroup($model,'employment_date');
        ?>
</fieldset>

<?php $this->endWidget(); ?>

