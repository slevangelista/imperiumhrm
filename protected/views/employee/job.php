<?php
/* @var $this EmployeeController */
/* @var $model Employee */
/* @var $form CActiveForm */

	$user_id = Yii::app()->user->id;
	$username = Yii::app()->user->name;
	$emp_id = Users::model()->userEmpId($user_id,$username);
	$job = EmployeeController::getJobDetails($emp_id);
?>

	<h1>Job Details</h1>
	</br>
	<table class='table'>
		<tr>
			<th>Employment Date</th>
			<td><?php echo date("F j, Y",strtotime($job['employment_date']));?></td>
		</tr>
		<tr>
			<th>Department</th>
			<td><?php echo $job['department_name'];?></td>
		</tr>
		<tr>
			<th>Sub-department</th>
			<td><?php echo $job['sub_departmentname'];?></td>
		</tr>
		<tr>
			<th>Position</th>
			<td><?php echo $job['position'];?></td>
		</tr>
<?php
		$employment_date = date_create(date("Y-m-d",strtotime($job['employment_date'])));
		$current_date = date_create(date("Y-m-d"));
		$diff=date_diff($employment_date,$current_date);
		$count = explode(" ",$diff->format("%y %m %d"));
		$years = $count[0] == 0 ? "" : $count[0]." Year/s ";
		$months = $count[1] == 0 ? "" : $count[1]." Month/s ";
		$days = $count[2] == 0 ? "" : $count[2]." Day/s";
?>
		<tr>
			<th>Years of Employment</th>
			<td><?php echo $years.$months.$days;?></td>
		</tr>
	</table>
	<?php
		if(Yii::app()->user->checkAccess('Admin')){
		$emp_id = $_GET['emp_id'];
		$update_url = Yii::app()->getBaseUrl(true)."/index.php/employee/jobdetails?emp_id=".$emp_id;
	?>
	<br>
	<a href='<?php echo $update_url;?>'><button class="btn btn-default">Update Job Details</button></a>
	<?php }?>


