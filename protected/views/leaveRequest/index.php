<!-- Load Route-Specific JS -->
<script>
    var app_url = "<?php echo Yii::app()->request->baseUrl; ?>"; // Get Base URL using Yii
</script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/angularApp/leaveRequestApp.js"></script> 

<div class='payroll-container' ng-app="leaveRequestApp" ng-controller="leaveRequestController" ng-init="getPendingLeaveRequest()">
	<h3>Manage Leave Request</h3>
	<p>View and manage pending leave request here</p>
	<br />

	<div class="payroll-header">
	    <span class="glyphicon glyphicon-calendar"></span> Pending Requests
	</div>
	<table class='table table-bordered table-striped table-hover'>
		<tr>
			<th>Employee ID</th>
			<th>Name</th>
			<th>Date Filed</th>
			<th>Start Date</th>
			<th>End Date</th>
			<th>Leave Type</th>
			<th>Leave Status</th>
			<th>Approve</th>
		</tr>
		<tr ng-repeat="(key, value) in leaveRequest">
			<td>{{value.emp_id}}</td>
			<td>{{value.emp_name}}</td>
			<td>{{value.date_filed}}</td>
			<td>{{value.start_date}}</td>
			<td>{{value.end_date}}</td>
			<td>{{value.leave_type}}</td>
			<td>{{value.request_type}} Approval from {{value.p_id}}</td>
			<td>
				<button class='btn btn-success btn-xs' ng-click='modalApprove(key, value.hid, value.emp_id, value.start_date, value.end_date, value.leave_type_id)'><span class='glyphicon glyphicon-ok'></span></button>
				<button class='btn btn-danger btn-xs' ng-click='modalDisapprove(key)'><span class='glyphicon glyphicon-remove'></span></button>
			</td>
		</tr>
	</table>
</div>