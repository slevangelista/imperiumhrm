<?php
/* @var $this LeaveRequestController */
/* @var $dataProvider CActiveDataProvider */

// $this->breadcrumbs=array(
// 	'Leave Requests',
// );

// $this->menu=array(
// 	array('label'=>'Create LeaveRequest', 'url'=>array('create')),
// 	array('label'=>'Manage LeaveRequest', 'url'=>array('admin')),
// );
?>
<div id='leave-body'>
<h2>Dispproved Leave Requests</h2>

<table align='center' class='table'>
	<tr>
		<th>Employee ID</th>
		<th>Name</th>
		<th>Date Filed</th>
		<th>Start Date</th>
		<th>End Date</th>
		<th>Leave Type</th>
		<th>Leave Status</th>
	</tr>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_disapproveview',
)); ?>

</table>

</div>