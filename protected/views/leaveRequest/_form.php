
<?php
/* @var $this LeaveRequestController */
/* @var $model LeaveRequest */
/* @var $form CActiveForm */
?>

<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'=>'leave-request-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>
	
<?php echo $form->hiddenField($model,'emp_id',array('value'=>"$userlogin[emp_id]")); ?>
<?php echo $form->hiddenField($model,'request_type_id',array('value'=>"3")); ?>

<?php 
	$posId = $userlogin['p'];
	$employeePosition = $model->getUserPositionName($posId);
	$hid = $model->getHierarchyId($posId);
	// echo $hid;
	echo $form->hiddenField($model,'position_id',array('value'=>"$hid")); 
?>

<table class='table table-bordered'>
	<tr>
		<td>	
		    <b>Name : </b>
	    </td>
 		<td>
 			<?php echo "$userlogin[firstname] $userlogin[lastname]" ;?> 
 		</td>	    
	</tr>
	<tr>
		<td>
            <b>Date Of Filing : </b>
        </td>
 		<td>
 			<!-- <?php //echo date('F d, Y'); ?> -->
 			<?php echo $form->datePickerGroup(
			$model,
			'date_filed',
			array(
				'widgetOptions' => array(
					'options' => array(
						'language' => 'en',
						'format' => 'yyyy-mm-dd',
                        'autoclose' => true,
					),
					'htmlOptions'=>array(
						'value'=>date('Y-m-d'),
						// 'disabled'=>true,
					),
				),
				'label'=>'',
				'wrapperHtmlOptions' => array(
					'class' => 'col-sm-5',
				),
				'hint' => '',
				'prepend' => '<i class="glyphicon glyphicon-calendar"></i>'
			)
		    ); ?>
 		</td>
	</tr>
	<tr>
 		<td>
 			<b>Position Designation : </b>
 		</td>
  		<td>
  			<?php echo $employeePosition; ?>
 		</td>
	</tr>
	<tr>
 		<td colspan=2>
 			<?php
			echo $form->select2Group($model, 'leave_type_id', array(
				'wrapperHtmlOptions' => array(
					'class' => 'col-sm-5',
				),
				'widgetOptions' => array(
					'name' => 'leave_type_id',
					'data' => $leaveList,
					'htmlOptions' => array(
						'id' => 'issue-574-checker-select'
					)
				)
				));
			?>
 		</td>
	</tr>	
	<tr>
 		<td colspan=2>
 			<b>INCLUSIVE DATES</b>
 		</td>
 	</tr>
	<tr>
  		<td>
  			<?php echo $form->datePickerGroup(
			$model,
			'start_date',
			array(
				'widgetOptions' => array(
					'options' => array(
						'language' => 'en',
						'format' => 'yyyy-mm-dd',
                        'autoclose' => true,
					),
				),
				'wrapperHtmlOptions' => array(
					'class' => 'col-sm-5',
				),
				'hint' => '',
				'prepend' => '<i class="glyphicon glyphicon-calendar"></i>'
			)
		    ); ?>
 		</td>
  		<td>
  			<?php echo $form->datePickerGroup(
			$model,
			'end_date',
			array(
				'widgetOptions' => array(
					'options' => array(
						'language' => 'en',
						'format' => 'yyyy-mm-dd',
						'required' => true,
                        'autoclose' => true,
					),
				),
				'wrapperHtmlOptions' => array(
					'class' => 'col-sm-5',
				),
				'hint' => '',
				'prepend' => '<i class="glyphicon glyphicon-calendar"></i>'
			)
		    ); ?>
 		</td>
	</tr>
	<tr>
 		<td>
 			<b>Leave Credits : </b>
 		</td>
  		<td>
  			<?php echo $userlogin[vl_count]; ?>
 		</td>
	</tr>
	<tr>
    	<td colspan=2>
        	<center>
        	<?php $this->widget(
    			'booster.widgets.TbButton',
    				array(
        				'label' => 'Submit Leave',
        				'context' => 'primary',
        				'buttonType' => 'submit',
        				#'url' => array('leaveRequest'),
        				'icon' => '',
    				)
			);
            ?>
        </center>
        </td>
	</tr>

</table>
</div>
<?php $this->endWidget(); ?>