<?php
/* @var $this LeaveRequestController */
/* @var $model LeaveRequest */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'emp_id'); ?>
		<?php echo $form->textField($model,'emp_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date_filed'); ?>
		<?php echo $form->textField($model,'date_filed'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'start_date'); ?>
		<?php echo $form->textField($model,'start_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'end_date'); ?>
		<?php echo $form->textField($model,'end_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'reason'); ?>
		<?php echo $form->textField($model,'reason',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date_submitted'); ?>
		<?php echo $form->textField($model,'date_submitted'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'leave_type_id'); ?>
		<?php echo $form->textField($model,'leave_type_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'if_leave_type_others'); ?>
		<?php echo $form->textField($model,'if_leave_type_others',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'days_with_pay'); ?>
		<?php echo $form->textField($model,'days_with_pay'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'request_type_id'); ?>
		<?php echo $form->textField($model,'request_type_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'request_disapproval'); ?>
		<?php echo $form->textField($model,'request_disapproval',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'position_id'); ?>
		<?php echo $form->textField($model,'position_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->