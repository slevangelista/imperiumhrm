<?php
/* @var $this LeaveRequestController */
/* @var $dataProvider CActiveDataProvider */

// $this->breadcrumbs=array(
// 	'Leave Requests',
// );

// $this->menu=array(
// 	array('label'=>'Create LeaveRequest', 'url'=>array('create')),
// 	array('label'=>'Manage LeaveRequest', 'url'=>array('admin')),
// );
?>
<div class='payroll-container'>
<h3>Submitted Leave Requests</h3>
<p>View submitted leave request here</p>

<?php 
	$this->widget('booster.widgets.TbGridView', array(
        'type' => 'striped bordered condensed',
        'dataProvider' => $model->search(),
        'template' => "{items}\n{pager}",
        'filter' => $model,
        'afterAjaxUpdate' => "function() {
            $('#typeID').select2({'allowClear':true});
            $('#reqID').select2({'allowClear':true});
            $('#sdID').datepicker({'showButtonPanel':true, 'format':'yyyy-mm-dd', 'autoclose':true});
            $('#edID').datepicker({'showButtonPanel':true, 'format':'yyyy-mm-dd', 'autoclose':true});
            $('#dfID').datepicker({'showButtonPanel':true, 'format':'yyyy-mm-dd', 'autoclose':true});
        }",
        'columns' => array(
			'emp_id',
			array(
           		'name'=>'leave_type_id',
            	'value'=>'LeaveType::model()->findByPk($data->leave_type_id)->name',
            	'filter'=>$this->widget(
                    'booster.widgets.TbSelect2',
                    array(
                        'model'=>$model,
                        'attribute' => 'leave_type_id',
                        'htmlOptions'=>array(
                            'id' => 'typeID'
                        ),
                        'data' => $leave_type_array,
                        'options' => array(
                            'placeholder' => 'Please select',
                            'allowClear' => true,
                        )
                    ),true
                )
            ),
			array(
				'name'=>'start_date',
                'value'=>'date("F d, Y", strtotime($data->start_date))',
				'filter'=>$this->widget(
    				'booster.widgets.TbDatePicker',
    				array(
    					'model'=>$model,
    					'attribute'=>'start_date',
        				'options' => array(
            				'language' => 'en',
            				'format' => 'yyyy-mm-dd',
                            'autoclose' => true,
       					),
                        'htmlOptions'=>array(
                            'id'=>'sdID'
                        ),
    				),true
				)

			),
			array(
				'name'=>'end_date',
                'value'=>'date("F d, Y", strtotime($data->end_date))',
				'filter'=>$this->widget(
    				'booster.widgets.TbDatePicker',
    				array(
    					'model'=>$model,
    					'attribute'=>'end_date',
        				'options' => array(
            				'language' => 'en',
            				'format' => 'yyyy-mm-dd',
                            'autoclose' => true,
       					),
                        'htmlOptions'=>array(
                            'id'=>'edID'
                        ),
    				),true
				)

			),
			array(
           		'name'=>'request_type_id',
            	'value'=>'RequestType::model()->FindByPk($data->request_type_id)->name',
            	'filter'=>$this->widget(
                    'booster.widgets.TbSelect2',
                    array(
                        'model'=>$model,
                        'attribute' => 'request_type_id',
                        'htmlOptions'=>array(
                            'id' => 'reqID'
                        ),
                        'data' => $request_type_array,
                        'options' => array(
                            'placeholder' => 'Please select',
                            'allowClear' => true,
                            // 'width' => '40%',
                        )
                    ),true
                )
            ),
            'request_disapproval',
			// 'date_filed',
			array(
				'name'=>'date_filed',
                'value'=>'date("F d, Y", strtotime($data->date_filed))',
				'filter'=>$this->widget(
    				'booster.widgets.TbDatePicker',
    				array(
    					'model'=>$model,
    					'attribute'=>'date_filed',
        				'options' => array(
            				'language' => 'en',
            				'format' => 'yyyy-mm-dd',
                            'autoclose' => true,
       					),
                        'htmlOptions'=>array(
                            'id'=>'dfID'
                        ),
    				),true
				)

			),
    	), //colums closing
    )); 

?>

</div>