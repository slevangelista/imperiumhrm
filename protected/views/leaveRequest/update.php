<?php
/* @var $this LeaveRequestController */
/* @var $model LeaveRequest */

$this->breadcrumbs=array(
	'Leave Requests'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List LeaveRequest', 'url'=>array('index')),
	array('label'=>'Create LeaveRequest', 'url'=>array('create')),
	array('label'=>'View LeaveRequest', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage LeaveRequest', 'url'=>array('admin')),
);
?>

<h1>Update LeaveRequest <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>