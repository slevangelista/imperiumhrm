<?php
/* @var $this LeaveRequestController */
/* @var $model LeaveRequest */

// $this->breadcrumbs=array(
// 	'Leave Requests'=>array('index'),
// 	$model->id,
// );

// $this->menu=array(
// 	array('label'=>'List LeaveRequest', 'url'=>array('index')),
// 	array('label'=>'Create LeaveRequest', 'url'=>array('create')),
// 	array('label'=>'Update LeaveRequest', 'url'=>array('update', 'id'=>$model->id)),
// 	array('label'=>'Delete LeaveRequest', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
// 	array('label'=>'Manage LeaveRequest', 'url'=>array('admin')),
// );
?>
<div id='leave-body'>
<h2>Leave Overview </h2>

<table class='table'>
	<tr>
		<td>
			Employee ID
		</td>
		<td>
			<?php echo $model->emp_id;?>
		</td>
	</tr>
	<tr>
		<td>
			Your Name
		</td>
		<td>	
			<?php echo (isset($model->emp->firstname) ? $model->emp->firstname. ' ' .$model->emp->lastname : ''); ?>
		</td>
	</tr>
	<tr>
		<td>
			Leave Start Date
		</td>
		<td>
			<?php 
				$sd = $model->start_date;
				echo date('F d, Y',strtotime($sd));
			?>
		</td>
	</tr>
	<tr>
		<td>
			Leave End Date
		</td>
		<td>
			<?php 
				$ed = $model->end_date;
				echo date('F d, Y',strtotime($ed));
			?>
		</td>
	</tr>
	<tr>
		<td>
			Leave Type
		</td>
		<td>
			<?php echo $model->leaveType->name;?>
		</td>
	</tr>
</table>
</div>