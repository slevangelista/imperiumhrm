<?php
/* @var $this LeaveRequestController */
/* @var $model LeaveRequest */
/*
$this->breadcrumbs=array(
	'Leave Requests'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List LeaveRequest', 'url'=>array('index')),
	array('label'=>'Manage LeaveRequest', 'url'=>array('admin')),
);
*/
?>
<div class='payroll-container'>
<h3>Leave Application Form</h3>
<p>File your leave application here</p>
<br />

<?php $this->renderPartial('_form', compact('model', 'user', 'leaveList', 'employeeID', 'userlogin')); ?>