<?php $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
	'id'=>'leave-request-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>true,
)); ?>
<div class='payroll-container'>

<h3>Leave Request Report</h3>
<p>View list of leave request here</p>
<p></p>
<br />
<center>
<table class="table">
	<tr>
		<td>
			From: 
		</td>
		<td>
			<?php
			$this->widget(
			    'booster.widgets.TbDatePicker',
			    array(
			        'name' => 'date1',
			        // 'id' => 'date1',
			        'htmlOptions' => array(
			        	'class'=>'col-md-4', 
			        	'style'=>'width: 25px;'
			        ),
			        'options'=>array(
			        	'format'=>'yyyy-mm-dd',
				        'width' => '40%',
			        ),
			    )
			);
			?>	
		</td>
		<td>
			To: 
		</td>
		<td>
			<?php
			$this->widget(
			    'booster.widgets.TbDatePicker',
			    array(
			        'name' => 'date2',
			        // 'id' => 'date2',
			        'htmlOptions' => array(
			        	'class'=>'col-md-4', 
			        	'style'=>'width: 25px;',
				        // 'width' => '10%',
			        ),
			        'options'=>array(
			        	'format'=>'yyyy-mm-dd',
			        ),
			    )
			);
			?>
		</td>
	</tr>
	<tr>
		<td>
			Type of Leave : 
		</td>
		<td>
			<?php 
				$this->widget(
				    'booster.widgets.TbSelect2',
				    array(
				        'name' => 'leaveTypeId',
				        'data' => $leave_type_array,
				        'options' => array(
				            'placeholder' => 'Select Type',
				            // 'width' => '40%',
				        )
				    )
				);
			?>
		</td>
		<td>
			Leave Status : 
		</td>
		<td>
			<?php 
				$this->widget(
				    'booster.widgets.TbSelect2',
				    array(
				        'name' => 'requestTypeId',
				        'data' => $request_type_array,
				        'options' => array(
				            'placeholder' => 'Select Status',
				            // 'width' => '40%',
				        )
				    )
				);
			?>
		</td>
	</tr>
	<tr>
		<td>
			Employee : 
		</td>
		<td>
			<?php 
				$this->widget(
				    'booster.widgets.TbSelect2',
				    array(
				        'name' => 'empID',
				        'data' => $emp_name_array,
				        'options' => array(
				            'placeholder' => 'Select Employee',
				            // 'width' => '70%',
				        )
				    )
				);
			?>
		</td>
		<td>
			Department : 
		</td>
		<td>
			<?php 
				$this->widget(
				    'booster.widgets.TbSelect2',
				    array(
				        'name' => 'depID',
				        'data' => $department_array,
				        'options' => array(
				            'placeholder' => 'Select Department',
				            // 'width' => '70%',
				        )
				    )
				);
			?>
		</td>
	</tr>
</table>
<br>
<button type="submit" name="submit" class="btn btn-primary">Process Report</button>   
</center>
<?php $this->endWidget(); ?>

<?php
if (isset($result)){ 
	foreach ($result as $r) {
		$emp_id       = $r['emp_id'];
		$sd           = $r['start_date'];
		$ed           = $r['end_date'];
		$leave_type   = $r['leave_type_id'];
		$reason       = $r['request_disapproval'];
		$date_filed   = $r['date_filed'];
		$status       = $r['request_type_id'];
		$hid          = $r['position_id'];
		$tr .= "<tr><td>$emp_name_array[$emp_id]</td><td>$sd</td><td>$ed</td><td>$leave_type_array[$leave_type]</td><td>$request_type_array[$status]</td><td>$reason</td><td>$date_filed</td></tr>";
	}
}
?>
<br><br>
<table><tr>Search Keywords:</tr></table>
<br>
<div class="payroll-header">
    <span class="glyphicon glyphicon-calendar"></span> Leave Request Report
</div>
<table class="table">
	<thead>
	<tr>
		<th>Employee Name</th>
		<th>Leave Start Date</th>
		<th>Leave End Date</th>
		<th>Type of Leave</th>
		<th>Leave Status</th>
		<th>Reason for Disapproval</th>
		<th>Date Filed</th>
	</tr>
</thead>
	<tr>
		<?php echo $tr;?>
	</tr>
</table>

</div>
