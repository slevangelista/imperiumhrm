<?php
/* @var $this LeaveRequestController */
/* @var $model LeaveRequest */

$this->breadcrumbs=array(
	'Leave Requests'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List LeaveRequest', 'url'=>array('index')),
	array('label'=>'Create LeaveRequest', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#leave-request-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Leave Requests</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'leave-request-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'emp_id',
		'start_date',
		'end_date',
		'reason',
		'date_submitted',
		/*
		'leave_type_id',
		'if_leave_type_others',
		'days_with_pay',
		'tl_emp_id',
		'tl_request_id',
		'tl_dissaproval',
		'sv_emp_id',
		'sv_request_id',
		'sv_dissaproval',
		'hr_emp_id',
		'hr_request_id',
		'hr_dissaproval',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
