<?php

class m141010_032432_ot_request_drop_fkpositonid_createfkheirarchy extends CDbMigration
{
	public function up()
	{
	
			//$this->dropForeignKey('fk_position', 'tbl_ot_request');
			$this->AddForeignKey('fk_heirarchy_id', 'tbl_ot_request', 'position_id', 'tbl_position', 'hierarchy_id');

	}
	public function down()
	{
			
			$this->AddForeignKey( 'fk_position', 'tbl_ot_request', 'position_id', 'tbl_position', 'id', 'NO_ACTION', 'NO_ACTION');
			$this->dropForeignKey('fk_heirarchy_id', 'tbl_ot_request');

	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/



}