<?php

class m140929_104549_removeFK_tbl_request_leave extends CDbMigration
{
	public function up()
	{	
		$this->createIndex('hierarchy_index', 'tbl_position', 'hierarchy_id', true);
		$this->addForeignKey('requestDisapprovalFK','tbl_leave_request','position_id','tbl_position','hierarchy_id');
	}

	public function down()
	{
		$this->dropIndex('hierarchy_index', 'tbl_position');
		$this->dropForeignKey('requestDisapprovalFK', 'tbl_leave_request');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
