<?php

class m140916_021851_alter_tbl_logs_add_column extends CDbMigration
{
	public function up()
	{
		$this->addColumn('tbl_logs', 'status', 'string');
	}

	public function down()
	{
		$this->dropColumn('tbl_logs', 'status');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}