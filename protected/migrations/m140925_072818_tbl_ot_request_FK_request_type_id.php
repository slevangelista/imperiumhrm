<?php

class m140925_072818_tbl_ot_request_FK_request_type_id extends CDbMigration
{
	public function up()
	{
	$this->AddForeignKey('tbl_ot_request_typefk','tbl_ot_request','request_type_id','tbl_request_type','id');
	}

	public function down()
	{
		$this->dropForeignKey('tbl_ot_request_typefk', 'tbl_ot_request');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}