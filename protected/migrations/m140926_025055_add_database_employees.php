<?php

class m140926_025055_add_database_employees extends CDbMigration
{
	public function up()
	{
		// $this->insert('tbl_employee', array(
		// 	'emp_id' => '8',
		// 	'lastname' => 'Contreras',
		// 	'firstname' => 'Fernando',
		// 	'gender_id' => '1',
		// 	'nationality_id' => '62',
		// 	'position_id' => '1',
		// 	'sub_department_id' => '12',
		// 	'status_id' => '1',
		// 	'created_at' => date('Y-m-d h:i:s'),
		// 	'updated_at' => date('Y-m-d h:i:s'),
		// ));

		// $this->insert('tbl_employee', array(
		// 	'emp_id' => '21',
		// 	'lastname' => 'Quijano',
		// 	'firstname' => 'Jelyn',
		// 	'gender_id' => '2',
		// 	'nationality_id' => '62',
		// 	'position_id' => '4',
		// 	'sub_department_id' => '10',
		// 	'status_id' => '1',
		// 	'created_at' => date('Y-m-d h:i:s'),
		// 	'updated_at' => date('Y-m-d h:i:s'),
		// ));

		// $this->insert('tbl_employee', array(
		// 	'emp_id' => '27',
		// 	'lastname' => 'Aguirre',
		// 	'firstname' => 'Mary Grace',
		// 	'gender_id' => '2',
		// 	'nationality_id' => '62',
		// 	'position_id' => '1',
		// 	'sub_department_id' => '9',
		// 	'status_id' => '1',
		// 	'created_at' => date('Y-m-d h:i:s'),
		// 	'updated_at' => date('Y-m-d h:i:s'),
		// ));

		// $this->insert('tbl_employee', array(
		// 	'emp_id' => '28',
		// 	'lastname' => 'Paluhas',
		// 	'firstname' => 'Jaime',
		// 	'gender_id' => '1',
		// 	'nationality_id' => '62',
		// 	'position_id' => '4',
		// 	'sub_department_id' => '7',
		// 	'status_id' => '1',
		// 	'created_at' => date('Y-m-d h:i:s'),
		// 	'updated_at' => date('Y-m-d h:i:s'),
		// ));

		$this->insert('tbl_employee', array(
			'emp_id' => '30',
			'lastname' => 'Dy',
			'firstname' => 'Ramon',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '3',
			'sub_department_id' => '3',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '35',
			'lastname' => 'Nepomuceno',
			'firstname' => 'Manuel Luis',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '3',
			'sub_department_id' => '5',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '51',
			'lastname' => 'Tanga',
			'firstname' => 'Juzen Khay',
			'gender_id' => '2',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '8',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '158',
			'lastname' => 'Arica',
			'firstname' => 'Daryll Smart',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '1',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '196',
			'lastname' => 'Collera',
			'firstname' => 'Dandy',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '1',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '202',
			'lastname' => 'Santiago',
			'firstname' => 'Vhilly',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '3',
			'sub_department_id' => '2',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '210',
			'lastname' => 'Cabug-os',
			'firstname' => 'Neil',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '3',
			'sub_department_id' => '2',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '33',
			'lastname' => 'Mapa',
			'firstname' => 'Gary',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '2',
			'sub_department_id' => '11',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '113',
			'lastname' => 'Rojas',
			'firstname' => 'Fredrick',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '3',
			'sub_department_id' => '3',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

	}

	public function down()
	{
		echo "m140926_025055_add_database_employees does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}