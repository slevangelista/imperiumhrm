<?php

class m140919_071511_add_column_hr_override_logs extends CDbMigration
{
	public function up()
	{
		$this->addColumn('tbl_logs', 'hr_status', 'int default NULL');
		$this->addColumn('tbl_logs', 'hr_logs', 'int default NULL');

		$this->addForeignKey('fk_tbl_logs_hr_status', 'tbl_logs', 'hr_status', 'tbl_log_status', 'id', 'NO ACTION', 'NO ACTION');
	}

	public function down()
	{
		$this->dropForeignKey('fk_tbl_logs_hr_status', 'tbl_logs');
		$this->dropColumn('tbl_logs', 'hr_logs');
		$this->dropColumn('tbl_logs', 'hr_status');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}