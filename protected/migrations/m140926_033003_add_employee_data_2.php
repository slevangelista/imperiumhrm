<?php

class m140926_033003_add_employee_data_2 extends CDbMigration
{
	public function up()
	{
		$this->insert('tbl_employee', array(
			'emp_id' => '107',
			'lastname' => 'Magparoc',
			'firstname' => 'Mariel Aikeem',
			'gender_id' => '2',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '4',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '124',
			'lastname' => 'Castro',
			'firstname' => 'Philip',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '4',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '140',
			'lastname' => 'Mallari',
			'firstname' => 'Mary Ann',
			'gender_id' => '2',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '4',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '145',
			'lastname' => 'Franco',
			'firstname' => 'Katrina',
			'gender_id' => '2',
			'nationality_id' => '62',
			'position_id' => '3',
			'sub_department_id' => '3',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '148',
			'lastname' => 'Samson',
			'firstname' => 'Camille',
			'gender_id' => '2',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '4',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '162',
			'lastname' => 'Tortoles',
			'firstname' => 'Edd Julius',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '4',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '163',
			'lastname' => 'Domingo',
			'firstname' => 'Danica',
			'gender_id' => '2',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '4',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '169',
			'lastname' => 'Yabut',
			'firstname' => 'Jocelyn',
			'gender_id' => '2',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '4',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '170',
			'lastname' => 'Morillo',
			'firstname' => 'Gilbert',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '4',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '173',
			'lastname' => 'Guevarra',
			'firstname' => 'Alexis',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '4',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '174',
			'lastname' => 'Frias',
			'firstname' => 'April John',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '4',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '180',
			'lastname' => 'De Guzman',
			'firstname' => 'John Felix',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '4',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '189',
			'lastname' => 'Violanda',
			'firstname' => 'Donnabelle',
			'gender_id' => '2',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '4',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '190',
			'lastname' => 'Pagtakhan',
			'firstname' => 'Kathy',
			'gender_id' => '2',
			'nationality_id' => '62',
			'position_id' => '3',
			'sub_department_id' => '3',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '192',
			'lastname' => 'Argame',
			'firstname' => 'Rishelle',
			'gender_id' => '2',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '4',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '197',
			'lastname' => 'Secarpio',
			'firstname' => 'Jefferson',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '4',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '214',
			'lastname' => 'Mendoza',
			'firstname' => 'James Harold',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '4',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '216',
			'lastname' => 'Bernal',
			'firstname' => 'Wilfredo',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '4',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '230',
			'lastname' => 'Cardeno',
			'firstname' => 'Rai Clarence',
			'gender_id' => '2',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '4',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '248',
			'lastname' => 'Abad',
			'firstname' => 'Nathaniel',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '4',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '251',
			'lastname' => 'Terrenal',
			'firstname' => 'Arvin Karlo',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '4',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '263',
			'lastname' => 'De Castro',
			'firstname' => 'Lloyd',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '4',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '264',
			'lastname' => 'De Jesus',
			'firstname' => 'Raymond',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '4',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '265',
			'lastname' => 'Tuazon',
			'firstname' => 'John Jervis',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '4',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '266',
			'lastname' => 'Lodronio',
			'firstname' => 'Marie Joy',
			'gender_id' => '2',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '4',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '270',
			'lastname' => 'Bacual',
			'firstname' => 'Jonathan Vincent',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '4',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '271',
			'lastname' => 'Aquino',
			'firstname' => 'Jimmhar',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '4',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '276',
			'lastname' => 'Bajet',
			'firstname' => 'Jaynar',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '4',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '277',
			'lastname' => 'Garcia',
			'firstname' => 'Ro-anne',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '4',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

	}

	public function down()
	{
		echo "m140926_033003_add_employee_data_2 does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}