<?php

class m140925_071043_create_tbl_shift extends CDbMigration
{
	public function up()
	{
		$this->createTable('tbl_shift', array(
			'id' => 'pk auto_increment',
			'schedule' => 'string',
			'department_id' => 'int'
		));
	}

	public function down()
	{
		$this->dropTable('tbl_shift');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}