<?php

class m141009_103744_add_column_is_leave_tbl_schedule extends CDbMigration
{
	public function up()
	{
		$this->addColumn('tbl_schedule', 'is_leave', 'tinyint DEFAULT 0');
	}

	public function down()
	{
		$this->dropColumn('tbl_schedule', 'is_leave');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}