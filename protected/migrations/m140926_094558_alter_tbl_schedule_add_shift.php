<?php

class m140926_094558_alter_tbl_schedule_add_shift extends CDbMigration
{
	public function up()
	{
		$this->addColumn('tbl_schedule', 'shift_id', 'int');
		$this->addForeignKey('fk_tbl_shift', 'tbl_schedule', 'shift_id', 'tbl_shift', 'id');
	}

	public function down()
	{
		$this->dropForeignKey('fk_tbl_shift', 'tbl_schedule');
		$this->dropColumn('tbl_schedule', 'shift_id');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}