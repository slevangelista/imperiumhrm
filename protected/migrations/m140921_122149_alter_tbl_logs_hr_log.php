<?php

class m140921_122149_alter_tbl_logs_hr_log extends CDbMigration
{
	public function up()
	{
		$this->alterColumn('tbl_logs', 'hr_logs', 'string');
	}

	public function down()
	{
		$this->alterColumn('tbl_logs', 'hr_logs', 'int');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}