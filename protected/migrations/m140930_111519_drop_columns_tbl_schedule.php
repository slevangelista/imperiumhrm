<?php

class m140930_111519_drop_columns_tbl_schedule extends CDbMigration
{
	public function up()
	{
		$this->dropColumn('tbl_schedule', 'name');
		$this->dropColumn('tbl_schedule', 'schedule');
	}

	public function down()
	{
		$this->addColumn('tbl_schedule', 'name', 'string');
		$this->addColumn('tbl_schedule', 'schedule', 'string');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}