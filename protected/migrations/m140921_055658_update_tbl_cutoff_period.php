<?php

class m140921_055658_update_tbl_cutoff_period extends CDbMigration
{
	public function up()
	{
		$this->alterColumn('tbl_cutoff_period', 'date_start', 'date');
		$this->alterColumn('tbl_cutoff_period', 'date_end', 'date');
	}

	public function down()
	{
		$this->alterColumn('tbl_cutoff_period', 'date_start', 'datetime');
		$this->alterColumn('tbl_cutoff_period', 'date_end', 'datetime');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}