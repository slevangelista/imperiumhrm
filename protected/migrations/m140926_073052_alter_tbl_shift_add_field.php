<?php

class m140926_073052_alter_tbl_shift_add_field extends CDbMigration
{
	public function up()
	{
		$this->addColumn('tbl_shift', 'name', 'string');
		$this->addColumn('tbl_shift', 'color', 'string');
		$this->addColumn('tbl_shift', 'personnel_required', 'int');
		$this->addColumn('tbl_shift', 'start_time', 'time');
		$this->addColumn('tbl_shift', 'end_time', 'time');
	}

	public function down()
	{
		$this->dropColumn('tbl_shift', 'name');
		$this->dropColumn('tbl_shift', 'color');
		$this->dropColumn('tbl_shift', 'personnel_required');
		$this->dropColumn('tbl_shift', 'start_time');
		$this->dropColumn('tbl_shift', 'end_time');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}