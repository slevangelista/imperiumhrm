<?php

class m140919_024134_create_table_tbl_cutoff_period extends CDbMigration
{
	public function up()
	{
		$this->createTable('tbl_cutoff_period', array(
			'id' => 'pk auto_increment',
			'date_start' => 'datetime',
			'date_end' => 'datetime'
		));
	}

	public function down()
	{
		$this->deleteTable('tbl_cutoff_period');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}