<?php

class m140923_074412_ot_request_drop_tl_sv_id_approval_create_request_index extends CDbMigration
{

	public function up()
	{
	$this->dropForeignKey('tbl_ot_request_ibfk_9', 'tbl_ot_request');
	$this->dropForeignKey('tbl_ot_request_ibfk_10', 'tbl_ot_request');
	$this->dropForeignKey('tbl_ot_request_ibfk_11', 'tbl_ot_request');
	$this->dropForeignKey('tbl_ot_request_ibfk_12', 'tbl_ot_request');
	$this->dropForeignKey('tbl_ot_request_ibfk_13', 'tbl_ot_request');
	$this->dropForeignKey('tbl_ot_request_ibfk_14', 'tbl_ot_request');
	}

	public function down()
	{
		$this->AddForeignKey('tbl_ot_request_ibfk_9', 'tbl_ot_request', 'tl_emp_id', 'tbl_employee', 'emp_id'. 'NO_ACTION', 'NO_ACTION');
		$this->AddForeignKey('tbl_ot_request_ibfk_10', 'tbl_ot_request','tl_request_id', 'tbl_request_type', 'id'. 'NO_ACTION', 'NO_ACTION');
		$this->AddForeignKey('tbl_ot_request_ibfk_11', 'tbl_ot_request', 'sv_emp_id', 'tbl_employee', 'emp_id' . 'NO_ACTION', 'NO_ACTION');
		$this->AddForeignKey('tbl_ot_request_ibfk_12', 'tbl_ot_request', 'sv_request_id', 'tbl_request_type','id'. 'NO_ACTION', 'NO_ACTION');
		$this->AddForeignKey('tbl_ot_request_ibfk_13', 'tbl_ot_request', 'hr_emp_id', 'tbl_employee','emp_id' . 'NO_ACTION', 'NO_ACTION');
		$this->AddForeignKey('tbl_ot_request_ibfk_14', 'tbl_ot_request', 'hr_request_id', 'tbl_request_type','id' . 'NO_ACTION', 'NO_ACTION');

	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}