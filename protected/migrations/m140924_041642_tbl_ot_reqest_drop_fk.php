<?php

class m140924_041642_tbl_ot_reqest_drop_fk extends CDbMigration
{
	public function up()
	{
		$this->dropColumn('tbl_ot_request', 'tl_emp_id');
		$this->dropColumn('tbl_ot_request', 'tl_request_id');
		$this->dropColumn('tbl_ot_request', 'tl_dissaproval');
		$this->dropColumn('tbl_ot_request', 'sv_emp_id');
		$this->dropColumn('tbl_ot_request', 'sv_request_id');
		$this->dropColumn('tbl_ot_request', 'sv_dissaproval');
		$this->dropColumn('tbl_ot_request', 'hr_emp_id');
		$this->dropColumn('tbl_ot_request', 'hr_request_id');
		$this->dropColumn('tbl_ot_request', 'hr_dissaproval');

	$this->addColumn('tbl_ot_request', 'request_type_id', 'int');
	$this->addColumn('tbl_ot_request', 'request_dissaproval', 'string');
	$this->addColumn('tbl_ot_request', 'position_id', 'int');
	}

	public function down()
	{
		$this->addColumn('tbl_ot_request', 'tl_emp_id');
		$this->addColumn('tbl_ot_request', 'tl_request_id');
		$this->addColumn('tbl_ot_request', 'tl_dissaproval');
		$this->addColumn('tbl_ot_request', 'sv_emp_id');
		$this->addColumn('tbl_ot_request', 'sv_request_id');
		$this->addColumn('tbl_ot_request', 'sv_dissaproval');
		$this->addColumn('tbl_ot_request', 'hr_emp_id');
		$this->addColumn('tbl_ot_request', 'hr_request_id');
		$this->addColumn('tbl_ot_request',																																																																																																																														 'hr_dissaproval');

	$this->dropColumn('tbl_ot_request', 'request_type_id', 'int');
	$this->dropColumn('tbl_ot_request', 'request_dissaproval', 'string');
	$this->dropColumn('tbl_ot_request', 'position_id', 'int');
	
	
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}