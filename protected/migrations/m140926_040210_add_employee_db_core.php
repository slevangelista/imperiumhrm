<?php

class m140926_040210_add_employee_db_core extends CDbMigration
{
	public function up()
	{
		$this->insert('tbl_employee', array(
			'emp_id' => '103',
			'lastname' => 'Ramos',
			'firstname' => 'Jheera',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '104',
			'lastname' => 'Oxina',
			'firstname' => 'Cristhamay',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '109',
			'lastname' => 'Alcaraz',
			'firstname' => 'Lara Marynell',
			'gender_id' => '2',
			'nationality_id' => '62',
			'position_id' => '3',
			'sub_department_id' => '5',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '110',
			'lastname' => 'Carlos',
			'firstname' => 'Mark Kristoffer',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '112',
			'lastname' => 'Dagami',
			'firstname' => 'Randell',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '117',
			'lastname' => 'Sumagyap',
			'firstname' => 'Patrick',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '119',
			'lastname' => 'Olermo',
			'firstname' => 'Perry James',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '123',
			'lastname' => 'Dawang',
			'firstname' => 'Mark Lloyd',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '138',
			'lastname' => 'Morales',
			'firstname' => 'Jose Jewel',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '139',
			'lastname' => 'Marin',
			'firstname' => 'Kristian',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '144',
			'lastname' => 'Cruz',
			'firstname' => 'Russel',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '146',
			'lastname' => 'Anir',
			'firstname' => 'Anna',
			'gender_id' => '2',
			'nationality_id' => '62',
			'position_id' => '3',
			'sub_department_id' => '5',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '149',
			'lastname' => 'Basuglan',
			'firstname' => 'Jayson',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '152',
			'lastname' => 'Cada',
			'firstname' => 'Lexter',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '153',
			'lastname' => 'Agustin',
			'firstname' => 'Jesie',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '154',
			'lastname' => 'Dupo',
			'firstname' => 'Raymart',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '156',
			'lastname' => 'Joaquin',
			'firstname' => 'Enjay',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '157',
			'lastname' => 'Ilagan',
			'firstname' => 'Lexter Angelo',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '164',
			'lastname' => 'Caluna',
			'firstname' => 'Casimiro',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '165',
			'lastname' => 'Go',
			'firstname' => 'Mark Angelo',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '166',
			'lastname' => 'Abling',
			'firstname' => 'Patrick Joseph',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '176',
			'lastname' => 'Castillo',
			'firstname' => 'Daryl',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '2',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '177',
			'lastname' => 'Nunez',
			'firstname' => 'Audi',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '179',
			'lastname' => 'Mariano',
			'firstname' => 'Ron',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '183',
			'lastname' => 'Recto',
			'firstname' => 'Ralph',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '185',
			'lastname' => 'Cornista',
			'firstname' => 'Earl John',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '194',
			'lastname' => 'Parsacala',
			'firstname' => 'Mark Jason',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '195',
			'lastname' => 'Carteciano',
			'firstname' => 'Jay Francis',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '219',
			'lastname' => 'Purunggan',
			'firstname' => 'Jaworski',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '225',
			'lastname' => 'Namoro',
			'firstname' => 'Richard',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '226',
			'lastname' => 'Genorga',
			'firstname' => 'Mark Ronald',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '227',
			'lastname' => 'Gutierrez',
			'firstname' => 'Alexis',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '229',
			'lastname' => 'Quito',
			'firstname' => 'Mary Rose',
			'gender_id' => '2',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '2',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '249',
			'lastname' => 'Villalobos',
			'firstname' => 'Roy Lopez',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '250',
			'lastname' => 'Abijan',
			'firstname' => 'Ryan',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '253',
			'lastname' => 'Buensalida',
			'firstname' => 'Franco Leo',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '254',
			'lastname' => 'Lacanlale',
			'firstname' => 'Gilbert',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '255',
			'lastname' => 'Ganipan',
			'firstname' => 'Giovani',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '256',
			'lastname' => 'Lingat',
			'firstname' => 'Jeffrey',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '269',
			'lastname' => 'Bismonte',
			'firstname' => 'John Alvir',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '272',
			'lastname' => 'Granadoz',
			'firstname' => 'Niel Lester',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '273',
			'lastname' => 'Dela Cruz',
			'firstname' => 'Robert Conrad',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '274',
			'lastname' => 'Gonzales',
			'firstname' => 'Mark Christian',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '275',
			'lastname' => 'Guillena',
			'firstname' => 'Kim Jim',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '6',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));
	}

	public function down()
	{
		echo "m140926_040210_add_employee_db_core does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}