<?php

class m140925_071304_alter_tbl_shift_add_fk extends CDbMigration
{
	public function up()
	{
		$this->addForeignKey('fk_tbl_shift_department_id', 'tbl_shift', 'department_id', 'tbl_department', 'id');
	}

	public function down()
	{
		$this->dropForeignKey('fk_tbl_shift_department_id', 'tbl_shift');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}