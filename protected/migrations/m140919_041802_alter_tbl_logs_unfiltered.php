<?php

class m140919_041802_alter_tbl_logs_unfiltered extends CDbMigration
{
	public function up()
	{
		$this->renameColumn('tbl_logs_unfiltered', 'department', 'name');
	}

	public function down()
	{
		$this->renameColumn('tbl_logs_unfiltered', 'name', 'department');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}