<?php

class m140916_071600_insert_log_status extends CDbMigration
{
	public function up()
	{
		$this->insert('tbl_log_status', array('name' => 'No Schedule'));
		$this->insert('tbl_log_status', array('name' => 'Normal'));
		$this->insert('tbl_log_status', array('name' => 'Late'));
		$this->insert('tbl_log_status', array('name' => 'Halfday'));
		$this->insert('tbl_log_status', array('name' => 'Absent'));
		$this->insert('tbl_log_status', array('name' => 'Restday'));
		$this->insert('tbl_log_status', array('name' => 'Restday Work'));
		$this->insert('tbl_log_status', array('name' => 'Undertime'));
		$this->insert('tbl_log_status', array('name' => 'Overtime'));
	}

	public function down()
	{
		$this->truncateTable('tbl_log_status');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}