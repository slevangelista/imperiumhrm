<?php

class m141014_092145_alter_tbl_schedule_add_holiday extends CDbMigration
{
	public function up()
	{
		$this->createTable('tbl_holidays', array(
			'id' => 'pk auto_increment',
			'name' => 'string',
		));

		$this->addColumn('tbl_schedule', 'is_holiday', 'int');
		$this->addForeignKey('fk_holiday', 'tbl_schedule', 'is_holiday', 'tbl_holidays', 'id');

		$this->insert('tbl_holidays', array(
			'name' => 'New Years Eve',
		));
	}

	public function down()
	{
		$this->dropForeignKey('fk_holiday', 'tbl_schedule');
		$this->dropColumn('tbl_schedule', 'is_holiday');
		$this->dropTable('tbl_holidays');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}