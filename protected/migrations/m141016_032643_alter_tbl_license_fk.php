<?php

class m141016_032643_alter_tbl_license_fk extends CDbMigration
{
	public function up()
	{
		$this->dropForeignKey('fk_license_1', 'tbl_license');
		$this->addForeignKey('fk_license_emp_id', 'tbl_license', 'emp_id', 'tbl_employee', 'emp_id');
	}

	public function down()
	{
		echo "m141016_032643_alter_tbl_license_fk does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/

	
}
