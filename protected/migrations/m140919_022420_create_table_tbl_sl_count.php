<?php

class m140919_022420_create_table_tbl_sl_count extends CDbMigration
{
	public function up()
	{
		$this->createTable('tbl_sl_count', array(
			'id' => 'pk auto_increment',
			'emp_id' => 'int',
			'value' => 'int'
		));

		$this->createIndex('emp_id', 'tbl_sl_count', 'emp_id', true);
	}

	public function down()
	{
		$this->dropIndex('emp_id', 'tbl_sl_count');
		$this->dropTable('tbl_sl_count');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}