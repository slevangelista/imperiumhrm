<?php

class m140926_074150_tbl_alter_shift_drop_schedule extends CDbMigration
{
	public function up()
	{
		$this->dropColumn('tbl_shift', 'schedule');
	}

	public function down()
	{
		$this->addColumn('tbl_shift', 'schedule', 'string');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}