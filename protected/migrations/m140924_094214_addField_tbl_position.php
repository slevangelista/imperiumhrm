<?php

class m140924_094214_addField_tbl_position extends CDbMigration
{
	public function up()
	{
               $this->addColumn('tbl_position','hierarchy_id','int','NULL');
	}

	public function down()
	{
		echo "m140924_094214_addField_tbl_position does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
