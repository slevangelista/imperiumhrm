<?php

class m141002_074358_alter_tbl_ot_create_fk_position extends CDbMigration
{
	public function up()
	{
		$this->addForeignKey('fk_position', 'tbl_ot_request', 'position_id', 'tbl_position', 'id');
	}

	public function down()
	{
		$this->dropForeignKey('fk_position', 'tbl_ot_request');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}