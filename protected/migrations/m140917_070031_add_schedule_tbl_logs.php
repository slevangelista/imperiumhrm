<?php

class m140917_070031_add_schedule_tbl_logs extends CDbMigration
{
	public function up()
	{
		$this->addColumn('tbl_logs', 'schedule', 'string');
	}

	public function down()
	{
		$this->dropColumn('tbl_logs', 'schedule');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}