<?php

class m141017_055327_ot_request_table_alter_total_hours extends CDbMigration
{
	public function up()
	{
		$this->alterColumn('tbl_ot_request', 'total_hours', 'decimal(5.1)');
	}

	public function down()
	{
		$this->alterColumn('tbl_ot_request', 'total_hours', 'int');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}