<?php

class m140928_155732_alter_tbl_shift_change_type extends CDbMigration
{
	public function up()
	{
		$this->alterColumn('tbl_shift', 'start_time', 'string');
		$this->alterColumn('tbl_shift', 'end_time', 'string');
	}

	public function down()
	{
		$this->alterColumn('tbl_shift', 'start_time', 'time');
		$this->alterColumn('tbl_shift', 'end_time', 'time');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}