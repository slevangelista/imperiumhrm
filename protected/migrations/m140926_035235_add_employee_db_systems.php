<?php

class m140926_035235_add_employee_db_systems extends CDbMigration
{
	public function up()
	{
		$this->insert('tbl_employee', array(
			'emp_id' => '257',
			'lastname' => 'Sadsad',
			'firstname' => 'Ma. Cristina',
			'gender_id' => '2',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '1',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '258',
			'lastname' => 'Granados',
			'firstname' => 'June Jovi',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '1',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '259',
			'lastname' => 'Legaspi',
			'firstname' => 'Francis Joshua',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '1',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));

		$this->insert('tbl_employee', array(
			'emp_id' => '260',
			'lastname' => 'Llorera',
			'firstname' => 'Dione Dave',
			'gender_id' => '1',
			'nationality_id' => '62',
			'position_id' => '4',
			'sub_department_id' => '1',
			'status_id' => '1',
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s'),
		));
	}

	public function down()
	{
		echo "m140926_035235_add_employee_db_systems does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}