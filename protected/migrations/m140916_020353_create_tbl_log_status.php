<?php

class m140916_020353_create_tbl_log_status extends CDbMigration
{
	public function up()
	{
		$this->createTable('tbl_log_status', array(
			'id' => 'pk auto_increment',
			'name' => 'string NOT NULL',
		));
	}

	public function down()
	{
		echo "m140916_020353_create_tbl_log_status does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}