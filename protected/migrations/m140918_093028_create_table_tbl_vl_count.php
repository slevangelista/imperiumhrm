<?php

class m140918_093028_create_table_tbl_vl_count extends CDbMigration
{
	public function up()
	{
		$this->createTable('tbl_vl_count', array(
			'id' => 'pk auto_increment',
			'emp_id' => 'int',
			'value' => 'int'
		));

		$this->createIndex('emp_id', 'tbl_vl_count', 'emp_id', true);
	}

	public function down()
	{
		$this->dropIndex('emp_id', 'tbl_vl_count');
		$this->dropTable('tbl_vl_count');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}