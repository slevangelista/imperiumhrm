<?php

class m140924_043520_update_tbl_leave_request extends CDbMigration
{
	public function up()
	{

		$this->addColumn('tbl_leave_request','request_type_id','int');
		$this->addColumn('tbl_leave_request','request_disapproval','string');
		$this->addColumn('tbl_leave_request','position_id','int');
		$this->dropForeignKey('tbl_leave_request_ibfk_11','tbl_leave_request');
		$this->dropForeignKey('tbl_leave_request_ibfk_12','tbl_leave_request');
		$this->dropForeignKey('tbl_leave_request_ibfk_13','tbl_leave_request');
		$this->dropForeignKey('tbl_leave_request_ibfk_14','tbl_leave_request');
		$this->dropForeignKey('tbl_leave_request_ibfk_15','tbl_leave_request');
		$this->dropForeignKey('tbl_leave_request_ibfk_16','tbl_leave_request');
		$this->dropColumn('tbl_leave_request', 'tl_emp_id');
		$this->dropColumn('tbl_leave_request', 'sv_emp_id');
		$this->dropColumn('tbl_leave_request', 'hr_emp_id');
		$this->dropColumn('tbl_leave_request', 'tl_request_id');
		$this->dropColumn('tbl_leave_request', 'sv_request_id');
		$this->dropColumn('tbl_leave_request', 'hr_request_id');
		$this->dropColumn('tbl_leave_request', 'tl_dissaproval');
		$this->dropColumn('tbl_leave_request', 'sv_dissaproval');
		$this->dropColumn('tbl_leave_request', 'hr_dissaproval');

#		$this->addForeignKey('requestTypeId','tbl_leave_request','request_type_id','tbl_request_type','hierarchy_id','NO ACTION','NO ACTION');
#		$this->addForeignKey('requestDisapproval','tbl_leave_request','position_id','tbl_employee','emp_id','NO ACTION','NO ACTION');
	}

	public function down()
	{
		echo "m140924_043520_update_tbl_leave_request does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
