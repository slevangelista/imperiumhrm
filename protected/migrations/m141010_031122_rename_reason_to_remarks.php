<?php

class m141010_031122_rename_reason_to_remarks extends CDbMigration
{
	public function up()
	{
	
	   $this->renameColumn('tbl_ot_request', 'reason', 'remarks');

	}



	public function down()
	{
				
        $this->renameColumn('tbl_ot_request', 'remarks', 'reason');
	 
		
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}