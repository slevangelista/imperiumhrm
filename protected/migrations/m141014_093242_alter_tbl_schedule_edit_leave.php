<?php

class m141014_093242_alter_tbl_schedule_edit_leave extends CDbMigration
{
	public function up()
	{
		$this->alterColumn('tbl_schedule', 'is_leave', 'int');
		$this->addForeignKey('fk_leave', 'tbl_schedule', 'is_leave', 'tbl_leave_type', 'id');
	}

	public function down()
	{
		$this->dropForeignKey('fk_leave', 'tbl_schedule');
		$this->alterColumn('tbl_schedule', 'is_leave', 'tinyint');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}