<?php

class m140917_071419_insert_tbl_log_status_halfday extends CDbMigration
{
	public function up()
	{
		$this->delete('tbl_log_status', 'name = :name', array(':name' => 'Halfday'));
		$this->insert('tbl_log_status', array('name' => 'Halfday (No Login)'));
		$this->insert('tbl_log_status', array('name' => 'Halfday (No Logout)'));
		$this->insert('tbl_log_status', array('name' => 'Halfday (More than 1 hr late)'));
	}

	public function down()
	{
		$this->delete('tbl_log_status', 'name = :name', array(':name' => 'Halfday'));
		$this->delete('tbl_log_status', 'name = :name', array(':name' => 'Halfday'));
		$this->delete('tbl_log_status', 'name = :name', array(':name' => 'Halfday'));
		$this->insert('tbl_log_status', array('name' => 'Halfday'));
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}