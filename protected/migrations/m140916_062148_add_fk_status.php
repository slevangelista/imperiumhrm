<?php

class m140916_062148_add_fk_status extends CDbMigration
{
	public function up()
	{
		$this->addForeignKey('fk_tbl_logs_status', 'tbl_logs', 'status', 'tbl_log_status', 'id', 'NO ACTION', 'NO ACTION');
	}

	public function down()
	{
		echo "m140916_062148_add_fk_status does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}