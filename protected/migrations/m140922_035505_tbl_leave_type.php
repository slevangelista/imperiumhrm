<?php

class m140922_035505_tbl_leave_type extends CDbMigration
{
	public function up()
	{
		$this->addColumn('tbl_leave_type', 'for_leave_form', 'tinyint');
	}

	public function down()
	{
		$this->dropColumn('tbl_leave_type', 'for_leave_form');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
