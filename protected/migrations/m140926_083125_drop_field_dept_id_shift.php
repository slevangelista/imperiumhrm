<?php

class m140926_083125_drop_field_dept_id_shift extends CDbMigration
{
	public function up()
	{
		$this->dropForeignKey('fk_tbl_shift_department_id', 'tbl_shift');
		$this->dropColumn('tbl_shift', 'department_id');
	}

	public function down()
	{
		$this->addColumn('tbl_shift', 'department_id', 'int');
		$this->addForeignKey('fk_tbl_shift_department_id', 'tbl_shift', 'department_id', 'tbl_department', 'id');
	}
	
	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}