<?php

class m140923_050105_insert_tbl_request_type extends CDbMigration
{
	public function up()
	{
		$this->insert('tbl_request_type',array(
			'id'=>1,
			'name'=>'Approved',
		));
		$this->insert('tbl_request_type',array(
			'id'=>2,
			'name'=>'Disapproved',
		));
		$this->insert('tbl_request_type',array(
			'id'=>3,
			'name'=>'Pending From Teamd Lead/Supervisor\'s Approval',
		));
		$this->insert('tbl_request_type',array(
			'id'=>4,
			'name'=>'Pending From Manager\'s Approval',
		));
		$this->insert('tbl_request_type',array(
			'id'=>5,
			'name'=>'Pending From HR\'s Approval',
		));
	}

	public function down()
	{
		echo "m140923_050105_insert_tbl_request_type does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
