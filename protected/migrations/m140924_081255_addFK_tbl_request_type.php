<?php

class m140924_081255_addFK_tbl_request_type extends CDbMigration
{
	public function up()
	{
		$this->addForeignKey('requestTypeIdFK','tbl_leave_request','request_type_id','tbl_request_type','id','NO ACTION','NO ACTION');
		$this->addForeignKey('requestDisapprovalFK','tbl_leave_request','position_id','tbl_employee','emp_id','NO ACTION','NO ACTION');
	}

	public function down()
	{
		echo "m140924_081255_addFK_tbl_request_type does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
