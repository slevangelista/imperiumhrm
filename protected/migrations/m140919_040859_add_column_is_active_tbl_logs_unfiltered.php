<?php

class m140919_040859_add_column_is_active_tbl_logs_unfiltered extends CDbMigration
{
	public function up()
	{
		$this->addColumn('tbl_logs_unfiltered', 'is_active', 'tinyint DEFAULT 1');
	}

	public function down()
	{
		$this->dropColumn('tbl_logs_unfiltered', 'is_active');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}