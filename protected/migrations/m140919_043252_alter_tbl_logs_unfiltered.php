<?php

class m140919_043252_alter_tbl_logs_unfiltered extends CDbMigration
{
	public function up()
	{
		$this->addColumn('tbl_logs_unfiltered', 'date', 'date');
	}

	public function down()
	{
		$this->dropColumn('tbl_logs_unfiltered', 'date');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}