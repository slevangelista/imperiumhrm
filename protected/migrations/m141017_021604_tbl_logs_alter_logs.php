<?php

class m141017_021604_tbl_logs_alter_logs extends CDbMigration
{
	public function up()
	{
		$this->alterColumn('tbl_logs', 'checkin', 'TIME DEFAULT NULL');
		$this->alterColumn('tbl_logs', 'checkout', 'TIME DEFAULT NULL');
	}

	public function down()
	{
		$this->alterColumn('tbl_logs', 'checkin', 'TIME');
		$this->alterColumn('tbl_logs', 'checkout', 'TIME');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}