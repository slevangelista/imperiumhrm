<?php

class m140919_034437_create_table_logs_unfiltered extends CDbMigration
{
	public function up()
	{
		$this->createTable('tbl_logs_unfiltered', array(
			'id' => 'pk auto_increment',
			'department' => 'string',
			'emp_id' => 'int',
			'logs' => 'string',
			'status' => 'string'
		));
	}

	public function down()
	{
		$this->dropTable('tbl_logs_unfiltered');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}