<?php

class m140926_020753_create_database_content extends CDbMigration
{
	public function up()
	{
		// ISS
		$this->insert('tbl_sub_department', array(
			'name' => 'ISS Team Leader',
			'department_id' => '2'
		));
		$this->insert('tbl_sub_department', array(
			'name' => 'ISS Network Engineer',
			'department_id' => '2'
		));

		// Core
		$this->insert('tbl_sub_department', array(
			'name' => 'Core Team Leader',
			'department_id' => '3'
		));
		$this->insert('tbl_sub_department', array(
			'name' => 'Core Network Engineer',
			'department_id' => '3'
		));

		// Admin Staff
		$this->insert('tbl_sub_department', array(
			'name' => 'Company Driver',
			'department_id' => '4'
		));
		$this->insert('tbl_sub_department', array(
			'name' => 'Office Maintenance',
			'department_id' => '2'
		));

		// Head
		$this->insert('tbl_sub_department', array(
			'name' => 'HR Manager',
			'department_id' => '5'
		));
		$this->insert('tbl_sub_department', array(
			'name' => 'HR Assistant',
			'department_id' => '5'
		));

		// Head
		$this->insert('tbl_sub_department', array(
			'name' => 'Main Manager',
			'department_id' => '6'
		));
		$this->insert('tbl_sub_department', array(
			'name' => 'Company President',
			'department_id' => '6'
		));

	}

	public function down()
	{
		echo "m140926_020753_create_database_content does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}