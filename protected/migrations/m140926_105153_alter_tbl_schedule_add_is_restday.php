<?php

class m140926_105153_alter_tbl_schedule_add_is_restday extends CDbMigration
{
	public function up()
	{
		$this->addColumn('tbl_schedule', 'is_restday', 'tinyint DEFAULT 0');
	}

	public function down()
	{
		$this->dropColumn('tbl_schedule', 'is_restday');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}