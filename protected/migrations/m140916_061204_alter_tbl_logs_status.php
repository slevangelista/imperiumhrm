<?php

class m140916_061204_alter_tbl_logs_status extends CDbMigration
{
	public function up()
	{
		$this->alterColumn('tbl_logs', 'status', 'int');
	}

	public function down()
	{
		$this->alterColumn('tbl_logs', 'status', 'string');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}