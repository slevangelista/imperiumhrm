<?php

class m141014_052935_alter_ot_request_table extends CDbMigration
{
	public function up()
	{
		$this->addColumn('tbl_ot_request', 'total_hours', 'int');
	}

	public function down()
	{
		$this->dropColumn('tbl_ot_request', 'total_hours');
	}
}