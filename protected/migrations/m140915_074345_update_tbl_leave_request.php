<?php

class m140915_074345_update_tbl_leave_request extends CDbMigration
{
	public function up()
	{
		$this->addColumn('tbl_leave_request', 'date_filed', 'date');
	}

	public function down()
	{
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}