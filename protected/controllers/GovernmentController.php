<?php

class GovernmentController extends RController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/profile';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			//'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
			'rights',
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView()
	{

		$emp_id = Yii::app()->request->getQuery('id');			
		$this->render('view',array(
			'model'=>$this->getEmployeeGovernmentIds($emp_id),
		));	
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$this->layout = "//layouts/admin_profile_view";
		$model=new Government;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		//$emp_id = Yii::app()->request->getQuery('id');
		$emp_id = $_GET['emp_id'];

		if(isset($_POST['Government']))
		{
			
			//var_dump($_POST['Government']);

			$model->attributes=$_POST['Government'];
			$model->emp_id = $emp_id;

			if($model->save())
				$this->redirect(array('create?emp_id='.$model->emp_id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate()
	{
		$this->layout = "//layouts/admin_profile_view";
		$id = $_GET['id'];
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Government']))
		{
			$model->attributes=$_POST['Government'];
			if($model->save())
				$this->redirect(array('create?emp_id='.$model->emp_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Government');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Government('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Government']))
			$model->attributes=$_GET['Government'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Government the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Government::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Government $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='government-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function getEmployeeGovernmentIds($emp_id){

		$sql = "SELECT gov.id,gov.emp_id,gov.value,gov.gov_requirement_type_id,req.name as id_type"
		     . " FROM tbl_government gov, tbl_gov_requirements req"
		     . " WHERE gov.gov_requirement_type_id = req.id"
		     . " AND gov.emp_id = :emp_id";
		$dataReader = Yii::app()->db->CreateCommand($sql);
		$dataReader->bindParam(":emp_id",$emp_id,PDO::PARAM_INT);
		$ids = $dataReader->queryAll();

		return $ids;
		
	}


	public function getGovernmentIdTypes(){

		$list = array();
		$sql = "SELECT id,name FROM tbl_gov_requirements";
		$gov_ids = Yii::app()->db->CreateCommand($sql)->queryAll();

		foreach($gov_ids as $key=>$gov){
			$list[$gov['id']] = $gov['name'];
		}

		return $list;
	}
}
