<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// $leaveModel = new LeaveRequest;

		$user = User::model()->findByPk(Yii::app()->user->id);
		$userid = Yii::app()->user->id; //get id in Users table
		$userData = Users::model()->findByAttributes(array('id'=>$userid));
    	$userEmpId = $userData->emp_id; //get employee id in users table

		$monthFirstDay = date("Y-m-01", strtotime("this month") ) ;
		$monthLastDay = date("Y-m-t", strtotime("this month") ) ;

		$uid = Users::model()->findByAttributes(array('id'=>$userid));
        $eid = $uid['emp_id'];

        	// echo $eid;
        $leaveFiled = Yii::app()->db->createCommand()
        	->select('start_date, end_date, leave_type_id, request_type_id')
        	->from('tbl_leave_request')
        	->where('emp_id=:emp_id', array(':emp_id'=>$eid))
        	->andWhere('start_date between :mfd AND :mld', array(':mfd'=>$monthFirstDay, ':mld'=>$monthLastDay))
        	->queryAll();

        $leaveType = LeaveType::model()->findAll(); //leave type model

        $leaveTypeArr = array();
        foreach ($leaveType as $ltype) {
        	$id = $ltype['id'];
        	$name = $ltype['name'];
        	$name = "$name Leave";
        	$leaveTypeArr[$id] = $name;
        }

        $requestType = RequestType::model()->findAll(); //request type model

        $requestTypeArr = array();
        foreach ($requestType as $rtype) {
        	$id = $rtype['id'];
        	$name = $rtype['name'];
        	$requestTypeArr[$id] = $name;
        }

        $employee = Employee::model()->findByAttributes(array('emp_id'=>$userEmpId));
        $empVlCount = $employee->vl_count;
        $empSlCount = $employee->sl_count;

        $shiftRes = Shift::model()->findAll();
        $shiftArr = array();
        foreach ($shiftRes as $sr) {
        	$id          = $sr['id'];
        	$shift_start = $sr['start_time'];
        	$shift_end   = $sr['end_time'];
        	$shift_time  = "$shift_start - $shift_end";
        	$shiftArr[$id] = $shift_time;
        }

        // $criteria = new CDbCriteria();
        // $criteria->addCondition("emp_id=$userEmpId");
        // $criteria->addCondition("is_active=1");
        // $criteria->addBetweenCondition('date', $monthFirstDay, $monthLastDay, $operator='AND');
        // $criteria->order = "date ASC";
        // $schedule = Schedule::model()->findAll($criteria);
        // foreach ($schedule as $s) {
        // 	$emp_id   = $s['emp_id'];
        // 	$date     = $s['date'];
        // 	$shift_id = $s['shift_id'];
        // 	$rest_day = $s['is_restday'];
        // 	$is_leave = $s['is_leave'];
        // 	$td .= "<tr><td>$date</td><td>$shiftArr[$shift_id] $leaveTypeArr[$is_leave]</td></tr>";
        // 	echo "$emp_id | $date | $shiftArr[$shift_id] | $rest_day | $leaveTypeArr[$is_leave]<br>";
        // }
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index', compact('leaveModel','leaveFiled','leaveTypeArr','requestTypeArr', 'empVlCount', 'empSlCount', 'td'));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
	
	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$this->layout = "//layouts/login.php";
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				//$this->redirect(Yii::app()->user->returnUrl);
				$this->redirect(array('/home'));
		}
		// display the login form
		$this->render('login',array('model'=>$model));

	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}