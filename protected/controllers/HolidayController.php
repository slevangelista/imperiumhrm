<?php

/**
 * Holiday Controller
 * Created By: Syd Evangelista
 * 
 * Class containing ROUTES for access (Whether route leading to views and/or AJAX methods)
 *
 */

class HolidayController extends RController
{
    /**
     * Filter Route. Used for user authentication
     *
     */
    public function filters()
    {
        return array(
            'rights',
        );
    }

    /**
     * Public function Index
     *
     * Sets Holiday Period.
     * Access By: HR
     */
    public function actionIndex() 
    {
        // Generate Cutoff Model
        $model['holiday'] = CutoffPeriod::model()->findAll();

        // Render View
        $this->render('index', $data);
    }

    public function actionGetHolidayAll()
    {
        $model['holiday'] = Holidays::model()->findAll();

        // Pass Holiday Value into array
        foreach ($model['holiday'] as $holiday):
            $holiday_array[$holiday['id']]['name'] = $holiday['name'];
            $holiday_array[$holiday['id']]['id'] = $holiday['id'];
        endforeach;

        echo ")]}',\n" . json_encode($holiday_array); 
    }

    public function actionAddHoliday() 
    {
        if (isset($_POST['name'])):

            $holiday = new Holidays;
            $holiday->name = $_POST['name'];
            $holiday->save();

        endif;
    }

    public function actionEditHoliday()
    {
        if (isset($_POST['id'])):

            Holidays::model()->updateAll(
                array(
                    'name' => $_POST['name'],
                ),
                "id = {$_POST['id']}"
            );

        endif;
    }

    public function actionDeleteHoliday()
    {
        if (isset($_POST['id'])):
            $holiday_delete = Holidays::model()->findByPk($_POST['id']);
            $holiday_delete->delete();
        endif;
    }

}