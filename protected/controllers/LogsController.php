<?php

/**
 * Logs Controller
 * Created By: Syd Evangelista
 * 
 * Class containing ROUTES for access (Whether route leading to views and/or AJAX methods)
 * Extended to LogsAPIController, which contains protected methods that this class will use
 *
 */

// Import Parent Controller
Yii::import('application.controllers.LogsAPIController');

class LogsController extends LogsAPIController
{
    public function actionIndex()
    {
        // Department
        $data['model']['department'] = Department::model()->findAll(array(
            'order' => 'name'
        ));

        // Generate LogsForm Model
        $data['model']['logs_form'] = new LogsForm;

        // Popover-Content HTML
        $data['popover_html'] = '
            Date: {{val.date}} <br />
            Schedule: {{val.schedule}} <br />
            Status: {{val.status}} <br />
        ';

        $this->render('index', $data);
    }

    public function actionUser()
    {
        // Get LogsForm Model
        $data['model']['logs_form'] = new LogsForm;

        // Get Employee
        $employee_details = $this->getEmployeeDetails();
        $model['employee'] = Employee::model()->find(array(
            'condition' => 'emp_id = :emp_id',
            'params' => array(
                ':emp_id' => $employee_details['emp_id']
            )
        ));

        // Get User
        $data['user'] = $model['employee']['fullnameunformal'];

        // Get Current Day
        $data['day'] = date('Y-m-d');
        $data['day_json'] = json_encode($data['day']);

        $this->render('user', $data);
    }

    public function actionGetShifts()
    {
        $model['shifts'] = Shift::model()->findAll();
        $shift = array();

        foreach($model['shifts'] as $value):
            $shift[$value['id']] = $value['name'] . ' (' . $value['start_time'] . ' - ' . $value['end_time'] . ')';
        endforeach;

        echo ")]}',\n" . json_encode($shift);
    }

    public function actionGetCutoffList()
    {
        // Cutoff
        $data['model']['cutoff_all'] = CutoffPeriod::model()->findAll();

        foreach($data['model']['cutoff_all'] as $cutoff):
            $cutoff_list[$cutoff['id']]['value'] = $cutoff['cutoffBeutified'];
        endforeach;

        echo ")]}',\n" . json_encode($cutoff_list);
    }

    public function actionGetLogStatus()
    {
        $model['log_status'] = LogStatus::model()->findAll();

        foreach ($model['log_status'] as $status)
            $status_array[$status['id']] = $status['name'];

        echo ")]}',\n" . json_encode($status_array);
    }

    public function actionGetCurrentCutoff()
    {
        // Cutoff for current day
        $current_day = date('Y-m-d');
        $model['cutoff_today'] = CutoffPeriod::model()->find(array(
            'condition' => 'date_start <= :date AND date_end >= :date',
            'params' => array(
                ':date' => $current_day
            )
        ));

        $data['value'] = $model['cutoff_today']['cutoffBeutified'];

        echo ")]}',\n" . json_encode($data);
    }

    public function actionGetDateRange()
    {
        if (isset($_GET['date_range'])):

            // Get Date Ranges (Date Start & Date End)
            $date_range_array = explode(' → ', $_GET['date_range']); // Explode Date Range

            // Date Start

                // Explode by comma, First index[0] will contain month day (January 01), second index[1] will contain year (2014)
                $date_start_array = explode(', ', $date_range_array[0]); 

                // Explode first index of $date_start_array by space
                $date_start_MD = explode(' ', $date_start_array[0]);                 

                // Generate date start
                $date_start['year'] = $date_start_array[1];
                $date_start['month'] = date('m', strtotime($date_start_MD[0]));
                $date_start['day'] = $date_start_MD[1];

                $start_date = $date_start['year'] . '-' . $date_start['month'] . '-' . $date_start['day'];

            // Date End

                // Explode by comma, First index[0] will contain month day (January 01), second index[1] will contain year (2014)
                $date_end_array = explode(', ', $date_range_array[1]); 

                // Explode first index of $date_end_array by space
                $date_end_MD = explode(' ', $date_end_array[0]);                 

                // Generate date end
                $date_end['year'] = $date_end_array[1];
                $date_end['month'] = date('m', strtotime($date_end_MD[0]));
                $date_end['day'] = $date_end_MD[1];

                $end_date = $date_end['year'] . '-' . $date_end['month'] . '-' . $date_end['day'];

            // Get Date Range
            $maps = $this->getDays($start_date, $end_date);

            foreach ($maps as $map):
                $date_array[$map['date_map']]['key'] = $map['date_map'];
                $date_array[$map['date_map']]['date'] = date('M d', strtotime($map['date_map']));
                $date_array[$map['date_map']]['day'] = date('(D)', strtotime($map['date_map']));
            endforeach;

           echo ")]}',\n" . json_encode($date_array);

        endif;
    }

    public function actionGetEmployeeSelf()
    {
        if (isset($_POST['date_interval'])):

            $employee_details = $this->getEmployeeDetails();

            // Get Employee
            $model['employee'] = Employee::model()->find(array(
                'condition' => 'emp_id = :emp_id',
                'params' => array(
                    ':emp_id' => $employee_details['emp_id']
                )
            ));

            $employee['name'] = $model['employee']['fullname'];
            $employee['emp_id'] = $employee_details['emp_id'];
            $employee['logs'] = $this->getEmployeeLogs($employee_details['emp_id'], $_POST['date_interval']);

            echo json_encode($employee);
            
        endif;
    }

    public function actionGetEmployeeByDepartment()
    {
        if (isset($_POST['department'])):

            // Get model for department
            $model['department'] = Department::model()->findByPk((int) $_POST['department']);
            $department = $model['department']['name'];

            // Get model for sub-department
            $model['sub_department'] = SubDepartment::model()->findAll(array(
                'condition' => 'department_id = :department_id',
                'params' => array(
                    ':department_id' => $model['department']['id']
                ),
            ));

            // Pass sub-department id's to array $sub_department
            foreach($model['sub_department'] as $value)
                $sub_department[$value['id']] = $value['id'];

            // Check if sub-department is found
            if (isset($sub_department)):

                $sub_dept_index = NULL;
                $is_first_index = TRUE;
                foreach ($sub_department as $value):

                    if ($is_first_index):
                        $sub_dept_index .= $value;
                        $is_first_index = FALSE;
                    else:
                        $sub_dept_index .= ',' . $value;
                    endif;

                endforeach;

                // Find Employees with sub-department pointing to department
                $model['employee'] = Employee::model()->findAll(array(
                    'condition' => "sub_department_id IN ({$sub_dept_index})",
                ));

                // Pass employee values to array $employee
                foreach ($model['employee'] as $value):
                    $employee[$value['emp_id']]['name'] = $value['fullname'];
                    $employee[$value['emp_id']]['emp_id'] = $value['emp_id'];
                    $employee[$value['emp_id']]['logs'] = $this->getEmployeeLogs($value['emp_id'], $_POST['date_interval']);
                endforeach;

                // Check if employee is found
                if (isset($employee))
                    echo ")]}',\n" . json_encode($employee);
                else
                    echo ")]}',\n" . json_encode(array());

            else:
                echo ")]}',\n" . json_encode(array());
            
            endif;

        endif;
    }

    public function actionOverrideLogs()
    {
        if (isset($_POST['emp_id'])):

            switch ($_POST['option']):

                case '1': // Add/Override Logs

                    // Find Corresponding Log
                    $logs = Logs::model()->find(array(
                        'condition' => 'emp_id = :emp_id AND date= :date',
                        'order' => 'id DESC',
                        'params' => array(
                            ':emp_id' => $_POST['emp_id'],
                            ':date' => $_POST['date'],
                        ),
                    ));

                    // Check if id value is not null            
                    if ($logs['id'] != NULL): // id exists (A corresponding log exist)

                        $logs->hr_logs = $_POST['hr_logs'];
                        $logs->hr_status = $_POST['hr_status'];
                        $logs->is_active = 1;

                        // Save
                        $logs->save();

                    else: // No log detected

                        $new_logs = new Logs;

                        $new_logs->emp_id = $_POST['emp_id'];
                        $new_logs->name = $_POST['emp_name'];
                        $new_logs->date = $_POST['date'];

                        $new_logs->hr_logs = $_POST['hr_logs'];
                        $new_logs->hr_status = $_POST['hr_status'];

                        // Save
                        $new_logs->save();

                    endif;
                break;

                case '2': // Set as Absent

                    // Step 1: Disable Current Log (If there are any)
                    $logs = Logs::model()->find(array(
                        'condition' => 'emp_id = :emp_id AND date= :date',
                        'order' => 'id DESC',
                        'params' => array(
                            ':emp_id' => $_POST['emp_id'],
                            ':date' => $_POST['date'],
                        ),
                    ));

                    if($logs['id'] != NULL): // Logs are found
                        $logs->is_active = 0;
                        $logs->save();
                    endif;

                    // Step 2: Deactivate Current Schedule
                    // Step 2: Deactivate Current Schedule
                    $this->deactivateSchedule($_POST['date'], $_POST['emp_id']);

                    // Step 3: Set Schedule based on shift assigned
                    $sched_model = new Schedule;

                    $sched_model->emp_id = $_POST['emp_id'];
                    $sched_model->date = $_POST['date'];
                    $sched_model->is_active = '1';
                    $sched_model->shift_id = $_POST['shift_assigned'];
                    $sched_model->is_restday = '0';

                    $sched_model->save();

                break;

                case '3': // Set as Sick Leave
                    
                    // Step 1: Disable Current Log (If there are any)
                    $logs = Logs::model()->find(array(
                        'condition' => 'emp_id = :emp_id AND date= :date',
                        'order' => 'id DESC',
                        'params' => array(
                            ':emp_id' => $_POST['emp_id'],
                            ':date' => $_POST['date'],
                        ),
                    ));

                    if($logs['id'] != NULL): // Logs are found
                        $logs->is_active = 0;
                        $logs->save();
                    endif;

                    // Step 2: Deactivate Current Schedule
                    $this->deactivateSchedule($_POST['date'], $_POST['emp_id']);

                    // Step 3: Set Schedule as Sick Leave
                    $sched_model = new Schedule;

                    $sched_model->emp_id = $_POST['emp_id'];
                    $sched_model->date = $_POST['date'];
                    $sched_model->is_active = '1';
                    $sched_model->is_leave = 3; // 3 Indicates sick leave

                    $sched_model->save();

                break;

                case '4': // Revert to original

                    // Phase 1: Restore original log
                    $logs = Logs::model()->find(array(
                        'condition' => 'emp_id = :emp_id AND date= :date',
                        'order' => 'id DESC',
                        'params' => array(
                            ':emp_id' => $_POST['emp_id'],
                            ':date' => $_POST['date'],
                        ),
                    ));

                    if ($logs['id'] != NULL):

                        if ($logs['checkin'] == NULL && $logs['checkout'] == NULL): // Product of overriding null logs. Delete it
                            $logs->delete();
                        
                        else:

                            $logs->is_active = 1;
                            $logs->hr_logs = NULL;
                            $logs->hr_status = NULL;
                            $logs->save();

                            // Phase 2: Restore original schedule
                            if ($logs['schedule'] != NULL):
                                $shift_array = explode(' - ', $logs['schedule']);

                                $shift = Shift::model()->find(array(
                                    'condition' => 'start_time = :start_time AND end_time = :end_time',
                                    'params' => array(
                                        ':start_time' => $shift_array[0],
                                        ':end_time' => $shift_array[1],
                                    )
                                ));

                                $this->deactivateSchedule($_POST['date'], $_POST['emp_id']);
                                $sched_model = new Schedule;

                                $sched_model->emp_id = $_POST['emp_id'];
                                $sched_model->date = $_POST['date'];
                                $sched_model->is_active = '1';
                                $sched_model->shift_id = $shift['id'];
                                $sched_model->is_restday = '0';

                                $sched_model->save();

                            else:
                                echo 'empty schedule';

                            endif;
                        endif;
                    endif;

                break;

            endswitch;

        endif;
    }

    /**
     * Public Function actionUpload
     *
     * Upload Logs Interface. 
     * Access By: HR
     */
    public function actionUpload()
    {
        // Generate Form Model LogsForm
        $data['model']['logs_form'] = new LogsForm;

        $this->render('upload', $data);
    }

    /**
     * Public Function actionGenerateUpload
     *
     * Route to upload logs into database.
     * Access By: HR
     */
    public function actionGenerateUpload()
    {
        var_dump($_POST);

        // Initialize Model
        $form = new LogsForm;

        if ( isset($_POST['LogsForm']) ):

            $form->attributes = $_POST['LogsForm'];

            // Determine extension type and file type (For file format filtering)
            $extension = pathinfo($_FILES['LogsForm']['name']['logs'], PATHINFO_EXTENSION) == 'csv';
            $type = $_FILES['LogsForm']['type']['logs'] == 'text/csv' || $_FILES['LogsForm']['type']['logs'] == 'application/vnd.ms-excel';

            // Determine if not empty, and is a .csv file
            if( !empty($_FILES['LogsForm']['tmp_name']['logs']) && ($extension && $type) ):
                
                $form_logs = CUploadedFile::getInstance($form,'logs');
                $logs = $_FILES['LogsForm']['tmp_name']['logs'];

                // Initialize Variables Here
                $emp_sched = array();
                $sched_range = array();
                $attendance = array();

                // Initialize Resetting Variables
                $date = array();
                $time = array();

                // Get list of Employees (All, excluding resigned ones)
                $employee_list = Employee::model()->findAll(array(
                    "condition" => "status_id = :status",
                    "order" => "lastname",
                    "params" => array(
                        ":status" => 1
                    )
                ));

                // Phase 1: Disable overridden logs

                    // Put schedule date range in array $sched_range
                    $is_header_loaded = FALSE;
                    $handle = fopen($form_logs->tempName, 'r'); // Set Handle

                    while ( ($logs = fgetcsv($handle,0,",")) !== FALSE ):
                        // Initialize Resetting Variables
                        $time = array();
                        $date = array();
                            
                        if ($is_header_loaded): // Used to ignore first row of CSV (Which contains desc) to avoid PHP Notices
                        
                            // Explode time to get date
                            $time = explode(" ", $logs[3]);
                            $date = explode("/", $time[0]);
                            $date['date'] = $date[2] . '-' . $date[0] . '-' . $date[1];
                                
                            $sched_range[$date['date']] = $date['date'];
                        
                        else:

                            // Verify if the user has uploaded the right csv file through the log header. The format should be as shown below
                            // Department | *Name | *No. | *Date/Time | *Status | Location | ID Number | VerifyCode | CardNo
                            // (Fields with * are necessary)
                            // Throw an exception if the wrong csv file is uploaded

                            if ($logs[1] != 'Name' || $logs[2] != 'No.' || $logs[3] != "Date/Time" || $logs[4] != "Status")
                                throw new CHttpException(500,'Wrong comma separated value (.csv) file format. Please follow the format as shown in the sample file.');

                        endif;

                        $is_header_loaded = TRUE;
                    endwhile;

                    // Determines max and min ranges of date
                    $greatest = NULL;
                    $least = NULL;
                    foreach ($sched_range as $range)
                    {
                        if ($greatest == NULL)
                            $greatest = $range;
                            
                        else if ( strtotime($range) >= strtotime($greatest) )
                            $greatest = $range;
                    
                        if ($least == NULL)
                            $least = $range;
                    
                        else if ( strtotime($range) <= strtotime($least) )
                            $least = $range;
                    }

                    // Disable Overridden logs

                        // Filtered Logs
                        Logs::model()->updateAll(
                            array( 
                                'is_active' => 0,
                            ), 
                            "date >= '{$least}' AND date <= '{$greatest}'"
                        );

                // End Phase 1

                $date_input = NULL;

                // Phase 2: Begin .csv insertion process

                    $handle = fopen($form_logs->tempName, 'r'); // Set Handle
                    while ( ($logs = fgetcsv($handle,0,",")) !== FALSE ):

                        #echo "<pre>" . print_r($logs,1) . "</pre>";
                        foreach ($employee_list as $emp):

                            if ($logs[2] == $emp->emp_id)
                            {                       
                                // Explode time to get date
                                    $time = explode(" ", $logs[3]);
                                    $date = explode("/", $time[0]);
                                    $date['date'] = $date[2] . '-' . $date[0] . '-' . $date[1];
            
                                // Determine time process whether 12 hour or 24 hour format
                                    if (isset($time[2]))
                                        $time['24h'] = date('H:i:s', strtotime($time[1] . ' ' . $time[2]));
                                    else
                                        $time['24h'] = date('H:i:s', strtotime($time[1]));
                                    
                                // Initialize User
                                    $attendance[$logs['2']][$date['date']]['name'] = $logs['1'];
                                    $attendance[$logs['2']][$date['date']]['id'] = $logs['2'];
                                    $attendance[$logs['2']][$date['date']]['date'] = $date['date'];
                                
                                if ($logs[4] == 'C/In')
                                {
                                    if ( ! isset($attendance[$logs['2']][$date['date']]['checkin']) )
                                    {
                                        $attendance[$logs['2']][$date['date']]['checkin'] = $time['24h'];
                                        $date_input = $date['date'];
                                    }
                                    else if ( (strtotime($time['24h']) - strtotime($attendance[$logs['2']][$date['date']]['checkin'])) < 0 )
                                    {
                                        $attendance[$logs['2']][$date['date']]['checkin'] = $time['24h'];
                                        $date_input = $date['date'];
                                    }
                                }
                                
                                else if ($logs[4] == 'C/Out')
                                {
                                    if (isset($attendance[$logs['2']][$date_input]['checkin']))
                                    {
                                        if ( (strtotime($attendance[$logs['2']][$date_input]['checkin']) - strtotime($time['24h'])) > 0)
                                        {
                                            //$attendance[$logs['2']][$date_input]['checkout'] = $time['24h'];
                                            
                                            if ( ! isset($attendance[$logs['2']][$date_input]['checkout']) )
                                                $attendance[$logs['2']][$date_input]['checkout'] = $time['24h'];
                                            else if ( (strtotime($time['24h']) - strtotime($attendance[$logs['2']][$date_input]['checkout'])) > 0 )
                                                $attendance[$logs['2']][$date_input]['checkout'] = $time['24h'];
                                        }
                                        else
                                        {
                                            if ( ! isset($attendance[$logs['2']][$date['date']]['checkout']) )
                                                $attendance[$logs['2']][$date['date']]['checkout'] = $time['24h'];
                                            else if ( (strtotime($time['24h']) - strtotime($attendance[$logs['2']][$date['date']]['checkout'])) > 0 )
                                                $attendance[$logs['2']][$date['date']]['checkout'] = $time['24h'];
                                        }
                                    }
                                }
                                
                                $date_index = $date['date'];
                            }

                        endforeach;

                    endwhile;

                    foreach ($attendance as $att):
                        foreach ($att as $att2):
                            if (isset($att2['name']) && isset($att2['id']) && isset($att2['date']) && (isset($att2['checkin']) || isset($att2['checkout']) )):
                        
                                if (!isset($att2['checkin']))
                                    $att2['checkin'] = NULL;
                                else
                                    $att2['checkin'] =  $att2['checkin'];
                                
                                if (!isset($att2['checkout']))
                                    $att2['checkout'] = NULL;
                                else
                                    $att2['checkout'] = $att2['checkout'];

                                // Explode Date
                                $att_date = explode('-', $att2['date']);
                                if (strlen($att_date[1]) >= 4)
                                    $att_date['date'] = $att_date[1] . '-' . $att_date[0] . '-' . $att_date[2];
                                else
                                    $att_date['date'] = $att_date[0] . '-' . $att_date[1] . '-' . $att_date[2];
                                
                                // Used for debugging. Display Results
                                #echo "Emp ID: {$att2['id']} Name: {$att2['name']} Date: {$att_date['date']} Checkin: {$att2['checkin']} Checkout: {$att2['checkout']} <br />";

                                // Get Schedule Data
                                $sched_array = $this->getLogStatus($att2['id'], $att2['checkin'], $att2['checkout'], $att_date['date']);

                                // Save Log instance to database
                                $employee_logs = new Logs;
                                $employee_logs->emp_id = $att2['id'];
                                $employee_logs->name = $att2['name'];
                                $employee_logs->date = $att_date['date'];
                                $employee_logs->checkin = $att2['checkin'];
                                $employee_logs->checkout = $att2['checkout'];
                                $employee_logs->status = $sched_array['schedule_id'];
                                $employee_logs->schedule = $sched_array['schedule_name'];

                                $employee_logs->save();

                            endif;
                        endforeach;
                    endforeach;

                // End Phase 2

                // Phase 3: Redirect
                Yii::app()->request->redirect( Yii::app()->getBaseUrl(true) . '/index.php/logs');

            else:
                throw new CHttpException(500,'Please upload a comma separated value (.csv) file.');

            endif; // End if

        else:
            $this->redirect(Yii::app()->getBaseUrl(true) . "/index.php/logs");
            #echo "error";

        endif; // End if

    }

}

// End of file controllers/logsController.php