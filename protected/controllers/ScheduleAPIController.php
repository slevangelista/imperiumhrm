<?php

/**
 * ScheduleAPI Controller
 * Created By: Syd Evangelista
 * 
 * Class containing protected API Methods for ScheduleController
 * Extend ScheduleController to this class for ease of use
 *
 */

class ScheduleAPIController extends RController
{

    /**
     * Filter Route. Used for user authentication
     *
     */
    public function filters()
    {
        return array(
            'rights',
        );
    }

    /**
     * Protected function deactivateSchedule()
     * 
     * Sets is_active to 0, based on the given parameters
     * @param $date
     * @param $emp_id
     */
    protected function deactivateSchedule($date, $emp_id)
    {
        Schedule::model()->updateAll(
            array(
                'is_active' => 0,
            ),
            "date = :date AND emp_id = :emp_id",
            array(
                ":date" => $date,
                ":emp_id" => $emp_id
            )
        );
    }

    /**
     * Protected function getWeeklyCutoff
     *
     * Returns date cutoff (weekly) of current month and year
     */
    protected function getWeeklyCutoff($year, $month, $day)
    {
        // Initialize data array
        $data = array();

        // Get current date data
        $current_date = date('Y-m-d', strtotime("{$year}-{$month}-{$day}"));

        // Calculate no. of days within current month and year
        $no_of_days = cal_days_in_month(CAL_GREGORIAN, $month, $year);

        // Determine start and end cutoff periods (start = Every Monday of mo. , end = Every Sunday of mo)
        for ($i = 0; $i < $no_of_days; $i++):

                // Get Start Date
                $start_date = date('Y-m-d', strtotime("{$year}-{$month}-1  +{$i} days"));

                // Get current type of day (Mon-Sun)
                $day = date("D", strtotime($start_date));

                // If day == Mon
                if ($day == "Mon"):

                        // Get End Date
                        $end_date = date('Y-m-d', strtotime($start_date . " +6 days"));

                        // Put start and end date values into data array
                        $data['range'][$start_date . ' - ' . $end_date] = $start_date . ' - ' . $end_date;

                        // Get what cutoff current day belongs to
                        if ( strtotime($start_date) <= strtotime($current_date) && strtotime($end_date) >= strtotime($current_date) )
                                $data['current'] = $start_date . ' to ' . $end_date;

                endif;

        endfor;
        
        // Return data
        return $data;
    }

    /**
     * Protected function getDays
     *
     * Returns date ranges between start and end date
     *
     * @param string $start_date
     * @param string $end_date
     */
    protected function getDays($start_date, $end_date) 
    {
        $date_interval = (strtotime ( $end_date ) - strtotime ( $start_date )) / 86400; // 1 day = 86400 seconds
        $date = array ();
        
        for($i = 0; $i <= $date_interval; $i ++) 
        {
            $date [$i] ['day'] = date ( 'D', strtotime ( $start_date . ' + ' . $i . ' days' ) );
            $date [$i] ['month'] = date ( 'M', strtotime ( $start_date . ' + ' . $i . ' days' ) );
            $date [$i] ['date'] = date ( 'j', strtotime ( $start_date . ' + ' . $i . ' days' ) );
            $date [$i] ['year'] = date ( 'Y', strtotime ( $start_date . ' + ' . $i . ' days' ) );
            $date [$i] ['date_map'] = date ( 'Y', strtotime ( $start_date . ' + ' . $i . ' days' ) ) . '-' . date ( 'm', strtotime ( $start_date . ' + ' . $i . ' days' ) ) . '-' . date ( 'd', strtotime ( $start_date . ' + ' . $i . ' days' ) );
        }
        
        return $date;
    }

    public function getScheduleByEmployee($emp_id, $currentWeek)
    {
        if ( isset($currentWeek) && isset($emp_id) ):

            $currentWeek = array_keys($currentWeek); // Get key as value
            $schedule = array(); // Will contain array value of result. Will be encoded to JSON later in the program

            foreach ($currentWeek as $date):

                // Call model schedule, pass values of emp_id and date as conditions
                $model['schedule'] = Schedule::model()->find(array(
                    'condition' => 'date = :date AND emp_id = :emp_id AND is_active = 1',
                    'params' => array(
                        ':date' => $date,
                        ':emp_id' => $emp_id
                    ),
                ));

                // Format Start Time and End Time
                $start_time = date('H:i', strtotime($model['schedule']['shift']['start_time']));
                $end_time = date('H:i', strtotime($model['schedule']['shift']['end_time']));

                // Check if there is no schedule
                if ($model['schedule'] == NULL):
                    $schedule[$date]['schedule'] = '';
                    $schedule[$date]['name'] = "No Schedule";
                    $schedule[$date]['color'] = "#FFFFFF";
                    $schedule[$date]['shift_id'] = NULL;
                    $schedule[$date]['restday'] = NULL;
                    $schedule[$date]['leave'] = NULL;
                    $schedule[$date]['holiday'] = NULL;
                
                // If there is schedule
                else:
                    
                    // Check if restday or leave or holiday
                    if ($model['schedule']['is_restday']): // Restday
                        $schedule[$date]['schedule'] = '';
                        $schedule[$date]['name'] = 'Restday';
                        $schedule[$date]['color'] = '#BDBDBD';
                        $schedule[$date]['shift_id'] = $model['schedule']['shift_id'];
                        $schedule[$date]['restday'] = '1';
                        $schedule[$date]['leave'] = NULL;
                        $schedule[$date]['holiday'] = NULL;

                    elseif ($model['schedule']['is_leave']): // Leave

                        // Determine Leave Type
                        $leave_type = $model['schedule']['isLeave']['name'];

                        $schedule[$date]['schedule'] = '';
                        $schedule[$date]['name'] = $leave_type . ' Leave';
                        $schedule[$date]['color'] = '#E1F5A9';
                        $schedule[$date]['shift_id'] = $model['schedule']['shift_id'];
                        $schedule[$date]['restday'] = NULL;
                        $schedule[$date]['leave'] = $model['schedule']['is_leave'];
                        $schedule[$date]['holiday'] = NULL;

                    elseif ($model['schedule']['is_holiday']): // Holiday

                        // Determine Holiday Type
                        $holiday = $model['schedule']['isHoliday']['name'];

                        $schedule[$date]['schedule'] = '';
                        $schedule[$date]['name'] = $holiday;
                        $schedule[$date]['color'] = '#A9F5BC';
                        $schedule[$date]['shift_id'] = $model['schedule']['shift_id'];
                        $schedule[$date]['restday'] = NULL;
                        $schedule[$date]['leave'] = NULL;
                        $schedule[$date]['holiday'] = $model['schedule']['is_holiday'];

                    else: // Not Restday or Leave
                        $schedule[$date]['schedule'] = "(" . $start_time . " - " . $end_time . ")";
                        $schedule[$date]['name'] = $model['schedule']['shift']['name'];
                        $schedule[$date]['color'] = $model['schedule']['shift']['color'];
                        $schedule[$date]['shift_id'] = $model['schedule']['shift_id'];
                        $schedule[$date]['restday'] = NULL;
                        $schedule[$date]['leave'] = NULL;
                        $schedule[$date]['holiday'] = NULL;

                    endif;

                endif;

            endforeach;

            return $schedule;

        endif;
    }

    protected function updateLogSchedule($emp_id, $date, $no_schedule = FALSE)
    {
        // If 3rd parameter is set to TRUE (Meaning no schedule)
        if ($no_schedule):
            Logs::model()->updateAll(
                array(
                    'status' => 1, // 1 Indicates No Schedule
                    'schedule' => '',
                ),
                "date = :date AND emp_id = :emp_id",
                array(
                    ":date" => $date,
                    ":emp_id" => $emp_id
                )
            );
        // Else
        else:

            // Phase 1: Query log checkin and out
                $logs = Logs::model()->find(array(
                    'condition' => 'date = :date AND emp_id = :emp_id AND is_active = 1',
                    'params' => array(
                        ':emp_id' => $emp_id,
                        ':date' => $date,
                    ),
                ));

                // Check if HR Logs has value
                if ($logs['hr_logs'] != NULL):
                    $hr_logs = explode(' - ', $logs['hr_logs']);
                    $checkin = $hr_logs[0];
                    $checkout = $hr_logs[1];
                // Else if no HR Logs (Use default checkin and out)
                else:
                    $checkin = $logs['checkin'];
                    $checkout = $logs['checkout'];
                endif;

            // Phase 2: Query Schedule
                $schedule = Schedule::model()->find(array(
                    'condition' => 'date = :date AND emp_id = :emp_id AND is_active = 1',
                    'params' => array(
                        ':emp_id' => $emp_id,
                        ':date' => $date,
                    ),
                ));

                // Determine Shift
                $schedin = $schedule['shift']['start_time'];
                $schedout = $schedule['shift']['end_time'];

            // Phase 3: Determine status based on log and new status

                // Check if Restday
                if ($schedule['is_restday']):
                    $status = 'Restday Work';
                    $sched_log = '';
                elseif ($schedule['is_leave']):
                    $status = 'Leave Work';
                    $sched_log = '';
                elseif ($schedule['is_holiday']):
                    $status = 'Holiday Work';
                    $sched_log = '';
                else:
                    $sched_log = $schedin . ' - ' . $schedout;

                    // Check if Half-day - No Login
                    if ($checkin == NULL && $checkout != NULL):
                        $status = 'Halfday (No Login)';

                    // Check if Half-day - No Logout
                    elseif ($checkin != NULL && $checkout == NULL):
                        $status = 'Halfday (No Logout)';
                        
                    // Check if Half-day - Late by more than 1 hour
                    elseif ( ( strtotime($schedin) - strtotime($checkin) ) < -3600 ): // 3600 indicates 1 hour
                        $status = 'Halfday (More than 1 hr late)';

                    // Check if Late
                    elseif ( ( strtotime($schedin) - strtotime($checkin) ) < -60 && // At least 1 minute late AND
                         ( strtotime($schedin) - strtotime($checkin) ) >= -3600 ): // Must not be greater that 1 hour
                        $status = 'Late';

                    else:
                        $status = 'Normal';

                    endif;

                endif;

            // Phase 4: Get Equivalent id of the said table
            $log_status = LogStatus::model()->find(array(
                "condition" => "name = :name",
                "params" => array(
                    ":name" => $status,
                ),
            ));

            // Phase 5: Update log schedule and status
            Logs::model()->updateAll(
                array(
                    'status' => $log_status['id'],
                    'schedule' => $sched_log,
                ),
                "date = :date AND emp_id = :emp_id",
                array(
                    ":date" => $date,
                    ":emp_id" => $emp_id
                )
            );

        endif;
    }

    protected function getEmployeeDetails()
    {
        $data = array();

        // Get Employee ID
        $data['user_id'] = Yii::app()->user->id;
        $data['username'] = Yii::app()->user->name;
        $data['emp_id'] = Users::model()->userEmpId($data['user_id'], $data['username']);

        return $data;
    }

    /**
     * Protected function getTotalHours
     *
     * Returns total hours of work (Excluding 1 hour break)
     *
     * @param string $start_time
     * @param string $end_time
     * @param string $date
     */
    protected function getTotalHours($start_time, $end_time, $date)
    {
        $duration = strtotime($end_time) - strtotime($start_time);
        if ($duration < 0)
        {   
            $date_tomorrow = date('Y-m-d', strtotime($date . ' +1 days'));
            $duration = strtotime($date_tomorrow . ' ' . $end_time) - strtotime($date . ' ' . $start_time);
        }
        
        $duration_in_hours = $duration / 3600;
        return $duration_in_hours - 1; // -1 Hours (Because of break time)
    }

/**
 * Yii Generated Methods
 *
 */

}