<?php

class OtController extends RController
{

//	*
  //  * Filter Route. Used for user authentication
   // *
     
    // public function filters()
    // {
    //     return array(
    //         'rights',
    //     );
    // }

	/*view FileOT */
	/*Filing Logic*/
	public function actionFileOt()
	{
		
		$user_id = Yii::app()->user->id;
        $username = Yii::app()->user->name;
        $emp_id = Users::model()->userEmpId($user_id,$username);

		$data['model']['employee'] = Employee::model()->find(array(
			'condition' => 'emp_id = :emp_id',
			'params' => array(
				':emp_id' => $emp_id,
			)
		));

		$position_id = $data['model']['employee']['position_id'];

		$data['model']['position'] = Position::model()->find(array(
			'condition' => 'id = :heirarchy_id',
			'params' => array(
					':heirarchy_id' => $position_id,
				)
			));
		$heirarchy_id =  $data['model']['position']['hierarchy_id'];

       //sorts tbl_log via date&emp_id 
        $date =  date("Y-m-d");
        $data['model']['logs'] = Logs::model()->find(array(
			'condition' => 'date = :date && emp_id = :emp_id',
			'params' => array(
				':date' => $date,
				':emp_id' =>$emp_id,
				)
       	));


         if(isset($_POST['back'])) 
         {
         Yii::app()->request->redirect( Yii::app()->getBaseUrl(true) . '/index.php/site/index');
         }
         if(isset($_POST['submit'])) 
         {
          	
          	if ($heirarchy_id!=4){
            $heirarchy_id = $heirarchy_id + 1;	
          	$model_ot_request = new OtRequest;
         	$model_ot_request->emp_id = $emp_id;
         	$model_ot_request->start_time = $_POST['hours'];
         	$model_ot_request->date = $_POST['current_date'];
         	$model_ot_request->remarks = $_POST['remarks'];
         	$model_ot_request->position_id = $heirarchy_id;
         	$model_ot_request->request_type_id = 3;
         	$model_ot_request->save();
         	Yii::app()->request->redirect( Yii::app()->getBaseUrl(true) . '/index.php/ot/status');
         	}
         	else {		
         	$model_ot_request = new OtRequest;
         	$model_ot_request->emp_id = $emp_id;
         	$model_ot_request->start_time = $_POST['hours'];
         	$model_ot_request->date = $_POST['current_date'];
         	$model_ot_request->remarks = $_POST['remarks'];
         	$model_ot_request->position_id = $heirarchy_id;
         	$model_ot_request->request_type_id = 3;
         	$model_ot_request->save();
         	Yii::app()->request->redirect( Yii::app()->getBaseUrl(true). '/index.php/ot/status');	


         		 }

         }	



        $this->render('FileOt', $data);
	}

	/*view pending Ot*/
	public function actionOtPending()
	{ 
	    $this->render('pending', $Otdata);
	}
	
	public function actionGetEmployeePendingRequest()
	{
		$ot_request_array = array();
		$user_id = Yii::app()->user->id;
        $username = Yii::app()->user->name;
        $emp_id = Users::model()->userEmpId($user_id,$username);

        $model['employee'] = Employee::model()->find(array(
			'condition' => 'emp_id = :emp_id',
			'params' => array(
				':emp_id' => $emp_id,
				
			)
		));

        $id['department'] =  $model['employee']['subDepartment']['department_id'];
	    $id['sub_department'] = $model['employee']['sub_department_id'];
	    $id['hierarchy'] =  $model['employee']['position']['hierarchy_id'];
	    $id['position'] = $model['employee']['position_id'];

	    $department = $Otdata['model']['employee']['subDepartment']['department']['name'];

	    $model['ot_request'] = OtRequest::model()->findAll(array(
			'condition' => 'position_id = :position_id AND request_type_id = :request_type_id',
			'params' => array(
				':position_id' =>  $id['hierarchy'],				    			
				':request_type_id' => 3,
			)
		));

	    if ( $id['hierarchy'] == 2)
	    {
		    foreach ($model['ot_request'] as $value)
		    {
		    	// If user department id equals the department id of pending employee
		  		if ($value['emp']['subDepartment']['department_id'] == $id['department'])
		  		{
		  			$ot_request_array[$value['id']] = $this->processOTPending($model, $value);
		  		}
		  	}
	    }
	    else
	    {
			foreach ($model['ot_request'] as $value)
		  		$ot_request_array[$value['id']] = $this->processOTPending($model, $value);
	    }

	    echo json_encode($ot_request_array);
	}

	protected function processOTPending($model, $value)
	{
		$ot_request_array['emp_name'] = $value['emp']['fullname'];
  		$ot_request_array['date'] = $value['date'];
  		$ot_request_array['start_time'] = $value['start_time'];

  		// Get end time data, render to td
  		$model['logs'] = Logs::model()->find(array(
  			//'select' => 'DATE_FORMAT(checkout, "%H:%I") as checkout',
  			'condition' => 'date = :date AND emp_id = :emp_id',
  			'params' => array(
  				':date' => $value['date'],
  				':emp_id' => $value['emp_id']
			)
  		));

  		if (!empty($model['logs']['hr_logs']))
  		{
  			$hr_logs = explode(" - ", $model['logs']['hr_logs']);
  			$end_time = $hr_logs[1];

  			$ot_request_array['end_time'] = $end_time;
  		}
  		else if ($model['logs']['checkout']!=NULL)	
  		{		
  			$end_time = $model['logs']['checkout'];
  			$ot_request_array['end_time'] = $end_time;
  		}
  		else 
  		{
  			$ot_request_array['end_time'] = 'No Checkout';
  		}
	  			
		// Compute total hours, render to td
		$duration = strtotime($end_time) - strtotime($value['start_time']);
		if ($duration < 0) // Indicates Night Shift
		{	
			$checkout_date = date('Y-m-d', strtotime($value['date'] . ' +1 days'));
			$duration = strtotime($checkout_date . ' ' . $end_time) - strtotime($value['date'] . ' ' . $value['start_time']);
		}	
		$total_hours = $duration / 3600;

		$ot_request_array['total_hours'] = $total_hours;
		$ot_request_array['remarks'] = $value['remarks'];
	  		
	  	//update database totalhours,endtime
	  	OtRequest::model()->updateAll(
			array(
			'total_hours' => $total_hours,  	
			'end_time' => $end_time,
		),
		"id = :id",
		array(
			":id" => $value['id'],
		)); 

	  	//Auto Dissaprove Ot if less than 2 hrs
	  	if ($total_hours<2)
	  	{
			OtRequest::model()->updateAll(
			array(
				'total_hours' => $total_hours,  	
				'end_time' => $end_time,
				'request_type_id' => 2,
				'position_id' =>1,
				'request_dissaproval' => "Dissapoved: Your application for OT is below minimun"  				
			),
			"id = :id",
			array(
				":id" => $value['id'],
			)); 
		}

		return $ot_request_array;
	}

	protected function renderOTPending($model, $id)
	{
		$html = NULL; // Initialize html string variable;
		$html .= "
			<div class =  'Pending_ot_forms'>  
			<b><h2>Pending OT Request</h2></b>
			<br>
			
			<table class  = 'table'>
			<tr>
		 	    <td><b> Name </b></td>
		 	    <td><b> Date </b></td>
	  	 	    <td><b> Start Time</b></td>
		   	    <td><b> End Time </b></td>
			   	<td><b> Total Hours </b></td>
			   	<td><b> Remarks</b> </td>
		        <td>	
			 </tr>
		";
	if ( $id['hierarchy'] == 2){

		foreach ($model['ot_request'] as $value)
		{
			// If user department id equals the department id of pending employee
	  		if ($value['emp']['subDepartment']['department_id'] == $id['department'])
	  		{
	  			$html .= "<tr>";

		  			$html .= "<td>{$value['emp']['fullname']}</td>";
		  			$html .= "<td>{$value['date']}</td>";
		  			$html .= "<td>{$value['start_time']}</td>";
		  			
		  			// Get end time data, render to td
		  			$model['logs'] = Logs::model()->find(array(
		  				//'select' => 'DATE_FORMAT(checkout, "%H:%I") as checkout',
		  				'condition' => 'date = :date AND emp_id = :emp_id',
		  				'params' => array(
		  					':date' => $value['date'],
		  					':emp_id' => $value['emp_id']
						)
		  			));

		  			if (!empty($model['logs']['hr_logs']))
		  			{
		  				$hr_logs = explode(" - ", $model['logs']['hr_logs']);
		  				$end_time = $hr_logs[1];
		  				$html .= "<td>{$end_time}</td>";
		  			}
		  			elseif ($model['logs']['checkout']!=NULL)	{		
		  				$end_time = $model['logs']['checkout'];
		  				$html .= "<td>{$end_time}</td>";
		  			}
		  			else{
		  			$html .= "<td>No Checkout</td>";
		  			}
		  			
		  			// Compute total hours, render to td
		  			$duration = strtotime($end_time) - strtotime($value['start_time']);
					if ($duration < 0) // Indicates Night Shift
					{	
						$checkout_date = date('Y-m-d', strtotime($value['date'] . ' +1 days'));
						$duration = strtotime($checkout_date . ' ' . $end_time) - strtotime($value['date'] . ' ' . $value['start_time']);
					}	
					$total_hours = $duration / 3600;

		  			$html .= "<td>{$total_hours}</td>";
		  			$html .= "<td>{$value['remarks']}</td>";
		  		
		  			//update database totalhours,endtime
		  			OtRequest::model()->updateAll(
					array(
						'total_hours' => $total_hours,  	
						'end_time' => $end_time,
					),
					"id = :id",
					array(
					":id" => $value['id'],
			
					)); 
		  		    //Auto Dissaprove Ot if less than 2 hrs
		  			if ($total_hours<2){

					OtRequest::model()->updateAll(
					array(
						'total_hours' => $total_hours,  	
						'end_time' => $end_time,
						'request_type_id' => 2,
						'position_id' =>1,
						'request_dissaproval' => "Dissapoved: Your application for OT is below minimun"  	
						
					),
					"id = :id",
					array(
					":id" => $value['id'],
			
					)); 

					}	
		  			// Render Buttons
		  			$html .= "<td>
		  				<button class='btn btn-success btn-xs'  ng-click='approveOT({$value['id']})'><span class='glyphicon glyphicon-ok'></span></button>
		  				<button class='btn btn-danger btn-xs' ng-click='dissaproveOT({$value['id']})'><span class='glyphicon glyphicon-remove'></span></button>
		  			</td>";

	  			$html .= "</tr>";
	  		}
	  	}
   }	
  	      else {


  					foreach ($model['ot_request'] as $value)
		      {
					
	  				$html .= "<tr>";
		  			$html .= "<td>{$value['emp']['fullname']}</td>";
		  			$html .= "<td>{$value['date']}</td>";
		  			$html .= "<td>{$value['start_time']}</td>";
		  			
		  			// Get end time data, render to td
		  			$model['logs'] = Logs::model()->find(array(
		  				//'select' => 'DATE_FORMAT(checkout, "%H:%I") as checkout',
		  				'condition' => 'date = :date AND emp_id = :emp_id',
		  				'params' => array(
		  					':date' => $value['date'],
		  					':emp_id' => $value['emp_id']
						)
		  			));

		  			if (!empty($model['logs']['hr_logs']))
		  			{
		  				$hr_logs = explode(" - ", $model['logs']['hr_logs']);
		  				$end_time = $hr_logs[1];
		  				$html .= "<td>{$end_time}</td>";
		  			}
		  			elseif ($model['logs']['checkout']!=NULL)	{		
		  				$end_time = $model['logs']['checkout'];
		  				$html .= "<td>{$end_time}</td>";
		  			}
		  			else{
		  			$html .= "<td>No Checkout</td>";
		  			}
		  			
		  			// Compute total hours, render to td
		  			$duration = strtotime($end_time) - strtotime($value['start_time']);
					if ($duration < 0) // Indicates Night Shift
					{	
						$checkout_date = date('Y-m-d', strtotime($value['date'] . ' +1 days'));
						$duration = strtotime($checkout_date . ' ' . $end_time) - strtotime($value['date'] . ' ' . $value['start_time']);
					}	
					$total_hours = $duration / 3600;

		  			$html .= "<td>{$total_hours}</td>";
		  			$html .= "<td>{$value['remarks']}</td>";


		  			//updates database totalhours,endtime
		  			OtRequest::model()->updateAll(
					array(
						'total_hours' => $total_hours,  	
						'end_time' => $end_time,
					),
					"id = :id",
					array(
					":id" => $value['id'],
			
					)); 
			
		  		    //Auto Dissaprove Ot if less than 2 hrs
		  			if ($total_hours<2){

					OtRequest::model()->updateAll(
					array(
						'total_hours' => $total_hours,  	
						'end_time' => $end_time,
						'request_type_id' => 2,
						'position_id' =>1,
						'request_dissaproval' => "Dissapoved: Your application for OT is below minimun"  	
						
					),
					"id = :id",
					array(
					":id" => $value['id'],
			
					));
				    }	

		  			// Render Buttons
		  			$html .= "<td>
		  				<button class='btn btn-success btn-xs'  ng-click='approveOT({$value['id']})'><span class='glyphicon glyphicon-ok'></span></button>
		  				<button class='btn btn-danger btn-xs' ng-click='dissaproveOT({$value['id']})'><span class='glyphicon glyphicon-remove'></span></button>
		  			</td>";

	  			   $html .= "</tr>";
	  		}
  			     }

  		$html .= "</table>";
  		return $html;

	}

	/*Ot Status*/
	public function actionStatus()
	{
		// Ot_Status
		//view/ot/OtStatus.php
     	
     	$user_id = Yii::app()->user->id;
        $username = Yii::app()->user->name;
        $emp_id = Users::model()->userEmpId($user_id,$username);
	    $data['model']['ot_request'] = OtRequest::model()->findAll(array(
			'condition' => 'emp_id = :emp_id',
			'order' => 'date DESC',
			'params' => array(
				':emp_id' => $emp_id,
			)
		));
	    


        $data['result'] = $this->renderStatus($data['model']);	
   
        $model=new OtRequest('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['OtRequest']))
			$model->attributes=$_GET['OtRequest'];
		$model->emp_id = $emp_id;
		

		$model_request_type = RequestType::model()->findAll();
        $request_type_array = array();
        foreach ($model_request_type as $mr) {
        	$id = $mr['id'];
        	$name = $mr['name'];
        	$request_type_array[$id] = $name;
        }

		// $this->render('Status',array('model'=>$model,));
		$this->render('Status', compact('model', 'request_type_array'));
	 	 
	}

	protected function renderStatus($data)
	{
		$html = '<table></table>';
		$html = NULL; // Initialize html string variable;
		$html .= "
				
			<b><h2>Submitted OT Request</h2></b>
			<br>
			<div class ='submitted_ot_forms_data'> 
			<table class  = 'table'>
			<tr>
		 	    <td> Name </td>
		 	    <td> Date </td>
	  	 	    <td> Start Time</td>
		   	    <td> End Time </td>
			   	<td> Total Hours </td>
			   	<td> Remarks </td>
		        <td>	
			 </tr>
		";
		foreach ($data['ot_request'] as $value) 
			{
				$html .= "<tr>";

		  			$html .= "<td>{$value['emp']['fullname']}</td>";
		  			$html .= "<td>{$value['date']}</td>";
		  			$html .= "<td>{$value['start_time']}</td>";
					$model['logs'] = Logs::model()->find(array(
		  				//'select' => 'DATE_FORMAT(checkout, "%H:%I") as checkout',
		  				'condition' => 'date = :date AND emp_id = :emp_id',
		  				'params' => array(
		  					':date' => $value['date'],
		  					':emp_id' => $value['emp_id']
						)
		  			));

		  			if (!empty($model['logs']['hr_logs']))
		  			{
		  				$hr_logs = explode(" - ", $model['logs']['hr_logs']);
		  				$end_time = $hr_logs[1];
		  			}
		  			else
		  			{
		  				$end_time = $model['logs']['checkout'];
		  			}
		  			
		  			$html .= "<td>{$end_time}</td>";
		  			
		  			// Compute total hours, render to td
		  			$duration = strtotime($end_time) - strtotime($value['start_time']);
					if ($duration < 0) // Indicates Night Shift
					{	
						$checkout_date = date('Y-m-d', strtotime($value['date'] . ' +1 days'));
						$duration = strtotime($checkout_date . ' ' . $end_time) - strtotime($value['date'] . ' ' . $value['start_time']);
					}	
					$total_hours = $duration / 3600;

		  			$html .= "<td>{$total_hours}</td>";
		  			$html .= "<td>{$value['remarks']}</td>";
		  			
		  			//display Status/position
		  			$modelStatus['request_type'] = RequestType::model()->find(array(
		  				'condition' => 'id = :request_type',
		  				'params' => array(
		  				   	':request_type' => $value['request_type_id'],		
		  				   	) 
		  				)); 
		  			$modelPosition['position'] = Position::model()->find(array(
		  				'condition' => 'hierarchy_id = :position',
		  				'params' => array(
		  					':position' => $value['position_id'],
		  					)  
		  				));

		  			OtRequest::model()->updateAll(array(
						'total_hours' => $total_hours,  	
						'end_time' => $end_time,
					),
					"id = :id",
					array(
					":id" => $value['id'],
			
					)); 

		  			//Auto Dissaprove if less than 2 hrs
		  			if ($total_hours<2){

					OtRequest::model()->updateAll(array(
						'total_hours' => $total_hours,  	
						'end_time' => $end_time,
						'request_type_id' => 2,
						'position_id' =>1,
						'request_dissaproval' => "Dissapoved: Your application for OT is below minimun"  	
						
					),
					"id = :id",
					array(
					":id" => $value['id'],
			
					)); 

					}	

		  			//display Status //
		  		    $positon_name = $modelPosition['position']['id'];
		  			$requestType = $modelStatus['request_type']['id'];
					if($requestType == 1 or $positon_name == 4)
					{
					$requestType = $modelStatus['request_type']['name'];
					$html .= "<td>{$requestType}</td>";	
					}
					else
					{
					$positon_name = $modelPosition['position']['name'];	
					$requestType = $modelStatus['request_type']['name'];
					$html .= "<td>{$requestType} {$positon_name} </td>";	
		  			}

			}

	           $html .= "</table>";
  		       return $html;
  		}

 public function actionOtReports() {

	//$data['model']['ot_request'] = OtRequest::model() -> findAll();
	//$data['reports'] = $this -> renderotreports($data['model']);
	
	    $criteria = new CDbCriteria();
        $criteria->addCondition("status_id=1");
        $model_employee = Employee::model()->findAll($criteria);
        // $model['SlCount'] = SlCount::model()->findAll();

        $emp_name_array = array();
        foreach ($model_employee as $e) {
            $eid = $e['emp_id'];
            $fname = $e['firstname'];
            $lname = $e['lastname'];
            $name = $lname.", ".$fname;

            // $emp_name_array[][$eid] = $eid;
            $emp_name_array[$eid] = $name;
        }

        $model_request_type = RequestType::model()->findAll();

        $request_type_array = array();
        foreach ($model_request_type as $mr) {
        	$id = $mr['id'];
        	$name = $mr['name'];
        	$request_type_array[$id] = $name;
        }

        $model_department = Department::model()->findAll();

        $department_array = array();
        foreach ($model_department as $d) {
        	$id = $d['id'];
        	$name = $d['name'];
        	$department_array[$id] = $name;
        }
        
        $data['result'] = array();
	 	if(isset($_POST['submit'])){
	 	    $_POST['start_date']; 
	 	    $_POST['end_date'];
		 	$_POST['empID'];
		 	$_POST['requestTypeId'];
		 	$_POST['depID'];
		 	
		 	$data['result'] = $this->processTime($_POST['start_date'], $_POST['end_date'],$_POST['empID'], $_POST['requestTypeId'] ,$_POST['depID'] );
 		}
  	    
  	       $html  = NULL;
           $html .= " 
             <table class  = 'table' >
			
			<tr>
		 	    <td><b> Employee Name</b></td>
		 	    <td><b> Date Filed</b> </td>
		   	    <td><b> Status </b> </td>
			   	<td><b> OT: Start </b></td>
			   	<td><b> OT: End </b></td>
			   	<td><b> Total Hours </b></td>
			   	<td><b> Employee Remarks </b></td>
			    <td><b> Reason for Disapproval </b></td>
		     </tr>
		";

		$html .= "<div class =  'payroll-header'> <span class = glyphicon glyphicon-th-list ></span> </div>"; 
        foreach ($data['result']  as $value){
 	    $html .= "<td>{$emp_name_array[$value['emp_id']]}</td>" ; 			
 	    $html .= "<td>{$value['date']} </td>";
 	    $html .= "<td>{$request_type_array[$value['request_type_id']]} </td>";
 	    $html .= "<td>{$value['start_time']}</td>";
 	    $html .= "<td>{$value['end_time']}</td>";
 	    $html .= "<td>{$value['total_hours']}</td>";
 	    $html .= "<td>{$value['remarks']}</td>";
 	    $html .= "<td>{$value['request_dissaproval']}</td>";
 	    $html .= "<tr> </tr>";
 	    }
 	    $html .= "</table>";
        $this->render('otreports',compact('result','emp_name_array', 'request_type_array', 'department_array', 'html')); 
}


protected function processTime($from, $to, $selectedEmployee, $selectedRequestType, $selectedDepartment ){
		
	$criteria = new CDbCriteria;

	if (isset($from) && isset($to)){
	$criteria->addBetweenCondition('date', $from, $to, $operator='AND');
    if ($selectedRequestType != '')
	$criteria->addCondition("request_type_id = $selectedRequestType");
    if ($selectedEmployee != '' && $selectedDepartment == ''){
	$criteria->addCondition("emp_id=$selectedEmployee");
    }elseif($selectedEmployee != '' && $selectedDepartment != ''){
	$criteria->addCondition("emp.emp_id=$selectedEmployee");
    }
	if ($selectedDepartment != ''){
	$criteria->with = array('emp','emp.subDepartment');
	$criteria->compare('subDepartment.department_id',$selectedDepartment);
    }
}
     $search = OtRequest::model()->findAll($criteria);
     
     return $search;

} 
  


	
// //buttons for approve/dissaprove 
	public function actionDissaproveAjax()
	{
		if (isset($_POST['id']))
		{
			$remarks = $_POST['remarks'];
            $id = $_POST['id'];
			
			OtRequest::model()->updateAll(
            array(
               'request_dissaproval' => $remarks,
               	'position_id' => 1,
               	'request_type_id'=>2,
            ),
            "id = :id",
            array(
                ":id" => $id
            ));
		}
	

	}
	public function actionApproveAjax()
	{
		if (isset($_POST['id']))
		{
			$id = $_POST['id'];	
			
			$data['model']['ot_request'] = OtRequest::model()->find(array(
				'condition' => 'id = :id',
				 'params' => array(
			             ':id' => $id,
				 	)
				));
			
			$positon = $data['model']['ot_request']['position_id'];
			
			if ($positon == 4)
			{
			$positon = 1;	
			$request_type_id = 1;
			OtRequest::model()->updateAll(
			array(
				'position_id' => $positon,  	
				'request_type_id' => $request_type_id,
			),
			"id = :id",
			array(
				":id" => $id,
			
			)); 
			}
				else
				{
					$positon = $positon +1;
					OtRequest::model()->updateAll(
					array(
						'position_id' => $positon,  	
					),
					"id = :id",
					array(
						":id" => $id,
			
					)); 
				
				}

			   
		}
	}


	/**
	 * Protected Methods
	 * Used for API Methods
	 */

	/**
	 * Yii-Generated Methods
	 */
	protected function add()
	{
		return 1+1;
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return OtRequest the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=OtRequest::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param OtRequest $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='ot-request-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}



// End of File protected/controller/OTController.php