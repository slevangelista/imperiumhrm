<?php

/**
 * Manhours Controller
 * Created By: Syd Evangelista
 * 
 * Class containing ROUTES for access (Whether route leading to views and/or AJAX methods)
 * Extended to ManhoursAPIController, which contains protected methods that this class will use
 *
 */

// Import Parent Controller
Yii::import('application.controllers.ManhoursAPIController');

class ManhoursController extends ManhoursAPIController
{

    public function actionIndex()
    {
        // Department
        $data['model']['department'] = Department::model()->findAll(array(
            'order' => 'name'
        ));

        // Generate LogsForm Model
        $data['model']['logs_form'] = new LogsForm;

        $this->render("index", $data);
    }

    public function actionGetCutoffList()
    {
        // Cutoff
        $data['model']['cutoff_all'] = CutoffPeriod::model()->findAll();

        foreach($data['model']['cutoff_all'] as $cutoff):
            $cutoff_list[$cutoff['id']]['value'] = $cutoff['cutoffBeutified'];
        endforeach;

        echo ")]}',\n" . json_encode($cutoff_list);
    }

    public function actionGetCurrentCutoff()
    {
        // Cutoff for current day
        $current_day = date('Y-m-d');
        $model['cutoff_today'] = CutoffPeriod::model()->find(array(
            'condition' => 'date_start <= :date AND date_end >= :date',
            'params' => array(
                ':date' => $current_day
            )
        ));

        $data['value'] = $model['cutoff_today']['cutoffBeutified'];

        echo ")]}',\n" . json_encode($data);
    }

    public function actionGetEmployeeByDepartment()
    {
        if (isset($_GET['department'])):

            // Get model for department
            $model['department'] = Department::model()->findByPk((int) $_GET['department']);
            $department = $model['department']['name'];

            // Get model for sub-department
            $model['sub_department'] = SubDepartment::model()->findAll(array(
                'condition' => 'department_id = :department_id',
                'params' => array(
                    ':department_id' => $model['department']['id']
                ),
            ));

            // Pass sub-department id's to array $sub_department
            foreach($model['sub_department'] as $value)
                $sub_department[$value['id']] = $value['id'];

            // Check if sub-department is found
            if (isset($sub_department)):

                $sub_dept_index = NULL;
                $is_first_index = TRUE;
                foreach ($sub_department as $value):

                    if ($is_first_index):
                        $sub_dept_index .= $value;
                        $is_first_index = FALSE;
                    else:
                        $sub_dept_index .= ',' . $value;
                    endif;

                endforeach;

                // Find Employees with sub-department pointing to department
                $model['employee'] = Employee::model()->findAll(array(
                    'condition' => "sub_department_id IN ({$sub_dept_index})",
                ));

                // Pass employee values to array $employee
                foreach ($model['employee'] as $value):
                    $employee[$value['emp_id']]['name'] = $value['fullname'];
                    $employee[$value['emp_id']]['emp_id'] = $value['emp_id'];
                    $employee[$value['emp_id']]['count'] = $this->getEmployeeData($value['emp_id'], $_GET['date_range']);
                endforeach;

                // Check if employee is found
                if (isset($employee))
                    echo ")]}',\n" . json_encode($employee);
                else
                    echo ")]}',\n" . json_encode(array());

            else:
                echo ")]}',\n" . json_encode(array());
            
            endif;

        endif;
    }
    
}

// End of file controllers/ManhoursController.php