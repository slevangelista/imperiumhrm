<?php

/**
 * Schedule Controller
 * Created By: Syd Evangelista
 * 
 * Class containing ROUTES for access (Whether route leading to views and/or AJAX methods)
 * Extended to ScheduleAPIController, which contains protected methods that this class will use
 *
 */

// Import Parent Controller
Yii::import('application.controllers.ScheduleAPIController');

class ScheduleController extends ScheduleAPIController
{
	public function actionIndex()
	{
		// Get Department List
        $data['model']['department'] = Department::model()->findAll(array(
        	'order' => 'name'
        ));

        // Get LogsForm Model
        $data['model']['logs_form'] = new LogsForm;

		// Get Current Day
		$data['day'] = date('Y-m-d');
		$data['day_json'] = json_encode($data['day']);

		$this->render('index', $data);
	}

	public function actionUser()
	{
		// Get LogsForm Model
        $data['model']['logs_form'] = new LogsForm;

        // Get Employee
        $employee_details = $this->getEmployeeDetails();
		$model['employee'] = Employee::model()->find(array(
			'condition' => 'emp_id = :emp_id',
			'params' => array(
				':emp_id' => $employee_details['emp_id']
			)
		));

		// Get User
        $data['user'] = $model['employee']['fullnameunformal'];

        // Get Current Day
		$data['day'] = date('Y-m-d');
		$data['day_json'] = json_encode($data['day']);

		$this->render('user', $data);
	}

	public function actionGetWeek()
	{
		if (isset($_GET['date'])):
			
			$date = str_replace('"', '', $_GET['date']);

			// Determine what 'day' is the date specified
			$day = date('N', strtotime($date));

			// If Monday...
			if ($day == 1): // 1 Represents Monday

				// Render dates from date given (Monday) to Sunday
				for ($i = 0; $i < 7; $i++):

					$render_day = date('F d (D)', strtotime($date . "+ {$i} days"));
					$week[date('Y-m-d', strtotime($date . "+ {$i} days"))] = $render_day;	

				endfor;

			// Otherwise...
			else:
				
				// Determine "Monday" of date inputted
				$day = (int) $day - 1; // Decrease day value by 1
				$date = date("Y-m-d", strtotime($date . "-{$day} days"));

				// Render dates from date given (Monday) to Sunday
				for ($i = 0; $i < 7; $i++):

					$render_day = date('F d (D)', strtotime($date . "+ {$i} days"));
					$week[date('Y-m-d', strtotime($date . "+ {$i} days"))] = $render_day;	

				endfor;

			endif;

			// Print JSON Value
			// ")]}',\n" is Padding for JSON Protection
			echo ")]}',\n" . json_encode($week);

		endif;
	}

	public function actionGetDayLastWeek()
	{
		if (isset($_GET['day'])):

			$day = str_replace('"', '', $_GET['day']);
			$previousDay = date('Y-m-d', strtotime($day . " -7 days"));

			// Print JSON Value
			// ")]}',\n" is Padding for JSON Protection
			echo ")]}',\n" . json_encode($previousDay);

		endif;
	}

	public function actionGetDayNextWeek()
	{
		if (isset($_GET['day'])):

			$day = str_replace('"', '', $_GET['day']);
			$nextDay = date('Y-m-d', strtotime($day . " +7 days"));

			// Print JSON Value
			// ")]}',\n" is Padding for JSON Protection
			echo ")]}',\n" . json_encode($nextDay);

		endif;
	}

	public function actionGetEmployeeSelf()	
	{
		if (isset($_POST['currentWeek'])):

			$employee_details = $this->getEmployeeDetails();

			// Get Employee
			$model['employee'] = Employee::model()->find(array(
				'condition' => 'emp_id = :emp_id',
				'params' => array(
					':emp_id' => $employee_details['emp_id']
				)
			));

			$employee['name'] = $model['employee']['fullname'];
			$employee['emp_id'] = $employee_details['emp_id'];
			$employee['schedule'] = $this->getScheduleByEmployee($employee_details['emp_id'], $_POST['currentWeek']);

			echo json_encode($employee); 

		endif;
	}

	public function actionGetEmployeeByDepartment()
	{
		if (isset($_POST['department'])):

			// Get model for department
			$model['department'] = Department::model()->findByPk((int) $_POST['department']);
			$department = $model['department']['name'];

			// Get model for sub-department
			$model['sub_department'] = SubDepartment::model()->findAll(array(
				'condition' => 'department_id = :department_id',
				'params' => array(
					':department_id' => $model['department']['id']
				),
			));

			// Pass sub-department id's to array $sub_department
			foreach($model['sub_department'] as $value)
				$sub_department[$value['id']] = $value['id'];

			// Check if sub-department is found
			if (isset($sub_department)):

				$sub_dept_index = NULL;
				$is_first_index = TRUE;
				foreach ($sub_department as $value):

					if ($is_first_index):
						$sub_dept_index .= $value;
						$is_first_index = FALSE;
					else:
						$sub_dept_index .= ',' . $value;
					endif;

				endforeach;

				// Find Employees with sub-department pointing to department
				$model['employee'] = Employee::model()->findAll(array(
					'condition' => "sub_department_id IN ({$sub_dept_index})",
				));

				// Pass employee values to array $employee
				foreach ($model['employee'] as $value):
					$employee[$value['fullname'] . '_' . $value['emp_id']]['name'] = $value['fullname'];
					$employee[$value['fullname'] . '_' . $value['emp_id']]['emp_id'] = $value['emp_id'];
					$employee[$value['fullname'] . '_' . $value['emp_id']]['schedule'] = $this->getScheduleByEmployee($value['emp_id'], $_POST['currentWeek']);
				endforeach;

				// Check if employee is found
				if (isset($employee))
					echo ")]}',\n" . json_encode($employee);
				else
					echo ")]}',\n" . json_encode(array());

			else:
				echo ")]}',\n" . json_encode(array());
			
			endif;

		endif;
	}

	public function actionGetShifts()
	{
		$model['shifts'] = Shift::model()->findAll();
		$shift = array();

		foreach($model['shifts'] as $value):
			$shift[$value['id']] = $value['name'] . ' (' . $value['start_time'] . ' - ' . $value['end_time'] . ')';
		endforeach;

		echo ")]}',\n" . json_encode($shift);
	}

	public function actionSaveShiftAjax()
	{
		if (isset($_POST['shift']))
		{
			$shift = new Shift;

			$shift->name = $_POST['shift']['name'];
			$shift->color = $_POST['shift']['color'];
			$shift->personnel_required = $_POST['shift']['personnel_no'];
			$shift->start_time = $_POST['shift']['start_time'];
			$shift->end_time = $_POST['shift']['end_time'];

			$shift->save();
		}
	}

	public function actionSaveScheduleAjax()
	{
		if (isset($_POST['emp_id'])):

			// Get date interval within start date and end date
			$date_interval = $this->getDays($_POST['start_date'], $_POST['end_date']);

			// Foreach Employee
			foreach ($_POST['emp_id'] as $emp_id):

				// Loop By date interval
				foreach ($date_interval as $date):

					// If there is existing schedule for this date, 
					// update is_active value of old schedule to 0
					$this->deactivateSchedule($date['date_map'], $emp_id);

					// Get date format numerical (1 -> Monday, 2-> Tuesday, etc)
					$day_in_week = date('N', strtotime($date['date_map']));

					// Check if result exist in 'days_assigned' array
					if ( in_array($day_in_week, $_POST['days_assigned']) )
						$restday = 0; // Set $restday numeric boolean value to 1
					// Otherwise
					else
						$restday = 1; // Set $restday numeric boolean value to 0

					// Generate Schedule Model
					$sched_model = new Schedule;

					$sched_model->emp_id = $emp_id;
					$sched_model->date = $date['date_map'];
					$sched_model->is_active = '1';
					$sched_model->shift_id = $_POST['assigned'];
					$sched_model->is_restday = $restday;

					$sched_model->save();

					// Update Schedule in logs
					$this->updateLogSchedule($emp_id, $date['date_map']);

				endforeach; // End $date_interval

			endforeach; // End $schedule['emp_id']

		endif;
	}

	public function actionUpdateSchedule()
	{
		if (isset($_POST['emp_id_self'])):

			// var_dump($_POST['sched_update']);

			// Determine action based on selected value from list
			switch ($_POST['selectedOption']):

				case '1': // Change Shift

					// If there is existing schedule for this date, 
					// update is_active value of old schedule to 0
					$this->deactivateSchedule($_POST['date'], $_POST['emp_id_self']);

					// Create new schedule for new shift
					$sched_model = new Schedule;

					$sched_model->emp_id = $_POST['emp_id_self'];
					$sched_model->date = $_POST['date'];
					$sched_model->is_active = '1';
					$sched_model->shift_id = $_POST['assigned'];
					$sched_model->is_restday = '0';

					$sched_model->save();

					// Update Schedule in logs
					$this->updateLogSchedule($_POST['emp_id_self'], $_POST['date']);

				break;

				case '2': // Switch shift from another employee
				
					// Phase 1: Explode render to get emp_id_other
					$emp_other_array = explode('_', $_POST['render_id_other']);
					$emp_id_other = $emp_other_array[1];

					// Phase 2: Disable overidden schedule
					$this->deactivateSchedule($_POST['date'], $_POST['emp_id_self']);
					$this->deactivateSchedule($_POST['date'], $emp_id_other);

					// Phase 3: Create schedule for self (Matching data from other)
					if (!empty($_POST['swap_key_other']) || !empty($_POST['swap_restday_other']) || !empty($_POST['swap_leave_other']) || !empty($_POST['swap_holiday_other'])):
						$sched_model = new Schedule;

						$sched_model->emp_id = $_POST['emp_id_self'];
						$sched_model->date = $_POST['date'];
						$sched_model->is_active = '1';
						$sched_model->shift_id = $_POST['swap_key_other'];
						$sched_model->is_restday = $_POST['swap_restday_other'];
						$sched_model->is_leave = $_POST['swap_leave_other'];
						$sched_model->is_holiday = $_POST['swap_holiday_other'];

						$sched_model->save();

					endif;

					// Phase 4: Create schedule for other (Matching data from self)
					if (!empty($_POST['swap_key_self']) || !empty($_POST['swap_restday_self']) || !empty($_POST['swap_leave_self']) || !empty($_POST['swap_holiday_self'])):
						$sched_model = new Schedule;

						$sched_model->emp_id = $emp_id_other;
						$sched_model->date = $_POST['date'];
						$sched_model->is_active = '1';
						$sched_model->shift_id = $_POST['swap_key_self'];
						$sched_model->is_restday = $_POST['swap_restday_self'];
						$sched_model->is_leave = $_POST['swap_leave_self'];
						$sched_model->is_holiday = $_POST['swap_holiday_self'];

						$sched_model->save();

					endif;

					// Update Schedule in logs for yourself
					$this->updateLogSchedule($_POST['emp_id_self'], $_POST['date']);

					// Update Schedule in logs for swapped employee
					$this->updateLogSchedule($emp_id_other, $_POST['date']);

				break;

				case '3': // Set as restday

					// If there is existing schedule for this date, 
					// update is_active value of old schedule to 0
					$this->deactivateSchedule($_POST['date'], $_POST['emp_id_self']);

					$sched_model = new Schedule;

					$sched_model->emp_id = $_POST['emp_id_self'];
					$sched_model->date = $_POST['date'];
					$sched_model->is_active = '1';
					$sched_model->is_restday = '1';

					$sched_model->save();

					// Update Schedule in logs
					$this->updateLogSchedule($_POST['emp_id_self'], $_POST['date']);

				break;

				case '4': // Set as leave

					if (isset($_POST['leave_assigned'])):

						// If there is existing schedule for this date, 
						// update is_active value of old schedule to 0
						$this->deactivateSchedule($_POST['date'], $_POST['emp_id_self']);

						$sched_model = new Schedule;

						$sched_model->emp_id = $_POST['emp_id_self'];
						$sched_model->date = $_POST['date'];
						$sched_model->is_active = '1';
						$sched_model->is_leave = $_POST['leave_assigned'];

						$sched_model->save();

						// Update Schedule in logs
						$this->updateLogSchedule($_POST['emp_id_self'], $_POST['date']);

					endif;

				break;

				case '5': // Remove schedule (In actuality just disable it)

					// Just disable the given date
					$this->deactivateSchedule($_POST['date'], $_POST['emp_id_self']);

					// Update Schedule in logs
					$this->updateLogSchedule($_POST['emp_id_self'], $_POST['date'], TRUE); // 3rd Parameter, when set to TRUE, sets logs schedule to "No Schedule"

				break;

			endswitch;
		endif;
	}

	public function actionGetLeaveList()
	{
		$model['leave_type'] = LeaveType::model()->findAll();

		foreach($model['leave_type'] as $leave)
			$leave_array[$leave['id']] = $leave['name']; 

		 echo ")]}',\n" . json_encode($leave_array);
	}

	public function actionSetLeaveAjax()
	{
		if (isset($_POST['employees'])):

			// Get date interval within start date and end date
			$date_interval = $this->getDays($_POST['start_date'], $_POST['end_date']);

			foreach ($_POST['employees'] as $emp_id):

				// Loop By date interval
				foreach ($date_interval as $date):

					// If there is existing schedule for this date, 
					// update is_active value of old schedule to 0
					$this->deactivateSchedule($date['date_map'], $emp_id);

					// Create New Leave Schedule
					$leave_schedule = new Schedule;

					$leave_schedule->emp_id = $emp_id;
					$leave_schedule->date = $date['date_map'];
					$leave_schedule->is_active = 1;
					$leave_schedule->is_leave = $_POST['leave_assigned'];

					$leave_schedule->save();

					// Update Schedule in logs
					$this->updateLogSchedule($emp_id, $date['date_map']);

				endforeach; // End $date_interval

			endforeach; // End Employees

		endif;
	}

	public function actionGetHolidayList()
	{
		$model['holiday'] = Holidays::model()->findAll();

		foreach($model['holiday'] as $holiday)
			$holiday_array[$holiday['id']] = $holiday['name']; 

		 echo ")]}',\n" . json_encode($holiday_array);
	}

	public function actionSetHolidayAjax()
	{
		if (isset($_POST['employees'])):
		
			foreach ($_POST['employees'] as $emp_id):

				// If there is existing schedule for this date, 
				// update is_active value of old schedule to 0
				$this->deactivateSchedule($_POST['date'], $emp_id);

				// Create New Leave Schedule
				$holiday_schedule = new Schedule;

				$holiday_schedule->emp_id = $emp_id;
				$holiday_schedule->date = $_POST['date'];
				$holiday_schedule->is_active = 1;
				$holiday_schedule->is_holiday = $_POST['holiday_assigned'];

				$holiday_schedule->save();

				// Update Schedule in logs
				$this->updateLogSchedule($emp_id, $_POST['date']);

			endforeach; // End Employees

		endif;
	}
}
