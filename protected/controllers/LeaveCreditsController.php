<?php

/**
 * Cutoff Controller
 * Created By: Syd Evangelista
 * 
 * Class containing ROUTES for access (Whether route leading to views and/or AJAX methods)
 *
 */

class LeaveCreditsController extends RController
{
    /**
     * Filter Route. Used for user authentication
     *
     */
    public function filters()
    {
        return array(
            'rights',
        );
    }

    public function actionIndex()
    {
        $model['VlCount'] = VlCount::model()->findAll();
        // print_r($model['VlCount']);
        $this->render('index', $data);
    }

    public function actionGetAllCredits()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("status_id=1");
        $model['Employee'] = Employee::model()->findAll($criteria);
        // $model['SlCount'] = SlCount::model()->findAll();

        $emp_name_array = array();
        foreach ($model['Employee'] as $e) {
            $eid = $e['emp_id'];
            $fname = $e['firstname'];
            $lname = $e['lastname'];
            $name = $lname.", ".$fname;

            $emp_name_array[][$eid] = $eid;
            $emp_name_array[][$eid] = $name;
        }

        $sl_credit_array = array();
        foreach ($model['Employee'] as $slCredits) {
            $id = $slCredits['id'];
            $emp_id = $slCredits['emp_id'];

            $sl_credit_array[][$emp_id] = $slCredits['emp_id'];
            $sl_credit_array[][$emp_id] = $slCredits['sl_count'];
        }
        $vl_credit_array = array();
        foreach ($model['Employee'] as $vlCredits) {
            $id = $vlCredits['id'];
            $emp_id = $vlCredits['emp_id'];

            $vl_credit_array[][$emp_id] = $vlCredits['emp_id'];
            $vl_credit_array[][$emp_id] = $vlCredits['vl_count'];
        }

        $result = array();
        foreach ($emp_name_array as $key => $array) {
            foreach ($array as $e => $value) {
                $result[$e]['fullname'] = $value;
            }
        }

        foreach ($vl_credit_array as $key => $array) {
            foreach ($array as $e => $value) {
                $result[$e]['vl'] = $value;
            }
        }

        foreach ($sl_credit_array as $key => $array) {
            foreach ($array as $e => $value) {
                $result[$e]['sl'] = $value;
            }
        }
        echo json_encode($result);

    }

    public function actionUpdateLeaveCredits()
    {
        // $emp_id = isset($_POST['empid']) ? $_POST['empid'] : '99';
        $emp_id = $_POST['empid'];
        $vl = $_POST['vl'];
        $sl = $_POST['sl'];

        echo "$vl $sl";

        $employeeModel = Employee::model()->findByAttributes(array('emp_id'=>$emp_id));

        // $vlModel = VlCount::model()->findByAttributes(array('emp_id'=>$emp_id));
        // $vl_id = $vlModel->id;
        // $vlCountModel = VlCount::model()->findByPk($vl_id);

        // $slModel = SlCount::model()->findByAttributes(array('emp_id'=>$emp_id));
        // $sl_id = $slModel->id;
        // $slCountModel = SlCount::model()->findByPk($sl_id);

// print_r($vlModel);
        if ($employeeModel)
        {
            $employeeModel->sl_count = $sl;
            $employeeModel->vl_count = $vl;
            $employeeModel->save();
        }
    }
}
?>