<?php

class EducationController extends RController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/profile';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			//'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
			'rights',
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView()
	{
		$model = new Education;
		$this->render('view',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$this->layout = "//layouts/admin_profile_view";
		$model=new Education;
		$emp_id = $_GET['emp_id'];

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Education']))
		{
			$model->attributes=$_POST['Education'];
			$model->emp_id = $emp_id;
			$model->created_at = date("Y-m-d H:i:s");
			if($model->save())
				$this->redirect(array('employeeEducation?emp_id='.$model->emp_id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate()
	{
		$this->layout = "//layouts/admin_profile_view";
		$id = $_GET['id'];
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Education']))
		{
			$model->attributes=$_POST['Education'];
			$model->updated_at = date("Y-m-d H:i:s");
			if($model->save())
				$this->redirect(array('employeeEducation?emp_id='.$model->emp_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Education');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Education('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Education']))
			$model->attributes=$_GET['Education'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Education the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Education::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Education $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='education-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionEmployeeEducation(){

		$this->layout = "//layouts/admin_profile_view";
		$model=new Education();
		$this->render('employeeEducation',array(
			'model'=>$model,
		));
	}

	public function getEmployeeEducation($emp_id){

		$sql = "SELECT educ.id,educ.education_level_id, educ.id, educ.emp_id, educ.school_name, educ.date_started, educ.date_ended, level.name AS educ_level,educ.GPA"
		     . " FROM tbl_education educ, tbl_education_level level"
             . " WHERE educ.education_level_id = level.id"
             . " AND educ.emp_id=:emp_id";
        $dataReader = Yii::app()->db->CreateCommand($sql);
        $dataReader->bindParam(":emp_id",$emp_id,PDO::PARAM_INT);
        $dataReader->setFetchMode(PDO::FETCH_GROUP);
        $education = $dataReader->queryAll();

        $level_ids = array();

        foreach($education as $key=>$value){

        	if(array_key_exists($value['education_level_id'],$level_ids)){	
        		array_push($level_ids[$value['education_level_id']],$value);
        	}

        	else{
        		$temp = array();
        		$temp[] = $value;
        		$level_ids[$value['education_level_id']] = $temp; 
        	}
        }

        return $level_ids;
	}

	public function getAllEducationLevel(){

		$list = array();
		$sql = "SELECT id,name FROM tbl_education_level";
		$education = Yii::app()->db->CreateCommand($sql)->queryAll();

		foreach($education as $key=>$value){
			$list[$value['id']] = $value['name'];
		}

		return $list;


	}
}
