<?php

class EmployeeController extends RController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/profile';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			//'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
			'rights',
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView()
	{
		$emp_id = Yii::app()->request->getQuery('id');
		$model = Employee::model()->findByAttributes(array('emp_id'=>$emp_id));
	
		$this->render('view',array(
			'model'=>$model,
		));
		
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$this->layout = Yii::app()->user->checkAccess('Admin') ? '//layouts/column1' : '//layouts/main/profile';
		$empModel=new Employee;
		$userModel=new Users;
		$roleModel=new AuthAssignment;

		if(isset($_POST['Employee']) && isset($_POST['Users']) && isset($_POST['AuthAssignment']))
		{
			$empModel->attributes=$_POST['Employee'];
			$empModel->created_at = date("Y-m-d H:i:s");

			$userModel->attributes=$_POST['Users'];;
			$userModel->create_at = date("Y-m-d H:i:s");
			$userModel->emp_id = $empModel->emp_id;


			if($empModel->save() && $userModel->save())
				$roleModel->attributes=$_POST['AuthAssignment'];
				$roleModel->userid = $userModel->id;
				$roleModel->bizrule = NULL;
				$roleModel->data = 'N;';
				$roleModel->save();
				$this->redirect(array('employee/adminEmployee/?emp_id='.$empModel->emp_id));
		}

		$this->render('_form', compact('empModel', 'userModel', 'roleModel'));
	}

	public function getRoles(){
		$list = array();
		$sql = "SELECT * FROM AuthItem WHERE type='2'";
		$roles = Yii::app()->db->CreateCommand($sql)->queryAll();
		foreach($roles as $key=>$value){
				$list[$value['name']] = $value['name'];
		}

		return $list;

	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate()
	{
		//$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$emp_id = $_GET['emp_id'];

		if(isset($_POST['Employee']))
		{
			$model->attributes=$_POST['Employee'];
			$model->emp_id = $emp_id;
			/*$model->nationality_id = $_POST['nationality_id'];
			echo "<pre>";
			print_r($_POST);
			print_r($model->attributes);
			echo "</pre>";
			die();
			*/

			if($model->save())
				$this->redirect(array('adminEmployee?emp_id='.$model->emp_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Employee');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$this->layout = "//layouts/column1";
		$model=new Employee('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Employee']))
			$model->attributes=$_GET['Employee'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Employee the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Employee::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Employee $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='employee-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function getGenderList(){

		$list = array();
		$sql="SELECT id,name FROM tbl_gender";
		$genderList = Yii::app()->db->CreateCommand($sql)->queryAll();

		foreach($genderList as $key=>$gender)
		{
			$list[$gender['id']] = $gender['name'];
		}
	
		return $list;
	}

	public function getMaritalStatus(){
		$list = array();
		$sql = "SELECT id,name FROM tbl_marital_status";
		$statusList = Yii::app()->db->CreateCommand($sql)->queryAll();

		foreach($statusList as $key=>$status){
			$list[$status['id']] = $status['name'];
		}

		return $list;
	}

	public function getNationality(){

		$list = array();
		$sql = "SELECT id,name FROM tbl_nationality";
		$nationalityList = Yii::app()->db->CreateCommand($sql)->queryAll();

		foreach($nationalityList as $key=>$national){
			$list[$national['id']] = $national['name'];
		}

		return $list;
	}

	public function getJobDetails(){

		$emp_id = $_GET['emp_id'];

		$sql = "SELECT emp.emp_id, emp.employment_date, dep.name AS department_name,"
		     . " subdep.name as sub_departmentname, pos.name as position"
			 . " FROM tbl_employee emp, tbl_department dep, tbl_sub_department subdep, tbl_position pos"
			 . " WHERE emp.sub_department_id = subdep.id"
			 . " AND subdep.department_id = dep.id"
			 . " AND pos.id = emp.position_id"
			 . " AND emp.emp_id = :emp_id";

	    $dataReader = Yii::app()->db->CreateCommand($sql);
		$dataReader->bindParam(":emp_id",$emp_id,PDO::PARAM_INT);
		$job = $dataReader->queryAll();

		return isset($job[0]) ? $job[0] : false;
	}

	public function actionJob()
	{
		$this->layout = Yii::app()->user->checkAccess('Admin') ? '//layouts/admin_profile_view' : '//layouts/profile';
    	$model=new Employee;
    	$this->render('job',array('model'=>$model));
	}

	public function actionEmployeeQualifications(){

		$model = new Employee;
		$this->render('employeeQualifications',array('model'=>$model));
	}

	public function actionAdminEmployee(){

		$this->layout = "//layouts/admin_profile_view";
		//$emp_id = Yii::app()->request->getQuery('id');
		$emp_id = $_GET['emp_id'];
		$model = Employee::model()->findByAttributes(array('emp_id'=>$emp_id));

		if(isset($_POST['Employee']))
		{
			$model->updated_at = date("Y-m-d H:i:s");
			$model->attributes=$_POST['Employee'];
			if($model->save())
				$this->redirect(array('/employee/adminEmployee?emp_id='.$emp_id,));
		}

		$this->render('adminEmployee',array(
			'model'=>$model,
		));
	}

	public function actionJobDetails(){

		$this->layout = "//layouts/admin_profile_view";
		//$emp_id = Yii::app()->request->getQuery('id');
		$emp_id = $_GET['emp_id'];
		$model = Employee::model()->findByAttributes(array('emp_id'=>$emp_id));
		$this->render('jobdetails',array(
			'model'=>$model,
		));
	}

	public function actionGetAllDepartments(){

		$list = array();
		$sql = "SELECT id,name FROM tbl_department";
		$departments = Yii::app()->db->CreateCommand($sql)->queryAll();

		foreach($departments as $key=>$dep){
			$list[$dep['id']] = $dep['name'];
		}

		echo ")]}',\n" . json_encode($departments);
	}

	public function actionGetSubDepartment(){

		$data=SubDepartment::model()->findAll();
 
    	foreach($data as $key=>$value)
    	{
    		$sub_department_array[$value['department_id']][$value['id']]['name'] = $value['name'];  
    		$sub_department_array[$value['department_id']][$value['id']]['subdepartment_id'] = $value['id'];
    	}

    	// echo '<pre>' . print_r($sub_department_array, 1) . '</pre>';
    	echo ")]}',\n" . json_encode($sub_department_array);
	}

	public function actionGetAllPositions(){

		$data = Position::model()->findAll();
		foreach($data as $key=>$value){
			$position_arr[$value['id']]['name'] = $value['name'];
			$position_arr[$value['id']]['position_id'] = $value['id'];
		}

		echo ")]}',\n" . json_encode($position_arr);
	}

	public function actionUpdateEmployeeJobAjax()
	{
		$emp_id = $_POST['emp_id'];
		$model = Employee::model()->findByAttributes(array('emp_id'=>$emp_id));
		if(isset($_POST['emp_id']))
		{
			
			$model->attributes=$_POST['Employee'];
			$model->sub_department_id = $_POST['subdepartment_id'];
			$model->position_id = $_POST['position_id'];
			$model->updated_at = date("Y-m-d H:i:s");
			$model->save();
			//if($model->save())
			//$this->redirect(array('/employee/job?emp_id='.$emp_id,));
		}

		$this->render('update',array(
			'model'=>$model,
		));
		
	}

	public function actionUploadPic(){

		$emp_id = $_GET['emp_id'];
		$uploaddir = YiiBase::getPathOfAlias('webroot').'/images/profile/';
		$uploadfile = $uploaddir . "emp_" .$emp_id.".png";

		echo '<pre>';
		if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
    		echo "File is valid, and was successfully uploaded.\n";
    		$this->redirect(array('adminEmployee?emp_id='.$emp_id));
		} 

		else {
    		echo "Possible file upload attack!\n";
    		$this->redirect(array('adminEmployee?emp_id='.$emp_id));
		}

		echo 'Here is some more debugging info:';
		print_r($_FILES);

		print "</pre>";

	}

}
