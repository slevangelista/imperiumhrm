<?php

/**
 * ManhoursAPI Controller
 * Created By: Syd Evangelista
 * 
 * Class containing protected API Methods for ManhoursController
 * Extend ManhoursController to this class for ease of use
 *
 */

class ManhoursAPIController extends RController
{
    /**
     * Filter Route. Used for user authentication
     *
     */
    public function filters()
    {
        return array(
            'rights',
        );
    }

    /**
     * Protected function getDays
     *
     * Returns date ranges between start and end date
     *
     * @param string $start_date
     * @param string $end_date
     */
    protected function getDays($start_date, $end_date) 
    {
        $date_interval = (strtotime ( $end_date ) - strtotime ( $start_date )) / 86400; // 1 day = 86400 seconds
        $date = array ();
        
        for($i = 0; $i <= $date_interval; $i ++) 
        {
            $date [$i] ['day'] = date ( 'D', strtotime ( $start_date . ' + ' . $i . ' days' ) );
            $date [$i] ['month'] = date ( 'M', strtotime ( $start_date . ' + ' . $i . ' days' ) );
            $date [$i] ['date'] = date ( 'j', strtotime ( $start_date . ' + ' . $i . ' days' ) );
            $date [$i] ['year'] = date ( 'Y', strtotime ( $start_date . ' + ' . $i . ' days' ) );
            $date [$i] ['date_map'] = date ( 'Y', strtotime ( $start_date . ' + ' . $i . ' days' ) ) . '-' . date ( 'm', strtotime ( $start_date . ' + ' . $i . ' days' ) ) . '-' . date ( 'd', strtotime ( $start_date . ' + ' . $i . ' days' ) );
        }
        
        return $date;
    }

    /**
     * Protected function getTotalHours
     *
     * Returns total hours of work (Excluding 1 hour break)
     *
     * @param string $start_time
     * @param string $end_time
     * @param string $date
     */
    protected function getTotalHours($start_time, $end_time, $date)
    {
        $duration = strtotime($end_time) - strtotime($start_time);
        if ($duration < 0)
        {   
            $date_tomorrow = date('Y-m-d', strtotime($date . ' +1 days'));
            $duration = strtotime($date_tomorrow . ' ' . $end_time) - strtotime($date . ' ' . $start_time);
        }
        
        $duration_in_hours = $duration / 3600;
        return $duration_in_hours - 1; // -1 Hours (Because of break time)
    }

    public function getOvertimeTotal($end_time, $end_schedule)
    {
        $duration = strtotime($end_time) - strtotime($end_schedule);
        if ($duration < 0)
            return 0;
        else
            return $duration/3600;
    }

    protected function getTardinessMins($start_time, $start_schedule)
    {
        $duration = strtotime($start_time) - strtotime($start_schedule);
        if ($duration < 0)
            return 0;
        else
            return $duration/60;
    }

    protected function getTardinessHours($start_time, $start_schedule)
    {
        $duration = strtotime($start_time) - strtotime($start_schedule);
        if ($duration < 0)
            return 0;
        else
            return $duration/3600;
    }

    protected function getEmployeeData($emp_id, $date_range)
    {
        if (isset($emp_id)):

            // Put $_GET['emp_id'] value into variable $emp_id
            //$emp_id = $_GET['emp_id'];

            // Initialize Count Variables
            $count['no_schedule'] = 0;
            $count['absences'] = 0;
            $count['no_of_days'] = 0;
            $count['restday_work_hours'] = 0;
            $count['restday'] = 0;
            $count['holiday'] = 0;
            $count['holiday_work'] = 0; 
            $count['overtime_hours'] = 0;
            $count['tardiness_mins'] = 0;
            $count['tardiness_hours'] = 0;
            $count['vacation_leave'] = 0;
            $count['sick_leave'] = 0;

            // Get Date Ranges (Date Start & Date End)
            $date_range_array = explode(' → ', $date_range); // Explode Date Range

                // Date Start

                    // Explode by comma, First index[0] will contain month day (January 01), second index[1] will contain year (2014)
                    $date_start_array = explode(', ', $date_range_array[0]); 

                    // Explode first index of $date_start_array by space
                    $date_start_MD = explode(' ', $date_start_array[0]);                 

                    // Generate date start
                    $date_start['year'] = $date_start_array[1];
                    $date_start['month'] = date('m', strtotime($date_start_MD[0]));
                    $date_start['day'] = $date_start_MD[1];

                    $start_date = $date_start['year'] . '-' . $date_start['month'] . '-' . $date_start['day'];

                // Date End

                    // Explode by comma, First index[0] will contain month day (January 01), second index[1] will contain year (2014)
                    $date_end_array = explode(', ', $date_range_array[1]); 

                    // Explode first index of $date_end_array by space
                    $date_end_MD = explode(' ', $date_end_array[0]);                 

                    // Generate date end
                    $date_end['year'] = $date_end_array[1];
                    $date_end['month'] = date('m', strtotime($date_end_MD[0]));
                    $date_end['day'] = $date_end_MD[1];

                    $end_date = $date_end['year'] . '-' . $date_end['month'] . '-' . $date_end['day'];

            // Get Date Range
            $maps = $this->getDays($start_date, $end_date);

            /** 
             * Process Employee Data:
             */

            // Phase 1: Find Relevant Logs for the given date and employee_id
            $model['logs'] = Logs::model()->findAll(array(
                'select' => "DATE_FORMAT(checkin, '%H:%i') AS checkin, DATE_FORMAT(checkout, '%H:%i') AS checkout, id, hr_logs, hr_status, date, status, schedule",
                'condition' => 'emp_id = :emp_id AND date >= :start_date AND date <= :end_date AND is_active = 1',
                'params' => array(
                    ':emp_id' => $emp_id,
                    ':start_date' => $start_date,
                    ':end_date' => $end_date,
                )
            )); 

            // Phase 2: Invoke Logic
            foreach($maps as $map):

                $is_present = false; // -> Boolean variable to determine whether an employee is "present" or not

                foreach ($model['logs'] as $logs):

                    if ($map['date_map'] == $logs['date'] && $logs['status'] != 1): // If date map and logs date are the same, and schedule is set

                        // Explode Schedule
                        $logs_schedule = explode(' - ', $logs['schedule']);

                        // Check if Halfday
                        if ($logs['status'] == '10' || $logs['status'] == '11' || $logs['status'] == '12'):
                            $count['no_of_days'] += .5; // Consider as only halfday
                            $count['absences'] += .5; // Add half a day to absences

                        // If Restday Work
                        elseif ($logs['status'] == '7'):
                            $count['restday_work_hours'] += number_format ($this->getTotalHours($logs['checkin'], $logs['checkout'], $logs['date']), 2);

                        // Complete Day
                        else: 
                            $count['no_of_days']++; // Count no. of days present
                            $count['tardiness_mins'] += number_format ($this->getTardinessMins($logs['checkin'], $logs_schedule[0]), 2);
                            $count['tardiness_hours'] += number_format ($this->getTardinessHours($logs['checkin'], $logs_schedule[0]), 2);
                            $count['overtime_hours'] += number_format ($this->getOvertimeTotal($logs['checkout'], $logs_schedule[1]), 2);

                        endif;
                        
                        $is_present = true;
                        break; // Ends unecessary loop
                    endif;

                endforeach; // End logs

                if (! $is_present):

                    // Load schedule model to check
                    $model['schedule'] = Schedule::model()->find(array(
                        'condition' => 'emp_id = :emp_id AND date = :date AND is_active = 1',
                        'params' => array(
                            ':emp_id' => $emp_id,
                            ':date' => $map['date_map'],
                        ),
                    ));

                    if ($model['schedule']['id'] != NULL):

                        if ($model['schedule']['is_restday']) // Check if restday
                            $count['restday']++;

                        elseif ($model['schedule']['is_leave'] == 1 || $employee_schedule['is_leave'] == 2) // Check if vacation/maternity paternity leave
                            $count['vacation_leave']++;

                        else if ($model['schedule']['is_leave'] == 3) // Check if sick leave
                            $count['sick_leave']++;

                        else if ($model['schedule']['is_holiday']) // Check if holiday
                            $count['holiday']++;

                        else // Absent
                            $count['absences']++; // Count no. of absences

                    else:
                        $count['no_schedule']++;

                    endif;

                endif;
            endforeach; // End $maps
            
            //echo ")]}',\n" . json_encode($count);
            return $count;

        endif;
    } 
}

// End of file controllers/ManhoursAPIController.php