<?php

/**
 * LogsAPI Controller
 * Created By: Syd Evangelista
 * 
 * Class containing protected API Methods for LogsController
 * Extend LogsController to this class for ease of use
 *
 */

class LogsAPIController extends RController
{
    /**
     * Filter Route. Used for user authentication
     *
     */
    public function filters()
    {
        return array(
            'rights',
        );
    }

    /**
     * Protected function deactivateSchedule()
     * 
     * Sets is_active to 0, based on the given parameters
     * @param $date
     * @param $emp_id
     */
    protected function deactivateSchedule($date, $emp_id)
    {
        Schedule::model()->updateAll(
            array(
                'is_active' => 0,
            ),
            "date = :date AND emp_id = :emp_id",
            array(
                ":date" => $date,
                ":emp_id" => $emp_id
            )
        );
    }

    /**
     * Protected function getDays
     *
     * Returns date ranges between start and end date
     *
     * @param string $start_date
     * @param string $end_date
     */
    protected function getDays($start_date, $end_date) 
    {
        $date_interval = (strtotime ( $end_date ) - strtotime ( $start_date )) / 86400; // 1 day = 86400 seconds
        $date = array ();
        
        for($i = 0; $i <= $date_interval; $i ++) 
        {
            $date [$i] ['day'] = date ( 'D', strtotime ( $start_date . ' + ' . $i . ' days' ) );
            $date [$i] ['month'] = date ( 'M', strtotime ( $start_date . ' + ' . $i . ' days' ) );
            $date [$i] ['date'] = date ( 'j', strtotime ( $start_date . ' + ' . $i . ' days' ) );
            $date [$i] ['year'] = date ( 'Y', strtotime ( $start_date . ' + ' . $i . ' days' ) );
            $date [$i] ['date_map'] = date ( 'Y', strtotime ( $start_date . ' + ' . $i . ' days' ) ) . '-' . date ( 'm', strtotime ( $start_date . ' + ' . $i . ' days' ) ) . '-' . date ( 'd', strtotime ( $start_date . ' + ' . $i . ' days' ) );
        }
        
        return $date;
    }

    /**
     * Protected function getTotalHours
     *
     * Returns total hours of work (Excluding 1 hour break)
     *
     * @param string $start_time
     * @param string $end_time
     * @param string $date
     */
    protected function getTotalHours($start_time, $end_time, $date)
    {
        $duration = strtotime($end_time) - strtotime($start_time);
        if ($duration < 0)
        {   
            $date_tomorrow = date('Y-m-d', strtotime($date . ' +1 days'));
            $duration = strtotime($date_tomorrow . ' ' . $end_time) - strtotime($date . ' ' . $start_time);
        }
        
        $duration_in_hours = $duration / 3600;
        return $duration_in_hours - 1; // -1 Hours (Because of break time)
    }

    protected function getEmployeeLogs($emp_id, $date_interval)
    {
        // Initialize $data
        $data = array();

        // Get start and end date values
        $start_date_array = reset($date_interval);
        $end_date_array = end($date_interval);

        // Pass values to start and end dates
        $start_date = $start_date_array['key'];
        $end_date = $end_date_array['key'];

        // Get logs of respective user
        $model['logs'] = Logs::model()->findAll(array(
            "select" => "id, emp_id, date, schedule, status, hr_logs, hr_status, DATE_FORMAT(checkin, '%H:%i') AS checkin, DATE_FORMAT(checkout, '%H:%i') AS checkout",
            "condition" => "date >= :start_date AND date <= :end_date AND is_active=:is_active AND emp_id = :emp_id",
            "params" => array(
                ":start_date" => $start_date,
                ":end_date" => $end_date,
                ":is_active" => 1,
                ":emp_id" => $emp_id,
            )
        ));

        foreach ($date_interval as $date):

            $is_present = FALSE;

            foreach ($model['logs'] as $log):

                if ($date['key'] == $log['date']):

                    // Determine Logs
                    if ($log['hr_logs'] != NULL):
                        $data[$date['key']]['logs'] = $log['hr_logs'];
                        $data[$date['key']]['or_logs'] = $log['checkin'] . ' - ' . $log['checkout'];
                        $data[$date['key']]['hr_logs'] = $log['hr_logs'];
                    else:
                        $data[$date['key']]['logs'] = $log['checkin'] . ' - ' . $log['checkout'];
                        $data[$date['key']]['or_logs'] = $log['checkin'] . ' - ' . $log['checkout'];
                        $data[$date['key']]['hr_logs'] = $log['hr_logs'];
                    endif;

                    // Determine Status
                    if ($log['hr_status'] != NULL):
                        $data[$date['key']]['status'] = $log['hrStatus']['name'];
                        $data[$date['key']]['og_status'] = $log['status0']['name'];
                        $data[$date['key']]['hr_status'] = $log['hr_status'];
                    else:
                        $data[$date['key']]['status'] = $log['status0']['name'];
                        $data[$date['key']]['og_status'] = $log['status0']['name'];
                        $data[$date['key']]['hr_status'] = $log['hr_status'];
                    endif;

                    // Determine Status Color
                    if ($log['hr_status'] != NULL || $log['hr_logs'] != NULL): // HR Override
                        $data[$date['key']]['color'] = '#E2A9F3';
                    else:

                        if ($log['status'] == 1) // No Schedule
                            $data[$date['key']]['color'] = '#CEE3F6';

                        else if ($log['status'] == 2) // Normal
                            $data[$date['key']]['color'] = '#FFFFFF';

                        else if ($log['status'] == 3) // Late
                            $data[$date['key']]['color'] = '#F9BCA9';

                        else if ($log['status'] == 7) // Restday Work
                            $data[$date['key']]['color'] = '#E6E6E6';

                        else if ($log['status'] == 8) // Undertime
                            $data[$date['key']]['color'] = '#FAAC58';

                        else if ($log['status'] == 13) // Leave Work
                            $data[$date['key']]['color'] = '#E1F5A9';

                        else if ($log['status'] == 14) // Holiday Work
                            $data[$date['key']]['color'] = '#A9F5BC';

                        else if ($log['status'] == 10 || $log['status'] == 11 || $log['status'] == 12) // Halfday
                            $data[$date['key']]['color'] = '#F78181';

                    endif;

                    // Determine Schedule
                    $data[$date['key']]['schedule'] = $log['schedule'];

                    // Determine Shift
                    $shift_array = explode(' - ', $log['schedule']);

                    $shift = Shift::model()->find(array(
                        'condition' => 'start_time = :start_time AND end_time = :end_time',
                        'params' => array(
                            ':start_time' => $shift_array[0],
                            ':end_time' => $shift_array[1],
                        )
                    ));
                    $data[$date['key']]['shift'] = $shift['id'];

                    $is_present = TRUE;
                    break;

                endif;

            endforeach; // End logs

            if (! $is_present): // No logs was found

                // Load schedule model to check
                $model['schedule'] = Schedule::model()->find(array(
                    'condition' => 'emp_id = :emp_id AND date = :date AND is_active = 1',
                    'params' => array(
                        ':date' => $date['key'],
                        ':emp_id' => $emp_id,
                    ),
                ));

                // Set Schedule
                $data[$date['key']]['schedule'] = '';

                // Set Shift
                $data[$date['key']]['shift'] = NULL;

                if ($model['schedule']['id'] != NULL):

                    if ($model['schedule']['is_restday']): // Check if restday
                        $data[$date['key']]['logs'] = 'Restday';
                        $data[$date['key']]['status'] = 'Restday';
                        $data[$date['key']]['color'] = '#E6E6E6';

                    elseif ($model['schedule']['is_leave']): // Check if leave
                        $data[$date['key']]['logs'] = $model['schedule']['isLeave']['name'];
                        $data[$date['key']]['status'] = $model['schedule']['isLeave']['name'] . ' Leave';
                        $data[$date['key']]['color'] = '#E1F5A9';

                    elseif ($model['schedule']['is_holiday']): // Check if holiday
                        $data[$date['key']]['logs'] = $model['schedule']['isHoliday']['name'];
                        $data[$date['key']]['status'] = $model['schedule']['isHoliday']['name'];
                        $data[$date['key']]['color'] = '#A9F5BC';

                    else: // Absent
                        $data[$date['key']]['logs'] = 'Absent';
                        $data[$date['key']]['status'] = 'Absent';
                        $data[$date['key']]['color'] = '#F78181';

                        // Set Schedule
                        $data[$date['key']]['schedule'] = $model['schedule']['shift']['start_time'] . ' - ' . $model['schedule']['shift']['end_time'];

                        // Set Shift
                        $data[$date['key']]['shift'] = $model['schedule']['shift_id'];

                    endif;

                else:
                    $data[$date['key']]['logs'] = 'No Schedule';
                    $data[$date['key']]['status'] = 'No Schedule';
                    $data[$date['key']]['color'] = '#CFE3F6';

                endif;

            endif;
            
            // Determine Date
            $data[$date['key']]['date'] = $date['key'];

        endforeach; // End date_interval

        return $data;
    }

    protected function getLogStatus($emp_id, $checkin, $checkout, $date)
    {
        // Initialize $data array
        $data = array();
        $schedin = null;
        $schedout = null;
        $sched_log = null;

        // Phase 1: Get schedule of employee for the said date
        $model['schedule'] = Schedule::model()->find(array(
            "condition" => "emp_id = :emp_id AND date = :date AND is_active = 1",
            "params" => array(
                ":emp_id" => $emp_id,
                ":date" => $date,
            ),
        ));

        $status = NULL; // Set Default value of status to NULL

        // Phase 2: Compare check values with sched values.
        if ($model['schedule']['id'] != NULL): // Schedule value is NOT NULL

            // Check if Restday
            if ($model['schedule']['is_restday']):
                $status = 'Restday Work';
            elseif ($model['schedule']['is_leave']):
                $status = 'Leave Work';
            elseif ($model['schedule']['is_holiday']):
                $status = 'Holiday Work';
            else:

                // Split Schedule into separate data (in and out values)
                $schedin = $model['schedule']['shift']['start_time'];
                $schedout = $model['schedule']['shift']['end_time'];
                $sched_log = $schedin . ' - ' . $schedout;

                // Check if Half-day - No Login
                if ($checkin == NULL && $checkout != NULL):
                    $status = 'Halfday (No Login)';

                // Check if Half-day - No Logout
                elseif ($checkin != NULL && $checkout == NULL):
                    $status = 'Halfday (No Logout)';
                    
                // Check if Half-day - Late by more than 1 hour
                elseif ( ( strtotime($schedin) - strtotime($checkin) ) < -3600 ): // 3600 indicates 1 hour
                    $status = 'Halfday (More than 1 hr late)';

                // Check if Late
                elseif ( ( strtotime($schedin) - strtotime($checkin) ) < -60 && // At least 1 minute late AND
                     ( strtotime($schedin) - strtotime($checkin) ) >= -3600 ): // Must not be greater that 1 hour
                    $status = 'Late';

                else:
                    $status = 'Normal';

                endif;

            endif;

        else: // No schedule was found for this date and employee.
            $status = 'No Schedule';

        endif;

        // Phase 3: Get Equivalent id of the said table
        $model['log_status'] = LogStatus::model()->find(array(
            "condition" => "name = :name",
            "params" => array(
                ":name" => $status,
            ),
        ));

        // Phase 4: Populate Data
        $data['schedule_id'] = $model['log_status']['id'];
        $data['schedule_name'] = $sched_log;

        // Phase 5: Return status log id
        return $data;
    }

    protected function getEmployeeDetails()
    {
        $data = array();

        // Get Employee ID
        $data['user_id'] = Yii::app()->user->id;
        $data['username'] = Yii::app()->user->name;
        $data['emp_id'] = Users::model()->userEmpId($data['user_id'], $data['username']);

        return $data;
    }

}

// End of file controllers/logsAPIController.php