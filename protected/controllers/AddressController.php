<?php

class AddressController extends RController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/profile';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			// 'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
			'rights',
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	// public function accessRules()
	// {
	// 	return array(
	// 		array('allow',  // allow all users to perform 'index' and 'view' actions
	// 			'actions'=>array('index','view'),
	// 			'users'=>array('*'),
	// 		),
	// 		array('allow', // allow authenticated user to perform 'create' and 'update' actions
	// 			'actions'=>array('create','update'),
	// 			'users'=>array('@'),
	// 		),
	// 		array('allow', // allow admin user to perform 'admin' and 'delete' actions
	// 			'actions'=>array('admin','delete'),
	// 			'users'=>array('admin'),
	// 		),
	// 		array('deny',  // deny all users
	// 			'users'=>array('*'),
	// 		),
	// 	);
	// }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView()
	{		
		$emp_id = Yii::app()->request->getQuery('id');
		$criteria = new CDbCriteria();
		$criteria->condition = "emp_id =:emp_id";
		$criteria->params = array(':emp_id' => $emp_id);
		$model = Address::model()->findAll($criteria);
	
		$this->render('view',array(
			'model'=>$model,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		if(Yii::app()->user->checkAccess('Admin')){
			$this->layout = "//layouts/admin_profile_view";
		}
		$model=new Address;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Address']))
		{
			$model->attributes=$_POST['Address'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate()
	{

		$this->layout = "//layouts/admin_profile_view";
		$model=new Address;
		$id = $_GET['id'];
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Address']))
		{
			$model->attributes=$_POST['Address'];
			if($model->save())
				$emp_id = $_GET['emp_id'];
				$this->redirect(array('create?emp_id='.$emp_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Address');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Address('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Address']))
			$model->attributes=$_GET['Address'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Address the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Address::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Address $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='address-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function getEmployeeAddresses($emp_id){

		$sql = "SELECT emp_id, id,name AS type,location FROM tbl_address WHERE emp_id=:emp_id";
		$dataReader = Yii::app()->db->CreateCommand($sql);
		$dataReader->bindParam(":emp_id",$emp_id,PDO::PARAM_INT);
		$addresses = $dataReader->queryAll();

		return $addresses;


	}

	public function actionGetAddress()
	{
		if (isset($_GET['emp_id']))
		{
			$emp_id = $_GET['emp_id'];

			$model['address'] = Address::model()->findAll(array(
				'condition' => 'emp_id = :emp_id',
				'params' => array(
					':emp_id' => $emp_id
				)
			));

			foreach ($model['address'] as $address){
				$address_array[$address['id']]['name'] = $address['name'];
				$address_array[$address['id']]['location'] = $address['location'];
				$address_array[$address['id']]['id'] = $address['id'];
			}			

			echo ")]}',\n" . json_encode($address_array);
		}
	}

	public function actionCreateAddressAjax()
	{
		$emp_id = Yii::app()->request->getQuery('id');

		if(isset($_POST['addresses'])){
			
			foreach($_POST['addresses'] as $key=>$value){
         
				$address_model = new Address;
				$address_model->name = $value['address'];
				$address_model->location = $value['location'];
				$address_model->emp_id = $emp_id;
				$address_model->save();

			}
		}
	}
}
