<?php

/**
 * Logs Controller
 * Created By: 
 * 
 * Add Comment Here
 * 
 */

// Import Schedule Controller to use methods within it
Yii::import('application.controllers.ScheduleController');

class HomeController extends ScheduleController
{
    public function actionIndex()
    {
    	// Get LogsForm Model
        $data['model']['logs_form'] = new LogsForm;

        // Get Employee
        $employee_details = $this->getEmployeeDetails();
		$model['employee'] = Employee::model()->find(array(
			'condition' => 'emp_id = :emp_id',
			'params' => array(
				':emp_id' => $employee_details['emp_id']
			)
		));

		// Get User
        $data['user'] = $model['employee']['fullnameunformal'];

        // Get Current Day
		$data['day'] = date('Y-m-d');
		$data['day_json'] = json_encode($data['day']);

    	$this->render('index', $data);
    }

    public function actionGetLeaveCredits()
    {
        $uid =  Yii::app()->user->id;
        $user = Users::model()->findByAttributes(array('id'=>$uid));
        $userEmp = $user->emp_id;
        $employee_model = Employee::model()->findByAttributes(array('emp_id'=>$userEmp));
        $vl_count = $employee_model->vl_count;
        $sl_count = $employee_model->sl_count;
        $leave_count['vl'] = $vl_count;
        $leave_count['sl'] = $sl_count;
        // print_r($leave_count);
        echo json_encode($leave_count);
    }

    public function actionGetLeaveRequest()
    {
        $uid =  Yii::app()->user->id;
        $user = Users::model()->findByAttributes(array('id'=>$uid));
        $userEmp = $user->emp_id;
        $monthFirstDay = date("Y-m-01", strtotime("this month") ) ;
        $monthLastDay = date("Y-m-t", strtotime("this month") ) ;

        $leaveFiled = Yii::app()->db->createCommand()
            ->select('start_date, end_date, leave_type_id, request_type_id')
            ->from('tbl_leave_request')
            ->where('emp_id=:emp_id', array(':emp_id'=>$userEmp))
            ->andWhere('start_date between :mfd AND :mld', array(':mfd'=>$monthFirstDay, ':mld'=>$monthLastDay))
            ->queryAll();

        $leaveType = LeaveType::model()->findAll(); //leave type model

        $leaveTypeArr = array();
        foreach ($leaveType as $ltype) {
            $id = $ltype['id'];
            $name = $ltype['name'];
            $name = "$name Leave";
            $leaveTypeArr[$id] = $name;
        }

        $requestType = RequestType::model()->findAll(); //request type model

        $requestTypeArr = array();
        foreach ($requestType as $rtype) {
            $id = $rtype['id'];
            $name = $rtype['name'];
            $requestTypeArr[$id] = $name;
        }

        $counter=0;
        $leave_request_array = array();
        foreach ($leaveFiled as $lf) {

            $start_date = $lf['start_date'];
            $end_date = $lf['end_date'];
            $leave_type_id = $lf['leave_type_id'];
            $request_type_id = $lf['request_type_id'];
            $leave_type = $leaveTypeArr[$leave_type_id];
            $request_type = $requestTypeArr[$request_type_id];

            $leave_request_array[$counter]['start_date'] = $start_date;
            $leave_request_array[$counter]['end_date'] = $end_date;
            $leave_request_array[$counter]['leave_type'] = $leave_type;
            $leave_request_array[$counter]['request_type'] = $request_type;
            $counter ++;
        }
        echo json_encode($leave_request_array);
    }
    
}