<?php

/**
 * Cutoff Controller
 * Created By: Syd Evangelista
 * 
 * Class containing ROUTES for access (Whether route leading to views and/or AJAX methods)
 *
 */

class CutoffController extends RController
{
    /**
     * Filter Route. Used for user authentication
     *
     */
    public function filters()
    {
        return array(
            'rights',
        );
    }

    /**
     * Public function Index
     *
     * Sets Cutoff Period.
     * Access By: HR
     */
    public function actionIndex() 
    {
        // Generate Cutoff Model
        $model['cutoff'] = CutoffPeriod::model()->findAll();

        // Render View
        $this->render('index', $data);
    }

    public function actionGetCutoffAll()    
    {
        // Generate Cutoff Model
        $model['cutoff'] = CutoffPeriod::model()->findAll();

        foreach ($model['cutoff'] as $cutoff):
            $key = $cutoff['date_start'];
            $cutoff_array[$key]['id'] = $cutoff['id'];
            $cutoff_array[$key]['date_start'] = $cutoff['date_start'];
            $cutoff_array[$key]['date_end'] = $cutoff['date_end'];
        endforeach;

        echo ")]}',\n" . json_encode($cutoff_array);
    }

    public function actionAddCutoff()
    {
        if (isset($_POST['id'])):
            $model_cutoff = new CutoffPeriod;

            $model_cutoff->date_start = $_POST['date_start'];
            $model_cutoff->date_end = $_POST['date_end'];

            $model_cutoff->save();
        endif;
    }

    public function actionEditCutoff()
    {
        if (isset($_POST['id'])):

            CutoffPeriod::model()->updateAll(
                array(
                    'date_start' => $_POST['date_start'],
                    'date_end' => $_POST['date_end']
                ),
                "id = {$_POST['id']}"
            );

        endif;
    }

    public function actionDeleteCutoff()
    {
        if (isset($_POST['key_validate_delete_u179803'])):
            $cutoff_delete = CutoffPeriod::model()->findByPk($_POST['id']);
            $cutoff_delete->delete();
        endif;
    }

}

// End of file controllers/CutoffController.php