
<?php

class LeaveRequestController extends RController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'rights',
			// 'accessControl', // perform access control for CRUD operations
			// 'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		// return array(
		// 	array('allow',  // allow all users to perform 'index' and 'view' actions
		// 		'actions'=>array('index','view'),
		// 		'users'=>array('*'),
		// 	),
		// 	array('allow', // allow authenticated user to perform 'create' and 'update' actions
		// 		'actions'=>array('create','update'),
		// 		'users'=>array('@'),
		// 	),
		// 	array('allow', // allow admin user to perform 'admin' and 'delete' actions
		// 		'actions'=>array('admin','delete'),
		// 		'users'=>array('admin'),
		// 	),
		// 	array('deny',  // deny all users
		// 		'users'=>array('*'),
		// 	),
		// );
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new LeaveRequest;

        $uid =  Yii::app()->user->id;
  		$user = Users::model()->findByAttributes(array('id'=>$uid));
    	$userEmp = $user->emp_id;
        $uid = Employee::model()->findByAttributes(array('emp_id'=>$userEmp));

        $eid = $uid['emp_id'];
    	$userlogin = Yii::app()->db->createCommand()
    		->select('emp_id, firstname, lastname, position_id as p, vl_count')
    		->from('tbl_employee te', 'users u')
    		->where('emp_id=:emp_id', array(':emp_id'=>$eid))
    		->queryRow();
		$leaveList = CHtml::listData(LeaveType::model()->findAll(array('condition'=>'for_leave_form=1')),'id','name');
		// print_r($leaveList);
		
		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['LeaveRequest']))
		{

			$sd = $_POST['LeaveRequest']['start_date'];
			$df = $_POST['LeaveRequest']['date_filed'];
			$ed = $_POST['LeaveRequest']['end_date'];
			$ltID = $_POST['LeaveRequest']['leave_type_id'];
			// echo $sd.$ltID;die();
			$start_date = strtotime($sd);
			$date_filed = strtotime($df);
			$end_date = strtotime($ed);
			$start_end_diff = $end_date - $start_date;
			$diff = abs($date_filed - $start_date);
			// echo $df.$sd ;die();
			$diff = $diff/86400;

			if($diff >= 14){
				if($start_end_diff >= 0)
				{	
					$model->attributes=$_POST['LeaveRequest'];
					if($model->save()){
						$this->redirect(array('view','id'=>$model->id));
					}
				}else{
					$user = Yii::app()->getComponent('user');
					$user->setFlash(
				    	'error',
    					'<strong>Leave end date should be later than leave start date.</strong>'
					);

					$this->widget('booster.widgets.TbAlert', array(
   						'fade' => true,
    					'closeText' => '&times;', // false equals no close link
    					'events' => array(),
    					'htmlOptions' => array(),
    					'userComponentId' => 'user',
    					'alerts' => array( // configurations per alert type
        					'error' => array('closeText' => '&times;'),
    					),
					));
				}
			}else{
				$user = Yii::app()->getComponent('user');
				$user->setFlash(
				    'error',
    				'<strong>You should file two weeks before your leave start date.</strong>'
				);

				$this->widget('booster.widgets.TbAlert', array(
   					'fade' => true,
    				'closeText' => '&times;', // false equals no close link
    				'events' => array(),
    				'htmlOptions' => array(),
    				'userComponentId' => 'user',
    				'alerts' => array( // configurations per alert type
        				'error' => array('closeText' => '&times;'),
    				),
				));
			}
		}

		$this->render('create', compact('model', 'user', 'leaveList', 'employeeID', 'userlogin'));
	}

	// public function actionTest()
	// {
	// 	$user = User::model()->findByPk(Yii::app()->user->emp_id);
	// 	var_dump($user);
	// }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['LeaveRequest']))
		{
			$model->attributes=$_POST['LeaveRequest'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionLeave()
	{
		$user = User::model()->findByPk(Yii::app()->user->id);
		//
        $userid = Yii::app()->user->id;

        //get employee id
        $uid = Users::model()->findByAttributes(array('id'=>$userid));
        	$eid = $uid['emp_id'];

        //get employee details
    	$userlogin = Yii::app()->db->createCommand()
    		->select('emp_id, firstname, lastname, position_id')
    		->from('tbl_employee te', 'users u')
    		->where('emp_id=:emp_id', array(':emp_id'=>$eid))
    		->queryRow();
    		$employeeid = $userlogin['emp_id'];
    		$empPositionId = $userlogin['position_id'];

    	//get hierarchy id
    	$hquery = Position::model()->findByAttributes(array('id'=>$empPositionId));
    	$hid = $hquery['hierarchy_id'];
    	$hierarchyId = $hid +1;

    	$model_request_type = RequestType::model()->findAll();

    	$request_type_array = array();
        foreach ($model_request_type as $mr) {
        	$id = $mr['id'];
        	$name = $mr['name'];
        	$request_type_array[$id] = $name;
        }

        $model_leave_type = LeaveType::model()->findAll();

        $leave_type_array = array();
        foreach ($model_leave_type as $ml) {
        	$id = $ml['id'];
        	$name = $ml['name'];
        	$leave_type_array[$id] = $name;
        }

    	$model=new LeaveRequest('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['LeaveRequest']))
			$model->attributes=$_GET['LeaveRequest'];

		$model->emp_id = $employeeid;

		$this->render('leave',compact('model', 'request_type_array', 'leave_type_array'));
	}

	public function actionApproveRequest()
	{
		// Phase 1: Get User
		$user_id = Yii::app()->user->id;
		$user = Users::model()->findByAttributes(array('id'=>$user_id));

		// Phase 2: Get Employee ID through user
		$emp_id = $user->emp_id;

		// Phase 3: Get Position using employee id
		$model['employee'] = Employee::model()->findByAttributes(array('emp_id'=>$emp_id));
		$position_id = $model['employee']->position_id;

		// Phase 4: Get Hierarchy ID of user logged in
		$model['position'] = Position::model()->findByAttributes(array('id'=>$position_id));
		$userHierarchyId = $model['position']->hierarchy_id;
		$hierarchy_id = $model['position']->hierarchy_id + 1;

		$error = 0;
		$id  = isset($_POST['id']) ? $_POST['id'] : '99';


		$lr = LeaveRequest::model()->findByPk($id);

		if($lr)
		{
			if ($userHierarchyId == 4) //if hr will approved
			{
				$lr->request_type_id=1; //request is approved
				$lr->position_id=4;
				//for schedule
				$eid = $_POST['eid'];
				$sd  = strtotime($_POST['start_date']);
				$ed  = strtotime($_POST['end_date']);
				$leave_type_id = $_POST['leave_type_id'];

				$leave_count = 0;

				$date_interval = $this->getDays($_POST['start_date'], $_POST['end_date']);

				foreach ($date_interval as $date) {
					$model['schedule'] = Schedule::model()->find(array(
						'condition'=>'emp_id=:emp_id AND is_active=1 AND date=:date',
						'params'=>array(
							':emp_id'=>$eid,
							':date'=>$date['date_map']		
						)
					));
					if (!$model['schedule']['is_restday']) // 0 Value
					{
						$this->deactivateSchedule($date['date_map'], $eid);

						$schedule_model = new Schedule;

						$schedule_model->emp_id = $eid;
						$schedule_model->date = $date['date_map'];
						$schedule_model->is_active = 1;
						$schedule_model->is_leave = $leave_type_id;
						$schedule_model->is_restday = 0;

						$schedule_model->save();

						$leave_count++;
					}
				}

				$empModel = Employee::model()->findByAttributes(array('emp_id'=>$eid));
				$empAvailLeave = $empModel->vl_count;
				
				$newVlCount = $empAvailLeave - $leave_count;

				$empModel->vl_count = $newVlCount;
				$empModel->save();
				//
			}
			else
			{
				$lr->request_type_id=3; //request pending
				$lr->position_id=$hierarchy_id;
			}

		}
		if($lr->save()){
			$error++;
		}else{
			$error++;
		}
		echo json_encode(compact('error'));
	}

	public function actionDisapproveRequest()
	{
		$id=isset($_POST['id']) ? $_POST['id'] : '99';
		$lr = LeaveRequest::model()->findByPk($id);
		if ($lr)
		{
			$lr->request_disapproval = $_POST['reason'];
			$lr->request_type_id = 2;
			$lr->save();
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new LeaveRequest('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['LeaveRequest']))
			$model->attributes=$_GET['LeaveRequest'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return LeaveRequest the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=LeaveRequest::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param LeaveRequest $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='leave-request-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	/**
	 * REST routes for Angular
	 *
	 */
	public function actionGetPendingLeaveRequest()
	{
		// Phase 1: Get User
		$user_id = Yii::app()->user->id;
		$user = Users::model()->findByAttributes(array('id'=>$user_id));

		// Phase 2: Get Employee ID through user
		$emp_id = $user->emp_id;

		// Phase 3: Get Position using employee id
		$model['employee'] = Employee::model()->findByAttributes(array('emp_id'=>$emp_id));
		$position_id = $model['employee']->position_id;

		//get department of logged in
		$sub_department_id = $model['employee']->sub_department_id;
		$model['subDepartment'] = SubDepartment::model()->findByAttributes(array('id'=>$sub_department_id));
		$department_id = $model['subDepartment']->department_id;
		// echo $department_id;die();

		// Phase 4: Get Hierarchy ID
		$model['position'] = Position::model()->findByAttributes(array('id'=>$position_id));
		$hierarchy_id = $model['position']->hierarchy_id;
		// echo $hierarchy_id; 

		$model['leave_request'] = LeaveRequest::model()->findAll(array(
			'condition' => 'request_type_id = 3 AND position_id = :userHierarchyId',
			'params' => array(
				':userHierarchyId' => $hierarchy_id // +1 to show employess of lower hierarchy
			)
		));
		
		foreach($model['leave_request'] as $request)
		{
			if ($request['position_id'] == 2)
			{
				if ($request['emp']['subDepartment']['department_id'] == $department_id)
				{
					// $request_array[$request['id']]['id'] = $request_array['id'];
					$request_array[$request['id']]['emp_id'] = $request['emp_id'];
					$request_array[$request['id']]['emp_name'] = $request['emp']['fullname'];
					$request_array[$request['id']]['date_filed'] = $request['date_filed'];
					$request_array[$request['id']]['start_date'] = $request['start_date'];
					$request_array[$request['id']]['end_date'] = $request['end_date'];
					$request_array[$request['id']]['leave_type'] = $request['leaveType']['name'];
					$request_array[$request['id']]['leave_type_id'] = $request['leave_type_id']; 
					$request_array[$request['id']]['request_type'] = $request['requestType']['name'];
					$request_array[$request['id']]['hid'] = $request['position_id'];
					$hname = $request['position_id'];
					$request_array[$request['id']]['p_id'] = $request->getHierarchyName($hname);
				}
			}
			else
			{
				// $request_array[$request['id']]['id'] = $request_array['id'];
				$request_array[$request['id']]['emp_id'] = $request['emp_id'];
				$request_array[$request['id']]['emp_name'] = $request['emp']['fullname'];
				$request_array[$request['id']]['date_filed'] = $request['date_filed'];
				$request_array[$request['id']]['start_date'] = $request['start_date'];
				$request_array[$request['id']]['end_date'] = $request['end_date'];
				$request_array[$request['id']]['leave_type'] = $request['leaveType']['name'];
				$request_array[$request['id']]['leave_type_id'] = $request['leave_type_id'];
				$request_array[$request['id']]['request_type'] = $request['requestType']['name'];
				$request_array[$request['id']]['hid'] = $request['position_id'];
				$hname = $request['position_id'];
				$request_array[$request['id']]['p_id'] = $request->getHierarchyName($hname);
			}
		}

		// Phase 5: Check if $request_array is empty
		if (empty($request_array))
			echo '{}';
		else
			echo json_encode($request_array);
	}

	/**
	 * Protected function getDays
	 *
	 * Returns date ranges between start and end date
	 *
	 * @param string $start_date
	 * @param string $end_date
	 */
	protected function getDays($start_date, $end_date) 
	{
	    $date_interval = (strtotime ( $end_date ) - strtotime ( $start_date )) / 86400; // 1 day = 86400 seconds
	    $date = array ();
	    
	    for($i = 0; $i <= $date_interval; $i ++) 
	    {
	        $date [$i] ['day'] = date ( 'D', strtotime ( $start_date . ' + ' . $i . ' days' ) );
	        $date [$i] ['month'] = date ( 'M', strtotime ( $start_date . ' + ' . $i . ' days' ) );
	        $date [$i] ['date'] = date ( 'j', strtotime ( $start_date . ' + ' . $i . ' days' ) );
	        $date [$i] ['year'] = date ( 'Y', strtotime ( $start_date . ' + ' . $i . ' days' ) );
	        $date [$i] ['date_map'] = date ( 'Y', strtotime ( $start_date . ' + ' . $i . ' days' ) ) . '-' . date ( 'm', strtotime ( $start_date . ' + ' . $i . ' days' ) ) . '-' . date ( 'd', strtotime ( $start_date . ' + ' . $i . ' days' ) );
	    }
	    
	    return $date;
	}

	/**
	 * Protected function deactivateSchedule()
	 * 
	 * Sets is_active to 0, based on the given parameters
	 * @param $date
	 * @param $emp_id
	 */
	protected function deactivateSchedule($date, $emp_id)
	{
	    Schedule::model()->updateAll(
	        array(
	            'is_active' => 0,
	        ),
	        "date = :date AND emp_id = :emp_id",
	        array(
	            ":date" => $date,
	            ":emp_id" => $emp_id
	        )
	    );
	}

	public function actionReport()
	{	
		if (isset($_POST['submit']))
		{
			$from = $_POST['date1'];
			$to   = $_POST['date2'];
			$selectedLeaveType = $_POST['leaveTypeId'];
			$selectedRequestType = $_POST['requestTypeId'];
			$selectedEmployee = $_POST['empID'];
			$selectedDepartment = $_POST['depID'];

			// echo $selectedDepartment;
			$criteria = new CDbCriteria;

			if (isset($from) && isset($to))
			$criteria->addBetweenCondition('start_date', $from, $to, $operator='AND');

			//for leave type
			if ($selectedLeaveType != '')
				$criteria->addCondition("leave_type_id = $selectedLeaveType");

			//for request type
			if ($selectedRequestType != '')
				$criteria->addCondition("request_type_id = $selectedRequestType");
			
			//for employee
			if ($selectedEmployee != '' && $selectedDepartment == ''){
				$criteria->addCondition("emp_id=$selectedEmployee");
			}elseif($selectedEmployee != '' && $selectedDepartment != ''){
				$criteria->addCondition("emp.emp_id=$selectedEmployee");
			}

			if ($selectedDepartment != ''){
				$criteria->with = array('emp','emp.subDepartment');
				$criteria->compare('subDepartment.department_id',$selectedDepartment);
			}

			$result = LeaveRequest::model()->findAll($criteria);
		}

		$criteria = new CDbCriteria();
        $criteria->addCondition("status_id=1");
        $model_employee = Employee::model()->findAll($criteria);
        // $model['SlCount'] = SlCount::model()->findAll();

        $emp_name_array = array();
        foreach ($model_employee as $e) {
            $eid = $e['emp_id'];
            $fname = $e['firstname'];
            $lname = $e['lastname'];
            $name = $lname.", ".$fname;

            // $emp_name_array[][$eid] = $eid;
            $emp_name_array[$eid] = $name;
        }

        $model_leave_type = LeaveType::model()->findAll();

        $leave_type_array = array();
        foreach ($model_leave_type as $ml) {
        	$id = $ml['id'];
        	$name = $ml['name'];
        	$leave_type_array[$id] = $name;
        }

        $model_request_type = RequestType::model()->findAll();

        $request_type_array = array();
        foreach ($model_request_type as $mr) {
        	$id = $mr['id'];
        	$name = $mr['name'];
        	$request_type_array[$id] = $name;
        }

        $model_department = Department::model()->findAll();

        $department_array = array();
        foreach ($model_department as $d) {
        	$id = $d['id'];
        	$name = $d['name'];
        	$department_array[$id] = $name;
        }
        // print_r($department_array);

		$this->render('report',compact('result','emp_name_array', 'leave_type_array', 'request_type_array', 'department_array'));
	}
}
