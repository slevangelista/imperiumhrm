<?php

// This is the database connection configuration.
return array(
	//'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
	// uncomment the following lines to use a MySQL database
	
	'connectionString' => 'mysql:host=localhost;dbname=Imperium_Emp_Tools',
	'emulatePrepare' => true,
	'username' => 'root',
	'password' => 'mysqladmin',
	'charset' => 'utf8',
        'tablePrefix' => 'tbl_',	
);
