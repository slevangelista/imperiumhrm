<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(

	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Imperium HRM',
	'defaultController' => 'site/login',

	// preloading 'log' component
	'preload'=>array('log', 'booster','EExcelWriter'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
        'application.components.REST.*',
                'application.modules.user.models.*',
                'application.modules.user.components.*',
                'application.modules.rights.*',
                'application.modules.rights.components.*',
	),

	'modules'=>array(
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'imperium',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),

                'user'=>array(

                'tableUsers' => 'users',
                'tableProfiles' => 'profiles',
                'tableProfileFields' => 'profiles_fields',
            	# encrypting method (php hash function)
            	'hash' => 'md5',
 
            	# send activation email
            	'sendActivationMail' => true,
 
            	# allow access for non-activated users
            	'loginNotActiv' => false,
 
            	# activate user on registration (only sendActivationMail = false)
            	'activeAfterRegister' => false,
 
            	# automatically login from registration
            	'autoLogin' => true,
 
            	# registration path
            	'registrationUrl' => array('/user/registration'),
 
            	# recovery password path
            	'recoveryUrl' => array('/user/recovery'),
 
            	# login form path
            	'loginUrl' => array('/user/login'),
 
            	# page after login
            	'returnUrl' => array('/user/profile'),
 
            	# page after logout
            	'returnLogoutUrl' => array('/user/login'),

        	),

		/*'rights'=>array(
			'install'=>true,
                ),*/

		'rights'=>array(
				'superuserName'=>'Admin',
				'authenticatedName'=>'Authenticated',
				'userIdColumn'=>'id',
				'userNameColumn'=>'username',
				'enableBizRule'=>true,
				'enableBizRuleData'=>false,
				'displayDescription'=>true,
				'flashSuccessKey'=>'RightsSuccess',
				'flashErrorKey'=>'RightsError',
				'install'=>true,
				'baseUrl'=>'/rights',
				'layout'=>'rights.views.layouts.main',
				'appLayout'=>'application.views.layouts.main',
				'cssFile'=>'rights.css',
				'install'=>false,
				'debug'=>false,
			),
	),

    // application components
    'components'=>array(

        'booster'=>array(
            'class' => 'ext.booster.components.Booster',
        ),

	'EExcelWriter'=>array(
        'class'=>'ext.EExcelWriter.components.EExcelWriter',
    ),

		'user'=>array(
			// enable cookie-based authentication
                        'class' => 'RWebUser',
			'allowAutoLogin'=>true,
                        'loginUrl' => array('/site/login'),
		),

                'authManager'=>array(
                	'class'=>'RDbAuthManager',
                	'connectionID'=>'db',
                	//'defaultRoles'=>array('Authenticated', 'Guest'),
                        'itemTable'=>'AuthItem',
            		'itemChildTable'=>'AuthItemChild',
                        'assignmentTable'=>'AuthAssignment',
            		'rightsTable'=>'Rights',
                ),

		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),

        // uncomment the following to enable URLs in path-format
        
        'urlManager'=>array(
            'urlFormat'=>'path',
            'rules'=>array(
                '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
            ),
        ),

        // database settings are configured in database.php
        'db'=>require(dirname(__FILE__).'/database.php'),

        'errorHandler'=>array(
            // use 'site/error' action to display errors
            'errorAction'=>'site/error',
        ),

        'log'=>array(
            'class'=>'CLogRouter',
            'routes'=>array(
                array(
                    'class'=>'CFileLogRoute',
                    'levels'=>'error, warning',
                ),
                // uncomment the following to show log messages on web pages
                /*
                array(
                    'class'=>'CWebLogRoute',
                ),
                */
            ),
        ),

    ),

    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params'=>array(
        // this is used in contact page
        'adminEmail'=>'webmaster@example.com',
    ),

);

