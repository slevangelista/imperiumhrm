<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

	/**
	 * PutOny and DeleteOnly filters (Since it is not defined by default in Yii Framework)
	 *
	 */

	public function filterPutOnly($filter)
	{
		if (Yii::app()->getRequest()->getIsPutRequest()):
			$filter->run();
		else:
			throw new CHttpException(400, Yii::t('yii', 'Your PUT request is invalid.'));
		endif;
	}

	public function filterDeleteOnly($filter)
	{
		if (Yii::app()->getRequest()->getIsDeleteRequest()):
			$filter->run();
		else:
			throw new CHttpException(400, Yii::t('yii', 'Your DELETE request is invalid.'));
		endif;
	}
}