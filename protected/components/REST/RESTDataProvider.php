<?php

class RESTDataProvider extends CActiveDataProvider implements RESTInterface
{

    public function getDataResponse()
    {
        $result = array();

        foreach($this->getData() as $record):
            $result[] = $record->getAttributes();
        endforeach;

        return $result;
    }

    public function getErrors()
    {
        return array();
    }
}