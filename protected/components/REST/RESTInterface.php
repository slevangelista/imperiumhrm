<?php

interface RESTInterface
{

    public function getDataResponse();

    public function getErrors();

}