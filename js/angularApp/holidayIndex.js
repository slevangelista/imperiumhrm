var holidayIndexApp = angular.module('holidayIndexApp', ['ui.bootstrap','dialogs']);

holidayIndexApp.factory('holidayIndexFactory', function($http, $rootScope){

    return  {
        editHoliday: function ($params)    {
            $http ({
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: app_url + '/index.php/holiday/editHoliday',
                method: "POST",
                data: $params
            }).then(function(response)  {
                $rootScope.$broadcast("updateHoliday",response);
            });
        },
        addHoliday: function ($params)   {
            $http ({
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: app_url + '/index.php/holiday/addHoliday',
                method: "POST",
                data: $params
            }).then(function(response)  {
                $rootScope.$broadcast("updateHoliday",response);
            });        
        },
        deleteHoliday: function ($params)    {
            $http ({
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: app_url + '/index.php/holiday/deleteHoliday',
                method: "POST",
                data: $params
            }).then(function(response)  {
                $rootScope.$broadcast("updateHoliday",response);
            }); 
        },
    };

});

holidayIndexApp.controller('holidayIndexController', function($rootScope, $scope, $http, $timeout, $dialogs, holidayIndexFactory){

    var model = holidayIndexFactory;
    $scope.holiday;

    /**
     * RootScope Broadcast Response
     *
     */
    $scope.$on("updateHoliday", function(event,response){
        if(response.status == 200)  {
            $scope.getHolidayAll();
        } else  {
            console.log('Error: ' + response.statusText);
        }
    });

    /**
     * Scope Methods
     *
     */

    $scope.check = function(value)   {
        console.log(value);
    };

    $scope.getHolidayAll = function()   {
        $http.get(app_url + '/index.php/holiday/getHolidayAll').then(function(response) {
            $scope.holiday = _(response.data).toArray()
        });
    };

    $scope.addHoliday = function() {
        dlg = $dialogs.create('/dialogs/addHoliday.html','addHoliday', {},{key: false,back: 'static'});
        dlg.result.then(function(holiday){

            $params = $.param({
                name: holiday.name
            });

            model.addHoliday($params);

        },function(){
            //console.log("Save not invoked");
        });
    };

    $scope.editHoliday = function(key, value)   {
        dlg = $dialogs.create('/dialogs/editHoliday.html','editHoliday', {id: key, name: value},{key: false,back: 'static'});
        dlg.result.then(function(holiday){

            $params = $.param({
                id: holiday.id,
                name: holiday.name,
            });

            model.editHoliday($params);

        },function(){
            //console.log("Save not invoked");
        });
    };

    $scope.deleteHoliday = function(key, value) {
        dlg = $dialogs.create('/dialogs/deleteHoliday.html','deleteHoliday', {id: key},{key: false,back: 'static'});
        dlg.result.then(function(holiday){

            $params = $.param({
                id: holiday.id,
            });

            model.deleteHoliday($params);

        },function(){
            //console.log("Save not invoked");
        });
    };

});

holidayIndexApp.controller('addHoliday', function($scope, $modalInstance, data){

    $scope.holiday = {};

    $scope.cancel = function(){
        $modalInstance.dismiss('canceled');
    }; // end done
    
    $scope.save = function(){
        $modalInstance.close($scope.holiday);
    };

});

holidayIndexApp.controller('editHoliday', function($scope, $modalInstance, data){

    $scope.holiday = {
        id: data.id,
        name: data.name,
    };

    $scope.cancel = function(){
        $modalInstance.dismiss('canceled');
    }; // end done
    
    $scope.save = function(){
        $modalInstance.close($scope.holiday);
    };

});

holidayIndexApp.controller('deleteHoliday', function($scope, $modalInstance, data){

    $scope.holiday = {
        id: data.id,
    };

    $scope.cancel = function(){
        $modalInstance.dismiss('canceled');
    }; // end done
    
    $scope.save = function(){
        $modalInstance.close($scope.holiday);
    };

});

holidayIndexApp.run(['$templateCache',function($templateCache){
    
    $templateCache.put(
        '/dialogs/addHoliday.html',
        '<div class="modal"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <h4 class="modal-title"><span class="glyphicon glyphicon-plus"></span> Add Holiday</h4>  <span class="help-block">Create new Holiday</span> </div> <div class="modal-body"> <ng-form name="addHoliday" novalidate role="form"> <div class="form-group input-group-lg"> <label class="control-label" for="holiday_name">Holiday Name</label> <input type="text" class="form-control" name="holiday_name" id="holiday_name" ng-model="holiday.name" required> </div> </ng-form> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button> <button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(addHoliday.$dirty && addHoliday.$invalid) || addHoliday.$pristine">Save</button> </div> </div> </div> </div>'
    );

    $templateCache.put(
        '/dialogs/editHoliday.html',
        '<div class="modal"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <h4 class="modal-title"><span class="glyphicon glyphicon-pencil"></span> Edit Holiday</h4>  <span class="help-block">Edit selected holiday</span> </div> <div class="modal-body"> <ng-form name="editHoliday" novalidate role="form"> <div class="form-group input-group-lg"> <label class="control-label" for="holiday_name">Holiday Name</label> <input type="text" class="form-control" name="holiday_name" id="holiday_name" ng-model="holiday.name" required> <br /> </div> </ng-form> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button> <button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(editHoliday.$dirty && editHoliday.$invalid) || editHoliday.$pristine">Save</button> </div> </div> </div> </div>'
    );

    $templateCache.put(
        '/dialogs/deleteHoliday.html',
        '<div class="modal"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <h4 class="modal-title"><span class="glyphicon glyphicon-trash"></span> Delete Holiday</h4>  <span class="help-block">Removes selected holiday</span> </div> <div class="modal-body">Are you sure you want to delete?<span class="help-block">Note: This cannot be undone</span></div> <div class="modal-footer"> <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button> <button type="button" class="btn btn-danger" ng-click="save()" ng-disabled="(editHoliday.$dirty && editHoliday.$invalid) || editHoliday.$pristine">Delete</button> </div> </div> </div> </div>'
    );

}]); // end run / module

