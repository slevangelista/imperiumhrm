var scheduleIndexApp = angular.module('scheduleIndexApp', ['ngSanitize','ui.bootstrap','dialogs','ui.select','colorpicker.module','mgcrea.ngStrap.timepicker','mgcrea.ngStrap.datepicker']);

scheduleIndexApp.factory('scheduleIndexFactory', function($http, $rootScope){

    return {
        saveShift: function ($params)    {
            $http ({
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: app_url + '/index.php/schedule/saveShiftAjax',
                method: "POST",
                data: $params
            }).then(function(response)  {

            });
        },
        getEmployees: function ($params)    {
            return $http ({
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: app_url + '/index.php/schedule/getEmployeeByDepartment',
                method: "POST",
                data: $params
            }).then(function(response)  {
                return response.data
            });
        },
        saveSchedule: function ($params)    {
            $http ({
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: app_url + '/index.php/schedule/saveScheduleAjax',
                method: "POST",
                data: $params
            }).then(function(response) {
                $rootScope.$broadcast("updateSchedule",response);
            });
        },
        updateSchedule: function ($params)  {
            $http ({
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: app_url + '/index.php/schedule/updateSchedule',
                method: "POST",
                data: $params
            }).then(function(response){
                $rootScope.$broadcast("updateSchedule",response);
            });
        },
        setLeave: function ($params)    {
            $http ({
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: app_url + '/index.php/schedule/setLeaveAjax',
                method: "POST",
                data: $params
            }).then(function(response){
                $rootScope.$broadcast("updateSchedule",response);
            });
        },
        setHoliday: function ($params)    {
            $http ({
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: app_url + '/index.php/schedule/setHolidayAjax',
                method: "POST",
                data: $params
            }).then(function(response){
                $rootScope.$broadcast("updateSchedule",response);
            });
        },
    }    
});

/**
 * AngularJS default filter with the following expression:
 * "person in people | filter: {name: $select.search, age: $select.search}"
 * performs a AND between 'name: $select.search' and 'age: $select.search'.
 * We want to perform a OR.
 */
scheduleIndexApp.filter('selectFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      items.forEach(function(item) {
        var itemMatches = false;

        var keys = Object.keys(props);
        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  }
});

scheduleIndexApp.controller('scheduleIndexController', function($rootScope, $scope, $http, $filter, $timeout, $dialogs, scheduleIndexFactory){

    /**
     * Initialize scope variables
     *
     */

    var model = scheduleIndexFactory;

    $scope.test= [{'value': 1},{'value': 2},{'value': 3},{'value': 4}];
    $scope.test2={};

    $scope.currentDay;
    $scope.currentWeek = {};
    $scope.weekTitle;

    $scope.department;
    $scope.employees;
    $scope.is_department = false;

    $scope.days_in_week = [{'name':'Monday', 'id': 1}, {'name':'Tuesday', 'id': 2}, {'name':'Wednesday', 'id': 3}, {'name':'Thursday', 'id': 4}, {'name':'Friday', 'id': 5}, 
                            {'name':'Saturday', 'id': 6}, {'name':'Sunday', 'id': 7}];
    $scope.shifts;
    $scope.leaveList;
    $scope.holidayList;

    $scope.updateOptions = {
        '1': 'Change Shift',
        '2': 'Swap Shift with Another Employee',
        '3': 'Set as Restday',
        '4': 'Set as Leave',
        '5': 'Remove Schedule',
    };

    /**
     * RootScope Broadcast Response
     *
     */
    $scope.$on("updateSchedule", function(event,response){
        if(response.status == 200)  {
            $scope.setEmployees();
        } else  {
            console.log('Error: ' + response.statusText);
        }
    });

    /**
     * Controller Methods
     *
     */

    $scope.formatTime = function(time)
    {
        return $filter('date')(time, 'HH:mm');
    };

    $scope.formatDate = function(date)
    {
        return $filter('date')(date, 'yyyy-MM-dd');
    };

    $scope.setWeekTitle = function()    {
        var firstDay = $scope.currentWeek[Object.keys($scope.currentWeek)[0]];
        var lastDay = $scope.currentWeek[Object.keys($scope.currentWeek)[6]]
        $scope.weekTitle = firstDay + ' to ' + lastDay;
    };

    $scope.employeesToArray = function(object)   {
        $scope.employees_array = _($scope.employees).toArray();
    };

    $scope.startSchedule = function()   {
        $scope.is_department = true;
        $scope.setEmployees();
    };

    $scope.setEmployees = function()  {
        $params = $.param({
            'department': $scope.department,
            'currentWeek': $scope.currentWeek
        });
        model.getEmployees($params).then(function(response){
            $scope.employees = response;
            $scope.employeesToArray();
        });
    };

    $scope.setShift = function()    {
        $http.get(app_url + '/index.php/schedule/getShifts').then(function(response)   {
            $scope.shifts = response.data;
        });
    };
    
    $scope.setCurrentDay = function(day)    {
        $scope.currentDay = day;
    };

    $scope.setCurrentWeek = function () {
        $http.get(app_url + '/index.php/schedule/getWeek?date=' + $scope.currentDay).then(function(response) {
            $scope.currentWeek = response.data;

            // Update Title
            $scope.setWeekTitle();

            // Update schedule upon change of week
            $scope.setEmployees();
        });
    };

    $scope.setSchedule = function(day)  {
        $scope.setCurrentDay(day);
        $scope.setCurrentWeek();
    };

    $scope.getPreviousWeek = function() {
        // Get day last week, bind to currentDay scope
        $http.get(app_url + '/index.php/schedule/getDayLastWeek?day=' + $scope.currentDay).then(function(response)    {
            $scope.setCurrentDay(response.data);
            $scope.setCurrentWeek();
        });
    };

    $scope.getNextWeek = function() {
        // Get day next week, bind to currentDay scope
        $http.get(app_url + '/index.php/schedule/getDayNextWeek?day=' + $scope.currentDay).then(function(response)    {
            $scope.setCurrentDay(response.data);
            $scope.setCurrentWeek();
        });
    };

    $scope.getLeaveList = function()    {
        $http.get(app_url + '/index.php/schedule/getLeaveList').then(function(response)    {
            $scope.leaveList = response.data;
        });
    };

    $scope.getHolidayList = function()  {
        $http.get(app_url + '/index.php/schedule/getHolidayList').then(function(response)    {
            $scope.holidayList = response.data;
        });    
    };

    $scope.check = function (value)  {
        // console.log($scope.employees_array);
    };

    $scope.modalCreateShift = function()    {
        dlg = $dialogs.create('/dialogs/createShift.html','createShift', {},{key: false,back: 'static'});
        dlg.result.then(function(shift){
            $params = $.param({
                'shift': shift
            });
            model.saveShift($params);
            $scope.setShift();

            alert('Success! Shift has successfully been added.');
        },function(){
            //console.log("Save not invoked");
        });
    };

    $scope.modalAddSchedule = function() {
        dlg = $dialogs.create('/dialogs/addShift.html','addShiftToEmployee', {employees: $scope.employees_array, shift_list: $scope.shifts, days_in_week: $scope.days_in_week},{key: false,back: 'static'});
        dlg.result.then(function(addSchedule){
            // console.log(addSchedule);
            $params = $.param({
                'assigned': addSchedule.assigned,
                'days_assigned': addSchedule.days_assigned,
                'emp_id': addSchedule.emp_id,
                'start_date': $scope.formatDate(addSchedule.start_date),
                'end_date': $scope.formatDate(addSchedule.end_date),
            });

            model.saveSchedule($params);

        },function(){
            //console.log("Save not invoked");
        });
    };

    $scope.modalUpdateSchedule = function(render_id, emp_id, date) {
        dlg = $dialogs.create('/dialogs/updateShift.html', 'updateEmployeeShift', {
            render_id: render_id,
            emp_id: emp_id, 
            date: date, 
            option_list: $scope.updateOptions,
            shift_list: $scope.shifts, 
            employee_list: $scope.employees,
            leave_list: $scope.leaveList,
        }, 
        {key: false,back: 'static'});
        dlg.result.then(function(updateSchedule){

            $params = $.param({
                'selectedOption': updateSchedule.selectedOption,
                'emp_name': updateSchedule.emp_name,
                'assigned': updateSchedule.assigned,
                'date': updateSchedule.date,
                'emp_id_self': updateSchedule.emp_id_self,
                'swap_key_self': updateSchedule.swap_key_self,
                'swap_restday_self': updateSchedule.swap_restday_self,
                'swap_leave_self': updateSchedule.swap_leave_self,
                'swap_holiday_self': updateSchedule.swap_holiday_self,
                'render_id_other': updateSchedule.render_id_other,
                'swap_key_other': updateSchedule.swap_key_other,
                'swap_restday_other': updateSchedule.swap_restday_other,
                'swap_leave_other': updateSchedule.swap_leave_other,
                'swap_holiday_other': updateSchedule.swap_holiday_other,
                'leave_assigned': updateSchedule.leave_assigned,
            });

            model.updateSchedule($params);

        },function(){
            //console.log("Save not invoked");
        });
    };

    $scope.modalSetLeave = function()   {
        dlg = $dialogs.create('/dialogs/setLeave.html','setLeave', {employees: $scope.employees_array, leave_list: $scope.leaveList},{key: false,back: 'static'});
        dlg.result.then(function(setLeave){
            
            $params = $.param({
                employees: setLeave.emp_id,
                leave_assigned: setLeave.leave_assigned,
                start_date: $scope.formatDate(setLeave.start_date),
                end_date: $scope.formatDate(setLeave.end_date),
            });

            model.setLeave($params);

        },function(){
            //console.log("Save not invoked");
        });
    };

    $scope.modalSetHoliday = function()   {
        dlg = $dialogs.create('/dialogs/setHoliday.html','setHoliday', {employees: $scope.employees_array, holiday_list: $scope.holidayList},{key: false,back: 'static'});
        dlg.result.then(function(setHoliday){
         
            $params = $.param({
                employees: setHoliday.emp_id,
                date: $scope.formatDate(setHoliday.date),
                holiday_assigned: setHoliday.holiday_assigned,
            });

            model.setHoliday($params);

        },function(){
            //console.log("Save not invoked");
        });
    };


    $scope.modalAlert = function(which){

        var dlg = null;

        switch(which){
            case 'error':
                dlg = $dialogs.error();
                break;
            case 'notify':
                dlg = $dialogs.notify("Success!", "Shift has successfully been added");
                break;
            case 'confirm':
                dlg = $dialogs.confirm();
                dlg.result.then(function(btn){
                    $scope.confirmed = 'You confirmed "Yes."';
                },function(btn){
                    $scope.confirmed = 'You confirmed "No."';
                });
                break;
        }
    }; // end launch

});

scheduleIndexApp.controller('createShift', function($scope, $modalInstance, data){

    $scope.shift = {};

    $scope.cancel = function(){
        $modalInstance.dismiss('canceled');
    }; // end done
    
    $scope.save = function(){
        $modalInstance.close($scope.shift);
    };

});

scheduleIndexApp.controller('addShiftToEmployee', function($scope, $modalInstance, data){

    $scope.addShift = {
        employees: data.employees,
        shift_list: data.shift_list,
        days_in_week: data.days_in_week
    };

    $scope.cancel = function(){
        $modalInstance.dismiss('canceled');
    }; // end done
    
    $scope.save = function(){
        $modalInstance.close($scope.addShift);
    };

});

scheduleIndexApp.controller('setLeave', function($scope, $modalInstance, data){

    $scope.setLeave = {
        employees: data.employees,
        leave_list: data.leave_list,
    };

    $scope.cancel = function(){
        $modalInstance.dismiss('canceled');
    }; // end done
    
    $scope.save = function(){
        $modalInstance.close($scope.setLeave);
    };

});

scheduleIndexApp.controller('setHoliday', function($scope, $modalInstance, data){

    $scope.setHoliday = {
        employees: data.employees,
        holiday_list: data.holiday_list,
    };

    $scope.cancel = function(){
        $modalInstance.dismiss('canceled');
    }; // end done
    
    $scope.save = function(){
        $modalInstance.close($scope.setHoliday);
    };

});

scheduleIndexApp.controller('updateEmployeeShift', function($scope, $modalInstance, data){

    var employee_list = {};
    var swap_value_self = data.employee_list[data.render_id]['schedule'][data.date]['name'] + ' ' + data.employee_list[data.render_id]['schedule'][data.date]['schedule'];
    var swap_key_self = data.employee_list[data.render_id]['schedule'][data.date]['shift_id'];
    var swap_restday_self = data.employee_list[data.render_id]['schedule'][data.date]['restday'];
    var swap_leave_self = data.employee_list[data.render_id]['schedule'][data.date]['leave'];
    var swap_holiday_self = data.employee_list[data.render_id]['schedule'][data.date]['holiday'];

    // Remove current employee from employee JSON
    for (var val in data.employee_list)  {
        if (val != data.render_id)
            employee_list[val] = data.employee_list[val];
    };

    $scope.updateShift = {
        selectedOption: '1',
        render_id: data.render_id,
        emp_id_self: data.emp_id,
        emp_name: data.employee_list[data.render_id]['name'],
        date: data.date,
        swap_value_self: swap_value_self,
        swap_key_self: swap_key_self,
        swap_restday_self: swap_restday_self,
        swap_leave_self: swap_leave_self,
        swap_holiday_self: swap_holiday_self,
        option_list: data.option_list,
        shift_list: data.shift_list,
        employee_list: employee_list,
        leave_list: data.leave_list,
        underscore: '_',
    };

    $scope.getSwapValue = function()    {
        if (typeof $scope.updateShift['render_id_other'] != 'undefined') {
            $scope.updateShift['swap_value_other'] =  data.employee_list[$scope.updateShift['render_id_other']]['schedule'][data.date]['name'] 
                                              + ' ' + data.employee_list[$scope.updateShift['render_id_other']]['schedule'][data.date]['schedule'];
            $scope.updateShift['swap_key_other'] = data.employee_list[$scope.updateShift['render_id_other']]['schedule'][data.date]['shift_id'];
            $scope.updateShift['swap_restday_other'] = data.employee_list[$scope.updateShift['render_id_other']]['schedule'][data.date]['restday'];
            $scope.updateShift['swap_leave_other'] = data.employee_list[$scope.updateShift['render_id_other']]['schedule'][data.date]['leave'];
            $scope.updateShift['swap_holiday_other'] = data.employee_list[$scope.updateShift['render_id_other']]['schedule'][data.date]['holiday'];
        }
    };

    $scope.cancel = function(){
        $modalInstance.dismiss('canceled');
    }; // end done
    
    $scope.save = function(){
        $modalInstance.close($scope.updateShift);
    };

});

scheduleIndexApp.run(['$templateCache',function($templateCache){
    $templateCache.put(
        '/dialogs/createShift.html',
        '<div class="modal"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <h4 class="modal-title"><span class="glyphicon glyphicon-calendar"></span> Create Shift</h4>  <span class="help-block">Create shift to be assigned to employees</span> </div> <div class="modal-body"> <ng-form name="createShift" novalidate role="form"> <div class="form-group input-group-lg"> <label class="control-label" for="shift_name">Shift Name</label> <input type="text" class="form-control" name="shift_name" id="shift_name" ng-model="shift.name" required> <span class="help-block">Enter any name for the shift. You can have duplicate names for multiple shifts <br />(As you can easily distinguish shifts by their time) </span> <label class="control-label" for="shift_color">Color</label> <input colorpicker type="text" class="form-control" name="shift_color" id="shift_color" ng-model="shift.color"> <br /> <div ng-hide="true"> <label class="control-label" for="shift_personnel_no">Personnel Required</label> <input type="text" class="form-control" name="shift_personnel_no" id="shift_personnel_no" ng-model="shift.personnel_no"> <span class="help-block">Enter 0 to disable personnel limit</span> </div> <label class="control-label" for="shift_start_time">Start Time</label> <input type="text" class="form-control" name="shift_start_time" id="shift_start_time" ng-model="shift.start_time" required> <br /> <label class="control-label" for="shift_end_time">End Time</label> <input type="text" class="form-control" name="shift_end_time" id="shift_end_time" ng-model="shift.end_time" required> <br /> </div> </ng-form> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button> <button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(createShift.$dirty && createShift.$invalid) || createShift.$pristine">Save</button> </div> </div> </div> </div>'
    );

    $templateCache.put(
        '/dialogs/addShift.html',
        '<div class="modal"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <h4 class="modal-title"><span class="glyphicon glyphicon-user"></span> Assign Shift to Employee</h4>  <span class="help-block">Assign created shifts to employee/s of choice</span> </div> <div class="modal-body"> <ng-form name="nameDialog" novalidate role="form"> <div class="form-group input-group-lg"> <label class="control-label" for="add_shift_employee">Select Employee/s</label> <ui-select multiple ng-model="$parent.addShift.emp_id" theme="select2" style="width: 100%;"> <ui-select-match placeholder="Select employee from list">{{$item.name}}</ui-select-match> <ui-select-choices repeat="val.emp_id as val in addShift.employees | selectFilter: {name: $select.search}">{{val.name}}</ui-select-choices> </ui-select> <br /> <br /> <label class="control-label" for="add_shift_assigned">Shift Assigned</label> <select class="form-control" ng-model="addShift.assigned" ng-options="key as val for (key, val) in addShift.shift_list" required></select> <br /> <label class="control-label" for="add_shift_days">Days Assigned</label> <ui-select multiple ng-model="$parent.addShift.days_assigned" theme="select2" style="width: 100%;"> <ui-select-match placeholder="Select working days">{{$item.name}}</ui-select-match> <ui-select-choices repeat="val.id as val in addShift.days_in_week | selectFilter: {name: $select.search}">{{val.name}}</ui-select-choices> </ui-select> <span class="help-block">Note: Unselected days will be serve as restday in the schedule</span> <label class="control-label" for="add_shift_date_start">Start Date</label> <input class="form-control" ng-model="addShift.start_date" data-date-format="yyyy-MM-dd" data-autoclose="1" name="datepicker" bs-datepicker type="text" required> <br /> <label class="control-label" for="add_shift_date_end">End Date</label> <input class="form-control" ng-model="addShift.end_date" data-date-format="yyyy-MM-dd" data-autoclose="1" name="datepicker" bs-datepicker type="text" required> <br /> </div> </ng-form> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button> <button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(nameDialog.$dirty && nameDialog.$invalid) || nameDialog.$pristine">Save</button> </div> </div> </div> </div>'
    );

    $templateCache.put(
        '/dialogs/updateShift.html',
        '<div class="modal"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <h4 class="modal-title"><span class="glyphicon glyphicon-edit"></span> Update Schedule</h4>  <span class="help-block">Change, swap or remove employee`s schedule for selected date</span> </div> <div class="modal-body"> <div class="form-group input-group-lg"> <label class="control-label" for="update_employee_list">Employee Name</label> <input type="text" class="form-control" ng-model="updateShift.emp_name" ng-disabled="true" /> <br /> <label class="control-label" for="update_employee_list">Date</label> <input type="text" class="form-control" ng-model="updateShift.date" ng-disabled="true" /> <br /> <label class="control-label" for="add_shift_employee">Select Option</label> <select class="form-control" ng-model="updateShift.selectedOption" ng-options="key as val for (key, val) in updateShift.option_list"></select> <span class="help-block" ng-show="updateShift.selectedOption == 1"> Change schedule to selected shift below for this date </span>  <span class="help-block" ng-show="updateShift.selectedOption == 2"> Swap shift with another employee for this selected date<br /> Note that you can only swap shifts with employees of the same department </span>  <span class="help-block" ng-show="updateShift.selectedOption == 3"> Will set current date as restday </span>  <span class="help-block" ng-show="updateShift.selectedOption == 5"> Will remove current date schedule <br /> You can easily re-assign this or another shift if you want should you choose to proceed </span>  <span class="help-block" ng-show="updateShift.selectedOption == 4"> Will set current date as vacation leave </span> <!-- Change Shift --> <ng-form name="changeShift" novalidate role="form"> <div class="form-group input-group-lg" ng-show="updateShift.selectedOption == 1"> <label class="control-label" for="update_shift_assigned">Shift Assigned</label> <select class="form-control" ng-model="updateShift.assigned" ng-options="key as val for (key, val) in updateShift.shift_list" required></select> </div> </ng-form> <!-- Swap Shift with Employee --> <ng-form name="swapShift" novalidate role="form"> <div class="form-group input-group-lg" ng-show="updateShift.selectedOption == 2"> <label class="control-label" for="update_employee_list">Swap with Employee</label> <select class="form-control" ng-change="getSwapValue()" ng-model="updateShift.render_id_other" required> <option ng-repeat="val in updateShift.employee_list" value="{{val.name + updateShift.underscore + val.emp_id}}">{{val.name}}</option> </select> <br /> <label class="control-label" for="update_employee_list">Current Schedule</label> <input type="text" class="form-control" ng-model="updateShift.swap_value_self" ng-disabled="true" /> <br /> <label class="control-label" for="update_employee_list">Schedule to swap with</label> <input type="text" class="form-control" ng-model="updateShift.swap_value_other" ng-disabled="true" /> </div> </ng-form> <!-- Set As Leave --> <ng-form name="setLeave" novalidate role="form"> <div class="form-group input-group-lg" ng-show="updateShift.selectedOption == 4"> <label class="control-label" for="update_shift_assigned">Set Leave Type</label> <select class="form-control" ng-model="updateShift.leave_assigned" ng-options="key as val for (key, val) in updateShift.leave_list" required></select> </div> </ng-form> </div> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button> <button ng-show="updateShift.selectedOption == 1" type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(changeShift.$dirty && changeShift.$invalid) || changeShift.$pristine">Proceed</button> <button ng-show="updateShift.selectedOption == 2" type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(swapShift.$dirty && swapShift.$invalid) || swapShift.$pristine">Proceed</button> <button ng-show="updateShift.selectedOption == 3" type="button" class="btn btn-primary" ng-click="save()">Proceed</button> <button ng-show="updateShift.selectedOption == 4" type="button" class="btn btn-primary" ng-disabled="(setLeave.$dirty && setLeave.$invalid) || setLeave.$pristine" ng-click="save()">Proceed</button> <button ng-show="updateShift.selectedOption == 5" type="button" class="btn btn-danger" ng-click="save()">Proceed</button> </div> </div> </div> </div>'
    );

    $templateCache.put(
        '/dialogs/setLeave.html',
        '<div class="modal"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <h4 class="modal-title"><span class="glyphicon glyphicon-gift"></span> Set Leave</h4>  <span class="help-block">Assign leaves to employee/s</span>  <span class="help-block">Note that all schedules (including restdays) will be overriden and set to leave and will therefore consume the leave points of the selected employee</span> </div> <div class="modal-body"> <ng-form name="nameDialog" novalidate role="form"> <div class="form-group input-group-lg"> <label class="control-label" for="set_leave_employee">Select Employee/s</label> <ui-select multiple ng-model="$parent.setLeave.emp_id" theme="select2" style="width: 100%;"> <ui-select-match placeholder="Select employee from list">{{$item.name}}</ui-select-match> <ui-select-choices repeat="val.emp_id as val in setLeave.employees | selectFilter: {name: $select.search}">{{val.name}}</ui-select-choices> </ui-select> <br /> <br /> <label class="control-label" for="set_leave">Set Leave Type</label> <select class="form-control" ng-model="setLeave.leave_assigned" ng-options="key as val for (key, val) in setLeave.leave_list" required></select> <br /> <label class="control-label" for="set_leave_start">Start Date</label> <input class="form-control" ng-model="setLeave.start_date" data-date-format="yyyy-MM-dd" data-autoclose="1" name="datepicker" bs-datepicker type="text" required> <br /> <label class="control-label" for="set_leave_end">End Date</label> <input class="form-control" ng-model="setLeave.end_date" data-date-format="yyyy-MM-dd" data-autoclose="1" name="datepicker" bs-datepicker type="text" required> <br /> </div> </ng-form> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button> <button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(nameDialog.$dirty && nameDialog.$invalid) || nameDialog.$pristine">Save</button> </div> </div> </div> </div>'
    );

    $templateCache.put(
        '/dialogs/setHoliday.html',
        '<div class="modal"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <h4 class="modal-title"><span class="glyphicon glyphicon-gift"></span> Set Holiday</h4>  <span class="help-block">Assign holiday to employee/s</span> </div> <div class="modal-body"> <ng-form name="nameDialog" novalidate role="form"> <div class="form-group input-group-lg"> <label class="control-label" for="set_holiday_employee">Select Employee/s</label> <ui-select multiple ng-model="$parent.setHoliday.emp_id" theme="select2" style="width: 100%;"> <ui-select-match placeholder="Select employee from list">{{$item.name}}</ui-select-match> <ui-select-choices repeat="val.emp_id as val in setHoliday.employees | selectFilter: {name: $select.search}">{{val.name}}</ui-select-choices> </ui-select> <br /> <br /> <label class="control-label" for="set_holiday">Set Holiday Type</label> <select class="form-control" ng-model="setHoliday.holiday_assigned" ng-options="key as val for (key, val) in setHoliday.holiday_list" required></select> <span class="help-block">You can add/remove holidays in <a href="holiday/">here</a></span> <label class="control-label" for="set_holiday_start">Start Date</label> <input class="form-control" ng-model="setHoliday.date" data-date-format="yyyy-MM-dd" data-autoclose="1" name="datepicker" bs-datepicker type="text" required> <br /> </div> </ng-form> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button> <button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(nameDialog.$dirty && nameDialog.$invalid) || nameDialog.$pristine">Save</button> </div> </div> </div> </div>'
    );
}]); // end run / module

