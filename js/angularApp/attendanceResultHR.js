var attendanceResultHR = angular.module('attendanceResultHR', ['ui.bootstrap','dialogs']);

attendanceResultHR.controller('attendanceResultHRController',function($scope, $rootScope, $timeout, $dialogs, attendanceResultHRFactory)
{
    var model = attendanceResultHRFactory; // Initialize Factory Model

    $scope.logs = model.retrieveLogs();
    $scope.status = model.retrieveStatus();
    $scope.hrOverride = model.checkIfHROverride();

    $scope.addLogs = function(key, value){
        model.addLogs(key, value);
    };

    $scope.addStatus = function(key, value){
        model.addStatus(key, value);
    };

    $scope.addHRLogs = function(key, value){
        model.addHRLogs(key, value);
    };

    $scope.addHRStatus = function(key, value){
        model.addHRStatus(key,value);
    }

    $scope.isHROverride = function(key, value){
        model.isHROverride(key, value);
    };

    $scope.launch = function(emp_array){

        // console.log(emp_array); // Output JSON in console (For development purposes)
        var dlg = null;
    
        dlg = $dialogs.create('/dialogs/logs.html','logStatusUpdate',emp_array,{key: false,back: 'static'});
        dlg.result.then(function(user){
            $params = $.param({
                'hr_log_override': user.emp_log_override,
                'hr_status_override': user.emp_status_override,
                'log_id': user.emp_log_id
            });
            model.save($params);

            // Update logs model if log override is not EMPTY or NULL
            if (user.emp_log_override != '' && user.emp_log_override != null)   {
                $scope['logs'][user.emp_log_id] = user.emp_log_override;
                $scope['hrOverride'][user.emp_log_id] = true;
                model.addHRLogs(user.emp_log_id, user.emp_log_override); 
            } 

            // Update status model if not NULL
            if (user.emp_status_override != null)   {
                $scope['status'][user.emp_log_id] = user.emp_log_status[user.emp_status_override];
                $scope['hrOverride'][user.emp_log_id] = true;
                model.addHRStatus(user.emp_log_id, user.emp_status_override);
            }

        },function(){
            //console.log("Save not invoked");
        });

    }; // end launch

});

attendanceResultHR.factory('attendanceResultHRFactory', function ($http)    {

    var logs = {}; // Contains JSON of log values
    var status = {}; // Contains JSON of status values
    var override = {}; // Contains JSON of hr override bool values 
    var hrLogs = {}; // Contains JSON of HR log values
    var hrStatus = {}; // Contains JSON of HR status values

    return  {
        save: function ($params)    {
            $http ({
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: app_url + '/index.php/attendance/updateLogAjax',
                method: "POST",
                data: $params
            }).then(function(response)  {
                // console.log("Factory method save() was successfully done.");
            });
        },
        addLogs: function (key, value) {
            logs[key] = value;
        },
        retrieveLogs: function ()   {
            return logs;
        },
        addStatus: function (key, value) {
            status[key] = value;
        },
        retrieveStatus: function ()   {
            return status;
        },
        addHRLogs: function(key, value)    {
            hrLogs[key] = value;
        },
        retrieveHRLogs: function()  {
            return hrLogs;
        },
        addHRStatus: function(key, value)    {
            hrStatus[key] = value;
        },
        retrieveHRStatus: function()  {
            return hrStatus;
        },
        isHROverride: function(hrKey, hrOverride)    {
            override[hrKey] = hrOverride;
        },
        checkIfHROverride: function()    {
            return override;
        },
    };
});

attendanceResultHR.controller('logStatusUpdate',function($scope,$modalInstance,data,attendanceResultHRFactory){

    var model = attendanceResultHRFactory;
    var hrLogs = model.retrieveHRLogs();
    var hrStatus = model.retrieveHRStatus();

    $scope.user = {
        emp_name: data.emp_name,
        emp_id: data.emp_id,
        emp_log_id: data.emp_log_id,
        emp_log: data.emp_log,
        emp_schedule: data.emp_schedule,
        emp_sched_status: data.emp_sched_status,
        emp_log_status: JSON.parse(data.emp_log_status),
        emp_log_override: hrLogs[data.emp_log_id],
        emp_status_override: hrStatus[data.emp_log_id]
    };

    $scope.cancel = function(){
        $modalInstance.dismiss('canceled');  
    }; // end cancel

    $scope.save = function(){
        $modalInstance.close($scope.user);
    }; // end save

    $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13) && !(angular.equals($scope.name,null) || angular.equals($scope.name,'')))
            $scope.save();
    }; // end hitEnter

}); // end whatsYourNameCtrl

attendanceResultHR.run(['$templateCache',function($templateCache){
    $templateCache.put(
        '/dialogs/logs.html',
        '<div class="modal"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <h4 class="modal-title"><span class="glyphicon glyphicon-time"></span> Log Status</h4> </div> <div class="modal-body"> <ng-form name="nameDialog" novalidate role="form"> <div class="form-group input-group-lg"> <label class="control-label" for="emp_name">Employee Name</label> <input type="text" class="form-control" name="emp_name" id="emp_name" ng-model="user.emp_name" ng-disabled="true"> <br /> <label class="control-label" for="emp_log">Employee Log</label> <input type="text" class="form-control" name="emp_log" id="emp_log" ng-model="user.emp_log" ng-disabled="true"> <br /> <label class="control-label" for="emp_schedule">Employee Schedule</label> <input type="text" class="form-control" name="emp_schedule" id="emp_schedule" ng-model="user.emp_schedule" ng-disabled="true"> <br /> <label class="control-label" for="emp_sched_status">Employee Status</label> <input type="text" class="form-control" name="emp_sched_status" id="emp_sched_status" ng-model="user.emp_sched_status" ng-disabled="true"> <br /> <label class="control-label" for="emp_log_override">Override Log</label> <input type="text" class="form-control" name="emp_log_override" id="emp_log_override" ng-model="user.emp_log_override" ng-keyup="hitEnter($event)" required> <span class="help-block">Enter log here to override current employee`s log</span> <label class="control-label" for="emp_status_override">Override Status</label> <select class="form-control" name="emp_status_override" id="emp_status_override" ng-model="user.emp_status_override" ng-options="key as val for (key, val) in user.emp_log_status"> </select> <span class="help-block">Select status from list to override current employee`s attendance status</span> </div> </ng-form> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button> <button type="button" class="btn btn-primary" ng-click="save()">Save</button> </div> </div> </div> </div>'
    );
}]); // end run / module