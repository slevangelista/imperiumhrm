var otRequestApp = angular.module('otRequestApp', ['ui.bootstrap','dialogs']);
 
otRequestApp.factory('otRequestFactory', function ($rootScope, $http)    {

    return  {
        dissaprove: function ($params) {
            $http ({
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: app_url + '/index.php/ot/dissaproveAjax',
                method: "POST",
                data: $params
            }).then(function(response)  {
                $rootScope.$broadcast("updateRequest",response);
            });
        },
    
        approve: function ($params) {
            $http ({
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: app_url + '/index.php/ot/ApproveAjax',
                method: "POST",
                data: $params
            }).then(function(response)  {
                $rootScope.$broadcast("updateRequest",response);
            });
        },
    };

});

otRequestApp.controller('otRequestController', function($rootScope, $scope, $http, $timeout, $dialogs, otRequestFactory){

    var model = otRequestFactory;

    $scope.pendingOT;

    /**
     * RootScope Broadcast Response
     *
     */
    $scope.$on("updateRequest", function(event,response){
        // Callback response on success
        if(response.status == 200)  {
            $scope.getEmployeePendingRequest();
        } else  {
            console.log('Error: ' + response.statusText);
        }
    });

    $scope.getEmployeePendingRequest = function()  {
        $http.get(app_url + '/index.php/ot/getEmployeePendingRequest').then(function(response)   {
            $scope.pendingOT = response.data;
            // console.log($scope.pendingOT);
        });
    };

	$scope.dissaproveOT = function(id){
        console.log(id);
		var dlg = null;
        dlg = $dialogs.create('/dialogs/dissaproveOT.html', 'dissaproveOT', {id: id},{key: false, back: 'static'});
        dlg.result.then(function(ot){
            
            $params = $.param({
                'remarks': ot.remarks,
                'id': ot.id,
            });
            model.dissaprove($params);

        },function(){
            console.log("fail");
        });

	};

    $scope.approveOT = function(id){
	   	    console.log(id);
            var dlg = null;
            dlg = $dialogs.create('/dialogs/approveOT.html', 'approveOT', {id: id},{key: false, back: 'static'});
            dlg.result.then(function(ot){

           $params = $.param({
                'id': id,
            });
            model.approve($params);
    
    },$scope.check = function(){
       console.log("testing");
	
    });

}}); 

otRequestApp.controller('dissaproveOT',function($scope, $modalInstance, data){

    $scope.ot = {
        id: data.id,
    };

    $scope.cancel = function(){
        $modalInstance.dismiss('canceled');  
    }; // end cancel

    $scope.save = function(){
        $modalInstance.close($scope.ot);
    }; // end save

    $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13) && !(angular.equals($scope.name,null) || angular.equals($scope.name,'')))
            $scope.save();
    }; // end hitEnter

}); // end whatsYourNameCtrl


otRequestApp.controller('approveOT',function($scope, $modalInstance, data){

    $scope.ot = {
        id: data.id,
    };

    $scope.cancel = function(){
        $modalInstance.dismiss('canceled');  
    }; // end cancel

    $scope.save = function(){
        $modalInstance.close($scope.ot);
    }; // end save

    $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13) && !(angular.equals($scope.name,null) || angular.equals($scope.name,'')))
            $scope.save();
    }; // end hitEnter

}); // end whatsYourNameCtrl


otRequestApp.run(['$templateCache',function($templateCache){
    $templateCache.put(
        '/dialogs/dissaproveOT.html',
        '<div class="modal"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <h4 class="modal-title"><span class="glyphicon glyphicon-question-sign"></span> Dissaproval Remarks </h4> </div> <div class="modal-body"> <ng-form name="nameDialog" novalidate role="form"> <div class="form-group input-group-lg"> <textarea style="height: 30%" class="form-control" name="ot_remarks" id="ot_remarks" ng-model="ot.remarks" ng-keyup="hitEnter($event)" rows="4" cols="50"></textarea><span class="help-block">Please enter reason of disapproval</span> </div> </ng-form> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button> <button type="button" class="btn btn-primary" ng-click="save()">Save</button> </div> </div> </div> </div>'   
    );
}]); // end run / module
 

otRequestApp.run(['$templateCache',function($templateCache){
    $templateCache.put(
        '/dialogs/approveOT.html',
        '<div class="modal"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <h4 class="modal-title"><span class="glyphicon glyphicon-question-sign"></span></h4> </div> <div class="modal-body"> <h4>Are you sure you want to Approve this request ?</h4> <ng-form name="nameDialog" novalidate role="form"> <div class="form-group input-group-lg"></div> </ng-form> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button> <button type="button" class="btn btn-primary" ng-click="save()">Ok</button> </div> </div> </div> </div>'
        ); 
}]);


