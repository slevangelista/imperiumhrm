var logsIndexApp = angular.module('logsIndexApp', ['ngSanitize', 'ui.bootstrap','dialogs', 'ui.select']);

logsIndexApp.factory('logsIndexFactory', function($http, $rootScope){

    return {
        getEmployees: function ($params)    {
            return $http ({
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: app_url + '/index.php/logs/getEmployeeByDepartment',
                method: "POST",
                data: $params
            }).then(function(response)  {
                return response.data
            });
        },
        overrideLogs: function ($params)    {
            $http ({
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: app_url + '/index.php/logs/overrideLogs',
                method: "POST",
                data: $params
            }).then(function(response) {
                $rootScope.$broadcast("updateLogs",response);
            });
        },
    };

});

/**
 * AngularJS default filter with the following expression:
 * "person in people | filter: {name: $select.search, age: $select.search}"
 * performs a AND between 'name: $select.search' and 'age: $select.search'.
 * We want to perform a OR.
 */
logsIndexApp.filter('selectFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      items.forEach(function(item) {
        var itemMatches = false;

        var keys = Object.keys(props);
        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  }
});

logsIndexApp.controller('logsIndexController', function($rootScope, $scope, $http, $timeout, $dialogs, logsIndexFactory){

    /**
     * Initialize Variables
     *
     */
    var model = logsIndexFactory;

    $scope.date_filter_type = 0; // Set to 0 by default
    $scope.is_filters = true; // Set to true by default

    $scope.is_logs = false;

    $scope.employees;

    $scope.dateRange;
    $scope.dateInterval;
    $scope.cutoffList;

    $scope.logStatusList;

    $scope.logOptions = {
        '1': 'Add/Override Log',
        '2': 'Set as Absent',
        '3': 'Set as Sick Leave',
        '4': 'Revert to Original Log'
    };

    /**
     * RootScope Broadcast Response
     *
     */
    $scope.$on("updateLogs", function(event,response){
        if(response.status == 200)  {
            $scope.setEmployees();
        } else  {
            console.log('Error: ' + response.statusText);
        }
    });

    /**
     * Controller Methods
     *
     */
    $scope.getLogStatus = function()  {
        $http.get(app_url + '/index.php/logs/getLogStatus').then(function(response) {
            $scope.logStatusList = response.data;
        });
    };

    $scope.getCutoffList = function()  {
        $http.get(app_url + '/index.php/logs/getCutoffList').then(function(response) {
            $scope.cutoffList = _(response.data).toArray();
        });
    };

    $scope.getCurrentCutoff = function()  {
        $http.get(app_url + '/index.php/logs/getCurrentCutoff').then(function(response) {
            $scope.dateRange = response.data;
        });
    };

    $scope.generateLogs = function()  {
        $scope.is_logs = true;

        $scope.getDateRange();
    };

    $scope.getShift = function()    {
        $http.get(app_url + '/index.php/schedule/getShifts').then(function(response)   {
            $scope.shifts = response.data;
        });
    };

    $scope.getDateRange = function()    {
        $http.get(app_url + '/index.php/logs/getDateRange?date_range=' + $scope.dateRange.value).then(function(response) {
            $scope.dateInterval = response.data;

            // Set Employees
            $scope.setEmployees();
        });
    };

    $scope.setEmployees = function()  {
        $scope.is_department = true;

        $params = $.param({
            department: $scope.department,
            date_interval: $scope.dateInterval,
        });

        model.getEmployees($params).then(function(response){
            $scope.employees = _(response).toArray();
        });
    };

    $scope.modalOverride = function(or_logs, hr_logs, emp_name, emp_id, schedule, og_status, hr_status, date, shift) {

        var dlg = null;
    
        dlg = $dialogs.create('/dialogs/logs.html','logStatusUpdate', {
            or_logs: or_logs, 
            hr_logs: hr_logs, 
            emp_name: emp_name, 
            emp_id: emp_id, 
            schedule: schedule, 
            og_status: og_status, 
            hr_status: hr_status, 
            log_status_list: $scope.logStatusList,
            date: date,
            log_options: $scope.logOptions,
            shift_list: $scope.shifts,
            shift_assigned: shift,
        },{key: false,back: 'static'});
        dlg.result.then(function(logs){

            $params = $.param({
                emp_name: logs.emp_name,
                emp_id: logs.emp_id,
                hr_logs: logs.hr_logs,
                hr_status: logs.hr_status,
                date: logs.date,
                option: logs.option,
                shift_assigned: logs.shift_assigned,
            });

            model.overrideLogs($params);
        },function(){
            //console.log("Save not invoked");
        });
    };
});

logsIndexApp.controller('logStatusUpdate',function($scope,$modalInstance,data){

    $scope.logs = {
        emp_name: data.emp_name,
        emp_id: data.emp_id,
        or_logs: data.or_logs,
        hr_logs: data.hr_logs,
        schedule: data.schedule,
        og_status: data.og_status,
        hr_status: data.hr_status,
        log_status_list: data.log_status_list,
        date: data.date,
        log_options: data.log_options,
        shift_list: data.shift_list,
        shift_assigned: data.shift_assigned,
        option: '1',
    };

    $scope.cancel = function(){
        $modalInstance.dismiss('canceled');  
    }; // end cancel

    $scope.save = function(){
        $modalInstance.close($scope.logs);
    }; // end save

});

logsIndexApp.run(['$templateCache',function($templateCache){
    $templateCache.put(
        '/dialogs/logs.html',
        '<div class="modal"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <h4 class="modal-title"><span class="glyphicon glyphicon-time"></span> Log Status</h4><span class="help-block">Checks logs status. Choose among the options if you wish to alter the logs</span> </div> <div class="modal-body"> <div class="form-group input-group-lg"> <label class="control-label" for="emp_name">Employee Name</label> <input type="text" class="form-control" name="emp_name" id="emp_name" ng-model="logs.emp_name" ng-disabled="true"> <br /> <label class="control-label" for="emp_date">Date</label> <input type="text" class="form-control" name="emp_date" id="emp_date" ng-model="logs.date" ng-disabled="true"> <br /> <label class="control-label" for="emp_select_option">Select Option</label> <select class="form-control" name="emp_select_option" id="emp_select_option" ng-model="logs.option" ng-options="key as val for (key, val) in logs.log_options"></select> <span class="help-block" ng-show="logs.option == 1">Add logs for this date and employee. If there is a pre-existing log already, it will be overidden (But will not be removed)</span> <span class="help-block" ng-show="logs.option == 2">Set selected log and date as absent. In order to mark log as absent, a schedule must be set for this date and employee</span> <span class="help-block" ng-show="logs.option == 3">Set selected log and date as sick leave</span> <span class="help-block" ng-show="logs.option == 4">Reverts back to original state of log. If no previous log state is found, this log will automatically be deleted (You cannot undo this action)</span> <!-- Add/Override Log --> <ng-form name="logOverride" novalidate role="form"> <div class="form-group input-group-lg" ng-show="logs.option == 1"> <label class="control-label" for="emp_log">Original Log</label> <input type="text" class="form-control" name="emp_log" id="emp_log" ng-model="logs.or_logs" ng-disabled="true"> <br /> <label class="control-label" for="emp_og_status">Original Status</label> <input type="text" class="form-control" name="emp_og_status" id="emp_og_status" ng-model="logs.og_status" ng-disabled="true"> <br /> <label class="control-label" for="emp_log_override">HR Add/Override</label> <input type="text" class="form-control" name="emp_log_override" id="emp_log_override" ng-model="logs.hr_logs"> <span class="help-block">Enter log here to override current employee`s log</span> <label class="control-label" for="emp_status_override">Override Status</label> <select class="form-control" name="emp_status_override" id="emp_status_override" ng-model="logs.hr_status" ng-options="key as val for (key, val) in logs.log_status_list"></select> <span class="help-block">Select status from list to override current employee`s attendance status</span> </div> </ng-form> <!-- Mark as Absent --> <ng-form name="logAbsent" novalidate role="form"> <div class="form-group input-group-lg" ng-show="logs.option == 2"> <label class="control-label" for="logs_select_shift">Select Shift</label> <select class="form-control" ng-model="logs.shift_assigned" ng-options="key as val for (key, val) in logs.shift_list" required></select> </div> </ng-form> </div> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button> <button type="button" class="btn btn-primary" ng-show="logs.option == 1" ng-click="save()" ng-disabled="(logOverride.$dirty && logOverride.$invalid) || logOverride.$pristine">Proceed</button> <button type="button" class="btn btn-danger" ng-show="logs.option == 2" ng-click="save()" ng-disabled="logs.shift_assigned == null">Proceed</button> <button type="button" class="btn btn-primary" ng-show="logs.option == 3" ng-click="save()" ng-disabled="(logSickLeave.$dirty && logSickLeave.$invalid) || logSickLeave.$pristine">Proceed</button> <button type="button" class="btn btn-primary" ng-show="logs.option == 4" ng-click="save()">Proceed</button> </div> </div> </div> </div>'
    );
}]); // end run / module

logsIndexApp.directive('logPopover', function() {
    return function(scope, element, attrs) {
        element.find("a[rel=popover]").popover({ placement: 'top', html: 'true'});
    };
});

