var attendanceIndex = angular.module('attendanceIndex', []);

attendanceIndex.controller('attendanceIndexController', function($scope){

    $scope.is_filters = false; // Used to toggle display of filters
    $scope.filter_type = 0; // Sets Filter type. 0 => Department, 1 => Employee
    $scope.date_filter_type = 0; // Sets Filter type. 0 => Cutoff, 1 => DatePicker

});