var leaveCreditsApp = angular.module('leaveCreditsApp', ['ui.bootstrap','dialogs']);

leaveCreditsApp.factory('leaveCreditsFactory', function ($http, $rootScope){

	return {
		updateCredits: function ($params)    {
            $http ({
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: app_url + '/index.php/leaveCredits/updateLeaveCredits',
                method: "POST",
                data: $params
            }).then(function(response)  {
                console.log("Factory method save() was successfully done.");
                $rootScope.$broadcast("updateLeave",response);
            });
        },
	};
});

leaveCreditsApp.controller('leaveCreditsController',function($scope, $http, $dialogs, leaveCreditsFactory)	{

	var model = leaveCreditsFactory;

	$scope.credits;
    // $scope.bulkCredits;
//onclick edit credits modal
	$scope.editCredits = function(empid, fullname, vl, sl) {
		var dlg = null;
    	// alert(vl);
        dlg = $dialogs.create('/dialogs/modalEditCredits.html','editCredits', {empid: empid, fullname:fullname, vl: vl, sl: sl} ,{key: false,back: 'static'});
        dlg.result.then(function(edit){

        	$params = $.param({
                'empid': edit.empid,
                'fullname': edit.fullname,
                'vl': edit.vl,
                'sl': edit.sl
            });
            model.updateCredits($params);

        },function(){
            
        });
	};
//onclick bulk credits modal
    $scope.bulkCredits = function() {
        // alert('sasa');
        var dlg = null;
        // alert(vl);
        dlg = $dialogs.create('/dialogs/modalBulkCredits.html','bulkCredits', {}, {key: false,back: 'static'});
        dlg.result.then(function(bulk){

            // $params = $.param({
            //     'empid': edit.empid,
            //     'fullname': edit.fullname,
            //     'vl': edit.vl,
            //     'sl': edit.sl
            // });
            // model.updateCredits($params);

        },function(){
            
        });
    };
//index edit button
	$scope.$on("updateLeave", function(event,response){
        if(response.status == 200)  {
            // Successful response callback
            $scope.getAllCredits();
        } else  {
            console.log('Error: ' + response.statusText);
        }
    });
//for ng-repeat
	$scope.getAllCredits = function()
	{
		$http.get(app_url + '/index.php/leaveCredits/getAllCredits').then(function(response) {
			$scope.credits = response.data;
			// console.log($scope.credits);			
		});
	}
});

//controller for modal
leaveCreditsApp.controller('editCredits',function($scope, $modalInstance, data){

    $scope.editCredits = {
        empid: data.empid,
        fullname: data.fullname,
        vl: data.vl,
        sl: data.sl,
	};

    $scope.cancel = function(){
        $modalInstance.dismiss('canceled');  
    }; // end cancel

    $scope.save = function(){
        $modalInstance.close($scope.editCredits);
    }; // end save

    $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13) && !(angular.equals($scope.name,null) || angular.equals($scope.name,'')))
            $scope.save();
    }; // end hitEnter

}); // end whatsYourNameCtrl

//controller for modal
leaveCreditsApp.controller('bulkCredits',function($scope, $modalInstance){

    // $scope.bulkCredits = {
        // empid: data.empid,
        // fullname: data.fullname,
        // vl: data.vl,
        // sl: data.sl,
    // };

    $scope.cancel = function(){
        $modalInstance.dismiss('canceled');  
    }; // end cancel

    $scope.save = function(){
        // $modalInstance.close($scope.bulkCredits);
    }; // end save

    $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13) && !(angular.equals($scope.name,null) || angular.equals($scope.name,'')))
            $scope.save();
    }; // end hitEnter

}); // end whatsYourNameCtrl

leaveCreditsApp.run(['$templateCache',function($templateCache){
    $templateCache.put(
        '/dialogs/modalEditCredits.html',
        '<div class="modal"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <h4 class="modal-title"><span class="glyphicon glyphicon-edit"></span> Edit Leave Credits</h4> </div> <div class="modal-body"> <ng-form name="nameDialog" novalidate role="form"> <div><font size=5><b>{{editCredits.fullname}}</b></font></div> <br><div class="form-group input-group-lg"> <label class="control-label" for="emp_name">Vacation Leave Credits</label> <input type=text class="form-control" ng-model="editCredits.vl"> </div> <div class="form-group input-group-lg"> <label class="control-label" for="emp_name">Sick Leave Credits</label> <input type=text class="form-control" ng-model="editCredits.sl"> </div> </ng-form> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button> <button type="button" class="btn btn-primary" ng-click="save()">Save</button> </div> </div> </div> </div>'
    );
    $templateCache.put(
        '/dialogs/modalBulkCredits.html',
        '<div class="modal"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <h4 class="modal-title"><span class="glyphicon glyphicon-edit"></span> Edit Leave Credits</h4> </div> <div class="modal-body"> <ng-form name="nameDialog" novalidate role="form"> <div><font size=5><b>{{editCredits.fullname}}</b></font></div> <br><div class="form-group input-group-lg"> <label class="control-label" for="emp_name">Vacation Leave Credits</label> <input type=text class="form-control" ng-model="editCredits.vl"> </div> <div class="form-group input-group-lg"> <label class="control-label" for="emp_name">Sick Leave Credits</label> <input type=text class="form-control" ng-model="editCredits.sl"> </div> </ng-form> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button> <button type="button" class="btn btn-primary" ng-click="save()">Save</button> </div> </div> </div> </div>'
    );
}]); // end run / module

