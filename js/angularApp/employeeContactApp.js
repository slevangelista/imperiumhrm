var employeeContactApp = angular.module('employeeContactApp', []);

employeeContactApp.controller('employeeContactController', function($scope, $http, $rootScope, employeeContactFactory)
{

	var model = employeeContactFactory;

    $scope.items = [];
    $scope.contact = {};

    /**
     * RootScope Broadcast Response
     *
     */

    $scope.check = function(test)   {
        console.log(test);
    }

    $scope.$on("updateContact", function(event,response, emp_id){
        if(response.status == 200)  {
            // Successful response callback
            $scope.getContact(emp_id);
        } else  {
            console.log('Error: ' + response.statusText);
        }
    });

    $scope.add = function () {
      $scope.items.push({ 
        contact: "",
        number: "",
        typePlaceholder: "contact type",
        numberPlaceholder: "number",
      });
    };

    $scope.getContact = function(emp_id)  {
        $http.get(app_url + '/index.php/contactNo/getContact?emp_id=' + emp_id).then(function(response)    {
            console.log(response.data);
            $scope.contact = response.data;
        });
    }

    $scope.test = function()
    {
    	for (var val in $scope.items)
    	{
    		console.log($scope.items[val].contact);
    	}
    }

    $scope.remove = function(index){
		$scope.items.splice(index,1);
	};

	$scope.save = function(emp_id){		
        $params = $.param({
            contactNos: $scope.items
        });

        console.log(emp_id);

        model.save($params, emp_id);
        
	}

    console.log($scope.items);

});

employeeContactApp.factory('employeeContactFactory', function ($http, $rootScope){

	return{
		save: function ($params, emp_id)    {
            $http ({
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: app_url + '/index.php/contactNo/createContactAjax/' + emp_id,
                method: "POST",
                data: $params
            }).then(function(response)  {
                $rootScope.$broadcast("updateContact",response, emp_id);
                console.log("This is done");
            });
        },

        
	};

});