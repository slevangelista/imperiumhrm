var employeeAddressApp = angular.module('employeeAddressApp', []);

employeeAddressApp.controller('employeeAddressController', function($scope, $http, $rootScope, employeeAddressFactory)
{

	var model = employeeAddressFactory;

    $scope.items = [];
    $scope.address = {};

    /**
     * RootScope Broadcast Response
     *
     */
    $scope.$on("updateAddress", function(event,response, emp_id){
        if(response.status == 200)  {
            // Successful response callback
            $scope.getAddress(emp_id);
        } else  {
            console.log('Error: ' + response.statusText);
        }
    });

    $scope.add = function () {
      $scope.items.push({ 
        address: "",
        location: "",
        typePlaceholder: " Address Type",
        locationPlaceholder: " Location",
      });
    };

    $scope.getAddress = function(emp_id)  {
        $http.get(app_url + '/index.php/address/getAddress?emp_id=' + emp_id).then(function(response)    {
            //console.log(response.data);
            $scope.address = response.data;
        });
    }

    $scope.test = function()
    {
    	for (var val in $scope.items)
    	{
    		console.log($scope.items[val].address);
    	}
    }

    $scope.remove = function(index){
		$scope.items.splice(index,1);
	};

	$scope.save = function(emp_id){		
        $params = $.param({
            addresses: $scope.items
        });

        model.save($params, emp_id);
	}

});

employeeAddressApp.factory('employeeAddressFactory', function ($http, $rootScope){

	return{
		save: function ($params, emp_id)    {
            $http ({
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: app_url + '/index.php/address/createAddressAjax/' + emp_id,
                method: "POST",
                data: $params
            }).then(function(response)  {
                $rootScope.$broadcast("updateAddress",response, emp_id);
            });
        },

        
	};

});