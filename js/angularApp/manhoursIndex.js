var manhoursIndexApp = angular.module('manhoursIndexApp', ['ngSanitize', 'ui.bootstrap','dialogs', 'ui.select']);

manhoursIndexApp.factory('manhoursIndexFactory', function($http, $rootScope){

    return {

    };

});

/**
 * AngularJS default filter with the following expression:
 * "person in people | filter: {name: $select.search, age: $select.search}"
 * performs a AND between 'name: $select.search' and 'age: $select.search'.
 * We want to perform a OR.
 */
manhoursIndexApp.filter('selectFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      items.forEach(function(item) {
        var itemMatches = false;

        var keys = Object.keys(props);
        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  }
});

manhoursIndexApp.controller('manhoursIndexController', function($rootScope, $scope, $http, $timeout, $dialogs){

    /**
     * Initialize Variables
     *
     */
    $scope.date_filter_type = 0; // Set to 0 by default
    $scope.is_filters = true; // Set to true by default

    $scope.is_man_hours = false;

    $scope.department;
    $scope.employees;

    $scope.cutoffList;
    $scope.dateRange;

    /**
     * Controller Methods
     *
     */
    $scope.setEmployees = function()  {
        $scope.is_department = true;

        $http.get(app_url + '/index.php/manhours/getEmployeeByDepartment?department=' + $scope.department + '&date_range=' + $scope.dateRange.value).then(function(response) {
            $scope.employees = _(response.data).toArray();
            // $scope.employees = response.data;
        });
    };

    $scope.getCutoffList = function()  {
        $http.get(app_url + '/index.php/manhours/getCutoffList').then(function(response) {
            $scope.cutoffList = _(response.data).toArray();
        });
    }

    $scope.getCurrentCutoff = function()  {
        $http.get(app_url + '/index.php/manhours/getCurrentCutoff').then(function(response) {
            $scope.dateRange = response.data;
        });
    }

    $scope.generateReport = function()  {
        $scope.is_man_hours = true;

        $scope.setEmployees();
    };

    $scope.getTotalHours = function(count)   {
        return ((parseFloat(count.no_of_days) * 8) + parseFloat(count.overtime_hours) + parseFloat(count.restday_work_hours) - parseFloat(count.tardiness_hours)).toFixed(2);
    };

    $scope.check = function(key)   {
        console.log($scope.employees[key]['count']);
    };

    $scope.modalAbsences = function(id, absences, emp_id) {
        dlg = $dialogs.create('/dialogs/absences.html','absences', {id:id, absences: absences, emp_id: emp_id},{key: false,back: 'static'});
        dlg.result.then(function(absences){
          
            for (var val in $scope.employees)   {
                if ($scope.employees[val]['emp_id'] == absences.emp_id) {
                    $scope.employees[val]['count']['absences'] = absences.absences;
                    break; // End loop
                }
            };

        },function(){
            //console.log("Save not invoked");
        });
    };

    $scope.modalVacationLeave = function(id, vl, emp_id) {
        dlg = $dialogs.create('/dialogs/vacationLeave.html','vacationLeave', {id: id, vl: vl, emp_id: emp_id},{key: false,back: 'static'});
        dlg.result.then(function(vl){
        
            for (var val in $scope.employees)   {
                if ($scope.employees[val]['emp_id'] == vl.emp_id) {
                    $scope.employees[val]['count']['vacation_leave'] = vl.vl;
                    break; // End loop
                }
            };

        },function(){
            //console.log("Save not invoked");
        });
    };

    $scope.modalTardinessMins = function(id, tardiness, emp_id) {
        dlg = $dialogs.create('/dialogs/tardinessMins.html','tardinessMins', {id: id, tardiness: tardiness, emp_id: emp_id},{key: false,back: 'static'});
        dlg.result.then(function(tardiness){
            
            for (var val in $scope.employees)   {
                if ($scope.employees[val]['emp_id'] == tardiness.emp_id) {
                    $scope.employees[val]['count']['tardiness_mins'] = tardiness.tardiness;
                    break; // End loop
                }
            };

        },function(){
            //console.log("Save not invoked");
        });
    };

    $scope.modalTardinessHours = function(id, tardiness, emp_id) {
        dlg = $dialogs.create('/dialogs/tardinessHours.html','tardinessHours', {id: id, tardiness: tardiness, emp_id: emp_id},{key: false,back: 'static'});
        dlg.result.then(function(tardiness){
            
            for (var val in $scope.employees)   {
                if ($scope.employees[val]['emp_id'] == tardiness.emp_id) {
                    $scope.employees[val]['count']['tardiness_hours'] = tardiness.tardiness;
                    break; // End loop
                }
            };

        },function(){
            //console.log("Save not invoked");
        });
    };

    $scope.modalNoOfDays = function(id, no_of_days, emp_id) {
        dlg = $dialogs.create('/dialogs/noOfDays.html','noOfDays', {id: id, no_of_days: no_of_days, emp_id: emp_id},{key: false,back: 'static'});
        dlg.result.then(function(no_of_days){
            
            for (var val in $scope.employees)   {
                if ($scope.employees[val]['emp_id'] == no_of_days.emp_id) {
                    $scope.employees[val]['count']['no_of_days'] = no_of_days.no_of_days;
                    break; // End loop
                }
            };

        },function(){
            //console.log("Save not invoked");
        });
    };

    $scope.modalOvertime = function(id, overtime, emp_id) {
        dlg = $dialogs.create('/dialogs/overtime.html','overtime', {id: id, overtime: overtime, emp_id: emp_id},{key: false,back: 'static'});
        dlg.result.then(function(overtime){
            
            for (var val in $scope.employees)   {
                if ($scope.employees[val]['emp_id'] == overtime.emp_id) {
                    $scope.employees[val]['count']['overtime_hours'] = overtime.overtime;
                    break; // End loop
                }
            };

        },function(){
            //console.log("Save not invoked");
        });
    };

    $scope.modalRestdayWork = function(id, restday, emp_id) {
        dlg = $dialogs.create('/dialogs/restdayWork.html','restdayWork', {id: id, restday: restday, emp_id: emp_id},{key: false,back: 'static'});
        dlg.result.then(function(restday){
            
            for (var val in $scope.employees)   {
                if ($scope.employees[val]['emp_id'] == restday.emp_id) {
                    $scope.employees[val]['count']['restday_work_hours'] = restday.restday;
                    break; // End loop
                }
            };

        },function(){
            //console.log("Save not invoked");
        });
    };

    $scope.modalSickLeave = function(id, sick_leave, emp_id)    {
        dlg = $dialogs.create('/dialogs/sickLeave.html', 'sickLeave', {id: id, sick_leave: sick_leave, emp_id: emp_id}, {key: false, back: 'static'});
        dlg.result.then(function(sick_leave){

            for (var val in $scope.employees)   {
                if ($scope.employees[val]['emp_id'] == sick_leave.emp_id) {
                    $scope.employees[val]['count']['sick_leave'] = sick_leave.sick_leave;
                    break; // End loop
                }
            };

        },function(){
            //console.log("Save not invoked");
        });
    };

});

manhoursIndexApp.controller('absences', function($scope, $modalInstance, data){

    $scope.absences = {
        id: data.id,
        absences: data.absences,
        emp_id: data.emp_id
    };

    $scope.cancel = function(){
        $modalInstance.dismiss('canceled');
    }; // end done
    
    $scope.save = function(){
        $modalInstance.close($scope.absences);
    };

});

manhoursIndexApp.controller('vacationLeave', function($scope, $modalInstance, data){

    $scope.vl = {
        id: data.id,
        vl: data.vl,
        emp_id: data.emp_id
    };

    $scope.cancel = function(){
        $modalInstance.dismiss('canceled');
    }; // end done
    
    $scope.save = function(){
        $modalInstance.close($scope.vl);
    };

});

manhoursIndexApp.controller('tardinessMins', function($scope, $modalInstance, data){

    $scope.tardiness = {
        id: data.id,
        tardiness: data.tardiness,
        emp_id: data.emp_id
    };

    $scope.cancel = function(){
        $modalInstance.dismiss('canceled');
    }; // end done
    
    $scope.save = function(){
        $modalInstance.close($scope.tardiness);
    };

});

manhoursIndexApp.controller('tardinessHours', function($scope, $modalInstance, data){

    $scope.tardiness = {
        id: data.id,
        tardiness: data.tardiness,
        emp_id: data.emp_id
    };

    $scope.cancel = function(){
        $modalInstance.dismiss('canceled');
    }; // end done
    
    $scope.save = function(){
        $modalInstance.close($scope.tardiness);
    };

});

manhoursIndexApp.controller('noOfDays', function($scope, $modalInstance, data){

    $scope.no_of_days = {
        id: data.id,
        no_of_days: data.no_of_days,
        emp_id: data.emp_id
    };

    $scope.cancel = function(){
        $modalInstance.dismiss('canceled');
    }; // end done
    
    $scope.save = function(){
        $modalInstance.close($scope.no_of_days);
    };

});

manhoursIndexApp.controller('overtime', function($scope, $modalInstance, data){

    $scope.overtime = {
        id: data.id,
        overtime: data.overtime,
        emp_id: data.emp_id
    };

    $scope.cancel = function(){
        $modalInstance.dismiss('canceled');
    }; // end done
    
    $scope.save = function(){
        $modalInstance.close($scope.overtime);
    };

});

manhoursIndexApp.controller('restdayWork', function($scope, $modalInstance, data){

    $scope.restday = {
        id: data.id,
        restday: data.restday,
        emp_id: data.emp_id
    };

    $scope.cancel = function(){
        $modalInstance.dismiss('canceled');
    }; // end done
    
    $scope.save = function(){
        $modalInstance.close($scope.restday);
    };

});

manhoursIndexApp.controller('sickLeave', function($scope, $modalInstance, data){

    $scope.sick_leave = {
        id: data.id,
        sick_leave: data.sick_leave,
        emp_id: data.emp_id
    };

    $scope.cancel = function(){
        $modalInstance.dismiss('canceled');
    }; // end done
    
    $scope.save = function(){
        $modalInstance.close($scope.sick_leave);
    };

});

manhoursIndexApp.run(['$templateCache',function($templateCache){
    $templateCache.put(
        '/dialogs/absences.html',
        '<div class="modal"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <h4 class="modal-title"><span class="glyphicon glyphicon-pencil"></span> Override Absences</h4> </div> <div class="modal-body"> <ng-form name="modalForm" novalidate role="form"> <div class="form-group input-group-lg"> <label class="control-label" for="absences">Absences</label> <input type="text" class="form-control" name="absences" id="absences" ng-model="absences.absences" required> </div> </ng-form> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button> <button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(modalForm.$dirty && modalForm.$invalid) || modalForm.$pristine">Save</button> </div> </div> </div> </div>'
    );

    $templateCache.put(
        '/dialogs/vacationLeave.html',
        '<div class="modal"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <h4 class="modal-title"><span class="glyphicon glyphicon-pencil"></span> Override Vacation Leave</h4> </div> <div class="modal-body"> <ng-form name="modalForm" novalidate role="form"> <div class="form-group input-group-lg"> <label class="control-label" for="vl">Vacation Leave</label> <input type="text" class="form-control" name="vl" id="vl" ng-model="vl.vl" required> </div> </ng-form> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button> <button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(modalForm.$dirty && modalForm.$invalid) || modalForm.$pristine">Save</button> </div> </div> </div> </div>'
    );

    $templateCache.put(
        '/dialogs/tardinessMins.html',
        '<div class="modal"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <h4 class="modal-title"><span class="glyphicon glyphicon-pencil"></span> Override Tardiness (Minutes)</h4> </div> <div class="modal-body"> <ng-form name="modalForm" novalidate role="form"> <div class="form-group input-group-lg"> <label class="control-label" for="tardiness">Tardiness (Minutes)</label> <input type="text" class="form-control" name="tardiness" id="tardiness" ng-model="tardiness.tardiness" required> </div> </ng-form> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button> <button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(modalForm.$dirty && modalForm.$invalid) || modalForm.$pristine">Save</button> </div> </div> </div> </div>'
    );

    $templateCache.put(
        '/dialogs/tardinessHours.html',
        '<div class="modal"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <h4 class="modal-title"><span class="glyphicon glyphicon-pencil"></span> Override Tardiness (Hours)</h4> </div> <div class="modal-body"> <ng-form name="modalForm" novalidate role="form"> <div class="form-group input-group-lg"> <label class="control-label" for="tardiness">Tardiness (Hours)</label> <input type="text" class="form-control" name="tardiness" id="tardiness" ng-model="tardiness.tardiness" required> </div> </ng-form> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button> <button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(modalForm.$dirty && modalForm.$invalid) || modalForm.$pristine">Save</button> </div> </div> </div> </div>'
    );

    $templateCache.put(
        '/dialogs/noOfDays.html',
        '<div class="modal"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <h4 class="modal-title"><span class="glyphicon glyphicon-pencil"></span> Override # of Days</h4> </div> <div class="modal-body"> <ng-form name="modalForm" novalidate role="form"> <div class="form-group input-group-lg"> <label class="control-label" for="no_of_days"># of Days</label> <input type="text" class="form-control" name="no_of_days" id="no_of_days" ng-model="no_of_days.no_of_days" required> </div> </ng-form> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button> <button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(modalForm.$dirty && modalForm.$invalid) || modalForm.$pristine">Save</button> </div> </div> </div> </div>'
    );

    $templateCache.put(
        '/dialogs/noOfDays.html',
        '<div class="modal"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <h4 class="modal-title"><span class="glyphicon glyphicon-pencil"></span> Override # of Days</h4> </div> <div class="modal-body"> <ng-form name="modalForm" novalidate role="form"> <div class="form-group input-group-lg"> <label class="control-label" for="no_of_days"># of Days</label> <input type="text" class="form-control" name="no_of_days" id="no_of_days" ng-model="no_of_days.no_of_days" required> </div> </ng-form> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button> <button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(modalForm.$dirty && modalForm.$invalid) || modalForm.$pristine">Save</button> </div> </div> </div> </div>'
    );
    
    $templateCache.put(
        '/dialogs/overtime.html',
        '<div class="modal"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <h4 class="modal-title"><span class="glyphicon glyphicon-pencil"></span> Override Overtime</h4> </div> <div class="modal-body"> <ng-form name="modalForm" novalidate role="form"> <div class="form-group input-group-lg"> <label class="control-label" for="overtime">Overtime</label> <input type="text" class="form-control" name="overtime" id="overtime" ng-model="overtime.overtime" required> </div> </ng-form> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button> <button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(modalForm.$dirty && modalForm.$invalid) || modalForm.$pristine">Save</button> </div> </div> </div> </div>'
    );

    $templateCache.put(
        '/dialogs/restdayWork.html',
        '<div class="modal"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <h4 class="modal-title"><span class="glyphicon glyphicon-pencil"></span> Override Restday</h4> </div> <div class="modal-body"> <ng-form name="modalForm" novalidate role="form"> <div class="form-group input-group-lg"> <label class="control-label" for="restday">Restday</label> <input type="text" class="form-control" name="restday" id="restday" ng-model="restday.restday" required> </div> </ng-form> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button> <button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(modalForm.$dirty && modalForm.$invalid) || modalForm.$pristine">Save</button> </div> </div> </div> </div>'
    );

    $templateCache.put(
        '/dialogs/sickLeave.html',
        '<div class="modal"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <h4 class="modal-title"><span class="glyphicon glyphicon-pencil"></span> Override Sick Leave</h4> </div> <div class="modal-body"> <ng-form name="modalForm" novalidate role="form"> <div class="form-group input-group-lg"> <label class="control-label" for="sick_leave">Sick Leave</label> <input type="text" class="form-control" name="sick_leave" id="sick_leave" ng-model="sick_leave.sick_leave" required> </div> </ng-form> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button> <button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(modalForm.$dirty && modalForm.$invalid) || modalForm.$pristine">Save</button> </div> </div> </div> </div>'
    );


}]); // end run / module

