var logsUserApp = angular.module('logsUserApp', ['ngSanitize', 'ui.bootstrap', 'ui.select']);

logsUserApp.factory('logsUserFactory', function($http, $rootScope){

    return {
        getEmployees: function ($params)    {
            return $http ({
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: app_url + '/index.php/logs/getEmployeeSelf',
                method: "POST",
                data: $params
            }).then(function(response)  {
                return response.data
            });
        },
    };

});

/**
 * AngularJS default filter with the following expression:
 * "person in people | filter: {name: $select.search, age: $select.search}"
 * performs a AND between 'name: $select.search' and 'age: $select.search'.
 * We want to perform a OR.
 */
logsUserApp.filter('selectFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      items.forEach(function(item) {
        var itemMatches = false;

        var keys = Object.keys(props);
        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  }
});


logsUserApp.controller('logsUserController', function($scope, $http, logsUserFactory){

    /**
     * Initialize Variables
     *
     */
    var model = logsUserFactory;

    $scope.date_filter_type = 0; // Set to 0 by default
    $scope.is_filters = true; // Set to true by default

    $scope.employees;
    $scope.is_logs = false;

    /**
     * RootScope Broadcast Response
     *
     */
    $scope.$on("updateLogs", function(event,response){
        if(response.status == 200)  {
            $scope.setEmployees();
        } else  {
            console.log('Error: ' + response.statusText);
        }
    });

    /**
     * Controller Methods
     *
     */
    $scope.getCutoffList = function()  {
        $http.get(app_url + '/index.php/logs/getCutoffList').then(function(response) {
            $scope.cutoffList = _(response.data).toArray();
        });
    };

    $scope.getCurrentCutoff = function()  {
        $http.get(app_url + '/index.php/logs/getCurrentCutoff').then(function(response) {
            $scope.dateRange = response.data;
        });
    };

    $scope.generateLogs = function()  {
        $scope.is_logs = true;

        $scope.getDateRange();
    };

    $scope.getDateRange = function()    {
        $http.get(app_url + '/index.php/logs/getDateRange?date_range=' + $scope.dateRange.value).then(function(response) {
            $scope.dateInterval = response.data;

            // Set Employees
            $scope.setEmployees();
        });
    };

    $scope.setEmployees = function()    {
        $params = $.param({
            date_interval: $scope.dateInterval,
        });

        model.getEmployees($params).then(function(response){
            $scope.employees = _(response).toArray();
        });
    };

});