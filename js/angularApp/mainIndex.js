var mainIndexApp = angular.module('mainIndexApp', []);

mainIndexApp.controller('mainIndexController', function($scope, $location){

    $scope.isActive = function (viewLocation) { 
        return viewLocation === $location.path();
    };

});
