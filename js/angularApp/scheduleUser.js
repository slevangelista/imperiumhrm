var scheduleUserApp = angular.module('scheduleUserApp', ['ui.bootstrap','dialogs', 'ui.select']);

scheduleUserApp.factory('scheduleUserFactory', function($http, $rootScope){

    return {
        getEmployee: function ($params)    {
            return $http ({
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: app_url + '/index.php/schedule/getEmployeeSelf',
                method: "POST",
                data: $params
            }).then(function(response)  {
                return response.data
            });
        },
    }    
});

scheduleUserApp.controller('scheduleUserController', function($rootScope, $scope, $http, $timeout, $dialogs, scheduleUserFactory)    {

    /**
     * Initialize Scope Variables
     */

    var model = scheduleUserFactory;

    $scope.employees;

    $scope.currentDay;
    $scope.currentWeek = {};

    /**
     * Scope Methods
     */

    $scope.getEmployee = function() {
        $params = $.param({
            'currentWeek': $scope.currentWeek
        });
        model.getEmployee($params).then(function(response){
            $scope.employees = response;
        });
    };

    $scope.setWeekTitle = function()    {
        var firstDay = $scope.currentWeek[Object.keys($scope.currentWeek)[0]];
        var lastDay = $scope.currentWeek[Object.keys($scope.currentWeek)[6]]
        $scope.weekTitle = firstDay + ' to ' + lastDay;
    };

    $scope.setCurrentDay = function(day)    {
        $scope.currentDay = day;
    };

    $scope.setCurrentWeek = function () {
        $http.get(app_url + '/index.php/schedule/getWeek?date=' + $scope.currentDay).then(function(response) {
            $scope.currentWeek = response.data;

            // Update Title
            $scope.setWeekTitle();

            // Update schedule upon change of week
            $scope.getEmployee();
        });
    };

    $scope.setSchedule = function(day)  {
        $scope.setCurrentDay(day);
        $scope.setCurrentWeek();
    };

    $scope.getPreviousWeek = function() {
        // Get day last week, bind to currentDay scope
        $http.get(app_url + '/index.php/schedule/getDayLastWeek?day=' + $scope.currentDay).then(function(response)    {
            $scope.setCurrentDay(response.data);
            $scope.setCurrentWeek();
        });
    };

    $scope.getNextWeek = function() {
        // Get day next week, bind to currentDay scope
        $http.get(app_url + '/index.php/schedule/getDayNextWeek?day=' + $scope.currentDay).then(function(response)    {
            $scope.setCurrentDay(response.data);
            $scope.setCurrentWeek();
        });
    };

});