var leaveRequestApp = angular.module('leaveRequestApp', ['ui.bootstrap','dialogs']);

leaveRequestApp.factory('leaveRequestFactory', function ($http, $rootScope){

	return {
		updateDisapproval: function ($params)    {
            $http ({
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: app_url + '/index.php/leaveRequest/disapproveRequest',
                method: "POST",
                data: $params
            }).then(function(response)  {
                console.log("Factory method save() was successfully done.");
                $rootScope.$broadcast("updateRequest",response);
            });
        },
        updateApproval: function ($params)    {
            $http ({
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: app_url + '/index.php/leaveRequest/approveRequest',
                method: "POST",
                data: $params
            }).then(function(response)  {
                console.log("Factory method save() was successfully done.");
                $rootScope.$broadcast("updateRequest",response);
            });
        },
	};

});

leaveRequestApp.controller('leaveRequestController',function($scope, $http, $dialogs, leaveRequestFactory)	{

    /**
     * Initialize Variables
     */ 

    var model = leaveRequestFactory;

    $scope.test = {};
    $scope.leaveRequest;

    /**
     * RootScope Broadcast Response
     *
     */
    $scope.$on("updateRequest", function(event,response){
        if(response.status == 200)  {
            // Successful response callback
            $scope.getPendingLeaveRequest();
        } else  {
            console.log('Error: ' + response.statusText);
        }
    });

    /**
     * Scope Methods
     */

	$scope.check = function(test)	{
		console.log(test);
	};

    $scope.getPendingLeaveRequest = function()  {
        $http.get(app_url + '/index.php/leaveRequest/getPendingLeaveRequest').then(function(response) {
            $scope.leaveRequest = response.data;
        });
    };

	$scope.modalDisapprove = function(id){

        // console.log(emp_array); // Output JSON in console (For development purposes)
        var dlg = null;
    
        dlg = $dialogs.create('/dialogs/modalDisapprove.html','modalDisapprove', {id: id} ,{key: false,back: 'static'});
        dlg.result.then(function(disapproval){

        	$params = $.param({
                'reason': disapproval.reason,
                'id': disapproval.id,
            });
            model.updateDisapproval($params);

        },function(){
            
        });

    }; // end launch

    $scope.modalApprove = function(id, position_id, eid, start_date, end_date, leave_type_id){

        // console.log(emp_array); // Output JSON in console (For development purposes)
        var dlg = null;
    
        dlg = $dialogs.create('/dialogs/modalApprove.html','modalApprove', {id: id, position_id: position_id, eid: eid, start_date: start_date, end_date: end_date, leave_type_id: leave_type_id} ,{key: false,back: 'static'});
        dlg.result.then(function(approval){

           $params = $.param({
                // 'reason': disapproval.reason,
                'id': approval.id,
                'position_id': approval.position_id,
                'eid': approval.eid,
                'start_date': approval.start_date,
                'end_date': approval.end_date,
                'leave_type_id': approval.leave_type_id
            }); 

            model.updateApproval($params);

        },function(){
            
        });

    }; // end launch

});

leaveRequestApp.controller('modalApprove',function($scope, $modalInstance, data){

    $scope.approval = {
        id: data.id,
        position_id: data.position_id,
        eid: data.eid,
        start_date: data.start_date,
        end_date: data.end_date,
        leave_type_id: data.leave_type_id
    };

    $scope.cancel = function(){
        $modalInstance.dismiss('canceled');  
    }; // end cancel

    $scope.save = function(){
        $modalInstance.close($scope.approval);
    }; // end save

    $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13) && !(angular.equals($scope.name,null) || angular.equals($scope.name,'')))
            $scope.save();
    }; // end hitEnter

}); // end whatsYourNameCtrl

leaveRequestApp.controller('modalDisapprove',function($scope, $modalInstance, data){

    $scope.disapproval = {
        id: data.id
	};

    $scope.cancel = function(){
        $modalInstance.dismiss('canceled');  
    }; // end cancel

    $scope.save = function(){
        $modalInstance.close($scope.disapproval);
    }; // end save

    $scope.hitEnter = function(evt){
        if(angular.equals(evt.keyCode,13) && !(angular.equals($scope.name,null) || angular.equals($scope.name,'')))
            $scope.save();
    }; // end hitEnter

}); // end whatsYourNameCtrl

leaveRequestApp.run(['$templateCache',function($templateCache){
    $templateCache.put(
        '/dialogs/modalDisapprove.html',
        '<div class="modal"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <h4 class="modal-title"><span class="glyphicon glyphicon-warning-sign"></span> Dissaproval Form</h4> </div> <div class="modal-body"> <ng-form name="nameDialog" novalidate role="form"> <div class="form-group input-group-lg"> <label class="control-label" for="emp_name">Dissaproval Reason</label> <textarea class="form-control" ng-model="disapproval.reason"></textarea> <span class="help-block">Dissaproval Reason</span> </div> </ng-form> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button> <button type="button" class="btn btn-primary" ng-click="save()">Save</button> </div> </div> </div> </div>'
    );
    $templateCache.put(
        '/dialogs/modalApprove.html',
        '    <div class="modal"> <div class="modal-dialog"> <div class="modal-content"><br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-thumbs-up"></span> &nbsp;Approve Leave Request? <div class="modal-footer"> <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button> <button type="button" class="btn btn-primary" ng-click="save()">Save</button> </div> </div> </div> </div>'
    );

}]); // end run / module

