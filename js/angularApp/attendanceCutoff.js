var attendanceCutoffApp = angular.module('attendanceCutoffApp', ['ui.bootstrap','dialogs','ngSanitize','ui.select','mgcrea.ngStrap.datepicker']);

attendanceCutoffApp.factory('attendanceCutoffFactory', function($http, $rootScope){

    return  {
        editCutoff: function ($params)    {
            $http ({
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: app_url + '/index.php/cutoff/editCutoff',
                method: "POST",
                data: $params
            }).then(function(response)  {
                $rootScope.$broadcast("updateCutoff",response);
            });
        },
        addCutoff: function ($params)   {
            $http ({
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: app_url + '/index.php/cutoff/addCutoff',
                method: "POST",
                data: $params
            }).then(function(response)  {
                $rootScope.$broadcast("updateCutoff",response);
            });        
        },
        deleteCutoff: function ($params)    {
            $http ({
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: app_url + '/index.php/cutoff/deleteCutoff',
                method: "POST",
                data: $params
            }).then(function(response)  {
                $rootScope.$broadcast("updateCutoff",response);
            }); 
        },
    };

});

attendanceCutoffApp.controller('attendanceCutoffController', function($rootScope, $scope, $http, $filter, $timeout, $dialogs, attendanceCutoffFactory){

    /**
     * Initialize scope variables
     *
     */
    var model = attendanceCutoffFactory;

    $scope.cutoff;

    /**
     * RootScope Broadcast Response
     *
     */
    $scope.$on("updateCutoff", function(event,response){
        if(response.status == 200)  {
            $scope.getCutoffAll();
        } else  {
            console.log('Error: ' + response.statusText);
        }
    });

    $scope.formatDate = function(date)
    {
        return $filter('date')(date, 'yyyy-MM-dd');
    };

    $scope.getCutoffAll = function()    {
        $http.get(app_url + '/index.php/cutoff/getCutoffAll').then(function(response) {
            $scope.cutoff = response.data;
        });
    };

    $scope.check = function()   {
        console.log($scope.cutoff);
    }

    $scope.addCutoff = function()   {
        dlg = $dialogs.create('/dialogs/addCutoff.html','addCutoff', {},{key: false,back: 'static'});
        dlg.result.then(function(cutoff){

            $params = $.param({
                id: cutoff.id,
                date_start: $scope.formatDate(cutoff.date_start),
                date_end: $scope.formatDate(cutoff.date_end),
            });

            model.addCutoff($params);

        },function(){
            //console.log("Save not invoked");
        });
    };

    $scope.editCutoff = function(data)   {
        dlg = $dialogs.create('/dialogs/editCutoff.html','editCutoff', data,{key: false,back: 'static'});
        dlg.result.then(function(cutoff){

            $params = $.param({
                id: cutoff.id,
                date_start: $scope.formatDate(cutoff.date_start),
                date_end: $scope.formatDate(cutoff.date_end),
            });

            model.editCutoff($params);

        },function(){
            //console.log("Save not invoked");
        });
    };

    $scope.deleteCutoff = function(data) {
        dlg = $dialogs.create('/dialogs/deleteCutoff.html','deleteCutoff', data,{key: false,back: 'static'});
        dlg.result.then(function(cutoff){

            $params = $.param({
                id: cutoff.id,
                key_validate_delete_u179803: 'validate',
            });

            model.deleteCutoff($params);

        },function(){
            //console.log("Save not invoked");
        });
    };

});

attendanceCutoffApp.controller('addCutoff', function($scope, $modalInstance, data){

    $scope.cutoff = {};

    $scope.cancel = function(){
        $modalInstance.dismiss('canceled');
    }; // end done
    
    $scope.save = function(){
        $modalInstance.close($scope.cutoff);
    };

});

attendanceCutoffApp.controller('editCutoff', function($scope, $modalInstance, data){

    $scope.cutoff = {
        id: data.id,
        date_start: data.date_start,
        date_end: data.date_end,
    };

    $scope.cancel = function(){
        $modalInstance.dismiss('canceled');
    }; // end done
    
    $scope.save = function(){
        $modalInstance.close($scope.cutoff);
    };

});

attendanceCutoffApp.controller('deleteCutoff', function($scope, $modalInstance, data){

    $scope.cutoff = {
        id: data.id,
    };

    $scope.cancel = function(){
        $modalInstance.dismiss('canceled');
    }; // end done
    
    $scope.save = function(){
        $modalInstance.close($scope.cutoff);
    };

});

attendanceCutoffApp.run(['$templateCache',function($templateCache){
    
    $templateCache.put(
        '/dialogs/addCutoff.html',
        '<div class="modal"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <h4 class="modal-title"><span class="glyphicon glyphicon-plus"></span> Add Cutoff</h4>  <span class="help-block">Create new cutoff</span> </div> <div class="modal-body"> <ng-form name="addCutoff" novalidate role="form"> <div class="form-group input-group-lg"> <label class="control-label" for="date_start">Start Date</label> <input class="form-control" ng-model="cutoff.date_start" data-date-format="yyyy-MM-dd" data-autoclose="1" name="datepicker" bs-datepicker type="text" required> <br /> <label class="control-label" for="date_end">End Date</label> <input class="form-control" ng-model="cutoff.date_end" data-date-format="yyyy-MM-dd" data-autoclose="1" name="datepicker" bs-datepicker type="text" required> </div> </ng-form> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button> <button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(addCutoff.$dirty && addCutoff.$invalid) || addCutoff.$pristine">Save</button> </div> </div> </div> </div>'
    );

    $templateCache.put(
        '/dialogs/editCutoff.html',
        '<div class="modal"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <h4 class="modal-title"><span class="glyphicon glyphicon-pencil"></span> Edit Cutoff</h4>  <span class="help-block">Edit selected cutoff</span> </div> <div class="modal-body"> <ng-form name="editCutoff" novalidate role="form"> <div class="form-group input-group-lg"> <label class="control-label" for="date_start">Start Date</label> <input class="form-control" ng-model="cutoff.date_start" data-date-format="yyyy-MM-dd" data-autoclose="1" name="datepicker" bs-datepicker type="text" required> <br /> <label class="control-label" for="date_end">End Date</label> <input class="form-control" ng-model="cutoff.date_end" data-date-format="yyyy-MM-dd" data-autoclose="1" name="datepicker" bs-datepicker type="text" required> </div> </ng-form> </div> <div class="modal-footer"> <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button> <button type="button" class="btn btn-primary" ng-click="save()" ng-disabled="(editCutoff.$dirty && editCutoff.$invalid) || editCutoff.$pristine">Save</button> </div> </div> </div> </div>'
    );

    $templateCache.put(
        '/dialogs/deleteCutoff.html',
        '<div class="modal"> <div class="modal-dialog"> <div class="modal-content"> <div class="modal-header"> <h4 class="modal-title"><span class="glyphicon glyphicon-trash"></span> Delete Cutoff</h4>  <span class="help-block">Removes selected cutoff</span> </div> <div class="modal-body">Are you sure you want to delete?<span class="help-block">Note: This cannot be undone</span></div> <div class="modal-footer"> <button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button> <button type="button" class="btn btn-danger" ng-click="save()" ng-disabled="(editCutoff.$dirty && editCutoff.$invalid) || editCutoff.$pristine">Delete</button> </div> </div> </div> </div>'
    );

}]); // end run / module

