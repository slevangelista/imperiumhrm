var homeIndexApp = angular.module('homeIndexApp', []);

homeIndexApp.factory('homeIndexFactory', function($http, $rootScope){

    return {
        getEmployee: function ($params)    {
            return $http ({
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: app_url + '/index.php/home/getEmployeeSelf',
                method: "POST",
                data: $params
            }).then(function(response)  {
                return response.data
            });
        },
    }    
});

homeIndexApp.controller('homeIndexController', function($rootScope, $scope, $http, homeIndexFactory){

	/**
	 * Initialize Scope Variables
 	 */

 	var model = homeIndexFactory;

 	$scope.employees;

    $scope.currentDay;
    $scope.currentWeek = {};
    $scope.leaveCredits;
    $scope.leaveRequest;

 	/**
	 * Scope Methods
 	 */

 	$scope.getEmployee = function() {
        $params = $.param({
            'currentWeek': $scope.currentWeek
        });
        model.getEmployee($params).then(function(response){
            $scope.employees = response;
        });
    };

    $scope.setWeekTitle = function()    {
        var firstDay = $scope.currentWeek[Object.keys($scope.currentWeek)[0]];
        var lastDay = $scope.currentWeek[Object.keys($scope.currentWeek)[6]]
        $scope.weekTitle = firstDay + ' to ' + lastDay;
    };

    $scope.setCurrentDay = function(day)    {
        $scope.currentDay = day;
    };

    $scope.setCurrentWeek = function () {
        $http.get(app_url + '/index.php/home/getWeek?date=' + $scope.currentDay).then(function(response) {
            $scope.currentWeek = response.data;

            // Update Title
            $scope.setWeekTitle();

            // Update schedule upon change of week
            $scope.getEmployee();
        });
    };

    $scope.getTest = function()
    {
    	$http.get(app_url + '/index.php/home/getTest').then(function(response) {
    		$scope.sl_vl = response.data;
    		// console.log($scope.test);
        });
    }


    $scope.getLeaveCredits = function()  
     {
        $http.get(app_url + '/index.php/home/getLeaveCredits').then(function(response) {
            $scope.leaveCredits = response.data;
            // console.log($scope.leaveCredits);
        });
    };

    $scope.getLeaveRequest = function()  
     {
        $http.get(app_url + '/index.php/home/getLeaveRequest').then(function(response) {
            $scope.leaveRequest = response.data;
            // console.log($scope.leaveRequest);
        });
    };

    $scope.setHome = function(day)  {
    	// For Schedule
        $scope.setCurrentDay(day);
        $scope.setCurrentWeek();
        $scope.getLeaveCredits();
        $scope.getLeaveRequest();
    };

});
