var employeeJobApp = angular.module('employeeJobApp', []);

employeeJobApp.controller('employeeJobController', function($scope, $http, $rootScope, employeeJobFactory)
{

	/**
	 * Initialize Scope Variables
	 */
	 
	var model = employeeJobFactory;

	$scope.currentDepartment = 1;
	$scope.currentSubDepartment;
	$scope.currentPositions;

	$scope.department;
	$scope.subDepartment;
	$scope.positions;

	$scope.$on("updateEmployee", function(event,response, emp_id){
        if(response.status == 200)  {
            // Successful response callback
          
        } else  {
            console.log('Error: ' + response.statusText);
        }
    });

	/**
	 * Scope Methods
	 */

	$scope.getAllData = function()	{
		$scope.getAllDepartments();
		$scope.getSubdepartment();
		$scope.getAllPositions();
	};

	$scope.getAllDepartments = function()	{
		$http.get(app_url + '/index.php/employee/getAllDepartments').then(function(response) {
            $scope.department = response.data;
            console.log($scope.department);
        });
	};

	$scope.getSubdepartment = function()	{
		$http.get(app_url + '/index.php/employee/getSubDepartment').then(function(response) {
            $scope.subDepartment = response.data;
            console.log($scope.subDepartment);
        });
	};

	$scope.getAllPositions = function() {
		$http.get(app_url + '/index.php/employee/getAllPositions').then(function(response){
			$scope.positions = response.data;
			console.log($scope.positions);
		});
	}

	$scope.check = function()	{
		//console.log($scope.currentSubDepartment);
		console.log($scope.currentDepartment);
	}


	$scope.save = function(subdepartment_id,position_id,emp_id) {
		$params = $.param({
			'subdepartment_id' : subdepartment_id,
			'position_id' : position_id,
			'emp_id' : emp_id
		});
		model.save($params);
	};

});


employeeJobApp.factory('employeeJobFactory', function ($http, $rootScope){

	return{
		save: function ($params)    {
            $http ({
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                url: app_url + '/index.php/employee/updateEmployeeJobAjax/',
                method: "POST",
                data: $params
            }).then(function(response)  {
                $rootScope.$broadcast("updateEmployee",response);
            });
        },

        
	};

});
